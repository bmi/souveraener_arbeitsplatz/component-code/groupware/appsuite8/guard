/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link UserIdentityPasswordlessGuestParser} - parses a guest user identity just for a an email, without having a password set
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class UserIdentityPasswordlessGuestParser implements UserIdentityParser {

    private static final Logger logger = LoggerFactory.getLogger(UserIdentityPasswordlessGuestParser.class);
    private OXUserService       userService;
    private JSONObject          json;

    /**
     * Initializes a new {@link UserIdentityPasswordlessGuestParser}.
     *
     * @param userService The {@link OXUserService}
     * @param json The json object to parse the guest from
     */
    public UserIdentityPasswordlessGuestParser(OXUserService userService, JSONObject json) {
        this.userService = Objects.requireNonNull(userService, "userService must not be null");
        this.json = Objects.requireNonNull(json, "json must not be null");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.authentication.UserIdentityParser#parse()
     */
    @Override
    public UserIdentity parse() throws OXException {
        try {
            if (json.has("user")) {
                JSONObject user = json.getJSONObject("user");
                if(user.has("email")) {
                   String email = user.getString("email");
                   OXGuardUser guestUser = userService.getGuestUser(email);
                   if(guestUser != null) {
                       return new UserIdentity(email).setOXGuardUser(guestUser);
                   }
                   else {
                       logger.error("Could not find guest user for email {}", email);
                       return null;
                   }
                }
                else {
                    throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create("email");
                }
            }
            else {
                throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create("user");
            }
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public UserIdentity parse(int userId, int cid) throws OXException {
        return parse();
    }

}
