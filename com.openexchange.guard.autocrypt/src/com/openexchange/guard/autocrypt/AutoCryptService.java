/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt;

import java.util.ArrayList;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.impl.AutoCryptKeysResponse;
import com.openexchange.guard.autocrypt.impl.AutoCryptStartInfoResponse;
import com.openexchange.guard.autocrypt.impl.TransferResponse;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;

/**
 * {@link AutoCryptService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public interface AutoCryptService {

    /**
     * Get AutoCrypt string for outgoing email
     * 
     * @param userId
     * @param cid
     * @param oxUserId
     * @param oxCid
     * @param emailAddress, should be the from address
     * @return
     * @throws OXException
     */
    public String getOutgoingHeaderString(int userId, int cid, int oxUserId, int oxCid, String emailAddress) throws OXException;

    /**
     * Import a key from a header string
     * @param userId
     * @param cid
     * @param header
     * @param date  Sent time in ticks
     * @param add  If the key should be added automatically
     * @param confiremd If the key was confirmed by the user
     * @return
     * @throws OXException
     */
    public AutoCryptKeysResponse importKey(int userId, int cid, String header, long date, boolean add, boolean confirmed) throws OXException;

    /**
     * Check for recipient key
     * @param userId  userId of sender
     * @param cid  cid of sender
     * @param email  email address of recipient
     * @return
     * @throws OXException
     */
    public RecipKey getRecipKey(int userId, int cid, String email) throws OXException;

    /**
     * Adds gossip headers for encrypted outgoing emails
     * @param userId
     * @param cid
     * @param recips
     * @return
     */
    public MimeMessage addGossipHeaders(MimeMessage msg, int userId, int cid, ArrayList<RecipKey> recips) throws OXException;

    /**
     * Get password information from an autocrypt start email
     * @param startAttachment
     * @return Object containing information regarding password length, as well as start characters if any
     * @throws OXException
     */
    public AutoCryptStartInfoResponse getStartData(String startAttachment) throws OXException;

    /**
     * Add the pgp secret key from a autocrypt start email
     * @param attachment
     * @param key
     * @return
     * @throws OXException
     */
    public PGPSecretKeyRing getKeyFromAttachment(String attachment, String key) throws OXException;

    /**
     * Send a private key transfer email to self, returning response with passcode
     * @param key to be transferred
     * @param password
     * @param senderIp IP address of the user
     * @return
     * @throws OXException
     */
    public TransferResponse sendTransferEmail(GuardKeys key, String password, String host, String senderIp) throws OXException;

    /**
     * Gets Autocrypt header for a given Guard key
     *
     * @param key
     * @return
     * @throws OXException
     */
    void addOutgoingHeader(MimeMessage msg, GuardKeys key) throws OXException;


}
