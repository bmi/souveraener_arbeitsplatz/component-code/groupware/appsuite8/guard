/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

/**
 * {@link PGPKeys} represents a mapping from a userId and contextId to a PGP Key id
 */
public class PGPKeys {

    private final long id;
    private final String email;
    private final String hexId;
    private final int contextId;
    private final boolean local;
    private final long keyId;

    /**
     * Initializes a new {@link PGPKeys}.
     * 
     * @param id the public key's id
     * @param email the mail
     * @param hexId the public Key's id in hex
     * @param contextId the context id
     * @param local whether the key is marked as local or not
     * @param keyId the key id
     */
    public PGPKeys(long id, String email, String hexId, int contextId, boolean local, long keyId) {
        this.id = id;
        this.email = email;
        this.hexId = hexId;
        this.contextId = contextId;
        this.local = local;
        this.keyId = keyId;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getHexId() {
        return hexId;
    }

    public int getContextId() {
        return contextId;
    }

    public boolean isLocal() {
        return local;
    }

    public long getKeyId() {
        return keyId;
    }
}
