/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.guard.guestreset;

import com.openexchange.exception.OXException;

/**
 * {@link GuestResetService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v3.0.0
 */
public interface GuestResetService {

    /**
     * Sends an email to the guest user to create new keys
     *
     * @param email The email of the guest user
     * @param fromEmail optional email to send email from
     * @param fromId, id of the sender if fromEmail not specified
     * @param fromCid, cid of the sender if FromEmail not specified
     * @param force Force the reset even if a password recovery exists
     * @throws OXException
     */
    public void reset(String email, String fromEmail, int fromId, int fromCid) throws OXException;



}
