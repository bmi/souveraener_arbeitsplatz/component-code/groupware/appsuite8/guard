/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.impl;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RdbCaCertStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class RdbCaCertStorage implements CaCertStorage {

    private static final String INSERT_CA = "INSERT INTO trusted_cert_auths (certificate, hash, group_id) VALUES (?, ?, ?)";
    private static final String SELECT_CAS = "SELECT certificate FROM trusted_cert_auths WHERE group_id = ?";

    private static final Logger LOG = LoggerFactory.getLogger(RdbCaCertStorage.class);
    private ServiceLookup services;

    public RdbCaCertStorage(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Generate X509Certificate from the encoded data
     *
     *
     * @param data
     * @return
     */
    private X509Certificate getCertFromString(String data) {
        if (data == null) {
            return null;
        }
        PEMParser parser = new PEMParser(new StringReader(data));
        Object nextObj = null;
        try {
            nextObj = parser.readObject();
        } catch (IOException e1) {
            LOG.error("Error reading CA certificate", e1);
        }
        while (nextObj != null) {
            if (nextObj instanceof X509CertificateHolder) {
                try {
                    return new JcaX509CertificateConverter().setProvider("BC").getCertificate((X509CertificateHolder) nextObj);
                } catch (CertificateException e) {
                    LOG.error("Error reading CA certificate", e);
                }
            }
        }
        return null;

    }

    @SuppressWarnings("resource")
    @Override
    public List<X509Certificate> getCAs(int group) throws OXException {
        ArrayList<X509Certificate> list = new ArrayList<X509Certificate>();
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_CAS);
            int i = 1;
            stmt.setInt(i++, group);
            rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(getCertFromString(rs.getString("certificate")));
            }
            return list;
        } catch (SQLException e) {
            LOG.error("Error retrieving CA certificates from database", e);
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return null;
    }

    /**
     * Simple hash of string data
     *
     * @param toHash String to hash
     * @return String hash
     * @throws OXException
     */
    private String getHash(String toHash) throws OXException {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(toHash.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            throw OXException.general("Encoder not found.  Unexpected error", e);
        }

    }

    @SuppressWarnings("resource")
    @Override
    public boolean addCertificate(String certString, int group) throws OXException {
        try {
            X509Certificate cert = getCertFromString(certString);
            if (cert == null) {
                throw OXException.general("Unable to get certificate from data");
            }
        } catch (Exception e1) {
            throw OXException.general("Unable to get certificate from data", e1);

        }
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_CA);
            int i = 1;
            stmt.setString(i++, certString);
            stmt.setString(i++, getHash(certString));
            stmt.setInt(i++, group);
            return stmt.execute();
        } catch (SQLException e) {
            LOG.error("Error writing CA certificates to database", e);
            if (e.getMessage() != null && e.getMessage().contains("Duplicate")) {
                throw OXException.general("Certificate already exists for that group id");
            }
            throw OXException.general(e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            databaseService.backWritableForGuard(con);
        }
    }

}
