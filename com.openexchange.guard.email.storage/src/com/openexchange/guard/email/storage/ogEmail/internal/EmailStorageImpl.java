/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.email.storage.ogEmail.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.email.storage.ogEmail.EmailStorageListener;
import com.openexchange.guard.email.storage.osgi.Services;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

public class EmailStorageImpl implements EmailStorage {

    ArrayList<EmailStorageListener> listeners = new ArrayList<EmailStorageListener>();

    @Override
    public void insertOrUpdate(String email, int contextId, int userId, int dbShard) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();
        try {
            insertOrUpdate(connection, email, contextId, userId, dbShard);
        } finally {
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void insertOrUpdate(Connection connection, String email, int contextId, int userId, int dbShard) throws OXException {

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(EmailSql.INSERT_OR_UPDATE_STMT);
            stmt.setString(1, email);
            stmt.setInt(2, userId);
            stmt.setInt(3, contextId);
            stmt.setInt(4, dbShard);
            stmt.setString(5, CipherUtil.getEmailUserHash(email));
            stmt.setInt(6, userId);
            stmt.setInt(7, contextId);
            stmt.setInt(8, dbShard);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    @Override
    public void deleteAllForUser(int contextId, int userId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(EmailSql.DELETE_ALL_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            int deleted = stmt.executeUpdate();
            if(deleted > 0) {
                for(EmailStorageListener l : listeners) {
                   l.deleted(userId, contextId);
                }
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void deleteAllForContext(int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(EmailSql.DELETE_ALL_FOR_CONTEXT_STMT);
            stmt.setInt(1, contextId);
            int deleted = stmt.executeUpdate();
            if(deleted > 0) {
                for(EmailStorageListener l : listeners) {
                   l.deleted(contextId);
                }
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public Email getByEmail(String email) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.SELECT_BY_EMAIL_STMT);
            stmt.setString(1, email);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new Email(resultSet.getString("email"), resultSet.getInt("id"), resultSet.getInt("cid"), resultSet.getInt("db"));
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
        return null;
    }

    @Override
    public List<Email> getById(int contextId, int userId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Email> ret = new ArrayList<Email>();
        try {
            stmt = connection.prepareStatement(EmailSql.SELECT_BY_USERID_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                ret.add(new Email(resultSet.getString(3), resultSet.getInt(2), resultSet.getInt(1), resultSet.getInt(4)));
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public List<Email> getByHu (String hu) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Email> ret = new ArrayList<Email>();
        try {
            stmt = connection.prepareStatement(EmailSql.SELECT_BY_HU);
            stmt.setString(1, hu);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                ret.add(new Email(resultSet.getString(3), resultSet.getInt(2), resultSet.getInt(1), resultSet.getInt(4)));
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public int getHighestGuestId() throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.SELECT_HIGHEST_GUEST_USERID_STMT);
            resultSet = stmt.executeQuery();

            int max = 1;
            if (resultSet.next()) {
                max = resultSet.getInt(1);
            }
            return max;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#getEmail(int, int)
     */
    @Override
    public String getEmail(int userId, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, cid, 0);
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        String email = "";
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(EmailSql.SELECT_EMAIL);
            stmt.setInt(1, cid);
            stmt.setInt(2, userId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                email = resultSet.getString("mail");
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return email;
    }


    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#getEmails()
     */
    @Override
    public List<Email> getEmails() throws OXException {
        List<Email> ret = new ArrayList<Email>();
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.SELECT_ALL_STMT);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                ret.add(new Email(resultSet.getString(3), resultSet.getInt(2), resultSet.getInt(1), resultSet.getInt(4)));
            }
            return ret;

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public boolean hasHu () throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.CHECK_HU);
            resultSet = stmt.executeQuery();

            return resultSet.next();

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public int getCount () throws OXException {
        return count(EmailSql.GET_COUNT);
    }

    @Override
    public int getMissingHuCount () throws OXException {
        return count(EmailSql.GET_MISSING_HU_COUNT);
    }

    private int count (String sql) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(sql);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#updateHu(java.lang.String, java.lang.String)
     */
    @Override
    public boolean updateHu(String email, String hu) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.UPDATE_HU);
            stmt.setString(1, hu);
            stmt.setString(2, email);
            stmt.execute();
            return true;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#getMissingHu()
     */
    @Override
    public List<Email> getMissingHu(int limit) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        List<Email> ret = new ArrayList<Email>();
        int count = 0;
        try {
            stmt = connection.prepareStatement(EmailSql.FIND_MISSING_HU);
            resultSet = stmt.executeQuery();

            while (resultSet.next() && count < limit) {
                ret.add(new Email(resultSet.getString(3), resultSet.getInt(2), resultSet.getInt(1), resultSet.getInt(4)));
                count++;
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#updateEmail(java.lang.String, java.lang.String)
     */
    @Override
    public void updateEmail(String oldEmail, String newEmail) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(EmailSql.UPDATE_EMAIL_ADDRESS);
            stmt.setString(1, newEmail);
            stmt.setString(2, oldEmail);
            stmt.execute();
            return;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#addListener(com.openexchange.guard.email.storage.ogEmail.EmailStorageListener)
     */
    @Override
    public void addListener(EmailStorageListener listener) {
        this.listeners.add(listener);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorage#removeListener(com.openexchange.guard.email.storage.ogEmail.EmailStorageListener)
     */
    @Override
    public boolean removeListener(EmailStorageListener listener) {
        return this.listeners.remove(listener);
    }
}
