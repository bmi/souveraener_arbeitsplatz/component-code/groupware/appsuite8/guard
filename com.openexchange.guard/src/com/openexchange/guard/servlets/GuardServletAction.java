/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.event.Level;
import com.google.gson.JsonObject;
import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.auth.AuthUtils;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.keymanagement.commons.antiabuse.KeyAntiAbuseWrapper;
import com.openexchange.guard.keymanagement.commons.antiabuse.UserAntiAbuseWrapper;
import com.openexchange.guard.logging.GuardLogProperties;
import com.openexchange.guard.logging.LoggingHelper;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.servlets.authentication.AuthenticationDataExtractor;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.GuestSessionValidator;
import com.openexchange.guard.servlets.authentication.RestSessionValidator;
import com.openexchange.guard.servlets.authentication.UserIdentityJsonParser;
import com.openexchange.guard.servlets.authentication.UserIdentityParameterParser;
import com.openexchange.guard.servlets.fileupload.AbstractTrackingFileItemFactory;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.translation.GuardTranslator;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.i18n.Translator;
import com.openexchange.java.Strings;

/**
 * Represents an OX Guard HTTP action
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public abstract class GuardServletAction {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(GuardServletAction.class);
    private static final String WWW_AUTHENTICATE_HEADER = "WWW-Authenticate";

    protected static final String CRYPTO_TYPE_PARAMETER = "type";
    protected static final CryptoType.PROTOCOL DEFAULT_CRYPTO_TYPE = CryptoType.PROTOCOL.PGP;

    private List<GuardAuthenticationHandler> authenticationHandlers;
    private GuardErrorResponseRenderer errorResponseRenderer;

    /**
     * Initializes a new {@link GuardServletAction}.
     */
    public GuardServletAction() {}

    /**
     * Initializes a new {@link GuardServletAction}.
     *
     * @param authenticationHandlers a set of authentication handlers
     */
    public GuardServletAction(GuardAuthenticationHandler... authenticationHandlers) {
        this.authenticationHandlers = new ArrayList<GuardAuthenticationHandler>(Arrays.asList(authenticationHandlers));
    }

    /**
     * Internal method to null-check the given {@link UserIdentity}.
     *
     * @param userIdentity The {@link UserIdentity} to check.
     * @param require true, if the method should throw an exception if the given userIdentity is null, false to return null.
     * @return The given userIdentity
     * @throws OXException
     */
    protected UserIdentity requireOrReturn(UserIdentity userIdentity, boolean require) throws OXException {
        if (userIdentity == null && require == true) {
            throw GuardAuthExceptionCodes.AUTH_MISSING.create();
        }
        return userIdentity;
    }

    /**
     * Gets the {@link org.slf4j.event.Level} to use for logging the execution of the {@link GuardServletAction}
     *
     * @return The {@link org.slf4j.event.Level} to use
     */
    protected Level getLogLevel() {
        return Level.INFO;
    }

    /**
     * Fluent like method to set the authentication handlers
     *
     * @param authenticationHandlers Authentication handlers to set
     * @return this, with the new authentication handlers set
     */
    public GuardServletAction setAuthenticationHandler(GuardAuthenticationHandler... authenticationHandlers) {
        this.authenticationHandlers = new ArrayList<GuardAuthenticationHandler>(Arrays.asList(authenticationHandlers));
        return this;
    }

    /**
     * Fluent like method to set the {@link GuardErrorResponseRenderer} which will be used for handling exceptions
     * occurred during the execution of the action.
     *
     * @param errorResponseRenderer The {@link GuardErrorResponseRenderer} used to render errors for clients.
     * @return this, with the new response renderer set.
     */
    public GuardServletAction setErrorResponseRenderer(GuardErrorResponseRenderer errorResponseRenderer) {
        this.errorResponseRenderer = errorResponseRenderer;
        return this;
    }

    /**
     * Gets the {@link GuardErrorResponseRenderer} which should be used to render exceptions for clients.
     *
     * @return The {@link GuardErrorResponseRenderer} which should be used for exception handling.
     */
    public GuardErrorResponseRenderer getErrorResponseRenderer() {
        return this.errorResponseRenderer;
    }

    /**
     * Checks authentication using all known authentication handler
     *
     * @param request the request to check authentication for
     * @param oxUserSession The user session or null if the action is not performed in a user context
     * @param response the response, used to set WWW_AUTHENTICATE_HEADER in case the authentication failed
     * @return true if authentication has been granted by all known authentication handlers of the action, false otherwise
     * @throws Exception
     */
    private boolean doAuthentication(HttpServletRequest request, GuardUserSession oxUserSession, HttpServletResponse response) throws Exception {
        if (authenticationHandlers != null && authenticationHandlers.size() > 0) {
            for (GuardAuthenticationHandler handler : authenticationHandlers) {
                if (!handler.authenticate(this, oxUserSession, request)) {
                    //Authentication failed - Adding the www-authenticate-header telling the client how authentication should be done
                    response.addHeader(WWW_AUTHENTICATE_HEADER, handler.getWWWAuthenticateHeader());
                    return false;
                }
            }
        }
        return true;
    }

    private GuardUserSession getSession(HttpServletRequest request) throws OXException {
        GuardUserSession guardUserSession = new GuestSessionValidator().validateSession(request);
        if (guardUserSession == null) {
            guardUserSession = new RestSessionValidator().validateSession(request);
            logAction(request, guardUserSession);
        } else {
            logGuestAction(request, guardUserSession);
        }
        return guardUserSession;
    }

    /**
     * Cleanup any temporary files created handling the request
     * cleanup
     *
     * @param request
     */
    private void cleanup(HttpServletRequest request) {
        Object fileItemFactory = request.getServletContext().getAttribute(FileUploadHandler.REQUEST_TRACKER);
        if (fileItemFactory == null) {
            return;
        }
        if (fileItemFactory instanceof AbstractTrackingFileItemFactory) {
            ((AbstractTrackingFileItemFactory) fileItemFactory).cleanup();
        }
    }

    /**
     * Performs the action after checking the authentication
     *
     * @param request the request related to the action
     * @param response the response related to the action
     * @throws Exception
     */
    public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Validate session if provided by the client and get corresponding context-id and user-id
        GuardUserSession guardUserSession = getSession(request);
        //authenticate
        if (doAuthentication(request, guardUserSession, response)) {
            doActionInternal(request, response, guardUserSession);
        } else {
            if (guardUserSession == null) {  // If authentication failed, and no user session
                GuardConfigurationService guardConfig = Services.getService(GuardConfigurationService.class);
                Thread.sleep(guardConfig.getIntProperty(GuardProperty.sessionSyncDelay), 3000);
                guardUserSession = getSession(request);  // Try again.  Hazelcast delay or sync issues
                if (doAuthentication(request, guardUserSession, response)) {
                    LOG.warn("Validated session only after delay.  Verify routing from Guard to middleware");
                    doActionInternal(request, response, guardUserSession);
                    return;
                }
            }
            ServletUtils.sendResponse(response, HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }
    }

    /**
     * Sets and logs current user and delegates to desired action
     *
     * @param request the request related to the action
     * @param userSession a user session or null, if invoked in a non-user context
     * @throws Exception
     */
    private void logGuestAction(HttpServletRequest request, GuardUserSession userSession) {
        if (userSession == null) {
            return;
        }
        GuardLogProperties.put(GuardLogProperties.Name.GUEST_CONTEXT_ID, userSession.getContextId());
        GuardLogProperties.put(GuardLogProperties.Name.GUEST_USER_ID, userSession.getUserId());
        GuardLogProperties.put(GuardLogProperties.Name.GUEST_SESSION_ID, userSession.getSessionId());

        LoggingHelper.Log(LOG, getLogLevel(), String.format("Command sent \"%s\" from IP %s", ServletUtils.getStringParameter(request, "action"), ServletUtils.getClientIP(request)));
    }

    /**
     * Sets and logs current user and delegates to desired action
     *
     * @param request the request related to the action
     * @param userSession a user session or null, if invoked in a non-user context
     * @throws Exception
     */
    private void logAction(HttpServletRequest request, GuardUserSession userSession) {
        if (userSession != null) {
            GuardLogProperties.put(GuardLogProperties.Name.SESSION_CONTEXT_ID, userSession.getContextId());
            GuardLogProperties.put(GuardLogProperties.Name.SESSION_USER_ID, userSession.getUserId());
            GuardLogProperties.put(GuardLogProperties.Name.SESSION_SESSION_ID, userSession.getSessionId());
        }

        LoggingHelper.Log(LOG, getLogLevel(), String.format("Command sent \"%s\" from IP %s", ServletUtils.getStringParameter(request, "action"), ServletUtils.getClientIP(request)));
    }

    /**
     * Gets the password from a given request.
     *
     * This will try to get the password from parameter, from authentication-token, or from epassword parameter
     *
     * @param request The request to get the password from
     * @return The password from the given request, or null if no password was found
     * @throws OXException
     */
    protected String getPassword(HttpServletRequest request, GuardUserSession userSession) throws OXException {
        return new AuthenticationDataExtractor().getPassword(request, userSession);
    }

    /**
     * Gets the password from a given request.
     *
     * This will try to get the password from parameter, from authentication-token, or from epassword parameter
     *
     * @param request The request to get the password from
     * @return The password from the given request, or null if no password was found
     * @throws OXException
     */
    protected String requirePassword(HttpServletRequest request, GuardUserSession userSession) throws OXException {
        String password = getPassword(request, userSession);
        if (password == null) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("password or auth");
        }
        return password;
    }

    /**
     * Gets a helper/simplified wrapper around {@link GuardAntiAbuseService} for handling failed login attempts and lockouts.
     *
     * @param keys The keys
     * @param password The password of the keys
     * @param userSession The user session
     * @return The helper for handling failed login attempts and lockouts.
     * @throws OXException
     */
    protected AntiAbuseWrapper getAntiAbuseWrapper(GuardKeys keys, AllowParameters allowParams) throws OXException {
        GuardAntiAbuseService antiAbuse = Services.getService(GuardAntiAbuseService.class);
        return new KeyAntiAbuseWrapper(antiAbuse, keys, allowParams);
    }

    /**
     * Gets a helper/simplified wrapper around {@link GuardAntiAbuseService} for handling failed login attempts and lockouts.
     *
     * @param userId The user's id
     * @param contextId The context id
     * @param Password the password
     * @param remoteIP the user's IP
     * @return The helper for handling failed login attempts and lockouts.
     * @throws OXException
     */
    protected AntiAbuseWrapper getAntiAbuseWrapper(AllowParameters allowParams) throws OXException {
        GuardAntiAbuseService antiAbuse = Services.getService(GuardAntiAbuseService.class);
        return new UserAntiAbuseWrapper(antiAbuse, allowParams);
    }

    protected AntiAbuseWrapper getAntiAbuseWrapper(HttpServletRequest request, GuardUserSession userSession, String password) throws OXException {
        return getAntiAbuseWrapper(AntiAbuseUtils.getAllowParameters(request, userSession.getUserId(), userSession.getContextId(), password));
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link HttpServletRequest}
     *
     * @param request The request to parse the {@link UserIdentity} from.
     * @return The parsed {@link UserIdentity}, or null if the request did not contain enough information for creating a {@link UserIdentity} object.
     * @throws OXException
     */
    protected UserIdentity getUserIdentityFrom(HttpServletRequest request, int userId, int cid) throws OXException {
        return new UserIdentityParameterParser(request).parse(userId, cid);
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link HttpServletRequest}, or throws an Exception if the request does not contain enough information for creating a {@link UserIdentity} object.
     *
     * @param request The request to parse the {@link UserIdentity} from.
     * @return The parsed {@link UserIdentity}
     * @throws OXException
     */
    protected UserIdentity requireUserIdentityFrom(HttpServletRequest request, int userId, int cid) throws OXException {
        final UserIdentity userIdentity = getUserIdentityFrom(request, userId, cid);
        final boolean require = true;
        return requireOrReturn(userIdentity, require);
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link HttpServletRequest}, or throws an Exception if the request does not contain enough information for creating a {@link UserIdentity} object.
     * 
     * @param request The request to parse the {@link UserIdentity} from.
     * @param guardUserSession The {@link GuardUserSession} to obtain user- and context-id from
     * @return The parsed {@link UserIdentity}
     * @throws OXException
     */
    protected UserIdentity requireUserIdentityFrom(HttpServletRequest request, GuardUserSession guardUserSession) throws OXException {
        return requireUserIdentityFrom(request, guardUserSession.getUserId(), guardUserSession.getContextId());
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link JSONObject}
     *
     * @param json The JSON object to parse the {@link UserIdentity} from.
     * @return The parsed {@link UserIdentity}, or null if the JSON object did not contain enough information for creating a {@link UserIdentity} object.
     * @throws OXException
     */
    protected UserIdentity getUserIdentityFrom(JSONObject json) throws OXException {
        return new UserIdentityJsonParser(Services.getService(TokenAuthenticationService.class), json).parse();
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link JSONObject}, or throws an Exception if the request does not contain enough information for creating a {@link UserIdentity} object.
     *
     * @param json The JSON object to parse the {@link UserIdentity} from.
     * @return The parsed {@link UserIdentity}.
     * @throws OXException
     */
    protected UserIdentity requireUserIdentityFrom(JSONObject json) throws OXException {
        final UserIdentity userIdentity = new UserIdentityJsonParser(Services.getService(TokenAuthenticationService.class), json).parse();
        final boolean require = true;
        return requireOrReturn(userIdentity, require);
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link GuardUserSession} and the provided authentication token
     *
     * @param guardSession The user session to use
     * @param authToken The auth token to use
     * @return The parsed {@link UserIdentity}, or null if the request did not contain enough information for creating a {@link UserIdentity} object.
     * @throws OXException
     */
    protected UserIdentity getUserIdentityFrom(GuardUserSession guardSession, String authToken) throws OXException {
        TokenAuthenticationService service = Services.getService(TokenAuthenticationService.class);
        return service.decryptUserIdentity(guardSession.getGuardSession(), authToken);
    }

    /**
     * Parses a {@link UserIdentity} from the given {@link GuardUserSession} and the provided authentication token, or throws an Exception if the request does not contain enough information for creating a {@link UserIdentity} object.
     *
     * @param guardSession The user session to use
     * @param authToken The auth token to use
     * @return The parsed {@link UserIdentity}, or null if the request did not contain enough information for creating a {@link UserIdentity} object.
     * @throws OXException
     */
    protected UserIdentity requireUserIdentityFrom(GuardUserSession guardSession, String authToken) throws OXException {
        final UserIdentity userIdentity = getUserIdentityFrom(guardSession, authToken);
        final boolean require = true;
        return requireOrReturn(userIdentity, require);
    }

    /**
     * Gets a {@link UserIdentity} from the given {@link GuardUserSession}
     *
     * @param guardUserSession The user session
     * @return The {@link UserIdentity} from the given {@link GuardUserSession}
     * @throws OXException
     */
    protected UserIdentity getUserIdentityFrom(GuardUserSession guardUserSession) throws OXException {
        return AuthUtils.getUserIdentityFrom(guardUserSession);
    }

    /**
     * Gets a {@link Translator} for the given session's locale
     *
     * @param guardUserSession The session containing the locale
     * @return The translator for the given sesison's locale, or the default translator
     * @throws OXException
     */
    protected GuardTranslator getTranslatorFrom(GuardUserSession guardUserSession) throws OXException {
        return new GuardTranslator(Services.getService(GuardTranslationService.class), guardUserSession.getLocale());
    }

    /**
     * Return parameters that were sent with the HTTPServletReqeuest
     *
     * @param request
     * @return Map<String, String[]> of parameters
     */
    protected static Map<String, String[]> getHttpParameters(HttpServletRequest request) {
        return request.getParameterMap();
    }

    /**
     * Get the first value from parameter map for parameter name
     *
     * @param map Map of parameters
     * @param parameter Parameter name to find
     * @return First value, or null if empty
     */
    protected static String getParameter(Map<String, String[]> map, String parameter) {
        if (map.containsKey(parameter)) {
            String[] vals = map.get(parameter);
            if (vals.length > 0) {
                return vals[0];
            }
        }
        return null;
    }

    /**
     * Pulls the json Object from request data
     * Verifies the user is logged into the UI if doVerification true
     *
     * @param request The HTTPRequest
     * @param userSession Guard user session
     * @param doVerification If true, MW will be queried to verify user logged in
     * @return JsonObject (may be null). Error if user not logged in.
     * @throws OXException
     */
    protected static JsonObject getJsonVerifyLogin(HttpServletRequest request, GuardUserSession userSession, boolean doVerification) throws OXException {
        JsonObject json = Services.getService(ResponseHandler.class).getJsonAndDecodeEncryptedPasswords(request, userSession.getUserId(), userSession.getContextId());
        if (doVerification) {
            // if ox member, verify actually logged in to ui
            if (userSession.getGuardContextId() > 0) {
                Api ap = new Api(new OxCookie(request.getCookies()), request);
                if (ap.verifyLogin() == false) {
                    throw GuardAuthExceptionCodes.NOT_LOGGED_IN.create();
                }
            }
        }
        return json;
    }

    protected static boolean hasCryptoType(HttpServletRequest request) {
        return ServletUtils.hasParameter(request, CRYPTO_TYPE_PARAMETER);
    }

    /**
     * Convenience method to obtain the {@link CryptoType.PROTOCOL} from the given request.
     * 
     * <p>
     * The "crypto type" is used in many situations to differentiate between cryptographic methods supported in OX Guard.
     * (for example PGP vs S/MIME)
     * </p>
     * 
     * @param request The {@link HttpServletRequest} to pull the crypto type from
     * @return The {@link CryptoType.PROTOCOL} type extracted from the given request, or, if missing, the default "crypto type".
     */
    protected static CryptoType.PROTOCOL getCryptoType(HttpServletRequest request) {
        String type = ServletUtils.getStringParameter(request, CRYPTO_TYPE_PARAMETER);
        return Strings.isEmpty(type) ? DEFAULT_CRYPTO_TYPE /* GUARD specific default */ : CryptoType.getTypeFromString(type);
    }

    /**
     * Convenience method to obtain the {@link CryptoType.PROTOCOL} from the given {@link GuardParsedMimeMessage}
     *
     * @param parsedMimeMessage The message to obtain the type from
     * @return The type parsed from the given {@link GuardParsedMimeMessage}, or the default type if no such type was found within the parsed message
     */
    protected static CryptoType.PROTOCOL getCryptoType(GuardParsedMimeMessage parsedMimeMessage) {
        return parsedMimeMessage.getType() != null ? parsedMimeMessage.getType() : DEFAULT_CRYPTO_TYPE;
    }

    /**
     * Convenience method to obtain the {@link CryptoType.PROTOCOL} either from the given {@link HttpServletRequest} or the given {@link GuardParsedMimeMessage}
     *
     * @param request The request
     * @param parsedMimeMessage The message
     * @return The crypto type from the given request, if present, or, otherwise, the crypto type from the given message. Returns the default crypto type
     *         if neither the request nor the message has a crypto type defined.
     */
    protected static CryptoType.PROTOCOL getCryptoType(HttpServletRequest request, GuardParsedMimeMessage parsedMimeMessage){
        if(hasCryptoType(request)) {
            return getCryptoType(request);
        }
        return parsedMimeMessage.getType() != null ? parsedMimeMessage.getType() : DEFAULT_CRYPTO_TYPE;
    }

    /**
     * Template method which allows implementation of the action execution by subclasses
     *
     * @param request the request related to the action
     * @param response the response related to the action
     * @param userSession a user session or null, if invoked in a non-user context
     * @throws Exception
     */
    protected abstract void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception;
}
