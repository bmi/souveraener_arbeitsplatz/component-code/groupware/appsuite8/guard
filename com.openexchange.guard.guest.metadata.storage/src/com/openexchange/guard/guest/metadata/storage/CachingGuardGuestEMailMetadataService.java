/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;

/**
 * {@link CachingGuardGuestEMailMetadataService} wraps an instance of {@link GuardGuestEMailMetadataService} in order to provide caching.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class CachingGuardGuestEMailMetadataService implements GuardGuestEMailMetadataService {

    private final GuardGuestEMailMetadataService        delegate;
    private final GenericCache<GuardGuestEmailMetadata> cache;

    /**
     * Initializes a new {@link CachingGuardGuestEMailMetadataService}.
     *
     * @param cacheFactory The factory for creating the cache to use
     * @param delegate The {@link GuardGuestEMailMetadataService} to enhance with caching.
     *
     **/
    public CachingGuardGuestEMailMetadataService(GenericCacheFactory cacheFactory, GuardGuestEMailMetadataService delegate) {
        this.cache = cacheFactory.createCache("GuestMetaData");
        this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#get(int, int, java.lang.String)
     */
    @Override
    public GuardGuestEmailMetadata get(int contextId, int userId, String id) throws OXException {
        final Serializable cacheKey = cache.createKey(userId, id);
        GuardGuestEmailMetadata item = cache.get(cacheKey);
        if (item == null) {
            item = delegate.get(contextId, userId, id);
            if (item != null) {
                cache.put(cacheKey, item);
            }
        }
        return item;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#getForFolder(int, int, java.lang.String)
     */
    @Override
    public Collection<GuardGuestEmailMetadata> getForFolder(int contextId, int userId, String folderId) throws OXException {
        Serializable cacheKey = cache.createKey(userId, folderId);
        GuardGuestEmailMetadata[] items = cache.getArray(cacheKey);
        if (items == null) {
            Collection<GuardGuestEmailMetadata> itemsCollection = delegate.getForFolder(contextId, userId, folderId);
            if (itemsCollection.size() > 0) {
                items = itemsCollection.toArray(new GuardGuestEmailMetadata[itemsCollection.size()]);
                cache.putArray(cacheKey, items);
            } else {
                items = new GuardGuestEmailMetadata[] {};
            }
        }
        return Arrays.asList(items);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#insert(int, int, com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata)
     */
    @Override
    public void insert(int contextId, int userId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException {
        delegate.insert(contextId, userId, guestEmailMetaData);
        cache.put(cache.createKey(userId, guestEmailMetaData.getId()), guestEmailMetaData);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateMessageFlags(int, int, java.lang.String, int)
     */
    @Override
    public void updateMessageFlags(int contextId, int userId, String itemId, int messageFlags) throws OXException {
        delegate.updateMessageFlags(contextId, userId, itemId, messageFlags);
        cache.remove(cache.createKey(userId, itemId));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateColorLabel(int, int, java.lang.String, int)
     */
    @Override
    public void updateColorLabel(int contextId, int userId, String itemId, int colorLabel) throws OXException {
        delegate.updateColorLabel(contextId, userId, itemId, colorLabel);
        cache.remove(cache.createKey(userId, itemId));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateFolderId(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public void updateFolderId(int contextId, int userId, String itemId, String folderId) throws OXException {
        delegate.updateFolderId(contextId, userId, itemId, folderId);
        cache.remove(cache.createKey(userId, itemId));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateAllFolderIdsInFolder(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public void updateAllFolderIdsInFolder(int contextId, int userId, String folderId, String newFolderId) throws OXException {
        delegate.updateAllFolderIdsInFolder(contextId, userId, folderId, newFolderId);
        cache.clear();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#delete(int, com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata)
     */
    @Override
    public int delete(int shardId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException {
        final int count = delegate.delete(shardId, guestEmailMetaData);
        cache.clear();
        return count;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#deleteUserMetaData(int, int)
     */
    @Override
    public int deleteUserMetaData(int userId, int contextId) throws OXException {
        final int count = delegate.deleteUserMetaData(userId, contextId);
        cache.clear();
        return count;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#createMetaDataFrom(java.lang.String, java.lang.String, javax.mail.internet.MimeMessage, int)
     */
    @Override
    public GuardGuestEmailMetadata createMetaDataFrom(String itemId, String folderId, MimeMessage message, int defaultMessageFlags) throws MessagingException {
        return delegate.createMetaDataFrom(itemId, folderId, message, defaultMessageFlags);
    }
}
