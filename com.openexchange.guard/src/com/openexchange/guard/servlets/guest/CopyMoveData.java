/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

public class CopyMoveData {

    private static final String ALL_IDS_STR = "ALL_MAIL_IDS";

    private String[] mailIds = ALL_IDS;
    private String   folderId;
    private String   newFolderId;

    /**
     * Constant for moving all known E-Mails in a folder
     */
    public static final String[] ALL_IDS = new String[] { ALL_IDS_STR };

    public String[] getMailIds() {
        return this.mailIds;
    }

    public void setMailIds(String[] mailIds) {
        if (mailIds == null || mailIds.length == 1 && mailIds[0].equals(ALL_IDS_STR)) {
            this.mailIds = ALL_IDS;
        } else {
            this.mailIds = mailIds;
        }
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String newFolderId) {
        this.folderId = newFolderId;
    }

    public String getNewFolderId() {
        return newFolderId;
    }

    public void setNewFolderId(String newFolderId) {
        this.newFolderId = newFolderId;
    }
}