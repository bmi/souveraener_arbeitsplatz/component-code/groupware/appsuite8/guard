/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;

/**
 * {@link AbstractGuardAccess} - Access to OX Guard API.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public abstract class AbstractGuardAccess {

    public static final String GUARDADMIN_ENDPOINT = "guardadmin";
    public static final String PGPCORE_ENDPOINT = "oxguard/pgpcore";
    public static final String LOGIN_ENDPOINT = "oxguard/login";
    public static final String GUEST_ENDPOINT = "oxguard/guest";
    public static final String CRYPTO_ENDPOINT = "oxguard/crypto";

    private static final AtomicReference<Map<String, GuardApiImpl>> API_REF = new AtomicReference<Map<String, GuardApiImpl>>();

    static {
        API_REF.set(new HashMap<String, GuardApiImpl>());
    }

    /**
     * Adds a {@link GuardApiImpl} reference with a given name
     *
     * @param endpointName The name
     * @param guardApi The instance to add
     */
    public static void addGuardApi(String endpointName, GuardApiImpl guardApi) {
        API_REF.get().put(endpointName, guardApi);
    }

    /**
     * Unsets the {@link GuardApiImpl} reference
     *
     * @param guardApi The instance to all guardApis
     */
    public static Map<String, GuardApiImpl> unsetGuardApi() {
        Map<String, GuardApiImpl> guardApis;
        do {
            guardApis = API_REF.get();
            if (null == guardApis) {
                return null;
            }
        } while (!API_REF.compareAndSet(guardApis, null));
        return guardApis;
    }

    /**
     * Initializes a new {@link AbstractGuardAccess}.
     */
    protected AbstractGuardAccess() {
        super();
    }

    /**
     * Gets the Guard API access for the given endpoint name.
     *
     * @param endpointName The name of the end point to get.
     * @return The Guard API for the given end point name, or null, if no such end point was found.
     */
    protected static GuardApiImpl getGuardApi(String endpointName) {
        return API_REF.get().get(endpointName);
    }

    /**
     * Requires the Guard API access for the given endpoint name.
     * @param endpointName The name of the end point to get.
     * @return The Guard API for the given end point name
     * @throws OXException If no such end point was found.
     */
    protected static GuardApiImpl requireGuardApi(String endpointName) throws OXException {
        GuardApiImpl guardApi = API_REF.get() != null ? getGuardApi(endpointName) : null;
        if(guardApi != null) {
            return guardApi;
        }

        throw GuardExceptionCodes.MISSING_ENDPOINT.create(endpointName);
    }
}
