/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.internal;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.osgi.Services;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link GuardAESCipherServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardAESCipherServiceImpl extends AbstractGuardCipherService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardAESCipherServiceImpl.class);

    /**
     * Initializes a new {@link GuardAESCipherServiceImpl}.
     *
     * @param cipherAlgorithm The cipher algorithm
     * @throws OXException If the specified cipher algorithm does not exist, or if the key length of the specified algorithm exceeds the maximum defined
     */
    public GuardAESCipherServiceImpl(GuardCipherAlgorithm cipherAlgorithm) throws OXException {
        super(cipherAlgorithm);
    }

    /**
     * Create string for AES data that includes cipher type, IV, and data
     * createEncrString
     *
     * @param ivString The init vector string
     * @param encrypted Encrypted data
     * @return Sring containing cipher type, IV, and data
     */
    private String createEncrString(String ivString, String encrypted) {
        StringBuilder sb = new StringBuilder();
        sb.append("#");
        sb.append(cipherAlgorithm.getValue());
        sb.append("#");
        sb.append(ivString);
        sb.append("!");
        sb.append(encrypted);
        return sb.toString();
    }

    @Override
    public String encrypt(String data, Key secretKey) {
        try {
            Cipher cipher = getCipher();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            AlgorithmParameters parameters = cipher.getParameters();

            byte[] iv = parameters.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] ciphertext = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));

            String encrypted = Base64.encodeBase64String(ciphertext);
            String iVString = Base64.encodeBase64String(iv);
            return createEncrString(iVString, encrypted);
            /*
             * String combo = iVString + encrypted;
             *
             * return combo;
             */
        } catch (Exception e) {
            //TOD throw exception?
            LOG.error("Error encrypting: " + e.getMessage(), e);
            return "";
        }
    }

    @Override
    public String encrypt(String data, String base64Key) {
        SecretKey secretKey = new SecretKeySpec(Base64.decodeBase64(base64Key), cipherAlgorithm.getAlgorithmName());
        return encrypt(data, secretKey);
    }

    @Override
    public String encrypt(String data, String password, String salt) throws OXException {
        SecretKey secretKey = getKey(password, salt);
        return encrypt(data, secretKey);
    }

    @Override
    public String decrypt(String data, String password, String salt, int keyLength) {
        SecretKey secretKey = getKey(password, salt, keyLength);
        return decrypt(data, secretKey);
    }

    @Override
    public String decrypt(String data, String password, String salt) {
        if (data == null) {
            return null;
        }
        try {
            SecretKey secretKey = getKey(password, salt);
            return decrypt(data, secretKey);
        } catch (Exception e) {
            //TOD throw exception?
            LOG.error("Bad Pass: " + e.getMessage(), e);
            return "";
        }
    }

    @Override
    public String decrypt(String data, String base64Key) {
        if (base64Key == null || data == null) {
            return (null);
        }
        SecretKey secretkey = new SecretKeySpec(Base64.decodeBase64(base64Key), cipherAlgorithm.getAlgorithmName());
        return decrypt(data, secretkey);
    }

    @Override
    public String decrypt(String data, Key secretKey) {
        try {
            if (data == null) {
                return "";
            }
            Cipher ciper = getDecrCipher(data);

            byte[] iv64 = getIVBytes(data);
            byte[] iv = Base64.decodeBase64(iv64);

            data = getEncrDataBytes(data);

            byte[] databytes = Base64.decodeBase64(data);
            ciper.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            return new String(ciper.doFinal(databytes), StandardCharsets.UTF_8);

        } catch (Exception e) {
            //TOD throw exception?
            LOG.error("Problem decoding AES: " + e.getMessage(), e);
            return "";
        }
    }

    /**
     * Get the IV bytes from the Base64 data
     * getIVBytes
     *
     * @param data String expected #int#IV!data where IV specifies Init Vector
     * @return byte array containing the Init vector
     * @throws UnsupportedEncodingException
     */
    private byte[] getIVBytes(String data) {
        if (data == null) {
            return null;
        }
        if (data.contains("#") && data.contains("!")) {
            data = data.substring(data.lastIndexOf("#") + 1, data.indexOf("!"));
            return data.getBytes(StandardCharsets.UTF_8);
        }
        // Old format.  Assume first 24 characters
        return data.substring(0, 24).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Return the data bytes from the Base64 data
     * getEncrDataBytes
     *
     * @param data String expected #int#IV!data where data is encrypted data
     * @return String containing the encrypted data
     */
    private String getEncrDataBytes(String data) {
        if (data == null) {
            return null;
        }
        if (data.contains("!")) {
            return data.substring(data.indexOf("!") + 1);
        }
        return data.substring(24);
    }

    @Override
    public String getRandomKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(cipherAlgorithm.getAlgorithmName(), cipherAlgorithm.getProvider());
            keyGen.init(getAESKeyLength());
            SecretKey generatedKey = keyGen.generateKey();

            return Base64.encodeBase64String(generatedKey.getEncoded());
        } catch (Exception e) {
            //TOD throw exception?
            LOG.error("Failure while getting random key: " + e.getMessage(), e);
            return "";
        }
    }

    ///////////////////////////////////// HELPERS ////////////////////////////////////////////

    /**
     * Returns the AES key length
     *
     * @return the AES key length
     * @throws OXException
     */
    private int getAESKeyLength() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        return configService.getIntProperty(GuardProperty.aesKeyLength);
    }

    /**
     * Generate key from password and salt
     *
     * @param password The password to generate the key from
     * @param salt The salt
     * @return secretKey The generated SecretKey
     * @throws OXException
     */
    private SecretKey getKey(String password, String salt) throws OXException {
        return getKey(password, salt, getAESKeyLength());
    }

    /**
     * Generate key from password and salt
     *
     * @param password The password to generate the key from
     * @param salt The salt
     * @param keyLength The key length
     * @return secretKey The generated SecretKey
     */
    private SecretKey getKey(String password, String salt, int keyLength) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC");
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(StandardCharsets.UTF_8), 5000, keyLength);
            SecretKey tmpKey = factory.generateSecret(spec);
            SecretKey secretKey = new SecretKeySpec(tmpKey.getEncoded(), "AES");
            return secretKey;
        } catch (Exception e) {
            //TOD throw exception?
            LOG.error("Bad Pass Keygen: " + e.getMessage(), e);
            return null;
        }
    }
}
