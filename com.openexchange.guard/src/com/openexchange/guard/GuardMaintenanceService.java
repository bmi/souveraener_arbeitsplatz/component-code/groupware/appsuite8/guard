/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard;

import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;

/**
 * {@link GuardMaintenanceService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardMaintenanceService {

    /**
     * Tests the specified email against the MailResolver
     *
     * @param mail The e-mail address to test with
     * @return The response from the server
     * @throws OXException
     */
    JsonObject test(String mail) throws OXException;

    /**
     * Resets the specified e-mail and sends a new password to the user
     *
     * @param mail The e-mail address of the user for whom to reset the password
     * @param type The type of crypto
     * @throws OXException
     */
    void reset(String mail, CryptoType.PROTOCOL type) throws OXException;

    /**
     * Removes the PIN for the specified guest user
     *
     * @param mail The e-mail address of the guest user for whom the PIN shall be removed
     * @throws OXException
     */
    void removePIN(String mail) throws OXException;

    /**
     * Upgrades a guest account to an OX account
     *
     * @param mail The e-mail address of the guest user that will get upgraded
     * @throws OXException
     */
    void upgrade(String mail, String userid, String cid) throws OXException;

    /**
     * Delete a user by email address
     * @param email
     * @throws OXException
     */
    void delete(String email) throws OXException;

    /**
     * Sends guest link to reset email
     *
     * @param email
     * @param fromEmail - email of user to use to generate share link
     * @param fromId - id of user to generate share link. Required if fromEmail not specified
     * @param fromCid - cid of user to generate share link. Required if fromEmail not specified
     * @throws OXException
     */
    void resetGuest(String email, String fromEmail, int fromId, int fromCid) throws OXException;

    /**
     * Imports Public Key to system
     * @param file
     * @throws OXException
     */
    void importPublic(String file) throws OXException;

    /**
     * Imports a user private key
     *
     * @param type smime vs pgp
     * @param file byte array containing key
     * @param userId
     * @param cid
     * @param oldPass password for the file containing the key
     * @param newPass new password for the user (if missing, uses oldpass)
     */
    void importKey(String type, byte[] file, int userId, int contextId, String oldPass, String newPass) throws OXException;

    /**
     * Repairs Guard database and checks integrity
     */
    StringBuilder repair (boolean dryRun) throws OXException;

    /**
     * Changes a user's email, context or id, indentified by email address
     * @param email
     * @param newEmail
     * @param newContext
     * @param newUserId
     */
    void changeUser (String email, String newEmail) throws OXException;

    /**
     * Creates new master keys and returns index
     * createMasterKeys
     *
     * @return The index of the created master key
     * @throws OXException
     */
    int createMasterKeys() throws OXException;

    /**
     * Clean Guard caches
     * clean
     *
     */
    void clean() throws OXException;

    /**
     * Import certificate as certificate authority for group
     * importCa
     *
     * @param file Byte array that contains the certificate
     * @param grpId Group ID to which this certificate will be associated
     * @return
     */
    boolean importCa(byte[] file, int grpId) throws OXException;

}
