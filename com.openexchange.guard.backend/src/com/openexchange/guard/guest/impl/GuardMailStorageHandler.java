/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.modules.Module;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.session.Session;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.ShareTargetPath;
import com.openexchange.share.core.HandlerParameters;
import com.openexchange.share.core.ModuleHandler;
import com.openexchange.share.groupware.TargetProxy;

/**
 * {@link GuardMailStorageHandler}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardMailStorageHandler implements ModuleHandler {

    MailAccountStorageService mass;
    GuardGuestUserService userService;

    public GuardMailStorageHandler(MailAccountStorageService mass, GuardGuestUserService userService) {
        this.mass = Objects.requireNonNull(mass, "Missing MailAccountStorageService");
        this.userService = Objects.requireNonNull(userService, "Missing Guest User service");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleAdjuster#adjustTarget(com.openexchange.share.ShareTarget, com.openexchange.session.Session, int, java.sql.Connection)
     */
    @Override
    public ShareTarget adjustTarget(ShareTarget target, Session session, int targetUserId, Connection connection) throws OXException {
        return target;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleAdjuster#adjustTarget(com.openexchange.share.ShareTarget, int, int, int, java.sql.Connection)
     */
    @Override
    public ShareTarget adjustTarget(ShareTarget target, int contextId, int requestUserId, int targetUserId, Connection connection) throws OXException {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.groupware.spi.ModuleExtension#getModules()
     */
    @Override
    public Collection<String> getModules() {
        return Arrays.asList(Module.MAIL.getName());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#loadTargets(java.util.List, com.openexchange.share.core.HandlerParameters)
     */
    @Override
    public List<TargetProxy> loadTargets(List<ShareTarget> targets, HandlerParameters parameters) throws OXException {
        return Arrays.asList(new GuardMailTargetProxy());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#loadTarget(com.openexchange.share.ShareTarget, com.openexchange.session.Session)
     */
    @Override
    public TargetProxy loadTarget(ShareTarget target, Session session) throws OXException {
        return new GuardMailTargetProxy();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#loadTarget(java.lang.String, java.lang.String, com.openexchange.groupware.contexts.Context, int)
     */
    @Override
    public TargetProxy loadTarget(String folder, String item, Context context, int guestID) throws OXException {
        return new GuardMailTargetProxy();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#canShare(com.openexchange.share.groupware.TargetProxy, com.openexchange.share.core.HandlerParameters)
     */
    @Override
    public boolean canShare(TargetProxy proxy, HandlerParameters parameters) {
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#updateObjects(java.util.List, com.openexchange.share.core.HandlerParameters)
     */
    @Override
    public void updateObjects(List<TargetProxy> modified, HandlerParameters parameters) throws OXException {
        /* no-op */
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#touchObjects(java.util.List, com.openexchange.share.core.HandlerParameters)
     */
    @Override
    public void touchObjects(List<TargetProxy> touched, HandlerParameters parameters) throws OXException {
        /* no-op */
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#isVisible(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public boolean isVisible(String folder, String item, int contextID, int guestID) throws OXException {
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#mayAdjust(com.openexchange.share.ShareTarget, com.openexchange.session.Session)
     */
    @Override
    public boolean mayAdjust(ShareTarget target, Session session) throws OXException {
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#exists(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public boolean exists(String folder, String item, int contextID, int guestID) throws OXException {
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#listTargets(int, int)
     */
    @Override
    public List<TargetProxy> listTargets(int contextID, int guestID) throws OXException {
        if (hasTargets(contextID, guestID)) {
            return Arrays.asList(new GuardMailTargetProxy());
        }
        return Collections.emptyList();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#hasTargets(int, int)
     */
    @Override
    public boolean hasTargets(int contextID, int guestID) throws OXException {
        if (!mass.existsMailAccount(0, guestID, contextID))
         {
            return false;  // If no mail account, not Guard share
        }
        return userService.hasMailItems(guestID, contextID);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#getPath(com.openexchange.share.ShareTarget, com.openexchange.session.Session)
     */
    @Override
    public ShareTargetPath getPath(ShareTarget target, Session session) throws OXException {
        return new ShareTargetPath(target.getModule(), target.getFolder(), target.getItem());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.share.core.ModuleHandler#getPath(com.openexchange.share.ShareTarget, int, int)
     */
    @Override
    public ShareTargetPath getPath(ShareTarget target, int contextID, int guestID) throws OXException {
        return null;
    }

}
