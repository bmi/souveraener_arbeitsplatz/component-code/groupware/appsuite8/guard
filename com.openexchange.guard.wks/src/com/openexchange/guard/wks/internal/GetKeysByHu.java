/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.internal;

import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.wks.osgi.Services;

/**
 * {@link GetKeysByHu}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GetKeysByHu {


    public static PGPPublicKeyRing getKeys (String hu, String domain) throws OXException {
        EmailStorage storage = Services.getService(EmailStorage.class);
        List<Email> emails = storage.getByHu(hu);
        for (Email email : emails) {
            String addr = email.getEmail();
            String emailDomain = addr.substring(addr.indexOf("@") + 1);
            if (email.getContextId() > 0 && domain.toLowerCase().contains(emailDomain.toLowerCase())) {
                KeyTableStorage keyStorage = Services.getService(KeyTableStorage.class);
                GuardKeys key = keyStorage.getCurrentKeyForUser(email.getUserId(), email.getContextId());
                if (key != null) {
                    return key.getPGPPublicKeyRing();
                }
            }
        }
        return null;
    }

    public static boolean hasKeys(String hu, String domain) throws OXException {
       return getKeys(hu, domain) != null;
    }
}
