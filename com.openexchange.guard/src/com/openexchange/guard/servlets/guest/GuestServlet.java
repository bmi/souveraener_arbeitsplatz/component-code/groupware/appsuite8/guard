/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.OXGuardSessionAuthenticationHandler;

/**
 * Provides functionality for guest accounts
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuestServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = 845555040236787927L;

    //new guest actions since 7.8.10
    private static final String GET_MESSAGE ="getmessage";
    private static final String GET_MESSAGES ="getmessages";
    private static final String GET_MESSAGE_LIST ="getmessagelist";
    private static final String UPDATE_MESSAGES = "updatemessages";
    private static final String MOVE_MESSAGES = "movemessages";
    private static final String COPY_MESSAGES = "copymessages";
    private static final String DELETE_MESSAGES = "deletemessages";
    private static final String APPEND_MESSAGE = "appendmessage";
    private static final String UPGRADE_GUEST = "convertGuest";

    private transient final HashMap<String, GuardServletAction> getActions;
    private transient final HashMap<String, GuardServletAction> postActions;

    /**
     * Initializes a new {@link GuestServlet}.
     * @throws OXException
     */
    public GuestServlet() throws OXException {
        OXGuardSessionAuthenticationHandler uiSessionAuthentication = new OXGuardSessionAuthenticationHandler();
        BasicAuthServletAuthenticationHandler basicAuthAuthentication = createBasicAuthHandler();

        //GET
        getActions = new HashMap<String, GuardServletAction>();

        //POST
        postActions = new HashMap<String, GuardServletAction>();

        //new guest actions since 7.10
        postActions.put(GET_MESSAGE, new GetMessage().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(GET_MESSAGE_LIST, new GetMessageList().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(GET_MESSAGES, new GetMessages().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(UPDATE_MESSAGES, new UpdateMessages().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(MOVE_MESSAGES, new MoveMessages().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(COPY_MESSAGES, new CopyMessages().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(DELETE_MESSAGES, new DeleteMessages().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(APPEND_MESSAGE, new AppendMessage().setAuthenticationHandler(basicAuthAuthentication));
        postActions.put(UPGRADE_GUEST, new GuestUpgrader().setAuthenticationHandler(uiSessionAuthentication));
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, getActions);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAction(request, response, postActions);
    }
}
