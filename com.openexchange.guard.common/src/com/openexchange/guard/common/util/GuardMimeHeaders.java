/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

/**
 * {@link GuardMimeHeaders}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class GuardMimeHeaders {

    /**
     * A header which contains information about the signature verification results of the mime message
     */
    public static final String X_GUARD_SIGNATURE_RESULT = "X-Guard-Signature-Result";

    /**
     * Signals the original PGPFormat after decrypting a mime mesage.
     * "inline" for  PGP/INLINE. Everything else for PGP/MIME:
     */
    public static final String X_GUARD_PGP_FORMAT = "X-PGPFormat";

    /**
     * For draft handling
     */
    public static final String X_GUARD_SECURITY = "X-Security";

    /**
     * For storing the 2nd factor PIN in the original email (sent-folder) in order to be recovered by the sender
     */
    public static final String X_GUARD_PIN = "X-OxGuard-PIN";

    /**
     * Contains information about whether or not the MDC check of the message failed during decryption
     */
    public static final String X_GUARD_FAILED_INTEGRITY = "X-Guard-Failed-Integrity";

    /**
     * Mail Reference for a reply to a guest message.
     */
    public static final String X_GUARD_MSG_REF = "X-Guard-msg-ref";
}
