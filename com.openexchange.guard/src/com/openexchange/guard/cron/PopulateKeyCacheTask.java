/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cron;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.sql.SQLException;
import java.util.Objects;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;

public class PopulateKeyCacheTask implements Runnable {

    private static Logger LOG = LoggerFactory.getLogger(PopulateKeyCacheTask.class);

    private GuardConfigurationService guardConfigurationService;
    private KeyCacheStorage keyCacheStorage;
    private GuardCipherService cipherService;
    private MasterKeyService masterKeyService;

    /**
     * Initializes a new {@link PopulateKeyCacheTask}.
     *
     * @param guardConfigurationService
     * @param keyCacheStorage
     * @param cipherService
     * @param masterKeyService
     */
    public PopulateKeyCacheTask(GuardConfigurationService guardConfigurationService,
                       KeyCacheStorage keyCacheStorage,
                       GuardCipherService cipherService,
        MasterKeyService masterKeyService) {

        this.guardConfigurationService = Objects.requireNonNull(guardConfigurationService, "guardConfigurationService must not be null");;
        this.keyCacheStorage = Objects.requireNonNull(keyCacheStorage, "keyCacheStorage must not be null");
        this.cipherService = Objects.requireNonNull(cipherService, "cipherService must not be null");
        this.masterKeyService = Objects.requireNonNull(masterKeyService,"masterKeyService must not be null");
    }

    @Override
    public void run() {
        try {
            int count = keyCacheStorage.getCount();
            int rsaCacheCount = guardConfigurationService.getIntProperty(GuardProperty.rsaCacheCount);
            if (count < rsaCacheCount) {
                while (count < rsaCacheCount) {
                    count++;
                    String newkey = genKeyPair();
                    storekey(newkey);
                }
            }
        } catch (Exception e) {
            LOG.error("Unable to access key_cache table.  Check database permissions", e);
        }
    }

    /**
     * Store RSA key PEM for future use if keyasc param is not empty. Encrypt with Config.rpass
     *
     * @param keyAsc the ascii armored to to save
     * @throws SQLException due an error
     */
    private void storekey(String keyAsc) throws SQLException {
        if (keyAsc == null || keyAsc.isEmpty()) {
            LOG.warn("Unable to store key as provided ascii armored version is empty!");
            return;
        }

        try {
            //encrypting the key for storing it in the DB
            keyAsc = cipherService.encrypt(keyAsc, masterKeyService.getMasterKey(0, false).getRC(), "key");
            keyCacheStorage.insert(keyAsc);
        } catch (Exception ex) {
            LOG.error("Error while storing key", ex);
        }
    }

    /**
     * Generate RSA keypair and return as PEM
     *
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchProviderException
     * @throws OXException
     */
    private String genKeyPair() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException, OXException {
        RSAKeyPairGenerator kpgen = new RSAKeyPairGenerator();
        kpgen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(0x10001), new SecureRandom(), guardConfigurationService.getIntProperty(GuardProperty.rsaKeyLength), guardConfigurationService.getIntProperty(GuardProperty.rsaCertainty)));
        AsymmetricCipherKeyPair kp = kpgen.generateKeyPair();
        RSAPrivateCrtKeyParameters rsapckp = (RSAPrivateCrtKeyParameters) kp.getPrivate();

        RSAKeyParameters rsakp = (RSAKeyParameters) kp.getPublic();

        KeyFactory fact = KeyFactory.getInstance("RSA", "BC");

        RSAPrivateCrtKeySpec prvKeySpecs = new RSAPrivateCrtKeySpec(rsapckp.getModulus(), rsapckp.getPublicExponent(), rsapckp.getExponent(), rsapckp.getP(), rsapckp.getQ(), rsapckp.getDP(), rsapckp.getDQ(), rsapckp.getQInv());

        PrivateKey privateKey = fact.generatePrivate(prvKeySpecs);

        RSAPublicKeySpec pubKeySpecs = new RSAPublicKeySpec(rsakp.getModulus(), rsakp.getExponent());
        PublicKey publicKey = fact.generatePublic(pubKeySpecs);

        KeyPair newkp = new KeyPair(publicKey, privateKey);
        // Export
        StringWriter writer = new StringWriter();
        JcaPEMWriter wr = new JcaPEMWriter(writer);
        wr.writeObject(newkp);
        wr.flush();
        wr.close();
        LOG.debug("Key generated for cache");
        return (writer.toString());

    }
}
