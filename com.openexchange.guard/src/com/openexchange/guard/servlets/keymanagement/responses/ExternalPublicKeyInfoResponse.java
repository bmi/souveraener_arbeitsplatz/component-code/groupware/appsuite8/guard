/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;

/**
 * {@link ExternalPublicKeyInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class ExternalPublicKeyInfoResponse {

    private final OGPGPKeyRing externalKeyRing;

    /**
     * Initializes a new {@link ExternalPublicKeyInfoResponse}.
     * @param externalKey
     */
    public ExternalPublicKeyInfoResponse(OGPGPKeyRing externalKeyRing) {
        this.externalKeyRing = externalKeyRing;
    }

    public String getIds() {
        return externalKeyRing.getIds();
    }

    public int getShareLevel() {
        return this.externalKeyRing.getShareLevel();
    }

    public boolean isShared() {
        return this.externalKeyRing.isShared();
    }

    public boolean isInline() {
        return this.externalKeyRing.isInline();
    }

    public boolean isGuardKey() {
        return this.externalKeyRing.isGuardKey();
    }

    public boolean isOwned() {
        return this.externalKeyRing.isOwned();
    }

    public PublicKeyRingInfoResponse getPublicRing() {
        return new PublicKeyRingInfoResponse(this.externalKeyRing.getKeyRing());
    }

    public boolean isVerified() {
        return this.externalKeyRing.isVerified();
    }
}
