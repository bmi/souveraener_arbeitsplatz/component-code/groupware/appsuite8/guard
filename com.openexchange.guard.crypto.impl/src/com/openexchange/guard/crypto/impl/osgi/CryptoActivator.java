/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.crypto.impl.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.crypto.impl.CryptoManagerImpl;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link CryptoActivator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.6
 */
public class CryptoActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return EMPTY_CLASSES;
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return EMPTY_CLASSES;
    }

    @Override
    protected void startBundle() throws Exception {
        CryptoManagerImpl cryptoManagerImpl = new CryptoManagerImpl();
        registerService(CryptoManager.class, cryptoManagerImpl);
        track(PasswordManagementService.class, new CustomKeyManagement(context, cryptoManagerImpl));
        track(MimeEncryptionService.class, new CustomMimeEncryption(context, cryptoManagerImpl));
        track(MimeSignatureVerificationService.class, new CustomVerificationService(context, cryptoManagerImpl));
        openTrackers();
    }

    /////////// Trackers

    final class CustomKeyManagement implements ServiceTrackerCustomizer<PasswordManagementService, PasswordManagementService> {

        @SuppressWarnings("hiding")
        private final BundleContext context;
        private CryptoManagerImpl cryptoManagerImpl;

        public CustomKeyManagement(final BundleContext context, CryptoManagerImpl cryptoManagerImpl) {
            this.context = context;
            this.cryptoManagerImpl = cryptoManagerImpl;
        }

        @Override
        public PasswordManagementService addingService(ServiceReference<PasswordManagementService> reference) {
            PasswordManagementService service = context.getService(reference);
            cryptoManagerImpl.registerPasswordManagementService(service);
            return service;
        }

        @Override
        public void modifiedService(ServiceReference<PasswordManagementService> reference, PasswordManagementService service) { /* no-op */ }

        @Override
        public void removedService(ServiceReference<PasswordManagementService> reference, PasswordManagementService service) {
            cryptoManagerImpl.removeKeyManagementService(service);
        }
    }

    final class CustomMimeEncryption implements ServiceTrackerCustomizer<MimeEncryptionService, MimeEncryptionService> {

        @SuppressWarnings("hiding")
        private final BundleContext context;
        private CryptoManagerImpl cryptoManagerImpl;

        public CustomMimeEncryption(final BundleContext context, CryptoManagerImpl cryptoManagerImpl) {
            this.context = context;
            this.cryptoManagerImpl = cryptoManagerImpl;
        }

        @Override
        public MimeEncryptionService addingService(ServiceReference<MimeEncryptionService> reference) {
            MimeEncryptionService service = context.getService(reference);
            cryptoManagerImpl.registerMimeEncryptionService(service);
            return service;
        }

        @Override
        public void modifiedService(ServiceReference<MimeEncryptionService> reference, MimeEncryptionService service) { /* no-op */ }

        @Override
        public void removedService(ServiceReference<MimeEncryptionService> reference, MimeEncryptionService service) {
            cryptoManagerImpl.removeMimeEncryptionService(service);
        }
    }

    final class CustomVerificationService implements ServiceTrackerCustomizer<MimeSignatureVerificationService, MimeSignatureVerificationService> {

        @SuppressWarnings("hiding")
        private final BundleContext context;
        private CryptoManagerImpl cryptoManagerImpl;

        public CustomVerificationService(final BundleContext context, CryptoManagerImpl cryptoManagerImpl) {
            this.context = context;
            this.cryptoManagerImpl = cryptoManagerImpl;
        }

        @Override
        public MimeSignatureVerificationService addingService(ServiceReference<MimeSignatureVerificationService> reference) {
            MimeSignatureVerificationService service = context.getService(reference);
            cryptoManagerImpl.registerMimeVerificationService(service);
            return service;
        }

        @Override
        public void modifiedService(ServiceReference<MimeSignatureVerificationService> reference, MimeSignatureVerificationService service) { /* no-op */ }

        @Override
        public void removedService(ServiceReference<MimeSignatureVerificationService> reference, MimeSignatureVerificationService service) {
            cryptoManagerImpl.removeMimeVerificationService(service);
        }
    }
}
