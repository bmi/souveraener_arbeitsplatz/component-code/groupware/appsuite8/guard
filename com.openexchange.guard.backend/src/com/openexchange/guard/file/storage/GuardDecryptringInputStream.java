/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Objects;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardDecryptringInputStream} decrypts data using OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class GuardDecryptringInputStream extends AbstractGuardInputStream {

    private final GuardApi guardApi;
    private final GuardAuthenticationToken authToken;
    private final InputStream encrypted;
    private final Session session;

    /**
     * Initializes a new {@link GuardDecryptringInputStream}.
     *
     * @param guardApi The guard enpoint to use
     * @param authToken The authentication token for accessing Guard
     * @param encrypted The data to decrypt
     */
    public GuardDecryptringInputStream(GuardApi guardApi, GuardAuthenticationToken authToken, InputStream encrypted, Session session) {
        this.guardApi = Objects.requireNonNull(guardApi,"guardApi must not be null");
        this.authToken = Objects.requireNonNull(authToken, "authToken must not be null");
        this.encrypted = Objects.requireNonNull(encrypted, "encrypted must not be null");
        this.session = session;
    }

    /**
     * Creates the InputStream which reads the decrypted data from Guard
     *
     * @return An InputStream containing the decrypted data obtained from Guard
     * @throws OXException
     */
    @Override
    protected InputStream getGuardStream() throws OXException {
        ByteArrayInputStream jsonStream = null;
        JSONObject json = new JSONObject();
        JSONObject user = new JSONObject();
        user.putSafe("session", authToken.getGuardSessionId());
        user.putSafe("auth", authToken.getValue());
        user.putSafe("id", session.getUserId());
        user.putSafe("cid", session.getContextId());
        json.putSafe("user", user);
        jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        LinkedHashMap<String, InputStream> multipartParameters = new LinkedHashMap<>();
        multipartParameters.put("json", jsonStream);
        multipartParameters.put("data", encrypted);

        return guardApi.processResources(GuardApis.mapFor("action", "decrypt", "respondWithJSON", "true" /* forcing guard to render errors in JSON */), multipartParameters, null, session);
    }
}
