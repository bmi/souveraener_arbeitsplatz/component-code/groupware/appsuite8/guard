/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime;

import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * {@link CertificateVerificationResult}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CertificateVerificationResult {

    /**
     * {@link Result}
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v2.10.7
     */
    public enum Result {

        /** The certificate is valid and verified */
        VERIFIED("verified"),
        /** The certificate is incomplete */
        INCOMPLETE("incomplete"),
        /** The certificate isn't valid or verified and thus can't be trusted */
        NOT_TRUSTED("not trusted");

        private String val;

        Result(String val) {
            this.val = val;
        }

        /**
         * Get the verification result as text
         *
         * @return The result
         */
        public String getVal() {
            return val;
        }
    }

    private SmimeKeys keys;
    private Result result;

    /**
     * Initializes a new {@link CertificateVerificationResult}.
     *
     * @param keys The {@link SmimeKeys}
     * @param result The result
     */
    public CertificateVerificationResult(SmimeKeys keys, Result result) {
        this.keys = keys;
        this.result = result;
    }

    /**
     * Get the verification result
     *
     * @return The {@link Result}
     */
    public Result getResult() {
        return result;
    }

    /**
     * Get the {@link SmimeKeys} the result is valid for
     *
     * @return The keys
     */
    public SmimeKeys getKeys() {
        return keys;
    }

}
