/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.crypto;

import java.util.List;
import javax.mail.internet.MimeMessage;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link MimeSignatureVerificationService} - High level service for verifying PGP Email signatures.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public interface MimeSignatureVerificationService {

    /**
     * Returns the {@link CryptoType} associated with the implementation of this this interface
     *
     * @return The related {@link CryptoType}
     */
    CryptoType.PROTOCOL getCryptoType();

    /**
     * Verifies signatures of a {@link MimeMessage}
     *
     * @param mimeMessage The message containing the plaintext signatures
     * @param user The user which wants to verify the message's signatures
     * @param decrypted If message was from decrypted email. Will not expect headers in message if decrypted component
     * @return A list of verification results
     */
    List<SignatureVerificationResult> verify(MimeMessage mimeMessage, UserIdentity user, boolean decrypted) throws OXException;
}
