/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.keymanagement.pgp.UploadPGPKeys;
import com.openexchange.guard.servlets.keymanagement.responses.GuardKeyInfoCollectionResponse;
import com.openexchange.server.ServiceLookup;

/**
 * {@link UploadKeysAction} allows to upload custom, client created, public and private keys.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UploadKeysAction extends GuardServletAction {

    private static final int MAX_KEY_FILE_UPLOAD_SIZE = 1000000; /* 1 MB */

    private ServiceLookup services;

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        Collection<FileItem> items = fileUploadHandler.parseItems(request, MAX_KEY_FILE_UPLOAD_SIZE);
        final String email = new EmailValidator().assertInput(new Api(new OxCookie(request.getCookies()), request).getPrimary(), "email");
        GuardKeyInfoCollectionResponse keyInfo = UploadPGPKeys.doUpload(items, email, userSession);
        ServletUtils.sendObject(response, keyInfo);
    }
}
