/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.session.Session;

/**
 * {@link PGPSessionKeyExtractor} uses an OX Guard endpipoint to extract "Public-Key Encrypted Session Key Packets" from a given PGP input stream.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 * @see RFC 4880 - "OpenPGP Message Format"
 */
public class PGPSessionKeyExtractor extends AbstractGuardAccess{

    private static String EXTRACT_SESSION_KEYS_ACTION = "extractsessionkeys";

    /**
     * Extracts the encrypted session packets from the given PGP data.
     *
     * @param inputStream The PGP data to extract the encrypted session key packets from
     * @return
     * @throws OXException
     */
    public String[] getEncryptedSession(InputStream inputStream, Session session) throws OXException {

        GuardApi guardApi = requireGuardApi(PGPCORE_ENDPOINT);
        InputStream response = guardApi.processResource(
            GuardApis.mapFor("action",EXTRACT_SESSION_KEYS_ACTION),
            inputStream,
            "application/octet-stream",
            "data",
            session);

        JSONObject result = parseResult(response);
        JSONArray sessionKeyPackets;
        try {
            JSONObject data = result.getJSONObject("data");
            sessionKeyPackets = data.getJSONArray("encryptedSessionKeyPackets");
            String[] ret = new String[sessionKeyPackets.length()];
            for (int i=0; i < sessionKeyPackets.length(); i++) {
                ret[i] = sessionKeyPackets.getString(i);
            }
            return ret;
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parses the JSON result
     *
     * @param response The result to parse
     * @return The parsed JSON result
     * @throws OXException
     * @throws JSONException
     */
    private JSONObject parseResult(InputStream response) throws OXException {
        try {
            return new JSONObject(new InputStreamReader(response, StandardCharsets.UTF_8));
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }
}
