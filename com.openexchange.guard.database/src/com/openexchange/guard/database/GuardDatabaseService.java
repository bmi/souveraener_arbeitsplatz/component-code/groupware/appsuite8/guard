/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database;

import java.sql.Connection;
import com.openexchange.database.Assignment;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;

/**
 * {@link GuardDatabaseService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public interface GuardDatabaseService {

    /**
     * Returns the schema name assigned to the given shardId.
     *
     * @param shardId The id of the shard to return the name for
     * @return String with the shard name related to given id
     */
    String getShardName(int shardId);

    /**
     * Returns a writable connection to registered ox guard database.
     *
     * @return a writable connection to the ox guard database.
     * @throws OXException if no connection can be obtained.
     */
    Connection getWritableForGuard() throws OXException;

    /**
     * Returns a writable connection without timeout to the registered ox guard database. This {@link Connection} should be used to perform administrative things like update tasks.
     *
     * @return a writable connection to the ox guard database.
     * @throws OXException if no connection can be obtained.
     */
    Connection getWritableForGuardNoTimeout() throws OXException;

    /**
     * Returns a read only connection to registered ox guard database.
     *
     * @return a read only connection to the ox guard database.
     * @throws OXException if no connection can be obtained.
     */
    Connection getReadOnlyForGuard() throws OXException;

    /**
     * Returns a read only connection to the ox guard database.
     *
     * @param connection Read only connection to return.
     */
    void backReadOnlyForGuard(Connection connection);

    /**
     * Returns a writable connection to the ox guard database.
     *
     * @param connection Writable connection to return.
     */
    void backWritableForGuard(Connection connection);

    /**
     * Returns a writable connection without timeout to the ox guard database.
     *
     * @param connection Writable connection to return.
     */
    void backWritableForGuardNoTimeout(Connection connection);

    /**
     * Returns a writable connection without timeout to the ox guard database that was only used for reading
     *
     * @param connection Writable connection to return.
     */
    void backWritableAfterReadingForGuardNoTimeout(Connection connection);

    /**
     * Returns a writable connection to the shard database.
     *
     * @param shardId The identifier of the shard
     * @return a writable connection to the shard database.
     * @throws OXException
     */
    Connection getWritableForShard(int shardId) throws OXException;

    /**
     * Returns a writable connection without timeout to the shard database. This {@link Connection} should be used to perform administrative things like update tasks.
     *
     * @return a writable connection to the ox guard database.
     * @throws OXException if no connection can be obtained.
     */
    Connection getWritableForShardNoTimeout(int shardId) throws OXException;

    /**
     * Returns a writable connection without timeout to the shard database.
     *
     * @param connection Writable connection to return.
     */
    void backWritableForShardNoTimeout(int shardId, Connection connection);

    /**
     * Returns a writable connection without timeout to the shard database that was only used for reading
     *
     * @param connection Writable connection to return.
     */
    void backWritableAfterReadingForShardNoTimeout(int shardId, Connection connection);

    /**
     * Returns a read only connection to the shard database.
     *
     * @param shardId The identifier of the shard
     * @return a read only connection to the shard database.
     * @throws OXException
     */
    Connection getReadOnlyForShard(int shardId) throws OXException;

    /**
     * Returns a read only connection to the shard database.
     *
     * @param shardId The shard id the connection is assigned to.
     * @param connection read only connection to return.
     */
    void backReadOnlyForShard(int shardId, Connection connection);

    /**
     * Returns a writable connection to the shard database.
     *
     * @param shardId The shard id the connection is assigned to.
     * @param connection writable connection to return.
     */
    void backWritableForShard(int shardId, Connection connection);

    /**
     * Returns a {@link GuardConnectionWrapper} containing a writable connection to the database the user is associated to.
     *
     * @param userId The identifier of the user
     * @param contextId The identifier of the context
     * @param shard The identifier of a possible shard
     * @return a {@link GuardConnectionWrapper} with a writable connection and connection related information.
     * @throws OXException
     */
    GuardConnectionWrapper getWritable(int userId, int contextId, int shard) throws OXException;

    /**
     * Returns a writable connection to the destination pool which will be detected based on meta information available within the {@link GuardConnectionWrapper}
     *
     * @param connectionWrapper The wrapper the contained writable connection should be returned for
     */
    void backWritable(GuardConnectionWrapper connectionWrapper);

    /**
     * Returns a {@link GuardConnectionWrapper} containing a read only connection to the database the user is associated to.
     *
     * @param userId The identifier of the user
     * @param contextId The identifier of the context
     * @param shard The identifier of a possible shard
     * @return a {@link GuardConnectionWrapper} with a read only connection and connection related information.
     * @throws OXException
     */
    GuardConnectionWrapper getReadOnly(int userId, int contextId, int shard) throws OXException;

    /**
     * Returns a read only connection to the destination pool which will be detected based on meta information available within the {@link GuardConnectionWrapper}
     *
     * @param connectionWrapper The wrapper the contained read only connection should be returned for
     */
    void backReadOnly(GuardConnectionWrapper connectionWrapper);

    /**
     * Returns a writable connection to the context database.
     *
     * @param contextId The identifier of the context database
     * @return a writable connection to the context database.
     * @throws OXException
     */
    Connection getWritable(int contextId) throws OXException;

    /**
     * Returns a read only connection to the context database.
     *
     * @param contextId The identifier of the context database
     * @return a read only connection to the context database.
     * @throws OXException
     */
    Connection getReadOnly(int contextId) throws OXException;

    /**
     * Returns a read only connection to the context database.
     *
     * @param contextId The context id the connection is assigned to.
     * @param connection read only connection to return.
     */
    void backReadOnly(int contextId, Connection connection);

    /**
     * Returns a writable connection to the context database.
     *
     * @param contextId The context id the connection is assigned to.
     * @param connection writable connection to return.
     */
    void backWritable(int contextId, Connection connection);

    /**
     * Returns a writable connection to the configdb.
     *
     * @return a writable connection to the configdb.
     * @throws OXException
     */
    Connection getWritableForConfigDB() throws OXException;

    /**
     * Returns a read only connection to the configdb.
     *
     * @return a read only connection to the configdb.
     * @throws OXException
     */
    Connection getReadOnlyForConfigDB() throws OXException;

    /**
     * Returns a read only connection to the configdb.
     *
     * @param connection read only connection to return.
     */
    void backReadOnlyForConfigDB(Connection connection);

    /**
     * Returns a writable connection to the configdb.
     *
     * @param connection The writable connection to return.
     */
    void backWritableForConfigDB(Connection connection);

    /**
     * Returns a writable connection to the guard MySQL service to register the guard schema.
     *
     * @return a writable connection to the guard server.
     * @throws OXException
     */
    Connection getWritableForGuardInit() throws OXException;

    /**
     * Returns a writable connection to the shard MySQL service to register the shard schema.
     *
     * @return a writable connection to the shard server.
     * @throws OXException
     */
    Connection getWritableForShardInit() throws OXException;

    /**
     * Returns a writable connection used to create a new schema (guard or shard).
     *
     * @param connection The writable connection to return.
     */
    void backWritableForInit(Connection connection);

    /**
     * Reloads the underlying database {@link Assignment}s. Should be used after a new database has been registered by using com.openexchange.guard.database.GuardDatabaseService.getWritableForGuardInit() or
     * com.openexchange.guard.database.GuardDatabaseService.getWritableForShardInit()
     *
     * @throws OXException
     */
    void reloadAssignments() throws OXException;

    /**
     * Returns a connection URL that might get used to create a database connection.
     *
     * @param hostname The hostname to create a connection URL for
     * @return String with the connection URL that looks like jdbc:mysql://9.9.9.9 (if 9.9.9.9 has been provided)
     */
    String getConnectionUrl(String hostname);

    /**
     * Returns databaseService
     * @return
     */
    DatabaseService getDatabaseService ();
}
