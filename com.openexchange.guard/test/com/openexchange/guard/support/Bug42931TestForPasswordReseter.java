/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.support;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.osgi.Services;

/**
 *
 * {@link Bug42931TestForPasswordReseter}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Services.class })
public class Bug42931TestForPasswordReseter {

    private static final int CONTEXT_ID = 88;

    private static final int USER_ID = 77;

    private static final String PASSWORD = "theNewGeneratedPassword";

    private static final String EMAIL = "martin.schneider@open-xchange.com";

    @InjectMocks
    private PasswordReseter passwordReseter;

    @Mock
    private GuardKeyService keyService;

    @Mock
    private MailCreatorService mailCreatorService;

    @Mock
    private GuardNotificationService guardNotificationService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Services.class);
        Mockito.when(Services.getService(GuardKeyService.class)).thenReturn(keyService);
        Mockito.when(keyService.isRecoveryEnabled(USER_ID, CONTEXT_ID)).thenReturn(true);
        Mockito.when(keyService.generatePassword(USER_ID, CONTEXT_ID)).thenReturn(PASSWORD);

        Mockito.when(Services.getService(MailCreatorService.class)).thenReturn(mailCreatorService);
        Mockito.when(mailCreatorService.getResetEmail(ArgumentMatchers.anyString(),
            ArgumentMatchers.<String> anyList(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt())).thenReturn(new JsonObject());

        Mockito.when(Services.getService(GuardNotificationService.class)).thenReturn(guardNotificationService);
    }

    @Test
    public void testReset_sendFailed_doNotReset() throws OXException {
        Mockito.doThrow(OXException.class).when(guardNotificationService).send((JsonObject) ArgumentMatchers.any(), ArgumentMatchers.eq(USER_ID), ArgumentMatchers.eq(CONTEXT_ID), ArgumentMatchers.eq(null));

        try {
            passwordReseter.reset(USER_ID, CONTEXT_ID, "de_DE", "sendToAddress@example.org", 0, EMAIL);
        } catch (OXException e) {
            Assert.assertNotNull(e);
        }

        Mockito.verify(keyService, Mockito.never()).resetPassword(EMAIL, PASSWORD);
    }

    @Test
    public void testReset_sendSuccessful_doReset() throws OXException {
        passwordReseter.reset(USER_ID, CONTEXT_ID, "de_DE", "sendToAddress@example.org", 0, EMAIL);

        Mockito.verify(keyService, Mockito.times(1)).resetPassword(EMAIL, PASSWORD);
    }

    @Test(expected = OXException.class)
    public void testReset_disableRecovery_throwException() throws OXException {
        Mockito.when(keyService.isRecoveryEnabled(USER_ID, CONTEXT_ID)).thenReturn(false);

        passwordReseter.reset(USER_ID, CONTEXT_ID, "de_DE", "sendToAddress@example.org", 0, EMAIL);
    }

}
