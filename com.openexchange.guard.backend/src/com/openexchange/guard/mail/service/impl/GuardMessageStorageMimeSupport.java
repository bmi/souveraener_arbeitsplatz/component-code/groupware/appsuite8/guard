/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import javax.mail.Message;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageMimeSupport;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizerService;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;


/**
 * {@link GuardMessageStorageMimeSupport} - The Guard-aware message storage delegating to {@link IMailMessageStorageMimeSupport}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorageMimeSupport extends GuardMessageStorage implements IMailMessageStorageMimeSupport {

    private final IMailMessageStorageMimeSupport messageStorageMimeSupport;
    private final CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMessageStorageMimeSupport}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     * @param authToken
     */
    public GuardMessageStorageMimeSupport(Session session, IMailMessageStorageMimeSupport delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(session, delegate, services, authToken, type);
        this.messageStorageMimeSupport = delegate;
        this.type = type;
    }

    @Override
    public boolean isMimeSupported() throws OXException {
        return messageStorageMimeSupport.isMimeSupported();
    }

    @Override
    public String[] appendMimeMessages(String destFolder, Message[] msgs) throws OXException {
        return messageStorageMimeSupport.appendMimeMessages(destFolder, msgs);
    }

    @Override
    public Message getMimeMessage(String fullName, String id, boolean markSeen) throws OXException {
        Message mimeMessage = messageStorageMimeSupport.getMimeMessage(fullName, id, markSeen);
        if (null == mimeMessage) {
            return null;
        }

        return processMimeMessage(mimeMessage);
    }

    /**
     * Internal method to process a MIME message using OX Guard
     *
     * @param authToken The authToken
     * @param message The MIME message
     * @return The processed (i.e decrypted) message
     * @throws OXException
     */
    private Message processMimeMessage(Message message) throws OXException {
        CryptoMailRecognizerService cryptoMail = services.getOptionalService(CryptoMailRecognizerService.class);
        // Check if encrypted email.  Drafts, for example, may or may not be encrypted depending on the crypto type
        if (cryptoMail != null) {
            if (cryptoMail.isEncryptedMessage(message)) {
                GuardAuthenticationToken guardAuthToken = getAuthToken(session, type, false);
                if (guardAuthToken == null) {
                    throw GuardExceptionCodes.MISSING_AUTH.create();
                }
                return new GuardMimeDecrypter().decrypt(session, guardAuthToken, message, null);
            }
        }
        return message;

    }
}
