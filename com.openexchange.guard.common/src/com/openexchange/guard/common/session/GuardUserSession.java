
package com.openexchange.guard.common.session;

import java.util.Locale;

/**
 * {@link GuardUserSession} Represents a user session for Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardUserSession {

    private final String sessionId;
    private final int userId;
    private final int contextId;
    private final int guardUserId;
    private final int guardContextId;
    private final Locale locale;
    private final ConnectionInformation connectionInformation;
    private final boolean isGuest;
    private final String guardSession;

    /**
     * Initializes a new {@link GuardUserSession}.
     *
     * @param sessionId the Appsuite session id or a guard guest session ID
     * @param contextId the context id
     * @param userId the user id
     * @param locale The user's locale
     * @param connectionInformation {@link ConnectionInformation}for the session
     */
    public GuardUserSession(String sessionId, String guardSession, int contextId, int userId, boolean isGuest, Locale locale, ConnectionInformation connectionInformation) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.contextId = contextId;
        this.locale = locale;
        this.connectionInformation = connectionInformation;
        this.isGuest = isGuest;
        this.guardContextId = contextId;
        this.guardUserId = userId;
        this.guardSession = guardSession;
    }

    /**
     * Initializes a new {@link GuardUserSession}.
     *
     * @param sessionId the Appsuite session id or a guard guest session ID
     * @param contextId the context id
     * @param userId the user id
     * @param locale The user's locale
     * @param connectionInformation {@link ConnectionInformation}for the session
     */
    public GuardUserSession(String sessionId, String guardSession, int contextId, int userId, int guardContextId, int guardUserId, boolean isGuest, Locale locale, ConnectionInformation connectionInformation) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.contextId = contextId;
        this.locale = locale;
        this.connectionInformation = connectionInformation;
        this.isGuest = isGuest;
        this.guardContextId = guardContextId;
        this.guardUserId = guardUserId;
        this.guardSession = guardSession;
    }

    /**
     * Initializes a new {@link GuardUserSession}.
     *
     * @param contextId the context id
     * @param userId the user id
     * @param guardContextId the guard context id
     * @param guardUserId the guard user id
     */
    public GuardUserSession(int contextId, int userId, int guardContextId, int guardUserId) {
        this.contextId = contextId;
        this.userId = userId;
        this.guardContextId = guardContextId;
        this.guardUserId = guardUserId;
        this.sessionId = null;
        this.locale = null;
        this.connectionInformation = null;
        this.isGuest = false;
        this.guardSession = null;
    }

    /**
     * @return the Appsuite session id
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @return the user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @return the context id
     */
    public int getContextId() {
        return contextId;
    }

    public int getGuardUserId() {
        return guardUserId;
    }

    public int getGuardContextId() {
        return guardContextId;
    }

    public String getGuardSession() {
        return guardSession;
    }

    /**
     * @return the user's locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * @return the session's {@link ConnectionInformation}
     */
    public ConnectionInformation getConnectionInformation() {
        return connectionInformation;
    }

    /**
     * Returns if user is a Guest account
     * @return
     */
    public boolean isGuest() {
        return isGuest;
    }
}
