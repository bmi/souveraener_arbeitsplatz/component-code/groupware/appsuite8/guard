/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.maintenance;

import java.rmi.RemoteException;
import javax.management.MBeanException;
import com.openexchange.guard.management.maintenance.internal.TestUserResult;

/**
 * {@link GuardMaintenanceMBean}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardMaintenanceMBean {

    public static final String DOMAIN = "com.openexchange.guard";

    public static final String KEY = "name";

    public static final String VALUE = "GuardManagement";

    public static final String DESCRIPTION = "Management Bean for the OX Guard";

    /**
     * Reset the password for the specified user
     *
     * @param email The user's email address for which to reset the password
     * @param type Type of crypto
     * @throws RemoteException
     */
    void reset(String email, String type) throws MBeanException;

    /**
     * Remove the PIN for the specified user
     *
     * @param email The user's email address for which to remove the PIN
     * @throws RemoteException
     */
    void removePIN(String email) throws MBeanException;

    /**
     * Upgrade the account of the specified user
     *
     * @param email The user's email address for which to perform the upgrade
     * @param userid The users user_id
     * @param cid The users context
     * @throws RemoteException
     */
    void upgrade(String email, String userid, String cid) throws MBeanException;

    /**
     * Perform a simple test for the provided e-mail address
     *
     * @param email The e-mail address of the user
     * @throws RemoteException
     */
    String test(String email) throws MBeanException;

    /**
     * Perform a simple test for the provided e-mail address
     *
     * @param email The e-mail address of the user
     * @throws RemoteException
     */
    TestUserResult testUser(String email) throws MBeanException;

    /**
     * Deletes a user from Guard
     * @param email
     * @throws MBeanException
     */
    void deleteUser(String email) throws MBeanException;

    /**
     * Sends Guest new link to create new keys
     *
     * @param email
     * @param fromEmail - email of user to use to generate share link
     * @param fromId - id of user to generate share link. Required if fromEmail not specified
     * @param fromCid - cid of user to generate share link. Required if fromEmail not specified
     * @throws MBeanException
     */
    void resetGuest(String email, String fromEmail, int fromId, int fromCid) throws MBeanException;

    /**
     * Imports a PGP Public Key into System keys for HKP server
     * @param file
     * @throws MBeanException
     */
    void importPublic(String file) throws MBeanException;

    /**
     * Imports a user private key
     *
     * @param type smime vs pgp
     * @param file byte array containing key
     * @param userId
     * @param cid
     * @param oldPass password for the file containing the key
     * @param newPass new password for the user (if missing, uses oldpass)
     */
    void importKey(String type, byte[] file, int userId, int cid, String oldPass, String newPass) throws MBeanException;

    /**
     * Attempts validation and repair of guard database
     *
     * @throws MBeanException
     */
    StringBuilder repair (boolean dryRun) throws MBeanException;

    /**
     * Changes email, userid, or context of a user Identified by email
     * @param email
     * @param newEmail
     * @throws MBeanException
     */
    void changeUser(String email, String newEmail) throws MBeanException;

    /**
     * Creates new master key and returns index
     * createMaster
     *
     * @throws MBeanException
     */
    int createMaster() throws MBeanException;

    /**
     * Cleans Guard caches
     * clean
     *
     * @throws MBeanException
     */
    void clean() throws MBeanException;

    /**
     * Imports a certificate authority for a group
     * importCA
     *
     * @param file byte array containing the crt
     * @param grpId Group Id to associate the key
     * @throws MBeanException
     */
    void importCA(byte[] file, int grpId) throws MBeanException;


}
