/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_EMAIL;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_NAME;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.GuardKeyInfoResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link CreateKeyPairAction} creates a new key pair, i.e. a new {@link GuardKeys} object, for a user.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class CreateKeyPairAction extends GuardServletAction {

    private GuardKeys createKey(GuardUserSession userSession, HttpServletRequest request, UserIdentity userIdentity) throws OXException {
        KeyCreationService keyCreationService = Services.getService(KeyCreationService.class);
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        KeyRecoveryService keyRecoveryService = Services.getService(KeyRecoveryService.class);

        final String name = ServletUtils.getStringParameter(request, PARAM_NAME, true);
        final String secondaryEmail = ServletUtils.getStringParameter(request, PARAM_EMAIL, false);
        final String email = new EmailValidator().assertInput(new Api(new OxCookie(request.getCookies()), request).getPrimary());
        final boolean createRecovery = keyRecoveryService.checkCreateRecovery(userSession.getContextId(), userSession.getUserId());
        GuardKeys existingKey = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId());
        final boolean isFirstKey = existingKey == null;
        final boolean markAsCurrent = true;
        boolean hasSecondary = false;

        if (secondaryEmail != null && !secondaryEmail.isEmpty()) {
            new EmailValidator().assertInput(secondaryEmail);
            hasSecondary = true;
        }

        GuardKeys keys = keyCreationService.create(userSession.getGuardUserId(),
            userSession.getGuardContextId(),
            name,
            email,
            new String(userIdentity.getPassword()),
            userSession.getLocale(),
            markAsCurrent,
            createRecovery,
            userSession.getUserId(),
            userSession.getContextId());

        if (isFirstKey) {
            final boolean isUserCreatedKey = true;
            keys = keyService.storeKeys(keys, isUserCreatedKey);
        } else {
            keyService.addNewKey(keys);
            if(existingKey != null) {
                keyService.updateSettingsForUser(existingKey);
                keys.setQuestion(existingKey.getQuestion());
                MasterKeyService mKeyService = Services.getService(MasterKeyService.class);
                if (mKeyService != null) {
                    String decrypted = mKeyService.getRcDecrypted(existingKey.getAnswer(), existingKey.getQuestion(), existingKey.getMasterKeyIndex());
                    keys.setAnswer(mKeyService.getRcEncryted(decrypted, keys.getQuestion(), keys.getMasterKeyIndex()));
                }
                keyService.updateAnswerQuestionForUser(keys);
            }
        }

        if (hasSecondary) {
            keyService.storeQuestion(keys.getUserid(), keys.getContextid(), "e", secondaryEmail);
        }

        return keys;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        if (userSession == null) {
            throw GuardAuthExceptionCodes.MISSING_SESSION.create();
        }

        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());
        final GuardKeys keys = createKey(userSession, request, userIdentity);

        ServletUtils.sendObject(response, new GuardKeyInfoResponse(keys, new String(userIdentity.getPassword())));
    }

}
