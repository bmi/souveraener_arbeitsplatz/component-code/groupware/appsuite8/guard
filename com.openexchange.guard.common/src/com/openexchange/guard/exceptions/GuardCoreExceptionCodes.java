/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 *
 * Exception codes for guard core purposes.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public enum GuardCoreExceptionCodes implements DisplayableOXExceptionCode {

    SQL_ERROR("An SQL error occurred: %1$", GuardCoreExceptionMessages.SQL_ERROR_MSG, CATEGORY_ERROR, 1),

    PARAMETER_MISSING("The parameter \"%1$s\" is missing.", GuardCoreExceptionMessages.PARAMETER_MISSING_MSG, Category.CATEGORY_ERROR, 2),

    JSON_PARAMETER_MISSING("The parameter \"%1$s\" is missing in provided JSON.", GuardCoreExceptionMessages.JSON_PARAMETER_MISSING_MSG, Category.CATEGORY_ERROR, 3),

    CONFIGURATION_ERROR("\"%1$s\" not configured correctly: \"%2$s\"", GuardCoreExceptionMessages.CONFIGURATION_ERROR_MSG, Category.CATEGORY_ERROR, 4),

    /** An error occurred: %1$s */
    UNEXPECTED_ERROR("An error occurred: %1$s", GuardCoreExceptionMessages.UNEXPECTED_ERROR_MSG, Category.CATEGORY_ERROR, 5),

    /** An I/O error occurred: %1$s */
    IO_ERROR("An I/O error occurred: %1$s", GuardCoreExceptionMessages.IO_ERROR_MSG, Category.CATEGORY_ERROR, 6),

    KEY_NOT_FOUND_FOR_MAIL_ERROR("Could not find key for email \"%1$s\" in context %2$d", GuardCoreExceptionMessages.KEY_NOT_FOUND_FOR_MAIL_ERROR_MSG, Category.CATEGORY_ERROR, 7),

    KEY_NOT_FOUND_FOR_IDS_ERROR("Could not find key for user with id \"%1$d\" in context %2$d", GuardCoreExceptionMessages.KEY_NOT_FOUND_FOR_IDS_ERROR_MSG, Category.CATEGORY_ERROR, 8),

    KEY_NOT_FOUND_FOR_KEY_ID_ERROR("Could not find key for key id \"%1$s\"", GuardCoreExceptionMessages.KEY_NOT_FOUND_FOR_KEY_ID_ERROR_MSG, Category.CATEGORY_ERROR, 9),

    JSON_ERROR("Error while processing provided JSON: %1$s.", GuardCoreExceptionMessages.JSON_ERROR_MSG, Category.CATEGORY_ERROR, 10),

    DISABLED_ERROR("Failed to %1$s because this is disabled per configuration.", GuardCoreExceptionMessages.DISABLED_ERROR_MSG, CATEGORY_ERROR, 11),

    KEY_NOT_FOUND("The requested key was not found", GuardCoreExceptionMessages.KEY_NOT_FOUND_MSG, CATEGORY_ERROR, 12),

    NOT_A_GUEST_ACCOUNT("The requested account is not a valid guest account", GuardCoreExceptionMessages.NOT_A_GUEST_ACCOUNT_MSG, CATEGORY_ERROR, 13),

    PARAMETER_MISMATCH("The parameters \"%1$s\" and \"%2$s\" cannot be specified at the same time", GuardCoreExceptionMessages.PARAMETER_MISMATCH_MSG, CATEGORY_ERROR,14),

    INVALID_PARAMETER_VALUE("The parameter \"%1$s\" does not contain a valid value", GuardCoreExceptionMessages.INVALID_PARAMETER_VALUE_MSG, CATEGORY_ERROR,15),

    MULTIPART_UPLOAD_MISSING("Missing multipart/form-data", GuardCoreExceptionMessages.MULTIPART_UPLOAD_MISSING_MSG, CATEGORY_ERROR, 16),

    MULTIPART_UPLOAD_ERROR("Error parsing multipart/form-data: %1$s", GuardCoreExceptionMessages.MULTIPART_UPLOAD_ERROR_MSG, CATEGORY_ERROR, 17),

    KEY_NOT_FOUND_PRIVATE_MASTER("Could not find the private master key of the specified key ring.", GuardCoreExceptionMessages.KEY_NOT_FOUND_PRIVATE_MASTER_MSG, CATEGORY_ERROR, 18),

    KEY_NOT_FOUND_FOR_MAIL_ONLY_ERROR("Could not find key for email \"%1$s\"", GuardCoreExceptionMessages.KEY_NOT_FOUND_FOR_MAIL_ONLY_ERROR_MSG, Category.CATEGORY_ERROR, 19),

    UNKOWN_SIGNATURE_TYPE("Unknown signature type", GuardCoreExceptionMessages.UNKOWN_SIGNATURE_TYPE_MSG, Category.CATEGORY_ERROR, 20);

    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    /**
     * Default constructor.
     *
     * @param message message.
     * @param category category.
     * @param number detail number.
     */
    private GuardCoreExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private GuardCoreExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-CORE";
    }

    /**
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    /**
     * @return the category
     */
    @Override
    public Category getCategory() {
        return category;
    }

    /**
     * @return the number
     */
    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
