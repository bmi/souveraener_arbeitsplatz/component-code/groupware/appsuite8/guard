/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_KEYID;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_KEY_TYPE;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.servlets.responses.BinaryResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link DownloadKeyAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class DownloadKeyAction extends GetKeyAction {

    /**
     * Exporting the private key in a brute force protected way
     *
     * @param userSession The user's Guard session
     * @param request The request
     * @param keys The user's Guard key
     * @return The exported key
     * @throws Exception
     */
    private InputStream exportPrivateKey(GuardUserSession userSession, HttpServletRequest request, final GuardKeys keys)  throws Exception {
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());
        final AntiAbuseWrapper antiAbuseWrapper = getAntiAbuseWrapper(keys, AntiAbuseUtils.getAllowParameters(request, userSession.getUserId(),  userSession.getContextId(), new String(userIdentity.getPassword())));
        return antiAbuseWrapper.doAction(new AntiAbuseAction<InputStream>() {

            @Override
            public InputStream doAction() throws Exception {
                return new ByteArrayInputStream(
                    KeyExportUtil.exportPGPPrivateKey(keys, new String(userIdentity.getPassword())).getBytes(StandardCharsets.UTF_8));
            }
        });
    }

    /**
     * Exporting the public and private key in a brute force protected way
     *
     * @param userSession The userB's Guard session
     * @param request THe request
     * @param keys The user's Guard key
     * @return The exported keys
     * @throws OXException
     */
    private InputStream exportPublicAndPrivateKey(GuardUserSession userSession, HttpServletRequest request, final GuardKeys keys) throws Exception {
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());
        final AntiAbuseWrapper antiAbuseWrapper = getAntiAbuseWrapper(keys, AntiAbuseUtils.getAllowParameters(request, userSession.getUserId(),  userSession.getContextId(), new String(userIdentity.getPassword())));
        return antiAbuseWrapper.doAction(new AntiAbuseAction<InputStream>() {

            @Override
            public InputStream doAction() throws Exception {
                return new ByteArrayInputStream(
                    KeyExportUtil.exportPGPPublicAndPrivateKey(keys, new String(userIdentity.getPassword())).getBytes(StandardCharsets.UTF_8));
            }
        });
    }

    private InputStream exportPublicKey(GuardKeys keys) throws OXException, UnsupportedEncodingException {
        return new ByteArrayInputStream(
            KeyExportUtil.export(keys.getPGPPublicKeyRing()).getBytes(StandardCharsets.UTF_8));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        if (userSession == null) {
            throw GuardAuthExceptionCodes.MISSING_SESSION.create();
        }
        final Long keyId = ServletUtils.getLongParameter(request, PARAM_KEYID);
        if (keyId == null && ServletUtils.hasParameter(request, PARAM_KEYID)) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create(PARAM_KEYID);
        }

        //Getting the requested key
        final GuardKeys keys = getKey(userSession, keyId);
        if (keys != null) {

            //The key type defines what parts of the key will be served to the client
            final KeyType mode = getKeyType(parseKeyType(ServletUtils.getStringParameter(request, PARAM_KEY_TYPE)), keys);

            //Serve the key content as raw download
            final boolean serveAsDownload = true;
            if (mode == KeyType.PUBLIC) {
                final  String fileName="public.asc";
                new BinaryResponse(response).send(exportPublicKey(keys), serveAsDownload, fileName);
            } else if (mode == KeyType.PUBLIC_PRIVATE) {
                final  String fileName="key.asc";
                new BinaryResponse(response).send(exportPublicAndPrivateKey(userSession,request, keys), serveAsDownload, fileName);
            } else if (mode == KeyType.PRIVATE) {
                final  String fileName="private.asc";
                new BinaryResponse(response).send(exportPrivateKey(userSession, request, keys), serveAsDownload, fileName);
            }
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }
}
