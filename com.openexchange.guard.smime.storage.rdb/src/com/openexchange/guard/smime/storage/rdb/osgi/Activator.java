/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.osgi;

import org.slf4j.Logger;
import com.openexchange.config.ConfigurationService;
import com.openexchange.database.CreateTableService;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.smime.SmimeProperty;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.guard.smime.storage.SmimePrivateKeyStorage;
import com.openexchange.guard.smime.storage.SmimePublicKeyStorage;
import com.openexchange.guard.smime.storage.rdb.impl.RdbCaCertStorage;
import com.openexchange.guard.smime.storage.rdb.impl.RdbSmimePrivateKeyStorage;
import com.openexchange.guard.smime.storage.rdb.impl.RdbSmimePublicKeyStorage;
import com.openexchange.guard.smime.storage.rdb.update.CreateSmimePrivateService;
import com.openexchange.guard.smime.storage.rdb.update.CreateSmimePrivateTask;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link Activator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class Activator extends HousekeepingActivator {


    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(Activator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class, GuardDatabaseService.class, MasterKeyService.class, KeyRecoveryService.class };
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting SMIME database storage service");
        ConfigurationService configService = this.getService(ConfigurationService.class);
        if (configService.getBoolProperty(SmimeProperty.databaseStorage.getFQPropertyName(), true)) {
            registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new CreateSmimePrivateTask()));
            registerService(CreateTableService.class, new CreateSmimePrivateService());
            final RdbSmimePublicKeyStorage publicKeyStorage = new RdbSmimePublicKeyStorage(this);
            registerService(SmimePublicKeyStorage.class, publicKeyStorage, 999);
            registerService(SmimePrivateKeyStorage.class, new RdbSmimePrivateKeyStorage(this, publicKeyStorage));
        } else {
            LOG.info("SMIME database key storage disabled by configuration");
        }
        if (configService.getBoolProperty(SmimeProperty.caDatabase.getFQPropertyName(), true)) {
            registerService(CaCertStorage.class, new RdbCaCertStorage(this), 100);
        } else {
            LOG.info("SMIME Certificate Authority database storage disabled by configuration");
        }
        trackService(GuardConfigurationService.class);
        openTrackers();
    }
}
