/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionCodes;
import com.openexchange.guard.oxapi.osgi.Services;

/**
 * {@link AbstractOXCalls}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class AbstractOXCalls {

    protected String getMainURI() throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        boolean ssl = guardConfigurationService.getBooleanProperty(GuardProperty.backendSSL);
        String host = guardConfigurationService.getProperty(GuardProperty.restApiHostname);
        int port = guardConfigurationService.getIntProperty(GuardProperty.oxBackendPort);

        return "http" + (ssl ? "s" : "") + "://" + host + ":" + port;
    }

    protected Header getBasicAuthenticationForCredentials(String userName, String password) throws OXException {
        try {
            String userPassword = userName + ":" + password;
            return new BasicHeader("Authorization", "Basic " + new String(Base64.encodeBase64(userPassword.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw OXApiExceptionCodes.BASIC_PWD_ENCODING_ERROR.create(e);
        }
    }

    protected Header getAuthenticationHeader() throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        return getBasicAuthenticationForCredentials(guardConfigurationService.getProperty(GuardProperty.restApiUsername),
            guardConfigurationService.getProperty(GuardProperty.restApiPassword));
    }
}
