/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage;

import java.io.IOException;
import java.io.InputStream;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.dataobject.EncryptedObject;

/**
 * Interface handler for the file storage.
 *
 * @author benjamin.otterbach@open-xchange.com
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
/*
 * TODO: Switch to OXException once we refactored the exception handling from the
 * invocations from this implementations
 */
public interface Storage {

    /**
     * The delimiter character to separate the prefix from the keys
     */
    public static final String DELIMITER = "/";

    /**
     * Read encrypted object from file
     *
     * @param ObjId Unique object id
     * @return An InputStream for the given object ID, or null if no such object was found in the storage.
     * @throws OXException
     */
    InputStream readObj(String objectId) throws OXException;

    /**
     * Checks whether or not an object exits
     *
     * @param objectId Unique object id
     * @return True, if the object exists, false otherwise
     * @throws OXException
     */
    boolean objectExists(String objectId) throws OXException;

    /**
     * Write encrypted object to file
     *
     * @param userid User Id
     * @param contextid Users context id
     * @param ObjId Unique object id
     * @param data byte array of data to write
     * @return
     * @throws OXException
     */
    Boolean saveEncrObj(int userId, int contextId, String objectId, byte[] data) throws OXException;

    boolean copy(int userId, int contextId, String sourceObjectId, String destinationObjectId) throws OXException;

    void deleteEncrObj(String location) throws OXException;

    /**
     * Get directory prefix for the user account
     *
     * @param userid
     * @param contextid
     * @param shardid
     * @return
     */
    String getDirectoryPrefix(int userId, int contextId, int shardId) throws OXException;

    InputStream getObjectStream(String location) throws OXException;

    InputStream getObjectStream(String directoryPrefix, String ObjId) throws OXException;

    /**
     * Delete a file from the cache store. Usually used by the Cache Cleaner.
     *
     * @param location File location including directory prefix. Example: ext_1_25/0ff183ed-097c-4a5a-b534-0198659c775a
     * @throws IOException
     * @throws OXException if the GuardConfigurationService is absent
     */
    void deleteObj(String location) throws OXException;

    /**
     * Get an object from the cache store.
     *
     * @param directoryPrefix Sub-directory of the files storage location, for example: ext_1_25
     * @param ObjId ID of the file
     * @return EncryptedObj
     * @throws OXException
     */
    EncryptedObject getEncrObj(String directoryPrefix, String ObjId) throws OXException;

    /**
     * Write byte array to filename
     *
     * @param filename Full filename of object
     * @param data byte array of data
     * @return
     * @throws IOException
     * @throws OXException if the GuardConfigurationService is absent
     */
    String writeObj(String directoryPrefix, String ObjId, byte[] data) throws OXException;

    /**
     * This method returns the shard ID contained in the given directoryPrefix
     *
     * @param directoryPrefix The directory Prefix
     * @return The shard ID from within the prefix, or null if the given string does not contain a guest shard ID
     */
    public Integer getGuestShardIdFromPrefix(String directoryPrefix);


}
