/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.certificatemanagement.responses;

import java.util.Objects;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.common.util.IDNUtil;

/**
 * {@link CertificateResponse} - Wraps a {@link SmimeKeys} for a response
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class CertificateResponse {

    private SmimeKeys key;

    /**
     * Initializes a new {@link CertificateResponse}.
     *
     * @param key The {@link SmimeKeys} to wrap as response
     */
    public CertificateResponse(SmimeKeys key) {
        this.key = Objects.requireNonNull(key, "key must not be null");
    }

    /**
     * Return the serial number of the certificate
     *
     * @return The unique serial number
     */
    public String getSerial() {
        return key.getSerial().toString();
    }

    /**
     * Gets the current flag of the internal {@link SmimePrivateKeys} instance
     *
     * @return The current flag of the internal {@link SmimePrivateKeys} instance, or <code>false</code> if no private key is present.
     */
    public boolean isCurrent() {
        return key.getPrivateKey().isCurrent();
    }

    /**
     * Return the email address associated with the owner of the public certificate
     *
     * @return the email associated with the certificate owner
     */
    public String getEmail() {
        return IDNUtil.decodeEmail(key.getEmail());
    }

    /**
     * Gets the certificate's date of expiration
     *
     * @return The date of expiration
     */
    public long getExpires() {
        return key.getExpires().getTime();
    }

    /**
     * Gets if the certificate is expired or not
     *
     * @return <code>True</code> if the certificate is expired, <code>False</code>otherwise
     */
    public boolean getExpired() {
        return key.isExpired();
    }

    /**
     * Gets the name of certificate's issuer
     *
     * @return The issuer name of the certificate
     */
    public String getCertifier() {
        return key.getCertifier();
    }
}
