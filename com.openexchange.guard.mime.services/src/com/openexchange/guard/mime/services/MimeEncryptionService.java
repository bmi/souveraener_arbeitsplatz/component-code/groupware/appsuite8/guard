/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.services;

import java.io.OutputStream;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link MimeEncryptionService} Encrypting and Decrypting of mime message service
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public interface MimeEncryptionService {

    /**
     * Returns the {@link CryptoType} associated with the implementation of this this interface
     *
     * @return The related {@link CryptoType}
     */
    CryptoType.PROTOCOL getCryptoType();

    /**
     * Encrypts a given {@link GuardParsedMimeMessage}
     *
     * @param mimeMessage The {@link GuardParsedMimeMessage} to be encrypted
     * @param userId The user Id
     * @param contextId The user context Id
     * @param password Password to use for signing if applicable, or <code>null</code> if the email should not be signed
     * @param output output stream for the encrypted data
     * @throws OXException
     */
    public void doEncryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, String password, OutputStream output) throws OXException;

    /**
     * Decrypt a message
     *
     * @param mimeMessage {@link GuardParsedMimeMessage} containing the encrypted data
     * @param userId The user Id
     * @param contextId The user context id
     * @param userIdentity Guard UserIdentity
     * @param output outputstream for decrypted data
     * @throws OXException
     */
    public void doDecryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, UserIdentity userIdentity, OutputStream output) throws OXException;

    /**
     * Decrypt a message, returning a {@link MimeMessage}
     *
     * @param mimeMessage {@link MimeMessage} to decrypt
     * @param userIdentity UserIdentity containing passsword, user info
     * @return Return {@link MimeMessage} with decrypted data
     * @throws OXException
     */
    public MimeMessage doDecryption(MimeMessage mimeMessage, UserIdentity userIdentity) throws OXException;

    /**
     * Decrypt attachement
     *
     * @param mimeMessage MimeMessage to decrypt
     * @param attachmentName Attachmetn name
     * @param userIdentity UserIdentity
     * @return MimeMessage Part
     * @throws OXException
     */
    public Part doDecryption(MimeMessage mimeMessage, String attachmentName, UserIdentity userIdentity) throws OXException;
}
