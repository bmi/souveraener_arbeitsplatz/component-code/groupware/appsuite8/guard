/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.pgp.mail.PGPMimeService;
import net.htmlparser.jericho.Source;

public class SignMime {

    private static Logger LOG = LoggerFactory.getLogger(SignMime.class);
    private final PGPMimeService pgpMimeService;
    private final PGPSigningService pgpSigningService;

    /**
     * Initializes a new {@link SignMime}.
     *
     * @param pgpMimeService The service used for signing
     */
    public SignMime(PGPMimeService pgpMimeService, PGPSigningService signingService) {
        super();
        this.pgpMimeService = pgpMimeService;
        this.pgpSigningService = signingService;
    }

    private MimeMessage sign(MimeMessage message, PGPSecretKey signingKey, String password, String salt) throws Exception {
        return pgpMimeService.sign(message, signingKey, PGPUtil.saltPassword(password, salt).toCharArray());
    }

    /**
     * Takes a string mime message and returns signed with new attachment
     *
     * @param mimemessage
     * @param key
     * @param password
     * @return
     * @throws Exception
     */
    public String signMime(GuardParsedMimeMessage msg, GuardKeys key, String password) throws Exception {
        MimeMessage message = msg.getMessage();
        message.removeHeader("X-OX-GUARD-AUTH"); //Remove header from previous Guard versions
        message.removeHeader("X-OX-Guard-Marker");
        message.removeHeader("X-Oxguard-PGPInline");

        if (password == null) {
            LOG.info("Bad password for signing");
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
        if (msg.isInline()) {
            SignMime sm = new SignMime(pgpMimeService, pgpSigningService);
            String signed = sm.SignInline(message, key, password);
            if (signed != null) {
                return signed;
            }
            return getMessageContent(message);
        }

        final PGPSecretKey secretSigningKey = PGPKeysUtil.getSigningKey(key.getPGPSecretKeyRing());
        return getMessageContent(sign(msg.getMessage(),secretSigningKey,password,key.getSalt()));
    }

    /**
     * Decode RSA encrypted passwords sent from UI
     *
     * @param epass
     * @return
     * @throws OXException
     */
    public String decodeRSApassword(String epass, int userId, int cid) throws OXException {
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.RSA);
        MasterKeyService masterKeyService = Services.getService(MasterKeyService.class);
        return cipherService.decrypt(epass, masterKeyService.getMasterKey(userId, cid).getClientEncodedPrivate());
    }

    /**
     * Sign a message inline format, should be plaintext
     *
     * @param mime
     * @param key
     * @param password
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    private String SignInline(MimeMessage mime, GuardKeys key, String password) throws IOException, MessagingException {
        Object content = mime.getContent();
        if (content instanceof Multipart || content instanceof MimeMultipart) {
            // This is experimental and not working well.  HTML failing, and known limitation of Inline
            MimeMultipart newcontent = signParts((MimeMultipart) content, key, password);
            mime.setContent(newcontent);
            mime.saveChanges();
            String data = getMessageContent(mime);
            return data;
        }
        // Sign plaintext email
        if (content instanceof String) {
            String msg = (String) content;
            String signed = signMessage(msg, key, password, mime.getContentType() == null ? false : mime.getContentType().contains("html"));
            mime.setContent(signed, mime.getContentType());
            mime.saveChanges();
            String data = getMessageContent(mime);
            return data;
        }
        return null;
    }

    /**
     * Expiremental. Not yet working or supported. Signing multipart alternative. HTML failing
     *
     * @param mp
     * @param key
     * @param password
     * @return
     * @throws MessagingException
     * @throws IOException
     */
    private MimeMultipart signParts(MimeMultipart mp, GuardKeys key, String password) throws MessagingException, IOException {
        int count = mp.getCount();
        String mtype = mp.getContentType();
        if (mtype != null) {
            String[] typeData = mtype.split(";");
            mtype = typeData[0];
            if (mtype.contains("/")) {
                mtype = mtype.substring(mtype.indexOf("/") + 1);
            }
        } else {
            mtype = "alternative";
        }
        MimeMultipart newMpart = new MimeMultipart(mtype);
        for (int i = 0; i < count; i++) {
            MimeBodyPart bp = (MimeBodyPart) mp.getBodyPart(i);
            Object content = bp.getContent();
            if (content instanceof Multipart || content instanceof MimeMultipart) {
                content = signParts((MimeMultipart) content, key, password);
            }
            if (content instanceof String) {
                if (bp.getContentType().contains("text")) {
                    String type = bp.getContentType();
                    boolean html = type.contains("html");
                    String[] ctypes = type.split(";");
                    String encoding = "UTF-8";
                    for (String ctype : ctypes) {
                        if (ctype.contains("charset=")) {
                            int cindex = ctype.indexOf("=");
                            encoding = ctype.substring(cindex + 1);
                        }
                    }
                    bp.setText(signMessage((String) content, key, password, html), encoding);
                    bp.setHeader("Content-Type", type);
                }

            }
            newMpart.addBodyPart(bp);
        }
        return (newMpart);
    }

    // Sign the message and return wrapped text
    private String signMessage(String message, GuardKeys key, String password, boolean html) {
        try {
            if (html) {   // Must convert the html emails to plaintext for PGP Inline signatures
                StringBuilder htmlSb = new StringBuilder();
                htmlSb.append("<pre>\r\n");
                htmlSb.append(getPlainText(message));
                htmlSb.append("</pre>");
                message = htmlSb.toString();
            }
            message = normalize(message);
            StringBuilder sig = new StringBuilder();
            sig.append("-----BEGIN PGP SIGNED MESSAGE-----\r\nHash: SHA256\r\n\r\n");
            sig.append(message);
            sig.append("\r\n");
            String signature = signString(message, key, password);
            sig.append(signature);
            return (sig.toString());
        } catch (Exception ex) {
            LOG.error("Error signing inline message", ex);
            return (message);
        }
    }

    private String signString(String message, GuardKeys key, String password) throws IOException, OXException {
        ByteArrayInputStream in = new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        final boolean armored = true;
        pgpSigningService.sign(in, out, armored, key, password.toCharArray());
        String signature = new String(out.toByteArray(), StandardCharsets.UTF_8);
        out.close();
        in.close();
        return (signature);
    }

    /**
     * Remove any whitespaces and normalize with \r\n after each line
     *
     * @param content
     * @return
     */
    public static String normalize(String content) {
        StringBuilder sb = new StringBuilder();
        String[] lines = content.split("\n");
        for (String line : lines) {
            sb.append(StringUtils.stripEnd(line, null) + "\r\n");
        }
        return (sb.toString());
    }

    private String getMessageContent(MimeMessage message) throws IOException, MessagingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        message.writeTo(out);
        out.close();
        return (out.toString("UTF-8"));
    }

    private String getPlainText(String html) {
        try {
            StringReader sr = new StringReader(html);
            Source src = new Source(sr);
            return (src.getRenderer().toString());
        } catch (Exception e) {
            LOG.error("Error converting HTML to plaintext ", e);
            return (html);
        }
    }
}
