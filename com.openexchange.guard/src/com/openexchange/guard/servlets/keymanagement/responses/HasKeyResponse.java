/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

/**
 * {@link HasKeyResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class HasKeyResponse {

    private final boolean hasPrivateKey;
    private final boolean hasPublicKey;

    /**
     * Initializes a new {@link HasKeyResponse}.
     *
     * @param hasPrivateKey Whether a private key is available or not
     * @param hasPublicKey Whether a public key is available or not
     */
    public HasKeyResponse(boolean hasPrivateKey, boolean hasPublicKey) {
        this.hasPublicKey = hasPublicKey;
        this.hasPrivateKey = hasPrivateKey;
    }

    public boolean getHasPrivateKey() {
        return this.hasPrivateKey;
    }

    /**
     * Gets the isPublicKeyAvailable
     *
     * @return The isPublicKeyAvailable
     */
    public boolean getHasPublicKey() {
        return hasPublicKey;
    }
}
