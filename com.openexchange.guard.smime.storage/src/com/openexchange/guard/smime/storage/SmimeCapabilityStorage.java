/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.smime.storage;

import java.security.cert.X509Certificate;
import org.bouncycastle.asn1.smime.SMIMECapabilities;

/**
 * {@link SmimeCapabilityStorage} - Stores S/MIME capabilities
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface SmimeCapabilityStorage {

    /**
     * Persists {@link SMIMECapabilities} for the given recipient
     *
     * @param recipient The recipient to persist the {@link SMIMECapabilities} for
     * @param cpabilities The {@link SMIMECapabilities} to persist
     */
    public void insert(X509Certificate recipient, SMIMECapabilities cpabilities);

    /**
     * Deletes all {@link SMIMECapabilities} for a recipient
     *
     * @param recipient The reicipient to delete the {@link SMIMECapabilities} for
     */
    public void delete(X509Certificate recipient);

    /**
     * Gets {@link SMIMECapabilities} for the given recipient
     *
     * @param recipient The recipient to get the {@link SMIMECapabilities} for
     * @return The {@link SMIMECapabilities} for the given recipient, or null if no {@link SMIMECapabilities} could be found
     */
    public SMIMECapabilities get(X509Certificate recipient);
}
