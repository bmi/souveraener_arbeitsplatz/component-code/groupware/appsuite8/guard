/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;


/**
 * {@link Parameters} provides common constants for the Key Management API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class Parameters {
    public static final String PARAM_KEYID = "keyid";
    public static final String PARAM_KEYIDS = "keyids";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_CONTACT_FOLDER = "contactFolder";
    public static final String PARAM_CONTACT_ID = "contactId";
    public static final String PARAM_DOWNLOAD = "download";
    public static final String PARAM_CREATE_IF_MISSING = "create";
    public static final String PARAM_KEY_TYPE = "keyType";
    public static final String PARAM_SHARE = "share";
    public static final String PARAM_INLINE = "inline";
    public static final String PARAM_REASON = "reason";
    public static final String PARAM_SUBKEYS = "subkeys";
    public static final String PARAM_VERIFIED = "verified";
    public static final String PARAM_AUTOCRYPT = "autocrypt";
}
