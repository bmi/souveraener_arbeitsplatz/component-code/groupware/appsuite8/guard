/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.ArrayList;
import java.util.Arrays;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;

/**
 * {@link ExternalPublicKeyRingCollectionResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class ExternalPublicKeyRingCollectionResponse {

    private final Iterable<OGPGPKeyRing> publicKeyRings;

    /**
     * Initializes a new {@link ExternalPublicKeyRingCollectionResponse}.
     * @param publicKeyRings
     */
    public ExternalPublicKeyRingCollectionResponse(OGPGPKeyRing...publicKeyRings) {
        this(Arrays.asList(publicKeyRings));
    }

    /**
     * Initializes a new {@link ExternalPublicKeyRingCollectionResponse}.
     * @param publicKeyRings
     */
    public ExternalPublicKeyRingCollectionResponse(Iterable<OGPGPKeyRing> publicKeyRings) {
        this.publicKeyRings = publicKeyRings;
    }

    public ExternalPublicKeyInfoResponse[] getExternalPublicKeyRings() {
        ArrayList<ExternalPublicKeyInfoResponse> ret = new ArrayList<ExternalPublicKeyInfoResponse>();
        for (OGPGPKeyRing ring : publicKeyRings) {
            ret.add(new ExternalPublicKeyInfoResponse(ring));
        }
        return ret.toArray(new ExternalPublicKeyInfoResponse[ret.size()]);
    }
}
