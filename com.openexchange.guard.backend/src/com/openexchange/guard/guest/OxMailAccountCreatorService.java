/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

import com.openexchange.exception.OXException;
import com.openexchange.session.Session;
import com.openexchange.share.GuestInfo;

/**
 * {@link OxMailAccountCreatorService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public interface OxMailAccountCreatorService {

    /**
     * Creates a user mail account entry for a Guard Guest Mail account
     * Adds as default if no accounts exist, otherwise adds as extra account
     * @param guest
     * @param session
     * @throws OXException
     */
    public void createAccount(GuestInfo guest, Session session) throws OXException;

    /**
     * Adds required mail settings to Guest account if none exist
     * @param guest
     * @throws OXException
     */
    public void addMailSettings(GuestInfo guest) throws OXException;

}
