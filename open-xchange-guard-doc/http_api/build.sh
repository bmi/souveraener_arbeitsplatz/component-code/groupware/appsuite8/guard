#!/bin/bash
# Simple script to build swagger documentation from yaml files
# Publish to local system /var/www/html/doc

node resolve.js http_api/
bootprint openapi http_api/swagger.json /var/www/html/doc
if ! type "swagger-tools" > /dev/null; then
      echo "Error validating swagger file. Swagger-tools not found.";
      echo "Please install swagger-tools (sudo npm install -g swagger-tools)";
else
    swagger-tools validate http_api/swagger.json
fi
