/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.EnumSet;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.composition.DelegatingIDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardAwareIDBasedFileAccessFactoryImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class GuardAwareIDBasedFileAccessFactoryImpl extends AbstractGuardAccess implements CryptographicAwareIDBasedFileAccessFactory {

    private final EncryptedFileRecognizer[] encryptedFileRecognizers = new EncryptedFileRecognizer[] { new MetadataFileRecognizer(), new NameBasedEncryptedFileRecognizer(), new ContentTypeFileRecognizer() };
    private final CapabilityService capabilityService;
    private final static String GUARD_DRIVE_CAP = "guard-drive";
    private final static String GUARD = "guard";

    /**
     * Initializes a new {@link GuardAwareIDBasedFileAccessFactoryImpl}.
     *
     * @param capabilityService The {@link CapabilityService}
     */
    public GuardAwareIDBasedFileAccessFactoryImpl(CapabilityService capabilityService) {
        this.capabilityService = capabilityService;
    }

    /**
     * Internal method to construct an {@link GuardAuthenticationToken} from a given string.
     *
     * @param authentication The string representation of the token.
     * @return The {@link GuardAuthenticationToken} created from the given string
     * @throws OXException
     */
    private GuardAuthenticationToken createToken(String authentication, Session session) throws OXException {
        try {
            AuthenticationTokenHandler handler = new AuthenticationTokenHandler();
            GuardAuthenticationToken sessionToken = handler.getForSession(session, CryptoType.PROTOCOL.PGP.getValue());
            if (sessionToken != null && ! sessionToken.isUsed()) return sessionToken;
            if(authentication != null) {
                GuardAuthenticationToken token = GuardAuthenticationToken.fromString(authentication);
                if (token != null) {
                    return token;
                } else {
                    throw GuardExceptionCodes.MISSING_AUTH.create();
                }
            } else {
                //Authentication is not required for all crypto modes
                return null;
            }
        } catch (Exception e) {
            throw GuardExceptionCodes.MISSING_AUTH.create();
        }
    }

    /**
     * Checks if the given session has guard-drive capability set.
     *
     * @param session The session
     * @return True if the session has the guard-drive capability, false otherwise
     * @throws OXException
     */
    private boolean hasGuardDriveCapabilities(Session session) throws OXException {
        CapabilitySet capabilities = this.capabilityService.getCapabilities(session);
        return capabilities.contains(GUARD_DRIVE_CAP);
    }

    private boolean hasMinDriveCapabilities(Session session) throws OXException {
        CapabilitySet capabilities = this.capabilityService.getCapabilities(session);
        return capabilities.contains(GUARD_DRIVE_CAP) || capabilities.contains(GUARD);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.api.filestorage.GuardAwareIDBasedFileAccessFactory#createAccess(com.openexchange.file.storage.composition.IDBasedFileAccess, com.openexchange.session.Session,
     * com.openexchange.guard.api.authentication.GuardAuthenticationToken)
     */
    @Override
    public IDBasedFileAccess createAccess(IDBasedFileAccess fileAccess, EnumSet<CryptographyMode> modes, Session session, String authentication) throws OXException {
        if(hasMinDriveCapabilities(session)) {
            return new GuardAwareIDBasedFileAccess(fileAccess,
                modes,
                requireGuardApi(PGPCORE_ENDPOINT),
                session,
                new CompositeEncryptedFileRecognizer(encryptedFileRecognizers),
                createToken(authentication, session),
                !hasGuardDriveCapabilities(session));
        } else {
            throw OXException.noPermissionForModule(GUARD_DRIVE_CAP);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.composition.DelegatingIDBasedFileAccessFactory#createAccess(com.openexchange.file.storage.composition.IDBasedFileAccess, com.openexchange.session.Session)
     */
    @Override
    public DelegatingIDBasedFileAccess createAccess(IDBasedFileAccess fileAccess, EnumSet<CryptographyMode> modes, Session session) throws OXException {
        if(hasMinDriveCapabilities(session)) {
            return new GuardAwareIDBasedFileAccess(fileAccess,
                modes,
                requireGuardApi(PGPCORE_ENDPOINT),
                session,
                new CompositeEncryptedFileRecognizer(encryptedFileRecognizers),
                !hasGuardDriveCapabilities(session));
        } else {
            throw OXException.noPermissionForModule(GUARD_DRIVE_CAP);
        }
    }
}
