/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import com.openexchange.exception.OXException;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link TokenAuthenticationService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public interface TokenAuthenticationService {

    /**
     * Creates an authentication token which can be used in further requests
     *
     * @param sessionIdentifier A unique client session identifier not known to other clients
     * @param identity The user's identity
     * @param password The user's key password
     * @param type Type of crypto (SMIME, PGP)
     * @return An authentication token if the authentication was successful, or null otherwise
     * @throws OXException
     */
    String createAuthenticationToken(String sessionIdentifier, String userIdentity, char[] password, String type) throws OXException;

    /**
     * Deletes an authentication token
     *
     * @param sessionIdentifier A unique client session related to the token
     * @throws OXException
     */
    void deleteAuthenticationToken(String sessionIdentifier) throws OXException;

    /**
     * Decrypts the UserIdentity from the given authToken
     *
     * @param sessionIdentifier A unique client session related to the token
     * @param authToken The token to decrypt
     * @return the UserIdentity for the given authToken
     * @throws OXException
     */
    UserIdentity decryptUserIdentity(String sessionIdentifier, String authToken) throws OXException;
}
