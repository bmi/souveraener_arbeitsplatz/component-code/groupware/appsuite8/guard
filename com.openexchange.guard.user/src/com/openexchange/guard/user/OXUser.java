/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user;

import java.util.Locale;
import com.openexchange.guard.common.util.LocaleUtil;

/**
 * {@link OXUser} represents a regular OX Appsuite User
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class OXUser {

    private final String  email;
    private final String  language;
    private final String  name;
    private final boolean isGuest;
    private int     id;
    private int     contextId;

    /**
     *
     * Initializes a new {@link OXUser}.
     *
     * @param email The user's email
     * @param id The user's ID
     * @param contextId The users' context ID
     * @param name The name of the user
     * @param isGuest True if the user is a OX Appsuite guest, False otherwise
     * @param language The language of the user
     */
    public OXUser(String email, int id, int contextId, String name, boolean isGuest, String language) {
        this.email = email;
        this.id = id;
        this.contextId = contextId;
        this.name = name;
        this.isGuest = isGuest;
        this.language = language;
    }

    /**
     * Gets the email of the user
     *
     * @return The user's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets the context ID of the user
     *
     * @return The context ID of the user
     */
    public int getContextId() {
        return contextId;
    }

    public void setContextId(int contextId) {
        this.contextId = contextId;
    }


    public int getId() {
        return id;
    }

    public void setUserId(int userId) {
        this.id = userId;
    }

    /**
     * Gets the user's language
     *
     * @return The language of the user
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Gets the user's locale
     *
     * @return The locale
     */
    public Locale getLocale() {
        return LocaleUtil.getLocalFor(language);
    }

    /**
     * Gets the user's name
     *
     * @return The name of the user
     */
    public String getName() {
        return name;
    }

    /**
     * Gets whether or not the user is an OX Appsuite guest
     * @return True if the user is an OX Appsuite guest, false otherwise
     */
    public boolean isGuest() {
        return this.isGuest;
    }
}
