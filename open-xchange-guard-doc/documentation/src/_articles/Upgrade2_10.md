---
title: Upgrading to 2.10
description: Changes in OX Guard 2.10 and how to upgrade from a previous version (2.6/2.8).
---

                 

# Introduction

With __OX Guard 2.10.0__ the experience for external recipients has changed significantly. 
Pre 2.10, there was a guest reader HTML package that allowed recipients to decrypt and reply to PGP emails. 
This has now changed. External recipients now have an Appsuite Guest account created, similar to a file share, with an pseudo 
inbox that lists all of the encrypted emails sent to them. 
The recipient will still be able to reply to emails sent to them, but will not be able to create new emails.

Bringing external users into Appsuite presents additional upsell opportunity as well as advertising options. 

# Changes

This section covers the changes introduced with OX Guard 2.10.0. 

## Guest Users

All Guest users must have guard capability in order to read encrypted emails. 
To do this, the following configuration file */opt/open-xchange/etc/share.properties* needs to be modified.

Assuming the *guestCapabilityMode* is set to *static*, please add guard to the *staticGuestCapabilities*

```
com.openexchange.share.guestCapabilityMode=static
com.openexchange.share.staticGuestCapabilities=guard
```

## WebKey Service

Optional WebKey server was added in 2.10. This allows external users to look up the public PGP keys of Guard users as described here. 
To enable this ability, an additional ProxyPass needs to be added to the *proxy_http.conf* file

```
ProxyPass /.well-known/openpgpkey/hu balancer://oxguard/hu
```

## Reader Package

The package *com.openexchange.guard.reader* is no longer required, but recommended for those upgrading from an earlier version. 
The package no longer contains a full guest reader, rather it redirects old Guard guest share links to the new appsuite guest. 
This package will be required for as long as old guest emails should remain functional. 

## Configuration Changes

###Guest Configuration

Encrypted emails sent to external recipients was previously cached for a period of time, defaulting to 90 days. 
After this time, the reader would not function unless the user uploaded the attachment sent with their emails.

This has changed in 2.10. Now, a Guest user has a virtual inbox, listing the encrypted emails sent to them. A new configuration setting has been added

```
com.openexchange.guard.guestCleanedAfterDaysOfInactivity=365
```

This configuration cleans up a Guest account after the configured number of days if the user has not logged in. 
All emails for the guest account will be purged, and the Guest accounts removed from appsuite. 
A setting of 0 disables the cleaning completely.

###Trust Levels

Some PGP Public keys can be trusted more than others. 
Guard now displays if the key is trusted or not by changing the color of the key next to a recipients email address, and provides details if the user hovers over the key. 
The trust level can be configured in the *guard-core.properties* file

```
com.openexchange.guard.keySources.trustThreshold=4
# The trust level for keys created by OX Guard
com.openexchange.guard.keySources.trustLevelGuard=5
# The trust level for keys uploaded by a user
com.openexchange.guard.keySources.trustLevelGuardUserUploaded=4
# The trust level for keys uploaded by a user and shared among users in the same context
com.openexchange.guard.keySources.trustLevelGuardUserShared=3
# The trust level for keys fetched from public HKP servers
com.openexchange.guard.keySources.trustLevelHKPPublicServer=1
# The trust level for keys fetched from HKP servers marked as trusted
com.openexchange.guard.keySources.trustLevelHKPTrustedServer=5
# The trust level for keys fetched from HKP servers queried via SRV DNS record
com.openexchange.guard.keySources.trustLevelHKPSRVServer=4
# The trust level for keys fetched from HKP servers queried via DNSSEC protected SRV DNS record
com.openexchange.guard.keySources.trustLevelHKPSRVDNSSECServer=4
```


