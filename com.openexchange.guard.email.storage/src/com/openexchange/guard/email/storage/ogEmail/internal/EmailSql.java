/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.email.storage.ogEmail.internal;

class EmailSql {

    static final String DELETE_ALL_FOR_USER_STMT = "DELETE FROM og_email WHERE id = ? AND cid = ?;";
    static final String SELECT_BY_EMAIL_STMT = "SELECT cid, id,email,db FROM og_email WHERE email = ?;";
    static final String INSERT_OR_UPDATE_STMT = "INSERT INTO og_email (email, id, cid, db, hu) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE id = ?, cid = ?, db = ?";
    static final String SELECT_BY_USERID_STMT = "SELECT cid, id,email,db from og_email WHERE id = ? AND cid = ?";
    static final String SELECT_HIGHEST_GUEST_USERID_STMT = "SELECT MAX(ID) FROM og_email WHERE cid < 0";
    static final String DELETE_ALL_FOR_CONTEXT_STMT = "DELETE FROM og_email WHERE cid = ?";
    static final String SELECT_EMAIL = "SELECT mail from user WHERE cid = ? AND id = ?";
    static final String SELECT_ALL_STMT = "SELECT cid, id,email,db FROM og_email;";
    static final String SELECT_BY_HU = "SELECT cid, id, email, db FROM og_email WHERE hu = ?";
    static final String CHECK_HU = "SELECT hu FROM og_email WHERE cid > 0 and hu IS NOT null";
    static final String GET_COUNT = "SELECT COUNT(email) FROM og_email";
    static final String GET_MISSING_HU_COUNT = "SELECT COUNT(email) FROM og_email WHERE cid > 0 AND hu IS null";
    static final String UPDATE_HU = "UPDATE og_email SET hu = ? WHERE email = ?";
    static final String FIND_MISSING_HU = "SELECT cid, id, email, db FROM og_email WHERE cid > 0 AND hu IS null";
    static final String UPDATE_EMAIL_ADDRESS = "UPDATE og_email SET email = ? WHERE email = ?";
}
