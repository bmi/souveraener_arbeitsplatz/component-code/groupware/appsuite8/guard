/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.servlets;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 *
 * Provides basic HKP (HTTP Keyserver Protocol) functionality letting OX Guard acting as a basic PGP Key Server
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class HkpServlet extends HttpServlet {

    private static final long serialVersionUID = -7405630378381362021L;
    private static final Logger logger = LoggerFactory.getLogger(HkpServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

        logger.debug("HKP request " + request.getQueryString());
        try {
            String operation = ServletUtils.getStringParameter(request, "op", true);
            String search = ServletUtils.getStringParameter(request, "search", true);

            if (search.contains("*")) { // Wildcard isn't being supported
                logger.debug("Attempt to use wildcard in remote lookup.  Rejected");
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
                return;
            }

            switch (operation) {
                case "index":
                    new HKPIndexAction(request, response, search).doAction();
                    break;
                case "get":
                    new HKPGetAction(response, search).doAction();
                    return;
                case "vindex":
                    //The "vindex" operation (verbose index) shows (in addition to the index operation) signatures of keys,
                    //but this is currently not supported by OX Guard
                    response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
                    return;
                default:
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (OXException ex) {
            if (GuardCoreExceptionCodes.PARAMETER_MISSING.equals(ex)) {
                logger.info("HKP request failed due to missing parameter");
                ServletUtils.sendNotAcceptable(response, "Missing Parameter " + ex);
            }
            else {
                logger.error(ex.getMessage());
                ServletUtils.sendError(response, ex);
            }
        } catch (Exception e) {
            logger.error("Unexpected error while handling HKP request", e);
            ServletUtils.sendError(response, e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            //OX Guard does not support/allow key uploads via HKP
            //To be RFC compliant we send a 501-not implemented
            logger.debug("HKP post not implemented");
            ServletUtils.sendAnswer(response, HttpServletResponse.SC_NOT_IMPLEMENTED, "HKP post not supported");
        } catch (IOException e) {
            logger.error("Unexpected error while handling HKP request", e);
            ServletUtils.sendError(response, e);
        }
    }
}
