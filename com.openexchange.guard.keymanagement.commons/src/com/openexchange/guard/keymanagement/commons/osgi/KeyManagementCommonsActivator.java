/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.keymanagement.commons.trust.GuardKeySources;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactoryImpl;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceTrustValidatorFactory;
import com.openexchange.guard.keymanagement.commons.trust.ThresholdKeySourceTrustValidatorFactory;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link KeyManagementCommonsActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyManagementCommonsActivator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(KeyManagementCommonsActivator.class);

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {

        Services.setServiceLookup(this);

        KeySourceFactory keySourceFactory = new KeySourceFactoryImpl();
        GuardKeySources.initialize(keySourceFactory);

        org.slf4j.LoggerFactory.getLogger(KeyManagementCommonsActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());
        registerService(KeySourceTrustValidatorFactory.class, new ThresholdKeySourceTrustValidatorFactory(getService(GuardConfigurationService.class)));
        registerService(KeySourceFactory.class, keySourceFactory);
        trackService(KeySourceTrustValidatorFactory.class);
        trackService(KeySourceFactory.class);

        openTrackers();
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(KeySourceTrustValidatorFactory.class);
        super.stopBundle();
    }

}
