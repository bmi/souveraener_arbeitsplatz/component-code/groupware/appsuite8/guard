/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.guest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.json.JSONException;
import org.slf4j.Logger;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.commons.util.RecipKeyListUtils;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.mime.encryptor.Encryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.encryptor.pgp.impl.HTMLStripper;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.oxapi.sharing.EmailShareLink;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.pgp.mail.PGPMimeService;

/**
 * {@link PGPGuestEncryptor}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class PGPGuestEncryptor extends Encryptor {

    private final PGPMimeService pgpMimeService;
    private final HelpFileService helpFileService;
    private final GuardGuestService guestService;
    private static Logger LOG = org.slf4j.LoggerFactory.getLogger(PGPGuestEncryptor.class);

    /**
     * Initializes a new {@link PGPGuestEncryptor}.
     * @param pgpCryptoService The {@link PGPCryptoService} to use for encryption
     * @param msg
     * @param recips
     * @param userid
     * @param cid
     * @param password
     */
    public PGPGuestEncryptor(PGPCryptoService pgpCryptoService,
                             PGPMimeService pgpMimeService,
                             GuardGuestService guestService,
                             HelpFileService helpFileService,
                             GuardParsedMimeMessage msg,
                             ArrayList<RecipKey> recips,
                             int userid,
                             int cid,
                             String password) {
        super(pgpCryptoService, msg, recips, userid, cid, password);
        this.pgpMimeService = Objects.requireNonNull(pgpMimeService, "pgpMimeService must not be null");
        this.guestService = guestService;
        this.helpFileService = helpFileService;

    }

    @Override
    public MimeMessage doEncrypt () throws OXException {
        return null;
    }

    private GuardKeys getSigningKey() throws OXException {
        GuardKeyService gk = Services.getService(GuardKeyService.class);
        return gk.getKeys(userid, cid);
    }

    private List<PGPPublicKey> toPGPPublicKeys(List<RecipKey> recipients){
        //Getting recipient keys suitable for encryption
        List<PGPPublicKey> pgpRecipientKeysList = new ArrayList<PGPPublicKey>();
        for (RecipKey recipientKey : recipients) {
            PGPPublicKey encryptionKey = recipientKey.getEncryptionKey();
            if(encryptionKey != null) {
                pgpRecipientKeysList.add(encryptionKey);
            }
        }
        return pgpRecipientKeysList;
    }

    /**
     * Remove headers that shouldn't be saved to the Guest encrypted mime such as bcc
     * removeHiddenHeaders
     *
     * @param message
     * @return message with headers removed as necessary
     * @throws MessagingException
     */
    private static MimeMessage removeHiddenHeaders(MimeMessage message) throws MessagingException {
        message.removeHeader("Bcc");
        return message;
    }

    private byte[] encrypt() throws OXException, JSONException, IOException, MessagingException {
        MimeMessage message = removeHiddenHeaders(msg.getMessage());

        MimeMessage encryptedMessage = null;

        List<BodyPart> helpFiles = helpFileService.attachHelpFile(userid, cid) ?
                helpFileService.getHelpFiles(userid, cid, RecipKeyListUtils.getLanguagesFor(recips)) :
                null;

        if(msg.isSign()) {
            GuardKeys signingKey = getSigningKey();
            if (signingKey == null) {
                throw MimeEncryptorExceptionCodes.SEND_UNABLE_FIND_KEYS.create("Unable to find signing keys");
            }
            PGPSecretKey pgpSigningKey = PGPKeysUtil.getSigningKey(signingKey.getPGPSecretKeyRing());
            char[] hashedPassword = CipherUtil.getSHA(password, signingKey.getSalt()).toCharArray();
            encryptedMessage = this.pgpMimeService.encryptSigned(message, pgpSigningKey, hashedPassword, toPGPPublicKeys(this.recips), helpFiles);
        }
        else {
            encryptedMessage = this.pgpMimeService.encrypt(message, toPGPPublicKeys(this.recips), helpFiles);
        }


        ByteArrayOutputStream ret = new ByteArrayOutputStream();
        encryptedMessage.writeTo(ret);
        return ret.toByteArray();
    }

    public void sendInvitationByMail(RecipKey recipient, GuardParsedMimeMessage msg, int templateId) throws OXException {

        final String fromEmail = msg.getFromAddress().getAddress();
        final String fromName = msg.getSenderName();
        final String guestMessage = msg.getGuestMessage();
        final String host = null;

        //Create the Guest invitation email
        MailCreatorService mailCreatorService = Services.getService(MailCreatorService.class);
        JsonObject inviteEmail = mailCreatorService.getPasswordEmail(
            recipient.getEmail(),
            fromName,
            fromName,
            fromEmail,
            recipient.getNewGuestPass(),
            recipient.getLang(),
            templateId,
            guestMessage,
            host,
            userid,
            cid);

        //Send the invitation mail
        GuardNotificationService guardNotificationService = Services.getService(GuardNotificationService.class);
        guardNotificationService.send(inviteEmail, userid, cid, msg.getSenderIP());
    }

    /**
     * Initialises an OX Guard Guest account
     *
     * @throws OXException
     */
    private void inviteGuests(int templateId) throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        if (guardConfigurationService.getBooleanProperty(GuardProperty.newGuestsRequirePassword, userid, cid)) {
            for (final RecipKey recipient : recips) {
                //Check for new guests
                if (recipient.isGuest() && recipient.getNewGuestPass() != null && !recipient.getNewGuestPass().isEmpty()) {
                    // Mark the key that password prompt will be required.  We just need to have question non-null
                    GuardKeyService keyStorage = Services.getService(GuardKeyService.class);
                    GuardKeys guestKey = keyStorage.getKeys(recipient.getUserid(), recipient.getCid());
                    if (guestKey != null) {
                        guestKey.setQuestion("");
                        keyStorage.updateAnswerQuestionForUser(guestKey);
                    }
                    sendInvitationByMail(recipient, msg, templateId);
                }
            }
        }
    }

    private String createShareLink(String senderSessionId, String emailId, RecipKey recipient) throws OXException {
        return new EmailShareLink().createEmailShare(senderSessionId, recipient.getEmail(), emailId, recipient.getLang());
    }

    public boolean doEncryptAndSend () throws OXException {
        //Invite new guests before sending the original message
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
        int templId = guardConfigService.getIntProperty(GuardProperty.templateID, userid, cid);
        inviteGuests(templId);

        byte[] encrypted;
        try {
            encrypted = encrypt();
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }

        MailCreatorService mailCreatorService = Services.getService(MailCreatorService.class);
        String host = msg.getHost();
        String messageId = getMessageID();
        ArrayList<BodyPart> bpAttachments = new ArrayList <BodyPart>();
        final BodyPart att = new MimeBodyPart();

        try {
            att.setText(new String(encrypted, StandardCharsets.UTF_8));
            att.setHeader("Content-Type", "mail/rfc822");
            att.setDisposition("attachment");
            att.setFileName("pgpMail.eml");
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            LOG.warn("Failed to initialize body part", e1);
        }
        bpAttachments.add(att);

        for (RecipKey recip : recips) {
            String id = createId();
            id = "pgp-" + id;  // After storing, add the pgp identifier

            String guestMessage = recip.isNewCreated() ? msg.getGuestMessage() : "";  // Only attach Guest message for new users
            if (guestMessage != null && !guestMessage.isEmpty()) {  // If GuestMessage is empty, remove all HTML tags as this should just be plaintext.
                guestMessage = new HTMLStripper().convertTextHTML(guestMessage);
            }
            JsonObject email = mailCreatorService.createBlankGuestMail(recip.getLang(), templId, messageId, host, userid, cid, guestMessage);
            if (email == null) {
                LOG.error("Unable to create blank Guest mail template");
                throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(recip.getEmail());
            }
            email = mailCreatorService.addTo(email, recip.getName(), recip.getEmail(), recip.getType());
            email = mailCreatorService.addFrom(email, msg.getFromAddress().getPersonal(), msg.getFromAddress().getAddress());
            email = mailCreatorService.addSubject(email, msg.getSubject());
            if (recip.isNewCreated()) {
                email = mailCreatorService.addPlainText(email, guestMessage);
            }
            email = mailCreatorService.noSave(email);

            final String shareLink = createShareLink(msg.getSenderSessionId(), id, recip);
            if (shareLink == null) {
                LOG.error("Unable to create Guard guest share link");
                throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(recip.getEmail());
            }
            email = mailCreatorService.addURL(email, shareLink);

            storeEmail(id, recip, encrypted);

            GuardNotificationService guardNotificationService = Services.getService(GuardNotificationService.class);
            guardNotificationService.send(email, bpAttachments, "", userid, cid, recip.getLang(), messageId, msg.getSenderIP());
        }
        return true;
    }

    private String createId() {
        String uid = CipherUtil.getUUID();
        return uid;
    }

    private void storeEmail (String id, RecipKey recip, byte[] encrypted) throws OXException {
        final UserIdentity guestUser = new UserIdentity(recip.getEmail()).setOXGuardUser(new OXGuardUser(recip.getUserid(), recip.getCid()));
        this.guestService.appendMessage(guestUser, new ByteArrayInputStream(encrypted), id, GuardGuestService.DEFAULT_FOLDER);
    }

    /**
     * Get the message identifier
     *
     * @return the message identifier
     * @throws OXException if the GuardConfigurationService is absent
     */
    protected String getMessageID() {
        final GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        String mailIdDomain = guardConfigurationService.getProperty(GuardProperty.mailIdDomain, userid, cid);
        final String oxURL = guardConfigurationService.getProperty(GuardProperty.externalEmailURL, userid, cid);
        if (mailIdDomain.equals("")) {  // If maildomain not defined, try to extract domain from URL
            if (oxURL.contains("/")) {
                mailIdDomain = oxURL.substring(0, oxURL.indexOf("/"));
            }
            if (mailIdDomain.contains(":")) {
                mailIdDomain = mailIdDomain.substring(0, mailIdDomain.indexOf(":"));
            }
        }
        if (mailIdDomain.equals("")) {
            mailIdDomain = "GUARD";
        }  // Otherwise, just define it as Guard
        return ("<" + CipherUtil.getUUID() + "@" + mailIdDomain + ">");
    }

}
