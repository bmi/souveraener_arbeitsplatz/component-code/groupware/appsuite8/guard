/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkp.osgi.Services;
import com.openexchange.guard.hkp.servlets.HkpServlet;
import com.openexchange.guard.hkp.utils.CheckWhitelist;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;

/**
 *
 * {@link HKPIndex} - The HKP "index" operation requests a list of keys on the keyserver
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class HKPIndex {

    private static final Logger logger = LoggerFactory.getLogger(HkpServlet.class);

    /**
     * Internal Method for assert that the mail does not contain some special characters
     *
     * @param email The mail
     * @return email If the mail does not contain any unwanted characters
     * @throws OXException In case email contains some unwanted characters
     */
    private String assertUnwantedCharacters(String email) throws OXException {
        if (email.contains("%")) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create("search");
        }
        return email;
    }

    /**
     * Creates string representation from a Public key ring list
     *
     * @param keyrings the list to create the string for
     * @param addSubkeys Whether the sub keys of a key ring should be written to the output or not
     * @return keyrings as string representation
     */
    private String keyRingsToString(List<PGPPublicKeyRing> keyrings, boolean addSubkeys) {
        StringBuilder resp = new StringBuilder();
        resp.append("info:1:" + keyrings.size() + "\r\n");
        for (int i = 0; i < keyrings.size(); i++) {
            Iterator<PGPPublicKey> keys = keyrings.get(i).getPublicKeys();
            while (keys.hasNext()) {
                PGPPublicKey key = keys.next();
                if (key.isMasterKey() || addSubkeys) {
                    String id = LongUtil.longToHexString(key.getKeyID());
                    String algo = Integer.toString(key.getAlgorithm());
                    String keylen = Integer.toString(key.getBitStrength());
                    long cr = key.getCreationTime().getTime() / 1000;
                    String creation = Long.toString(cr);
                    String exp = key.getValidSeconds() == 0 ? "" : Long.toString((key.getValidSeconds()) + cr);
                    String pub = "pub:" + id + ":" + algo + ":" + keylen + ":" + creation + ":" + exp + ":";
                    resp.append(pub + "\r\n");
                    // create primary key line
                    Iterator<?> it = key.getUserIDs();
                    while (it.hasNext()) {
                        resp.append("uid:\"" + (String) it.next() + "\":" + creation + ":" + exp + ":\r\n");
                        // get userids
                    }
                }
            }
        }
        return resp.toString();
    }

    /**
     * "The (HKP) index operation requests a list of keys on the keyserver that match the text or key ID in the "search" variable."
     *
     * @param requestIP The IP of the caller
     * @param search The search pattern
     * @return The key list formatted string, or null if the search did not found any keys
     * @throws OXException
     */
    public String index(String requestIP, String search) throws OXException {

        HKPSearch hkpSearch = new HKPSearch();
        if (search.contains("@")) {

            //Searching for the given email
            List<PGPPublicKeyRing> keyrings = hkpSearch.getPublicKeyRing(assertUnwantedCharacters(search)); // zero cid for local search only

            if (keyrings.size() > 0) {
                logger.debug("Sending list of keys, length = " + keyrings.size());
                return keyRingsToString(keyrings,false /* do not include subkeys */);
            } else {
                //No keys found - check if the requester is on the white list; In this case we create a new PGP key, if the requestED user is an OX user
                logger.debug("No Keys found, check whitelist against " + requestIP);
                // If calling server in whitelist, check if OX member to create
                if (CheckWhitelist.isWhiteListed(requestIP)) {

                    logger.debug("IP in whitelist, checking against OX backend");

                    //Check and see if we can get public key from the backend if member; No guest and no HKP lookup;
                    RecipKeyService recipientKeyService = Services.getService(RecipKeyService.class);
                    RecipKey key = recipientKeyService.getInternalRecipKey(0, 0, assertUnwantedCharacters(search));
                    if (key != null) {
                        //This will create a key for a requested user if it is an OX user; but not a guest
                        if(key.isNewKey() && !key.isGuest()) {
                            Services.getService(AccountCreationService.class).createUserFor(key);
                        }
                        //Lookup the new created key
                        keyrings = hkpSearch.getPublicKeyRing(assertUnwantedCharacters(search));
                        logger.debug("Sending list of keys, length = " + keyrings.size());
                        return keyRingsToString(keyrings, false /* do not include subkeys */);
                    }
                }
            }
        } else {  // Search by keyId
            search = normalizeSearch(search);

            PGPPublicKeyRing resultKeyRing = null;
            if (search.length() > 8) {
                Long req_id = LongUtil.stringToLong(search);
                resultKeyRing = hkpSearch.getPublicKeyRingById(req_id, 0, 0);
            } else {
                resultKeyRing = hkpSearch.getPublicKeyRingById(normalizeSearch(search), 0, 0);
            }
            if (resultKeyRing != null) {
                logger.debug("Sending list of keys, length = 1");
                List<PGPPublicKeyRing> keyrings = new ArrayList<PGPPublicKeyRing> ();
                keyrings.add(resultKeyRing);
                return keyRingsToString(keyrings, true);
            }

        }
        return null;
    }

    /**
     * Normalize search parameter. If more than the 16 character ID (Fingerprint), then shorten
     *
     * @param search The search parameter to normalize
     * @return The normalized search parameter
     */
    private String normalizeSearch(String search) {
        search = search.replace(" ", "");  // remove padding from a fingerprint
        search = search.replace("0x", "").toLowerCase();
        if (search.length() > 16) {  // If more than 16, we'll just look at the last 16
            search = search.substring(search.length() - 16);
        }
        return search;
    }
}
