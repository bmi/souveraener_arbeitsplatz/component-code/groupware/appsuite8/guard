/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import java.util.Locale;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link KeyCreationService} defines functionality for creating OX Guard Keys
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public interface KeyCreationService {

    /**
     * Gets the {@link KeyPairSource} which will be used for creating new keys.
     *
     * @return The current {@link KeyPairSource}
     */
    public KeyPairSource getKeyPairSource();

    /**
     * Sets the {@link KeyPairSource} which will be used for creating new keys.
     *
     * @param keyPairSource
     */
    public void setKeyPairSource(KeyPairSource keyPairSource);


    /**
     * Creates a new {@link GuardKeys} for a specific user using a default, implementation specific {@link KeyPairSource}.
     *
     * @param userid The ID of the user
     * @param contextid The ID Of the user's context
     * @param name The name of the user
     * @param email The email of the user
     * @param password The password used for the key
     * @param locale The language related to the user
     * @param markAsCurrent True, to set the "current" flag of the new key, false otherwise
     * @param createRecovery True in order to create a password recovery for the key, false otherwise
     * @return A new, not yet persisted, {@link GuardKeys} instance
     * @throws OXException
     */
    GuardKeys create(int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery) throws OXException;

    /**
     * Creates a new {@link GuardKeys} for a specific user using a default, implementation specific {@link KeyPairSource}.
     *
     * @param userid The ID of the user
     * @param contextid The ID Of the user's context
     * @param name The name of the user
     * @param email The email of the user
     * @param password The password used for the key
     * @param locale The language related to the user
     * @param markAsCurrent True, to set the "current" flag of the new key, false otherwise
     * @param createRecovery True in order to create a password recovery for the key, false otherwise
     * @param oxUserId the userId of the ox user. Required when creating guests.
     * @param oxCid the contextId of the ox user. Required when creating guests.
     * @return A new, not yet persisted, {@link GuardKeys} instance
     * @throws OXException
     */
    GuardKeys create(int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery, int oxUserId, int oxCid) throws OXException;

    /**
     * Creates a new {@link GuardKeys} for a specific user using a defined {@link KeyPairSource}.
     *
     * @param keyPairSource The {@link KeyPairSource} to use for creating the key pairs of the new {@link GuardKeys} object.
     * @param userid The ID of the user
     * @param contextid The ID Of the user's context
     * @param name The name of the user
     * @param email The email of the user
     * @param password The password used for the key
     * @param locale The language related to the user
     * @param markAsCurrent True, to set the "current" flag of the new key, false otherwise
     * @param createRecovery True in order to create a password recovery for the key, false otherwise
     * @param oxUserId the userId of the ox user. Required when creating guests.
     * @param oxCid the contextId of the ox user. Required when creating guests.
     * @return A new, not yet persisted, {@link GuardKeys} instance
     * @throws OXException
     */
    GuardKeys create(KeyPairSource keyPairSource, int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery, int oxUserId, int oxCid) throws OXException;
}
