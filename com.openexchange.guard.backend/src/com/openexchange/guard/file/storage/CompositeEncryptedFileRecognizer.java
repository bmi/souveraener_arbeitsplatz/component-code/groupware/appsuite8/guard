/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.ArrayList;
import java.util.Collection;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;

/**
 * {@link CompositeEncryptedFileRecognizer}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class CompositeEncryptedFileRecognizer implements EncryptedFileRecognizer {

    private final EncryptedFileRecognizer[] recognizers;

    /**
     * Initializes a new {@link CompositeEncryptedFileRecognizer}.
     *
     * @param recognizers A set of {@link EncryptedFileRecognizer}
     */
    public CompositeEncryptedFileRecognizer(EncryptedFileRecognizer... recognizers) {
        this.recognizers = recognizers;
    }

    /**
     * @returns true, if one of the {@link EncryptedFileRecognizer} return true, false otherwise
     * @see com.openexchange.guard.file.storage.GuardFileRecognizer#isGuardFile(com.openexchange.file.storage.Document)
     */
    @Override
    public boolean isEncrypted(File file) {
        for (EncryptedFileRecognizer recognizer : recognizers) {
            if (recognizer.isEncrypted(file)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markEncrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        for (EncryptedFileRecognizer recognizer : recognizers) {
            ret.addAll(recognizer.markEncrypted(file));
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markEncrypted(Document document) {
        ArrayList<Field> ret = new ArrayList<Field>();
        for (EncryptedFileRecognizer recognizer : recognizers) {
            ret.addAll(recognizer.markEncrypted(document));
        }
        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markDecrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        for (EncryptedFileRecognizer recognizer : recognizers) {
            ret.addAll(recognizer.markDecrypted(file));
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markDecrypted(Document document) {
        ArrayList<Field> ret = new ArrayList<Field>();
        for (EncryptedFileRecognizer recognizer : recognizers) {
            ret.addAll(recognizer.markDecrypted(document));
        }
        return ret;
    }
}
