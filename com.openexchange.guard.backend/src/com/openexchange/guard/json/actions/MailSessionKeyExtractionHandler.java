/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.mail.service.impl.PGPMailSessionKeyExtractor;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.session.Session;
import com.openexchange.tools.servlet.OXJSONExceptionCodes;

/**
 * {@link MailSessionKeyExtractionHandler} extracts encrypted PGP session keys from a PGP encrypted email object
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class MailSessionKeyExtractionHandler implements SessionKeyExtractionHandler {

    private static final String JSON_MODULE       = "module";
    private static final String MAIL_MODULE       = "mail";
    private static final String JSON_FIELD_FOLDER = "folder";
    private static final String JSON_FIELD_ID     = "id";

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.json.actions.SessionKeyExtractionHandler#canHandle(org.json.JSONObject)
     */
    @Override
    public boolean canHandle(JSONObject json) throws JSONException {
        return json != null && json.has(JSON_MODULE) && json.getString(JSON_MODULE).equals(MAIL_MODULE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.json.actions.SessionKeyExtractionHandler#handle(org.json.JSONObject, com.openexchange.session.Session)
     */
    @Override
    public String[] handle(JSONObject json, Session session) throws OXException, JSONException {
        json = Objects.requireNonNull(json, "json must not be null");
        if (canHandle(json)) {
            if (!json.has(JSON_FIELD_FOLDER)) {
                throw OXJSONExceptionCodes.MISSING_FIELD.create(JSON_FIELD_FOLDER);
            }
            final String folder = json.getString(JSON_FIELD_FOLDER);

            if (!json.has(JSON_FIELD_ID)) {
                throw OXJSONExceptionCodes.MISSING_FIELD.create(JSON_FIELD_FOLDER);
            }
            final String id = json.getString(JSON_FIELD_ID);

            MailServletInterface mailServletInterface = MailServletInterface.getInstance(session);
            try {
                MailMessage message = mailServletInterface.getMessage(folder, id);
                if (message != null) {
                    PGPMailSessionKeyExtractor extractor = new PGPMailSessionKeyExtractor();
                    return extractor.getEncryptedSession(message, session);
                } else {
                    throw MailExceptionCode.MAIL_NOT_FOUN_BY_MESSAGE_ID.create(folder, id);
                }
            } finally {
                mailServletInterface.close();
            }
        }
        return null;
    }
}
