/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard;


/**
 * {@link UserTestResult}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UserTestResult {

    private final int userId;
    private final int contextId;
    private final String displayName;
    private final String displayLanguage;

    /**
     * Initializes a new {@link UserTestResult}.
     * @param userId
     * @param contextId
     * @param displayName
     * @param displayLanguage
     */
    public UserTestResult(int userId, int contextId, String displayName, String displayLanguage) {
        this.userId = userId;
        this.contextId = contextId;
        this.displayName = displayName;
        this.displayLanguage = displayLanguage;
    }

    /**
     * Gets the userId
     *
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Gets the contextId
     *
     * @return The contextId
     */
    public int getContextId() {
        return contextId;
    }

    /**
     * Gets the displayName
     *
     * @return The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Gets the displayLanguage
     *
     * @return The displayLanguage
     */
    public String getDisplayLanguage() {
        return displayLanguage;
    }
}
