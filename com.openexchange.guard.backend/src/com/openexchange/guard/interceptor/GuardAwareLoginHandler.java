/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.interceptor;

import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.login.LoginHandlerService;
import com.openexchange.login.LoginResult;

/**
 * {@link GuardAwareLoginHandler} removes the Guard authentication token attached to a session on logout.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class GuardAwareLoginHandler implements LoginHandlerService {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.login.LoginHandlerService#handleLogin(com.openexchange.login.LoginResult)
     */
    @Override
    public void handleLogin(LoginResult login) throws OXException {}

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.login.LoginHandlerService#handleLogout(com.openexchange.login.LoginResult)
     */
    @Override
    public void handleLogout(LoginResult logout) throws OXException {
        new AuthenticationTokenHandler().destroyForSession(logout.getSession(), true, null);
    }
}
