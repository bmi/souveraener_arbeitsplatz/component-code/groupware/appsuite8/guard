/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.pgpmime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.file.FileTyper;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link PGPEncryptedAttachmentManager}
 *
 * Handle PGP attachments in an email.  Need to be decoded with the senders key
 * before sending encrypted with the recipient keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class PGPEncryptedAttachmentManager {

    private final GuardParsedMimeMessage msg;
    private final MimeMessage message;
    private boolean encrypted;
    private final String password;

    public PGPEncryptedAttachmentManager (GuardParsedMimeMessage msg, String password) throws OXException {
        this.msg = msg;
        this.message = msg.getMessage();
        this.password = password;
        encrypted = false;
        parse();
    }

    /**
     * Return true if has an encrypted attachment
     * @return
     */
    public boolean hasEncryptedAttachment () {
        return encrypted;
    }

    public MimeMessage getDecoded () {
        return message;
    }

    /**
     * Parse the message
     * @throws OXException
     */
    private void parse() throws OXException {
        Object content;
        try {
            content = message.getContent();
            if (content instanceof Multipart || content instanceof MimeMultipart) {
                final Multipart mp = (Multipart) content;
                parseMultipart(mp);
            }
            // If not multipart, will not have encrypted attachments
        } catch (IOException | MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parse the multiparts and decrypt any attachments that are pgp
     * @param mp
     * @throws OXException
     */
    private void parseMultipart(final Multipart mp) throws OXException {
        try {
            final int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                final BodyPart bp = mp.getBodyPart(i);
                final Object content = bp.getContent();
                if (content instanceof String) {  // Check if this string is a text attachment, or just the message body
                    // Nothing to do here, not an encrypted attachment
                } else if (content instanceof InputStream) {
                    boolean isEncrypted = false;
                    if (bp.getContentType() != null && bp.getContentType().contains("application/pgp")) {
                        if (!bp.getContentType().contains("application/pgp-key")) {  // Keys are not encrypted
                            isEncrypted = true;
                        }
                    }
                    if (bp.getFileName() != null && bp.getFileName().contains(".pgp")) {
                        isEncrypted = true;
                    }
                    if (isEncrypted) {
                        encrypted = true;
                        if (password == null) {
                            throw MimeEncryptorExceptionCodes.PASSWORD_MISSING.create();
                        }
                        // decode attachment here
                        GuardKeyService keyService = Services.getService(GuardKeyService.class);
                        GuardKeys key = keyService.getKeys(msg.getSenderUserId(), msg.getSenderContextId());
                        if (key == null) {
                            throw MimeEncryptorExceptionCodes.PROBLEM_DECODING.create("Unable to retrieve keys");
                        }
                        PGPCryptoService cryptoService = Services.getService(PGPCryptoService.class);
                        ByteArrayOutputStream decoded = new ByteArrayOutputStream();
                        UserIdentity ident = new UserIdentity(key.getEmail(), password).setOXGuardUser(new OXGuardUser(key.getUserid(), key.getContextid()));
                        try {
                            cryptoService.decrypt((InputStream) bp.getContent(), decoded, ident);
                            if (decoded.size() > 0) {
                                String filename = bp.getFileName().replaceAll("\\.pgp", "");
                                bp.setFileName(filename);
                                bp.setContent(decoded.toByteArray(), FileTyper.getFileType(filename));
                            }
                        } catch (Exception ex) {
                            throw MimeEncryptorExceptionCodes.PROBLEM_DECODING.create(ex.getMessage());
                        }
                        decoded.close();
                    }
                } else if (content instanceof Multipart || content instanceof MimeMultipart) {
                    final Multipart mp2 = (Multipart) content;
                    parseMultipart(mp2);
                }
            }
            if (encrypted) {
                message.saveChanges();
            }

        } catch (MessagingException | IOException e) {

        }

    }

}
