/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.codec.binary.Base64InputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.mail.MailField;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.SecurityInfo;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.mail.mime.converters.MimeMessageConverter;

/**
 * {@link MailMessageFactory} internal simple factory for creating {@link MailMessage} instances from a given OX Guard JSON response
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
class MailMessageFactory {

    private static final String EMAILS        = "emails";
    private static final String DATA          = "data";
    private static final String CONTENT       = "content";
    private static final String META_DATA     = "metaData";
    private static final String ID            = "id";
    private static final String FOLDER_ID     = "folderId";
    private static final String MESSAGE_FLAGS = "messageFlags";
    private static final String BCC           = "bcc";
    private static final String CC            = "cc";
    private static final String TO            = "to";
    private static final String FROM          = "from";
    private static final String COLOR_LABEL   = "colorLabel";
    private static final String RECEIVED_DATE = "receivedDate";
    private static final String SENT_DATE     = "sentDate";
    private static final String SIZE          = "size";
    private static final String SUBJECT       = "subject";
    private static final String CONTENT_TYPE  = "contentType";

    /**
     * Internal method to create a {@link MimeMessage} from the given data
     *
     * @param input The data
     * @return The {@link MimeMessage} read from the given data
     * @throws MessagingException
     */
    private MimeMessage createMimeMessage(InputStream input) throws MessagingException {
        return input != null ?
            new MimeMessage(MimeDefaultSession.getDefaultSession(), input) :
            new MimeMessage(MimeDefaultSession.getDefaultSession());
    }

    /**
     * Internal method to create a {@link MailMessage} from the given {@link MimeMessage}
     *
     * @param mimeMessage The {@link MimeMessage}
     * @param mailId The unique ID of the mail
     * @return The created {@link MailMessage}
     * @throws OXException
     */
    private MailMessage createMailMessage(MimeMessage mimeMessage, String mailId) throws OXException {
        final MailField[] FIELDS = {MailField.BODY, MailField.HEADERS};
        MailMessage mailMessage =
            MimeMessageConverter.convertMessage(mimeMessage,
                mailId,
                "GUARD-GUEST",
                com.openexchange.mail.MailPath.SEPERATOR,
                FIELDS);
        mailMessage.setMailId(mailId);
        SecurityInfo securityInfo = new SecurityInfo(true, false, CryptoType.PROTOCOL.PGP);
        mailMessage.setSecurityInfo(securityInfo);
        return mailMessage;
    }

    /**
     * Internal method to apply the meta data to given {@link MailMessage} object
     *
     * @param mailMessage The message to apply the meta data for
     * @param metaData The meta data to apply
     * @throws OXException
     * @throws JSONException
     * @throws AddressException
     */
    private void applyMetaData(MailMessage mailMessage, JSONObject metaData) throws OXException, JSONException, AddressException {
        if(!metaData.isNull(CONTENT_TYPE)) {
            mailMessage.setContentType(metaData.getString(CONTENT_TYPE));
        }

        if(!metaData.isNull(SUBJECT)) {
            mailMessage.setSubject(metaData.getString(SUBJECT));
        }

        if(!metaData.isNull(SIZE)) {
            mailMessage.setSize(metaData.getLong(SIZE));
        }

        if(!metaData.isNull(SENT_DATE)) {
            mailMessage.setSentDate(new Date(metaData.getLong(SENT_DATE)));
        }

        if(!metaData.isNull(RECEIVED_DATE)) {
            mailMessage.setReceivedDate(new Date(metaData.getLong(RECEIVED_DATE)));
        }

        if(!metaData.isNull(COLOR_LABEL)) {
            mailMessage.setColorLabel(metaData.getInt(COLOR_LABEL));
        }

        if(!metaData.isNull(FROM)) {
            JSONArray jsonArray = metaData.getJSONArray(FROM);
            for(int i=0; i< jsonArray.length(); i++) {
               mailMessage.addFrom(new InternetAddress(jsonArray.getString(i)));
            }
        }

        if(!metaData.isNull(TO)) {
            JSONArray jsonArray = metaData.getJSONArray(TO);
            for(int i=0; i< jsonArray.length(); i++) {
               mailMessage.addTo(new InternetAddress(jsonArray.getString(i)));
            }
        }

        if(!metaData.isNull(CC)) {
            JSONArray jsonArray = metaData.getJSONArray(CC);
            for(int i=0; i< jsonArray.length(); i++) {
               mailMessage.addCc(new InternetAddress(jsonArray.getString(i)));
            }
        }

        if(!metaData.isNull(BCC)) {
            JSONArray jsonArray = metaData.getJSONArray(BCC);
            for(int i=0; i< jsonArray.length(); i++) {
               mailMessage.addBcc(new InternetAddress(jsonArray.getString(i)));
            }
        }

        if(!metaData.isNull(MESSAGE_FLAGS)) {
            mailMessage.setFlags(metaData.getInt(MESSAGE_FLAGS));
        }

        if(!metaData.isNull(FOLDER_ID)) {
            mailMessage.setFolder(metaData.getString(FOLDER_ID));
        }

    }

    /**
     * Internal method to create a {@link MailMessage} from the given JSON object
     *
     * @param json The JSON to create the mail message form
     * @return The created mail message
     * @throws OXException
     */
    private MailMessage createaMailMessageFrom(JSONObject json) throws OXException {
        try {
            final String mailId = json.getString(ID);
            final String content = !json.isNull(CONTENT) ? json.getString(CONTENT) : null;
            final MimeMessage mimeMessage = createMimeMessage(
                content != null ?
                new Base64InputStream(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8))) :
                null);

            MailMessage mailMessage = createMailMessage(mimeMessage, mailId);
            applyMetaData(mailMessage, json.getJSONObject(META_DATA));

            return mailMessage;
        } catch (JSONException | MessagingException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parses the given JSONObject to a set of {@link MailMessage} instances
     *
     * @param json The JSONObject to parse
     * @return A set of parsed {@link MailMessages}
     * @throws OXException
     */
    MailMessage[] createFrom(JSONObject json) throws OXException {
        try {
            JSONObject data = json.getJSONObject(DATA);
            JSONArray emails = data.getJSONArray(EMAILS);
            MailMessage[] ret = new MailMessage[emails.length()];
            for (int i = 0; i < emails.length(); i++) {
                JSONObject email = emails.getJSONObject(i);
                ret[i] = createaMailMessageFrom(email);
            }
            return ret;
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }
}
