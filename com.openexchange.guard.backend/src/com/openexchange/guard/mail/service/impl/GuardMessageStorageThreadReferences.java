/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailFields;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageThreadReferences;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.MailThread;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.mail.search.SearchTerm;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;


/**
 * {@link GuardMessageStorageThreadReferences} - The Guard-aware message storage delegating to {@link IMailMessageStorageThreadReferences}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorageThreadReferences extends GuardMessageStorage implements IMailMessageStorageThreadReferences {

    private final IMailMessageStorageThreadReferences messageStorageThreadReferences;
    private final CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMessageStorageThreadReferences}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     * @param authToken
     */
    public GuardMessageStorageThreadReferences(Session session, IMailMessageStorageThreadReferences delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(session, delegate, services, authToken, type);
        this.messageStorageThreadReferences = delegate;
        this.type = type;
    }

    @Override
    public boolean isThreadReferencesSupported() throws OXException {
        return messageStorageThreadReferences.isThreadReferencesSupported();
    }

    @Override
    public List<MailThread> getThreadReferences(String fullName, int size, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields, String[] headerNames) throws OXException {
        List<MailThread> threads = messageStorageThreadReferences.getThreadReferences(fullName, size, sortField, order, searchTerm, fields, headerNames);
        if (null == threads || threads.isEmpty()) {
            return threads;
        }

        // Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
            GuardAuthenticationToken authToken = new AuthenticationTokenHandler().getForSession(session, CryptoType.PROTOCOL.PGP.getValue());
            List<MailThread> processedThreads = new ArrayList<>(threads.size());
            for (MailThread mailThread : threads) {
                processedThreads.add(processMailThread(authToken, mailThread));
            }
            threads = processedThreads;
        }

        return threads;
    }

    private MailThread processMailThread(GuardAuthenticationToken authToken, MailThread mailThread) throws OXException {
        MailThread processed;
        {
            MailMessage parent = mailThread.getParent();
            if (null == parent) {
                processed = new MailThread();
            } else {
                processed = new MailThread(processMessage(parent));
            }
        }

        List<MailThread> children = mailThread.getChildren();
        List<MailThread> processedChildren = new ArrayList<>(children.size());
        for (MailThread child : children) {
            processedChildren.add(processMailThread(authToken, child));
        }
        processed.addChildren(processedChildren);
        return processed;
    }

}
