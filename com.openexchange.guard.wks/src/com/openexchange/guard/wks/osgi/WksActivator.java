/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.osgi;

import org.osgi.service.http.HttpService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.wks.internal.WksSetup;
import com.openexchange.guard.wks.servlets.WksServletAdvanced;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link WksActivator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since 2.10.0
 */
public class WksActivator extends HousekeepingActivator {

    // Support for direct only.  Backwards compatibility
    private static final String GUARD_HU_SERVLET_PATH = "/hu";
    // Support for advanced or direct
    private static final String GUARD_OPENPGPKEY_PATH = "/openpgpkey";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { HttpService.class, GuardConfigurationService.class, EmailStorage.class,
            KeyTableStorage.class, GuardDatabaseService.class};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(WksActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        getService(HttpService.class).registerServlet(GUARD_HU_SERVLET_PATH, new WksServletAdvanced(true), null, null);
        getService(HttpService.class).registerServlet(GUARD_OPENPGPKEY_PATH, new WksServletAdvanced(), null, null);
        checkAvailable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(WksActivator.class).info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());

        HttpService httpService = getService(HttpService.class);
        if (httpService != null) {
            httpService.unregister(GUARD_HU_SERVLET_PATH);
        }

        Services.setServiceLookup(null);

        super.stopBundle();
    }

    /**
     * Check if the user hash tables have been set up and are available.
     * If not, calls setup
     * @throws OXException
     */
    private void checkAvailable() throws OXException {
        EmailStorage storage = Services.getService(EmailStorage.class);
        if (storage.getCount() == 0)
         {
            return;  // No keys
        }
        if (storage.getMissingHuCount() == 0)
         {
            return;  // Fully populated, return
        }
        WksSetup.doUpgradeHu(); // Otherwise, we need to do upgrade
    }
}
