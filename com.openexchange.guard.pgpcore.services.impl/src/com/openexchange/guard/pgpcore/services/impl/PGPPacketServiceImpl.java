/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.regex.Pattern;
import com.openexchange.exception.OXException;
import com.openexchange.guard.pgpcore.services.PGPPacketService;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.pgp.core.exceptions.PGPCoreExceptionCodes;
import com.openexchange.pgp.core.packethandling.ExtractSessionProcessorHandler;
import com.openexchange.pgp.core.packethandling.ExtractSessionProcessorHandler.EncryptedSession;
import com.openexchange.pgp.core.packethandling.PacketProcessor;

/**
 * {@link PGPPacketServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class PGPPacketServiceImpl implements PGPPacketService {

    private final Pattern       pgpPattern;
    private static final int    STREAM_INSPECTION_SIZE   = 512;
    private static final String BEGIN_PGP_MESSAGE_MARKER = "-----BEGIN PGP MESSAGE-----";

    /**
     * Initializes a new {@link PGPPacketServiceImpl}.
     */
    public PGPPacketServiceImpl() {
        this.pgpPattern = Pattern.compile(
            "-----BEGIN PGP MESSAGE-----.*?-----END PGP MESSAGE-----",
            Pattern.DOTALL);
    }

    /**
     * Encodes a given collection of session packets to a PGP conform byte represention.
     *
     * @param sessionPackets The session packets to encode.
     * @return The session packets encoded in byte representation
     * @throws IOException
     */
    private Collection<byte[]> toByteCollection(Collection<EncryptedSession> sessionPackets) throws IOException {
        ArrayList<byte[]> ret = new ArrayList<byte[]>();
        for (EncryptedSession sessionPacket : sessionPackets) {
            ret.add(sessionPacket.getEncoded());
        }
        return ret;
    }

    private boolean containsASCIIArmor(InputStream inputStream) throws IOException {
        inputStream.mark(STREAM_INSPECTION_SIZE);
        byte[] peekedData = new byte[STREAM_INSPECTION_SIZE];
        int read = inputStream.read(peekedData);
        inputStream.reset();
        if (read > 0) {
            String peekedContent = new String(peekedData, StandardCharsets.UTF_8);
            return peekedContent.contains(BEGIN_PGP_MESSAGE_MARKER);
        }

        return false;
    }

    /**
     * Internal method to extract the PGP data from the given InputStream
     *
     * @param inputStream The Inputstream to extract the PGP data from
     * @return An InputStream to the pgp subset data, extracted from the given InputStream.
     * @throws IOException
     */
    private InputStream extractPGPDataFrom(InputStream inputStream) throws IOException {
        if(!inputStream.markSupported()) {
           inputStream = new BufferedInputStream(inputStream, STREAM_INSPECTION_SIZE);
        }
        if (containsASCIIArmor(inputStream)) {
            try (Scanner scanner = new Scanner(inputStream)) {
                String pgpData = scanner.findWithinHorizon(pgpPattern, 0);
                if (pgpData != null && !pgpData.isEmpty()) {
                    return new ByteArrayInputStream(pgpData.getBytes(StandardCharsets.UTF_8));
                }
                return null;
            }
        } else {
            //non ASCII PGP data
            return inputStream;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPPacketService#getEncrytedSessions()
     */
    @Override
    public Collection<byte[]> getEncrytedSessions(InputStream pgpStream) throws OXException {

        try {
            pgpStream = extractPGPDataFrom(pgpStream);
        } catch (IOException e1) {
            PGPCoreExceptionCodes.IO_EXCEPTION.create(e1, e1.getMessage());
        }

        if (pgpStream != null) {
            try {
                final PacketProcessor packetProcessor = new PacketProcessor();
                final ExtractSessionProcessorHandler handler = new ExtractSessionProcessorHandler();
                packetProcessor.process(pgpStream, handler);
                return toByteCollection(handler.getEncryptedSessions());
            } catch (Exception e) {
                throw PGPCoreServicesExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        } else {
            throw PGPCoreExceptionCodes.NO_PGP_DATA_FOUND.create();
        }
    }
}
