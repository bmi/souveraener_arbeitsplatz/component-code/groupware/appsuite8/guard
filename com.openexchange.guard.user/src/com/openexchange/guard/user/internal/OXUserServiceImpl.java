/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user.internal;

import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.oxapi.MailResolver;
import com.openexchange.guard.oxapi.capabilities.Capabilities;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.OXUser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.exceptions.GuardUserExceptionCodes;

/**
 * {@link OXUserServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class OXUserServiceImpl implements OXUserService {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OXUserServiceImpl.class);
    private final MailResolver mailResolver;
    private final GuardConfigurationService configurationService;
    private final EmailStorage emailStorage;

    /**
     * Initializes a new {@link OXUserServiceImpl}.
     *
     * @param configurationService The configuration service
     */
    public OXUserServiceImpl(GuardConfigurationService configurationService, EmailStorage emailStorage) {
        this.configurationService = configurationService;
        this.emailStorage = emailStorage;
        this.mailResolver = new MailResolver();
    }

    /**
     * Method to determine whether the user is an OX Appsuite guest or not.
     *
     * @param userId The ID of the user to check
     * @param contextId The context ID of the user to check
     * @return true, if the user with the given ID is an OX Appsuite guest, false if a regular non-guest user.
     * @throws OXException
     */
    @Override
    public boolean isGuest(int userId, int contextId) throws OXException {
        JsonArray userCapabilities = new Capabilities().getUserCapabilities(userId, contextId);
        if (userCapabilities != null) {
            for (JsonElement capability : userCapabilities) {
                JsonObject jsonCapability = capability.getAsJsonObject();
                if (jsonCapability.has("id")) {
                    String capabilityId = jsonCapability.get("id").getAsString();
                    if (capabilityId.equals("guest")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public OXUser getUser(String email) throws OXException {
        JsonObject userdat = mailResolver.resolveUser(email);
        if ((userdat != null) && (userdat.has(email))) {
            JsonObject user = userdat.get(email).getAsJsonObject();
            int userId = user.get("uid").getAsInt();
            int contextId = user.get("cid").getAsInt();
            String language = user.get("user").getAsJsonObject().get("language").getAsString();
            String name = "";
            if (user.get("user").getAsJsonObject().has("displayName")) {
                name = user.get("user").getAsJsonObject().get("displayName").getAsString();
            }
            return new OXUser(email, userId, contextId, name, isGuest(userId, contextId), language);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.user.GuardUserService#getUser(int, int)
     */
    @Override
    public OXUser getUser(int contextId, int userId) throws OXException {
        List<Email> email = this.emailStorage.getById(contextId, userId);
        if(email != null && email.size() > 0) {
            return getUser(email.get(0).getEmail());
        }
        return null;
    }
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.user.GuardUserService#getUserCapabilieties(int, int)
     */
    @Override
    public GuardCapabilities getGuardCapabilieties(int contextId, int userId) throws OXException {
        try {
            GuardCapabilities guardCapabilities = new GuardCapabilities();
            guardCapabilities.setPerm(GuardCapabilities.Permissions.NONE);
            JsonArray userCapabilities = new Capabilities().getUserCapabilities(userId, contextId);
            String jsonData = userCapabilities.toString();
            if (jsonData.contains("guard")) {
                guardCapabilities.setPerm(GuardCapabilities.Permissions.GUARD);
            }
            if (jsonData.contains("guard:mail") || jsonData.contains("guard-mail")) {
                guardCapabilities.setPerm(GuardCapabilities.Permissions.MAIL);
            }
            if (jsonData.contains("guard:drive") || jsonData.contains("guard-drive")) {
                if (guardCapabilities.getPerm().equals(GuardCapabilities.Permissions.MAIL)) {
                    guardCapabilities.setPerm(GuardCapabilities.Permissions.GUARD_MAIL_FILE);
                } else {
                    guardCapabilities.setPerm(GuardCapabilities.Permissions.FILE);
                }
            }

            if (jsonData.contains("guard-noprivate")) {
                guardCapabilities.setPrivatepass(false);
            }
            if (jsonData.contains("guard-nodeleterecovery")) {
                guardCapabilities.setNoDeleteRecovery(true);
            } else {
                guardCapabilities.setNoDeleteRecovery(configurationService.getBooleanProperty(GuardProperty.noDeleteRecovery, userId, contextId));
            }

            if (jsonData.contains("guard-norecovery")) {
                guardCapabilities.setNorecovery(true);
            } else {
                guardCapabilities.setNorecovery(configurationService.getBooleanProperty(GuardProperty.noRecovery, userId, contextId));
            }

            if (jsonData.contains("guard-nodeleteprivate")) {
                guardCapabilities.setNoDeletePrivate(true);
            } else {
                guardCapabilities.setNoDeletePrivate(configurationService.getBooleanProperty(GuardProperty.noDeletePrivate, userId, contextId));
            }
            return (guardCapabilities);
        } catch (Exception e) {
            logger.error("Error while fetching user capabilities: " + e.getMessage());
            throw GuardUserExceptionCodes.CAPABILITY_ERROR.create(e, "guard");
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.user.OXUserService#getGuestUser(java.lang.String)
     */
    @Override
    public OXGuardUser getGuestUser(String email) throws OXException {
        EmailStorage emailStorage = Services.getService(EmailStorage.class);
        Email userEmail = emailStorage.getByEmail(email);
        if (userEmail == null) return null;
        OXGuardUser oxUser = new OXGuardUser(userEmail.getUserId(), userEmail.getContextId());
        return oxUser;
    }
}
