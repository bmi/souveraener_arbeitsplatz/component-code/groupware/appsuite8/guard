/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.translation.internal;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.LocaleUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.translation.TemplateTransformation;
import com.openexchange.guard.translation.exceptions.GuardTranslationExceptionCodes;
import com.openexchange.guard.translation.osgi.Services;

/**
 * {@link GuardTranslationServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardTranslationServiceImpl implements GuardTranslationService {

    private static final String HELP_ATTACHMENT_PREFIX = "help_attachment_";

    private static final String HELP_ATTACHMENT_FOLDER = "guard";

    private static final Logger LOG = LoggerFactory.getLogger(GuardTranslationServiceImpl.class);

    private final Map<String, Map<String, String>> availableLanguages;

    private final Map<String, Map<String, Translation>> languageMaps;

    private String defaultCode;

    /**
     * Initialises a new {@link GuardTranslationServiceImpl}.
     *
     * @throws OXException if the GuardConfigurationService is absent
     */
    public GuardTranslationServiceImpl() throws OXException {
        super();
        availableLanguages = new HashMap<String, Map<String, String>>();
        languageMaps = new HashMap<String, Map<String, Translation>>();

        loadAvailableLanguages();
        loadTranslationFiles();
    }

    /**
     * Load available languages into local memory
     *
     * @throws OXException if the GuardConfigurationService is absent
     */
    private void loadAvailableLanguages() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String l10nDirectory = configService.getProperty("i18n.language.path");
        if (null == l10nDirectory) {
            throw OXException.notFound("Configuration property 'i18n.language.path' is not defined.");
        }
        // Get and set the default code
        defaultCode = configService.getProperty(GuardProperty.defaultLanguage);
        try {
            // Read the languages file
            FileInputStream inputStream = new FileInputStream(l10nDirectory + File.separator + "languages.xml");
            String xmlfile;
            try {
                xmlfile = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
            // Build the XML document
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(xmlfile.getBytes(StandardCharsets.UTF_8))));
            doc.getDocumentElement().normalize();

            // Read all available languages
            File[] mappings = new File(l10nDirectory).listFiles();  // Load filelist of i18 director
            Arrays.sort(mappings, Collections.reverseOrder());
            NodeList languages = doc.getElementsByTagName("language");
            for (int i = 0; i < languages.getLength(); i++) {
                Node lang = languages.item(i);
                NodeList elements = lang.getChildNodes();
                String id = elements.item(1).getTextContent();

                // Make sure our translation files actually exists
                if (mappings != null && mappings.length > 0) {
                    boolean found = false;
                    for (int f = 0; f < mappings.length; f++) {
                        File mapping = mappings[f];
                        if (mapping.getName().endsWith("templates-" + id + ".po")) {  // Look for any files that are templates in this langauge
                            LOG.info("Loading translation file {}", mapping.getName());
                            found = true;
                            // Load the actual PO file, a.k.a. the translation
                            loadPOFile(mapping.getAbsolutePath(), id);
                        }
                    }
                    if (found) {
                        // Load the translations of the available languages
                        Map<String, String> translated = new HashMap<String, String>();
                        for (int j = 2; j < elements.getLength(); j++) {
                            String langcode = elements.item(j).getNodeName();
                            if (langcode != "#text") {
                                String value = elements.item(j).getTextContent();
                                translated.put(langcode, value); // Put the country name to the translated list by tag
                            }
                        }
                        availableLanguages.put(id, translated);
                    } else {
                        LOG.info("Unable to load translation file {}", "templates-" + id + ".po");
                    }

                } else {
                    LOG.info("Unable to load translation file {}", "templates-" + id + ".po");
                }
            }
        } catch (IOException e) {
            LOG.error("Error loading available translations", e);
        } catch (ParserConfigurationException | SAXException e) {
            LOG.error("", e);
        }
    }

    /**
     * Load the translation files
     *
     * @throws OXException
     */
    private void loadTranslationFiles() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String l10nDirectory = configService.getProperty("i18n.language.path");
        String domain = "guard";

        File i18folder = new java.io.File(l10nDirectory);
        File[] list = i18folder.listFiles();

        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                // Make sure for the domain specified
                if (list[i].getName().startsWith(domain) || (domain.equals(""))) {
                    String name = list[i].getName();
                    // Only load the PO files
                    if (name.endsWith(".po")) {
                        String lang = name;
                        if (name.contains("-")) {
                            lang = name.substring(name.indexOf("-") + 1);
                        }
                        if (lang.contains(".")) {
                            lang = lang.substring(0, lang.indexOf("."));
                        }
                        loadPOFile(list[i].getAbsolutePath(), lang);
                    }
                }
            }
        } else {
            LOG.error("Could not load Guard PO files from non existing directory " + l10nDirectory);
        }
    }

    /**
     * Load the specified translation file
     */
    private void loadPOFile(String filename, String lanuage) {
        Map<String, Translation> mapping = languageMaps.containsKey(lanuage) ? languageMaps.get(lanuage) : new HashMap<String, Translation>();

        Path path = Paths.get(filename);
        File f = new File(filename);
        if (!f.exists()) {
            LOG.error("Unable to load file " + filename);
            return;
        }
        try {
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).contains("msgid")) { // First message ID line
                    StringBuilder sb = new StringBuilder();
                    sb.append(getBetweenQuotes(lines.get(i)));
                    i++;
                    String line2 = lines.get(i);
                    while (line2.trim().startsWith("\"")) {
                        sb.append(getBetweenQuotes(line2));
                        i++;
                        line2 = lines.get(i);
                    }
                    String id = sb.toString();
                    ArrayList<String> plurals = new ArrayList<String>();
                    if (!id.isEmpty()) {
                        if (line2.contains("msgid_plural")) {// Pleural based field
                            String plural = getBetweenQuotes(line2);
                            i++;
                            while (lines.get(i).contains("msgstr")) {
                                plurals.add(getBetweenQuotes(lines.get(i)));
                                i++;
                            }
                            Translation trans = new Translation(id, plural, plurals);
                            mapping.put(id, trans);
                            mapping.put(plural, trans);
                        } else {
                            // Singular based field
                            String data = lines.get(i);
                            if (data.contains("msgstr")) {
                                StringBuilder trsb = new StringBuilder();
                                trsb.append(getBetweenQuotes(data));

                                while (((i + 1) < lines.size()) && lines.get(i + 1).trim().startsWith("\"")) {
                                    i++;
                                    trsb.append(getBetweenQuotes(lines.get(i)));
                                }
                                String transl = trsb.toString();
                                Translation trans = new Translation(id, transl);
                                mapping.put(id, trans);
                            }
                        }
                    }
                }
            }

            languageMaps.put(lanuage, mapping);
        } catch (Exception ex) {
            LOG.error("Error loading .po file {} for language {}", filename, lanuage, ex);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getHelpFile(java.lang.String)
     */
    @Override
    public String getHelpFile(String language) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String l10nDirectory = configService.getProperty("i18n.language.path");
        if (l10nDirectory.endsWith("/")) {
            l10nDirectory = l10nDirectory.substring(0, l10nDirectory.length() - 1);
        }
        if (language == null || language.equals("")) {
            language = defaultCode;
        }
        String filename = l10nDirectory + File.separator + HELP_ATTACHMENT_FOLDER + File.separator + HELP_ATTACHMENT_PREFIX + language + ".txt";
        File f = new File(filename);
        if (f.exists()) {
            return readFile(filename);
        } else {
            LOG.warn("No help file found for language {}", language);
            filename = l10nDirectory + File.separator + HELP_ATTACHMENT_FOLDER + File.separator + HELP_ATTACHMENT_PREFIX + defaultCode + ".txt";
            f = new File(filename);
            if (f.exists()) {
                return readFile(filename);
            } else {
                LOG.error("Unable to load default help file");
                return null;
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getTemplateTranslation(java.lang.String, java.lang.String, java.util.List)
     */
    @Override
    public String getTemplateTranslation(String templateName, String language, List<TemplateTransformation> templateTransformations) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String template = configService.getProperty(GuardProperty.templatesDirectory) + File.separator + templateName;
        language = getLanguage(language);
        File f = new File(template);
        if (!f.exists()) {
            LOG.info("Unable to load file " + template);
            throw GuardTranslationExceptionCodes.TEMPLATE_NOT_AVAILABLE.create(template, language, 0);
        }
        String templ = readFile(template);
        if (templateTransformations != null) {
            for (TemplateTransformation transformation : templateTransformations) {
                templ = transformation.transformTemplate(templ);
            }
        }
        int counter = 0;
        while (templ.contains("<$gettext") && (counter < 1000)) {
            int i = templ.indexOf("<$gettext(");
            int j = templ.substring(i).indexOf(")>");
            if (j < 0) {
                LOG.error("Poorly formed template");
                return (null);
            }

            String toreplace = templ.substring(i, i + j + 2);
            String totranslat = removeQuotes(toreplace.replace("<$gettext(", "").replace(")>", ""));
            templ = templ.replace(toreplace, getTranslation(totranslat, language));
        }
        return (templ);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getTranslation(java.lang.String, java.lang.String)
     */
    @Override
    public String getTranslation(String text, String language) {
        try {
            String lang = getLanguage(language);
            if (lang == null) {
                LOG.error("Missing default language setting");
                return text;
            }
            Map<String, Translation> languageMap = languageMaps.get(lang);
            if (languageMap != null) {
                Translation trans = languageMap.get(text);
                if (trans != null) {
                    String translation = trans.getTranslation();
                    if (Strings.isNotEmpty(translation)) { // only if translation is available. Ignore empty ones.
                        return translation;
                    }
                    return text;
                }
            }
            return text;
        } catch (Exception ex) {
            LOG.error("Error translating '{}' into {}", text, language, ex);
            return text;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getTranslation(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String getTranslation(String text, String language, String variable) {
        String translated = getTranslation(text, language);
        return translated.replace("%s", variable);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getTranslation(java.lang.String, java.lang.String, int)
     */
    @Override
    public String getTranslation(String template, String language, int templateId) throws OXException {
        return getTranslation(template, language, templateId, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getTranslation(java.lang.String, java.lang.String, int, java.util.List)
     */
    @Override
    public String getTranslation(String template, String language, int templateId, List<TemplateTransformation> templateTransformations) throws OXException {
        if (templateId == 0) {
            return getTemplateTranslation(template, language, templateTransformations);
        }
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String templatesDirectory = configService.getProperty(GuardProperty.templatesDirectory);
        File f = new File(templatesDirectory + File.separator + templateId + "-" + template);
        if (f.exists()) {
            return getTemplateTranslation(templateId + "-" + template, language, templateTransformations);
        } else {
            LOG.warn("no template found for custom template " + template + " id " + templateId);
            String templateTranslation = getTemplateTranslation(template, language, templateTransformations);
            if (templateTranslation != null) {
                return templateTranslation;
            }
            throw GuardTranslationExceptionCodes.TEMPLATE_NOT_AVAILABLE.create(template, language, templateId);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#customTemplateExists(int, java.lang.String)
     */
    @Override
    public boolean customTemplateExists(int templateId, String template) throws OXException {
        if (templateId == 0) {
            return false;
        }

        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String templatesDirectory = configService.getProperty(GuardProperty.templatesDirectory);

        File f = new File(templatesDirectory + File.separator + templateId + "-" + template);
        if (f.exists()) {
            return true;
        } else {
            LOG.warn("no custom templates found for teplate id " + templateId);
            return false;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getAvailableCode(java.lang.String)
     */
    @Override
    public String getAvailableCode(String code) {
        if (code == null || code.equals("")) {
            return defaultCode;
        }
        if (availableLanguages.containsKey(code)) {
            return (code);
        }
        if (availableLanguages.containsKey(code.substring(0, 2))) {
            return (code.substring(0, 2));
        }
        // Not available, so return default
        return defaultCode;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getAvailableLocale(java.lang.String)
     */
    @Override
    public Locale getAvailableLocale(String code) {
        return LocaleUtil.getLocalFor(getAvailableCode(code));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.translation.GuardTranslationService#getAvailableLanguages(java.lang.String)
     */
    @Override
    public Map<String, String> getAvailableLanguages(String code) {
        Map<String, String> available = new HashMap<String, String>();
        Iterator<Entry<String, Map<String, String>>> it = availableLanguages.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Map<String, String>> entry = it.next();
            Map<String, String> value = entry.getValue();
            String avail = null;
            // Find what locale is available
            if (value.containsKey(code)) {
                avail = code;
            } else {
                if (value.containsKey(code.substring(0, 2))) {
                    avail = code.substring(0, 2);
                } else {  // Try defaults
                    if (value.containsKey(defaultCode)) {
                        avail = defaultCode;
                    } else {
                        if (value.containsKey(defaultCode.substring(0, 2))) {
                            avail = defaultCode.substring(0, 2);
                        }
                    }
                }
            }
            if (avail != null) {  // If available, add
                available.put(entry.getKey(), value.get(avail));
            }
        }
        return available;
    }

    /////////////////////////////////// HELPERS /////////////////////////////////////////

    /**
     * Check if we have that language translation. If not, return default
     *
     * @param language
     * @return
     */
    private String getLanguage(String language) {
        if ((language == null) || language.length() < 2) {
            return defaultCode;
        }
        if (!languageMaps.containsKey(language)) {
            // If we don't have the 4 character translation, see if we have 2
            if (languageMaps.containsKey(language.substring(0, 2))) {
                language = language.substring(0, 2);
            } else {
                // If we don't have that language, go to default
                language = defaultCode;
            }
        }
        return language;
    }

    /**
     * Read the specified file
     *
     * @param fileName
     * @return
     * @throws OXException
     * @throws IOException
     */
    private String readFile(String fileName) throws OXException {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                return sb.toString();
            } finally {
                br.close();
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    private String getBetweenQuotes(String id) {
        if (id.contains("\"")) {
            id = id.substring(id.indexOf("\"") + 1);
            if (id.contains("\"")) {
                id = id.substring(0, id.indexOf("\""));
            }
        }
        return (id);
    }

    private String removeQuotes(String data) {
        if (data.startsWith("\"")) {
            data = data.substring(1);
            if (data.endsWith("\"")) {
                data = data.substring(0, data.length() - 1);
            }
        }
        if (data.startsWith("'")) {
            data = data.substring(1);
            if (data.endsWith("'")) {
                data = data.substring(0, data.length() - 1);
            }
        }
        return (data);
    }
}
