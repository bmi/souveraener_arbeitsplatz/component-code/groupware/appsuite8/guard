/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.impl;


/**
 * {@link SmimeStorageSql}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
class SmimeStorageSql {

    public final static String INSERT_SMIME_KEY = "INSERT INTO smime_pub (serial, email, userid, cid, publicKey, chain, expires, userKey, lastSeen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE lastSeen = ?";
    public final static String INSERT_SMIME_KEY_LOCAL =
        "INSERT INTO smime_pub (serial, email, userid, cid, publicKey, chain, expires, userKey, lastSeen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE userid = ?, cid = ?, userKey = userKey || ?, lastSeen = ?";

    public final static String SELECT_PUBLIC_SERIAL_EMAIL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE serial = ? AND email = ?";
    public final static String SELECT_ALL_PUBLIC_KEYS = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub;";
    public final static String SELECT_PUBLIC_KEY_EMAIL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE email = ?";
    public final static String SELECT_PUBLIC_KEY_SERIAL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE serial = ? AND userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_KEY_SERIAL = "DELETE from smime_pub WHERE serial = ? AND userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_BY_USER = "DELETE from smime_pub WHERE userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_BY_CONTEXT = "DELETE from smime_pub WHERE cid = ?";
    public final static String UPDATE_LAST_SEEN = "UPDATE smime_pub SET `lastSeen` = ? WHERE serial = ? AND userid = ? AND cid = ?";

}
