/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.custom;

import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.internal.Services;
import liquibase.change.custom.CustomSqlChange;
import liquibase.database.Database;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;

/**
 * A {@link CustomSqlChange} that initialize the global settings table
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class InitGlobalSettings implements CustomSqlChange {

    @SuppressWarnings("unused")
    private ResourceAccessor resourceAccessor;

    private int version = -1;

    /**
     * {@inheritDoc}
     */
    @Override
    public SqlStatement[] generateStatements(Database database) {
        return new SqlStatement[] { new RawSqlStatement("INSERT INTO global_settings VALUES ('version','" + version + "');") };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConfirmationMessage() {
        return "global_settings table successfully initialized.";
    }

    /**
     * {@inheritDoc}
     *
     * @throws SetupException
     */
    @Override
    public void setUp() throws SetupException {
        version = Services.getService(GuardConfigurationService.class).getIntProperty(GuardProperty.version);
        if (version == -1) {
            throw new SetupException("Cannot find version to set within guard configuration (" + GuardProperty.version.getFQPropertyName() + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        this.resourceAccessor = resourceAccessor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }
}
