/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines a handler interface which is called for encrypting chunk of streamed data
 *
 * @author Benjamin Gruedelbach
 */
public interface StreamHandler {

    /**
     * Process the input stream and writes the processed data to the output stream
     *
     * @param inputStream the stream of data to process
     * @param outputStream the output stream to write the processed data top
     * @param suggestedBufferSize a suggested size of the chunks to read from the inputStream
     * @exception IOException on error writing to the outputStream
     */
    public void processStream(InputStream inputStream, OutputStream outputStream, int suggestedBufferSize) throws IOException;

    /**
     * Returns the new content type for the uploading stream based on the old content type
     * and filename
     *
     * @param oldContentType The old content type of the item to process
     * @return the new content type of the stream
     */
    public String getNewContentType(String oldContentType, String fileName);

    /**
     * Creates a name for the new stream based on the old name
     *
     * @param oldName the old name from which the new name will be created
     * @return the new name of the new stream item
     */
    public String getNewName(String oldName);
}
