/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.servlets.certificatemanagement.actions;

import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.PARAM_NEW_PASSWORD;
import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.PARAM_SERIAL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.responses.BinaryResponse;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.SmimeKeysExport;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link DownloadCertificateAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class DownloadCertificateAction extends GuardServletAction {

    @SuppressWarnings("resource")
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        final SmimeKeyService keyService = Services.getService(SmimeKeyService.class);
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession);
        final String password = new String(userIdentity.getPassword());

        getAntiAbuseWrapper(request, userSession, password).doAction(() -> {
            final String serial = ServletUtils.getStringParameter(request, PARAM_SERIAL, false);
            final String newPassword = ServletUtils.hasParameter(request, PARAM_NEW_PASSWORD) ? ServletUtils.getStringParameter(request, PARAM_SERIAL, false) : password;

            try (SmimeKeysExport exportData = keyService.exportKey(serial, userSession.getUserId(), userSession.getContextId(), password, newPassword)) {
                //@formatter:off
                String filename = (exportData.getCertificate() != null && exportData.getCertificate().getEmail() != null) ?
                                  exportData.getCertificate().getEmail() + ".pfx" :
                                  "key.pfx";
                //@formatter:on
                new BinaryResponse(response).send(exportData.getData(), true, filename);
                return null;
            }
        });
    }
}
