/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.smime.impl;

import java.io.IOException;
import java.security.cert.X509Certificate;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.smime.SMIMECapabilities;
import org.bouncycastle.cms.SignerInformation;
import com.openexchange.exception.OXException;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;

/**
 * {@link SMIMECapabilitiesParser} - Simple helper class for parsing S/MIME capabilities
 * from a message's SignerInfo (RFC 8551) as well as from a X509 certificate's capabilities extension (RFC 4262).
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
class SMIMECapabilitiesParser {

    /**
     * Internal method to parse {@link SMIMECapabilities} from the given {@link ASN1Encodable} structure
     *
     * @param capabilityData The data to parse the capabilities from
     * @return The parsed capabilities as {@link SMIMECapabilities} instance
     * @throws OXException due an parsing or validation error
     */
    private static SMIMECapabilities parseCapabilities(ASN1Encodable[] capabilityData) throws OXException {
        //There must only be one element which itself is a sequence
        if (capabilityData != null && capabilityData.length == 1) {
            ASN1Encodable capabilityList = capabilityData[0];
            try {
                return SMIMECapabilities.getInstance(capabilityList);
            } catch (IllegalArgumentException e) {
                throw SmimeExceptionCodes.CRYPTO_ERROR.create(e, "Unable to parse SMIMECapabilities");
            }
        }
        throw SmimeExceptionCodes.CRYPTO_ERROR.create("Unable to parse SMIMECapabilities due multipe ambiguous attribues.");
    }

    /**
     * Parses {@link SMIMECapabilities} from the given {@link SignerInformation}
     *
     * @param signerInformation The signer information extracted from a signed message
     * @return The parsed {@link SMIMECapabilities}, or null if no capabilities were found within the given information
     * @throws OXException if an parsing error occurs
     */
    public static SMIMECapabilities parseCapabilities(SignerInformation signerInformation) throws OXException {
        if (signerInformation != null) {
            AttributeTable signedAttributes = signerInformation.getSignedAttributes();
            if (signedAttributes != null) {
                //It is save to assume that there is only one SMIME capability attribute (RFC 8551 - 2.5.2)
                Attribute attributeData = signedAttributes.get(PKCSObjectIdentifiers.pkcs_9_at_smimeCapabilities);
                if (attributeData != null) {
                    ASN1Encodable[] capabilityData = attributeData.getAttributeValues();
                    return parseCapabilities(capabilityData);
                }
            }
        }
        return null;
    }

    /**
     * Parses {@link SMIMECapabilities} from the given {@link X509Certificate}.
     * <p>
     * This method tries to parse {@link SMIMECapabilities} from the capabilities extension (as described in RFC 4262) present in the given {@link X509Certificate}s if available.
     * </p>
     *
     * @param certificate The {@link X509Certificate} to get the {@link SMIMECapabilities} from
     * @return The {@link SMIMECapabilities} parsed from the certificate's capabilities extension, or null if no such extension is present.
     * @throws OXException if an parsing error occurs
     */
    public static SMIMECapabilities parseCapabilities(X509Certificate certificate) throws OXException {
        if (certificate != null) {
            //Get Capabilities from certificate extension if available
            byte[] rawExtensionData = certificate.getExtensionValue(PKCSObjectIdentifiers.pkcs_9_at_smimeCapabilities.getId());
            if (rawExtensionData != null) {
                try {
                    ASN1Primitive extensionData = ASN1Primitive.fromByteArray(ASN1OctetString.getInstance(rawExtensionData).getOctets());
                    //Wrap so that it matches same structure than a signed attribute
                    ASN1Encodable[] wrappedExntensionData = new ASN1Encodable[] { extensionData };
                    //Parse
                    return parseCapabilities(wrappedExntensionData);
                } catch (IllegalArgumentException | IOException e) {
                    throw SmimeExceptionCodes.CRYPTO_ERROR.create(e, "Unable to parse SMIMECapabilities");
                }
            }
        }
        return null;
    }
}
