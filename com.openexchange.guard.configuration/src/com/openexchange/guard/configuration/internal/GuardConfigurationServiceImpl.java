/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration.internal;

import java.io.File;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.config.cascade.ComposedConfigProperty;
import com.openexchange.config.cascade.ConfigView;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProp;
import com.openexchange.guard.configuration.osgi.Services;
import com.openexchange.server.ServiceLookup;
import com.openexchange.serverconfig.ServerConfigService;

/**
 * {@link GuardConfigurationServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardConfigurationServiceImpl implements GuardConfigurationService {

    private static final Logger logger = LoggerFactory.getLogger(GuardConfigurationServiceImpl.class);

    private final ServiceLookup services;

    /**
     * All reloadable properties
     */
    private static final String[] PROPERTIES = new String[] { "all properties in file" };

    /**
     * Initialises a new {@link GuardConfigurationServiceImpl}.
     */
    public GuardConfigurationServiceImpl(ServiceLookup services) {
        super();
        this.services = services;
        reloadConfiguration(getConfigService());
    }

    /**
     * Get configuration service
     * getConfigService
     *
     * @return
     */
    private ConfigurationService getConfigService() {
        return services.getService(ConfigurationService.class);
    }
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        // no-op
    }

    @Override
    public String getProperty(String property) {
        return getServerProperty(property, String.class, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfiguration#getProperty(com.openexchange.guard.config.GuardProp)
     */
    @Override
    public String getProperty(GuardProp property) {
        return getServerProperty(property.getFQPropertyName(), String.class, property.getDefaultValue(String.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.configuration.GuardConfigurationService#getPropertyFromFile(java.lang.String, java.lang.String)
     */
    @Override
    public String getPropertyFromFile(String fileName, String property) {
        Properties properties = getConfigService().getFile(fileName);
        return properties.getProperty(property);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfiguration#getIntProperty(com.openexchange.guard.config.GuardProp)
     */
    @Override
    public int getIntProperty(GuardProp property) {
        return getServerProperty(property.getFQPropertyName(), int.class, property.getDefaultValue(Integer.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfiguration#getBooleanProperty(com.openexchange.guard.config.GuardProp)
     */
    @Override
    public boolean getBooleanProperty(GuardProp property) {
        return getServerProperty(property.getFQPropertyName(), boolean.class, property.getDefaultValue(Boolean.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getProperty(com.openexchange.guard.config.GuardProp, int, int)
     */
    @Override
    public String getProperty(GuardProp property, int userId, int contextId) {
        return getProperty(property.getFQPropertyName(), userId, contextId, property.getDefaultValue(String.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getIntProperty(com.openexchange.guard.config.GuardProp, int, int)
     */
    @Override
    public int getIntProperty(GuardProp property, int userId, int contextId) {
        return getIntProperty(property.getFQPropertyName(), userId, contextId, property.getDefaultValue(Integer.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getBooleanProperty(com.openexchange.guard.config.GuardProp, int, int)
     */
    @Override
    public boolean getBooleanProperty(GuardProp property, int userId, int contextId) {
        return getBooleanProperty(property.getFQPropertyName(), userId, contextId, property.getDefaultValue(Boolean.class));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getProperty(java.lang.String, int, int)
     */
    @Override
    public String getProperty(String property, int userId, int contextId, String defaultValue) {
        return getUserProperty(property, userId, contextId, String.class, defaultValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getIntProperty(java.lang.String, int, int)
     */
    @Override
    public int getIntProperty(String property, int userId, int contextId, int defaultValue) {
        return getUserProperty(property, userId, contextId, int.class, defaultValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.config.GuardConfigurationService#getBooleanProperty(java.lang.String, int, int)
     */
    @Override
    public boolean getBooleanProperty(String property, int userId, int contextId, boolean defaultValue) {
        return getUserProperty(property, userId, contextId, boolean.class, defaultValue);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.configuration.GuardConfigurationService#getPropertyFile(java.lang.String)
     */
    @Override
    public File getPropertyFile(String propertyFileName) {
        // TODO: Make sysenv-aware
        return getConfigService().getFileByName(propertyFileName);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.configuration.GuardConfigurationService#getPropertyDirectory(java.lang.String)
     */
    @Override
    public File getPropertyDirectory(String propertyDirectory) {
        // TODO: Make sysenv-aware
        return getConfigService().getDirectory(propertyDirectory);
    }

    @Override
    public <T> T getServerConfiguration(String host, String configKey) throws OXException {
        return getServerConfiguration(host, 0, 0, configKey);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getServerConfiguration(String host, int userid, int cid, String configKey) throws OXException {
        ServerConfigService serverConfig = Services.getService(ServerConfigService.class);
        com.openexchange.serverconfig.ServerConfig config = serverConfig.getServerConfig(host, userid, cid);
        return (T) config.asMap().get(configKey);
    }

    /**
     * Gets the value T of specified server property and coerce it to the specified type T
     *
     * @param property The name of the property
     * @param coerceTo The type T to coerce the value of the property to
     * @param defaultValue The default value to return if the given property is not defined.
     * @return The value of the given property, or the default value if the property is not defined
     */
    private <T> T getServerProperty(String property, Class<T> coerceTo, T defaultValue) {
        try {
            ConfigViewFactory factory = Services.getService(ConfigViewFactory.class);
            ConfigView view = factory.getView();
            ComposedConfigProperty<T> p = view.property(property, coerceTo);
            if (!p.isDefined()) {
                return defaultValue;
            }
            return p.get();
        } catch (OXException e) {
            logger.error("Error getting '{}' property. Returning the default value of '{}'", property, defaultValue, e);
        }
        return defaultValue;
    }

    /**
     * Get the value T of specified property for the specified user in the specified context and coerce it to the specified type T
     *
     * @param property The property's name
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param coerceTo The type T to coerce the value of the property
     * @return The value T of the property
     * @throws OXException
     */
    private <T> T getUserProperty(String property, int userId, int contextId, Class<T> coerceTo, T defaultValue) {
        if (contextId == 0) {
            return getServerProperty(property, coerceTo, defaultValue);
        }
        if (contextId < 0) {
            contextId = -contextId;
            userId = -1;
        }

        try {
            ConfigViewFactory factory = Services.getService(ConfigViewFactory.class);
            ConfigView view = factory.getView(userId, contextId);

            ComposedConfigProperty<T> p = view.property(property, coerceTo);
            if (!p.isDefined()) {
                return defaultValue;
            }

            return p.get();
        } catch (OXException e) {
            logger.error("Error getting '{}' property for user '{}' in context '{}'. Returning the default value of '{}'", property, userId, contextId, defaultValue, e);
        }
        return defaultValue;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    @Override
    public Interests getInterests() {
        return DefaultInterests.builder().propertiesOfInterest("com.openexchange.guard.*").configFileNames("guard-core.properties", "server.properties").build();
    }

}
