/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import javax.mail.internet.MimeMessage;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link SMIMESigatureVerificationService} Verifies signed mime messages
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SMIMESigatureVerificationService implements MimeSignatureVerificationService {

    private final SignatureVerifier verifier;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link SMIMESigatureVerificationService}.
     *
     * @param services The service lookup
     */
    public SMIMESigatureVerificationService(ServiceLookup services) {
        this.services = services;
        this.verifier = new SignatureVerifier(services);
    }

    private Locale getLocale(UserIdentity userId) {
        UserService userService = services.getService(UserService.class);
        if (userService != null) {
            User user;
            try {
                user = userService.getUser(userId.getOXUser().getId(), userId.getOXUser().getContextId());
            } catch (OXException e) {
                return null;
            }
            return user.getLocale();
        }
        return null;
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.SMIME;
    }

    @Override
    public List<SignatureVerificationResult> verify(MimeMessage mimeMessage, UserIdentity user, boolean decrypted) throws OXException {
        try {
            return verifier.verify(mimeMessage, user);
        } catch (OXException e) {
            return Collections.singletonList(new SMIMESignatureResult(false, e.getDisplayMessage(getLocale(user))));
        } catch (Exception e) {
            return Collections.singletonList(new SMIMESignatureResult(false, e.getMessage()));
        }
    }
}
