/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.crypto;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.auth.AuthUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.SignatureVerificationResultUtil;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link VerifyMimeEmailAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class VerifyMimeEmailAction extends AbstractCryptoServletAction {

    private static final String MESSAGE_FIELD_NAME = "file";

    private static GuardParsedMimeMessage parseMimeMessage(HttpServletRequest request, InputStream message, int userId, int contextId) throws MessagingException, JSONException, OXException, IOException {
        return parse(request, null, message, userId, contextId, null, null, false);
    }

    /**
     * Just sends a simple json data with true/false if signature verifies.
     * Returns data: missing if no signatures found
     * sendSimpleResponse
     *
     * @param response
     * @param verificationResults
     */
    private static void sendSimpleResponse(HttpServletResponse response, List<SignatureVerificationResult> verificationResults) {
        if (verificationResults == null) {
            ServletUtils.sendObject(response, "missing");
            return;
        }
        boolean verified = false;
        for (SignatureVerificationResult result : verificationResults) {
            verified = verified || result.isVerified();
        }
        ServletUtils.sendObject(response, B(verified));
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        RangeInputValidator<Integer> positiveValidator = new RangeInputValidator<Integer>(I(0), I(Integer.MAX_VALUE));

        final boolean mandatory = true;
        final int userId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "user", mandatory), "user"));
        final int contextId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "context", mandatory), "context"));
        final boolean simpleResponse = ServletUtils.getBooleanParameter(request, "simple");

        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        try (InputStream messageStream = fileUploadHandler.getFileItemStreamFrom(items, MESSAGE_FIELD_NAME, true);) {
            final GuardParsedMimeMessage parsedMimeMessage = parseMimeMessage(request, messageStream, userId, contextId);
            UserIdentity userIdentity = AuthUtils.getUserIdentityFrom(userId, contextId, userId, contextId);

            final MimeMessage messageToVerify = parsedMimeMessage.getMessage();

            CryptoManager manager = Services.getService(CryptoManager.class);
            CryptoType.PROTOCOL cryptoType = getCryptoType(request, parsedMimeMessage);
            MimeSignatureVerificationService mimeSigningService = manager.getSignatureVerificationService(cryptoType);
            if (mimeSigningService == null) {
                // No service response
                throw GuardCoreExceptionCodes.UNKOWN_SIGNATURE_TYPE.create();
            }
            messageToVerify.removeHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT);
            List<SignatureVerificationResult> verificationResults = mimeSigningService.verify(messageToVerify, userIdentity, false);
            if (simpleResponse) {
                sendSimpleResponse(response, verificationResults);
                return;
            }
            if (verificationResults != null) {
                String[] headers = SignatureVerificationResultUtil.toHeaders(verificationResults);
                for (String header : headers) {
                    messageToVerify.addHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT, header);
                }
            }
            messageToVerify.writeTo(response.getOutputStream());
        } catch (OXException ex) {
            if (simpleResponse && ex.similarTo(PGPCoreServicesExceptionCodes.SIGNATURE_ERROR_NO_SIGNED_DATA_IN_EMAIL)) {
                sendSimpleResponse(response, null);
                return;
            }
            throw ex;
        }
    }

}
