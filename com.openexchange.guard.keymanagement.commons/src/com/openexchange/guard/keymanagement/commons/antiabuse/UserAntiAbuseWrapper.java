/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.antiabuse;

import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.ReportValue;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;

/**
 * {@link UserAntiAbuseWrapper}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.8.0
 */
public class UserAntiAbuseWrapper implements AntiAbuseWrapper {

    private final GuardAntiAbuseService antiAbuseService;
    private final AllowParameters allowParams;

    /**
     * Initializes a new {@link UserAntiAbuseWrapper}.
     * @param antiAbuseService
     */
    public UserAntiAbuseWrapper(GuardAntiAbuseService antiAbuseService, AllowParameters allowParams) {
        this.antiAbuseService = antiAbuseService;
        this.allowParams = allowParams;
    }

    @Override
    public <T> T doAction(AntiAbuseAction<T> action) throws Exception {
        if(allowParams.getLogin() != null){
            if(antiAbuseService.allowLogin(allowParams)){
                try {
                    T ret = action.doAction();
                    antiAbuseService.report(AntiAbuseUtils.getReportParameter(ReportValue.SUCCESS, allowParams));
                    return ret;
                }
                catch(OXException e) {
                    if(GuardAuthExceptionCodes.BAD_PASSWORD.equals(e)) {
                        antiAbuseService.report(AntiAbuseUtils.getReportParameter(ReportValue.FAILURE, allowParams));
                    }
                    throw e;
                }
            }
            else {
                throw GuardAuthExceptionCodes.LOCKOUT.create();
            }
        }
        else {
            antiAbuseService.report(AntiAbuseUtils.getReportParameter(ReportValue.FAILURE, allowParams));
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
    }
}
