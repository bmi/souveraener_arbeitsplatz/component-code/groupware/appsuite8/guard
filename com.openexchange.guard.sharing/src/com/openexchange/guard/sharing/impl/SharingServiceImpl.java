/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.sharing.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.json.JSONException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.sharing.SharingService;
import com.openexchange.guard.sharing.exceptions.GuardSharingExceptionCodes;
import com.openexchange.guard.sharing.osgi.Services;
import com.openexchange.guard.user.OXUser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.packethandling.PacketProcessor;
import com.openexchange.pgp.core.packethandling.RelaceAllRecipientsPacketProcessorHandler;
import com.openexchange.pgp.core.packethandling.SecretKeyForPacketService;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link SharingServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class SharingServiceImpl implements SharingService {

    /* (non-Javadoc)
     * @see com.openexchange.guard.sharing.SharingService#shareFile(java.io.InputStream, org.json.JSONObject, com.openexchange.guard.user.UserIdentity, java.io.OutputStream)
     */
    @Override
    public void shareFile(InputStream origData, List<Integer> userIds, UserIdentity userIdentity, OutputStream out) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        // Pull current key here for re-encryption
        GuardKeys owner = keyService.getKeys(userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId());
        if (owner == null) {
            throw GuardSharingExceptionCodes.OWNER_KEY_NOT_FOUND.create();
        }
        SecretKeyForPacketService keyPacketService = new SecretKeyForPacketImpl(userIdentity);
        try {
            List<PGPPublicKey> keys = getPublicKeys(owner, userIds, keyService);
            PacketProcessor processor = new PacketProcessor();
            processor.process(
                origData,
                out,
                new RelaceAllRecipientsPacketProcessorHandler(keyPacketService, keys.toArray(new PGPPublicKey[keys.size()])),
                false);
        } catch (Exception e) {
            throw GuardSharingExceptionCodes.PROBLEM_UPDATING_ENCRYPTION.create();
        }

    }

    /**
     * Get a list of public keys for a share.  Create keys if needed.
     * @param ownerKey The owner of the file.  Will be added to the list
     * @param userIds User Ids of the shares.  Will all have the same context as the owner
     * @param keyService  Guard Key Service
     * @return new list of PGPPublic keys
     * @throws JSONException
     * @throws OXException
     */
    private List<PGPPublicKey> getPublicKeys (GuardKeys ownerKey, List<Integer> userIds, GuardKeyService keyService) throws JSONException, OXException {
        ArrayList<PGPPublicKey> keys = new ArrayList<PGPPublicKey>();
        keys.add(new RecipKey(ownerKey).getEncryptionKey());
        UserService userService = Services.getService(UserService.class);
        for (int id : userIds) {
            User oxUser = userService.getUser(id, ownerKey.getContextid());
            if (oxUser == null) {
                throw GuardSharingExceptionCodes.UNABLE_TO_FIND_OX_USER.create();  // Really shouldn't happen
            }
            String email = oxUser.getMail();
            GuardKeys userKey = keyService.getKeys(email);
            if (userKey != null) {
                keys.add(new RecipKey(userKey).getEncryptionKey());
            } else {
                OXUser userCheck = Services.getService(OXUserService.class).getUser(email);  // Let's check if OX user
                RecipKey rKey = new RecipKey();
                if (userCheck != null && !userCheck.isGuest()) {
                    rKey.setCid(userCheck.getContextId());
                    rKey.setUserid(userCheck.getId());
                    rKey.setLang(userCheck.getLanguage());
                    rKey.setName(userCheck.getName());
                    rKey.setEmail(userCheck.getEmail());
                    rKey.setPgp(true);
                } else { // OK, it's going to be a guest user
                    rKey.setCid(ownerKey.getContextid() * -1);
                    rKey.setLang(ownerKey.getLanguage());
                    rKey.setName(oxUser.getDisplayName());
                    rKey.setEmail(email);
                    rKey.setPgp(true);
                    rKey.setGuest(true);
                    rKey.setSenderCid(ownerKey.getContextid());
                    rKey.setSenderUserId(ownerKey.getUserid());
                }
                RecipKey newKey = Services.getService(AccountCreationService.class).createUserFor(rKey);
                if (newKey == null) {
                    throw GuardSharingExceptionCodes.UNABLE_TO_GET_KEY.create(email);
                }
                keys.add(newKey.getEncryptionKey());
            }
        }
        return keys;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.sharing.SharingService#getAdditionalShareRecipients(java.util.List, java.util.List)
     */
    @Override
    public List<String> getAdditionalShareRecipients(List<String> recipients, List<String> emails) throws OXException {
        ArrayList<String> allRecips = new ArrayList<String> ();
        allRecips.addAll(recipients);
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        for (String email: emails) {
            GuardKeys key = keyService.getKeys(email);
            if (key == null) {
                throw GuardSharingExceptionCodes.UNABLE_TO_GET_KEY.create(email);
            }
            String id = key.getContextid() + "/" + key.getUserid();
            if (!allRecips.contains(id)) {  // Avoid duplicates
                allRecips.add(id);
            }

        }
        return allRecips;
    }


}
