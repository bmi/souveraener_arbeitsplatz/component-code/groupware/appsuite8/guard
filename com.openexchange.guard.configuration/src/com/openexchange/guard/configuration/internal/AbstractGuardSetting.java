package com.openexchange.guard.configuration.internal;

import com.openexchange.groupware.settings.PreferencesItemService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.settings.IValueHandler;
import com.openexchange.groupware.settings.ReadOnlyValue;
import com.openexchange.groupware.settings.Setting;
import com.openexchange.groupware.userconfiguration.UserConfiguration;
import com.openexchange.jslob.ConfigTreeEquivalent;
import com.openexchange.session.Session;
import com.openexchange.user.User;

/**
 * Abstract Guard settings to register with JSLOB (UI Settings)
 * @author Greg Hill
 *
 * @param <V>
 */
public abstract class AbstractGuardSetting<V> implements PreferencesItemService, ConfigTreeEquivalent {

     /** The name in setting's path; e.g. <code>"name"</code> */
     protected final String nameInPath;

     /**
     * Initializes a new {@link AbstractGuardSetting}.
     */
     protected AbstractGuardSetting(String nameInPath) {
      super();
      this.nameInPath = nameInPath;
     }

     /**
     * Gets the value for this setting.
     * <p>
     * E.g. use config-cascade to read the value of a certain property.
     *
     * @param session The session
     * @param ctx The context
     * @param user The user
     * @param userConfig The user configuration
     * @return The value
     * @throws OXException If value cannot be returned
     */
     protected abstract V getSettingValue(Session session, Context ctx, User user, UserConfiguration userConfig) throws OXException;

     @Override
     public String[] getPath() {
      return new String[] { "modules", "oxguard", nameInPath };
     }

     @Override
     public IValueHandler getSharedValue() {
      return new ReadOnlyValue() {

        @Override
        public void getValue(Session session, Context ctx, User user, UserConfiguration userConfig, Setting setting) throws OXException {
          Object value = getSettingValue(session, ctx, user, userConfig);
          setting.setSingleValue(value);
        }

        @Override
        public boolean isAvailable(UserConfiguration userConfig) {
          return true;
        }

      };
     }

     @Override
     public String getConfigTreePath() {
      return "modules/oxguard/" + nameInPath;
     }

     @Override
     public String getJslobPath() {
      return "oxguard//" + nameInPath;
     }
    }

