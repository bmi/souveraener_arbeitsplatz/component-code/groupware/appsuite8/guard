/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.AbstractContentBody;

/**
 * A content body which can be used in HTTP request to
 * read from an input stream
 *
 * @author Benjamin Gruedelbach
 */
public class StreamedContentBody extends AbstractContentBody {

    private final InputStream inputStream;
    private final StreamHandler handler;
    private final int inputBufferSize;
    private final String filename;
    private final ContentType contentType;

    /**
     * Constructor
     *
     * @param inputStream An input stream representing the remote file to process
     * @param contentType The content type
     * @param handler A handler defining the concrete data processing
     *            method to apply for the InputStream
     */
    public StreamedContentBody(InputStream inputStream, int inputBufferSize, ContentType contentType, StreamHandler handler, String filename) {
        super(contentType.getMimeType());

        if (inputStream == null) {
            throw new NullPointerException("File inputstream is null");
        }
        if (handler == null) {
            throw new NullPointerException("Encryption handleris null");
        }
        this.contentType = contentType;
        this.inputStream = inputStream;
        this.inputBufferSize = inputBufferSize;
        this.handler = handler;
        this.filename = filename;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void writeTo(OutputStream os) throws IOException {
        handler.processStream(inputStream, os, inputBufferSize);
    }

    @Override
    public long getContentLength() {
        //-1 because of chunked upload
        return -1;
    }

    @Override
    public String getTransferEncoding() {
        return "UTF-8";
    }

    @Override
    public String getCharset() {
        final Charset charset = this.contentType.getCharset();
        return charset != null ? charset.name() : null;
    }
}
