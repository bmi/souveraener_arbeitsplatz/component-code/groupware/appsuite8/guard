/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keyretrieval;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.DeletedKey;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.osgi.Services;

/**
 * Class for handling retrieving deleted keys
 *
 * @author greg
 *
 */
public class KeyRetriever {

    private static Logger LOG = LoggerFactory.getLogger(KeyRetriever.class);

    /**
     * Exports all exposed keys for a given user
     *
     * @param email The email of the user associated with his keyrings
     * @param userId The user's ID
     * @param contextId The user's context ID
     * @param password The password of the private keyring(s)
     * @return An ASCII-Armored representation of the user's private key rings
     * @throws OXException
     */
    public String exportAllKeys(String email, int userId, int contextId, String password) throws OXException {
        DeletedKeysStorage deletedKeysStorage = Services.getService(DeletedKeysStorage.class);
        ArrayList<DeletedKey> keys = deletedKeysStorage.getAllExposedForEmail(email, userId, contextId);
        if (keys != null && keys.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (DeletedKey deletedKey : keys) {
                GuardKeys key = new GuardKeys(deletedKey.getUserId(), deletedKey.getCid(), deletedKey.getSecretKeyRing(), deletedKey.getSalt());
                stringBuilder.append(KeyExportUtil.exportPGPPrivateKey(key, password));
                stringBuilder.append("\r\n");
            }
            LOG.info("Exported {} exposed keys for user {} in context {}", keys.size(), userId, contextId);
            return stringBuilder.toString();
        }
        LOG.info("No exposed keys found for user {} in context {}", userId, contextId);
        return null;
    }
}
