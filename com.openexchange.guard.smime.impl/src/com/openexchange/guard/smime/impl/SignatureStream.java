/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import javax.mail.MessagingException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link SignatureStream} to write and normalize Mime Part
 * Required for outlook to verify SMIME signatures if lines contain non standard line
 * terminators (such as just a \r)
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SignatureStream extends InputStream {

    static final Logger LOG = LoggerFactory.getLogger(SignatureStream.class);

    private final BufferedReader in;  // Input stream from encoded Mime part
    ByteArrayInputStream cleanedLineInputStream;  // An input stream of a line of mime after cleaned
    Thread pipeWriter;

    /**
     * Initialize Signature Stream with a mime part
     * Initializes a new {@link SignatureStream}.
     *
     * @param mimePart
     * @throws IOException
     * @throws MessagingException
     */
    public SignatureStream(InputStream in) throws IOException {
        // Pipe the output from the mime part.writeTo to an inputStream
        final PipedOutputStream out = new PipedOutputStream();
        final PipedInputStream inPut = new PipedInputStream(out);
        pipeWriter = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    IOUtils.copy(in, out);
                    out.close();
                } catch (final IOException e) {
                    LOG.error("Problem writing mime part for signature verification", e);
                }
            }
        });
        pipeWriter.setName("Message Writer");
        pipeWriter.start();
        // Define the input reader
        this.in = new BufferedReader(new InputStreamReader(inPut, "UTF-8"));
        cleanedLineInputStream = null;
    }

    /**
     * Cleanup the filtered stream
     * @throws IOException
     */
    private void cleanupStream() throws IOException {
        if (cleanedLineInputStream != null) {
            cleanedLineInputStream.close();
            cleanedLineInputStream = null;
        }
    }

    /**
     * Normalize a line for signature
     * Correct - - line start, and normalize cr/lf
     * Load up the cleanedLineInputStream
     *
     * @param line
     * @throws IOException
     */
    private void normalizeAndLoadLine (String line) throws IOException {
        if (line == null) {
            cleanupStream();
            return;
        }
        if (line.startsWith("- -")) {
            line = line.substring(2);
        }
        cleanedLineInputStream = new ByteArrayInputStream((StringUtils.stripEnd(line, null) + "\r\n").getBytes("UTF-8"));
    }

    /**
     * Get the next line from the reader
     *
     * @throws IOException
     */
    private void getNextLine() throws IOException {
        cleanupStream();
        final String nextLine = in.readLine();

        if (nextLine != null) {
            normalizeAndLoadLine(nextLine);
        }

    }

    @Override
    public void reset() throws IOException {
        // Not supported
    }

    @Override
    public int read() throws IOException {
        if (cleanedLineInputStream == null) {
            getNextLine();
        }
        if (cleanedLineInputStream == null) {
            return -1;
        }
        final int data = cleanedLineInputStream.read();
        if (data == -1) {
            getNextLine();
            return read();
        }
        return data;
    }

    @Override
    public int available() throws IOException {
        // Not supported or accurate
        return cleanedLineInputStream.available();
    }

    @Override
    public void close() throws IOException {
        in.close();
        if (pipeWriter != null) {
            pipeWriter.interrupt();
        }
    }

    @Override
    public synchronized void mark(int readlimit) {
        // not supported
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public int read(byte[] buf) throws IOException {
        return this.read(buf, 0, buf.length);
    }

    @Override
    public int read(byte[] buf, int off, int len) throws IOException {
        int count = 0;

        if (len == 0) {
            return 0;
        }

        while (count < len) {
            final int ch = this.read();

            if (ch < 0) {
                buf[off + count] = 0;  // Mark the end of the buffer array
                break;
            }

            buf[off + count] = (byte) ch;
            count++;
        }

        if (count == 0) {
            return -1;
        }

        return count;
    }

}
