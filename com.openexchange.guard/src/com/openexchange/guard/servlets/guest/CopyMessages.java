/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.guest.GuardGuestEmail;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.servlets.guest.responses.GuestEmailsResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link CopyMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class CopyMessages extends GuardGuestServletAction {

    private static final String JSON_DATA_FIELD_NAME      = "json";
    private static final String JSON_COPY_DATA_FIELD_NAME = "copy";

    /**
     * Internal method to parse the request data
     *
     * @param json The json to parse the request data from
     * @return The request data
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     * @throws JSONException
     */
    private CopyMoveData parseCopyData(JSONObject json) throws JsonParseException, JsonMappingException, IOException, JSONException {
        JSONObject jsonUpdate = json.getJSONObject(JSON_COPY_DATA_FIELD_NAME);
        return new ObjectMapper().readValue(jsonUpdate.toString(), CopyMoveData.class);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        final GuardGuestService guardGuestService = Services.getService(GuardGuestService.class);

        //guest identity in the JSON body
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, true);
        UserIdentity guestIdentity = requireAnyGuestIdentityFrom(json);

        //Parse the update object
        CopyMoveData copyData = parseCopyData(json);
        //Copy the messages
        if (Arrays.equals(copyData.getMailIds(), CopyMoveData.ALL_IDS)) {
            //
        } else {
            final Collection<GuardGuestEmail> copiedMessages = guardGuestService.copyMessages(guestIdentity, copyData.getMailIds(), copyData.getNewFolderId());
            ServletUtils.sendObject(response, new GuestEmailsResponse(copiedMessages));
        }

    }

}
