/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.caching.CacheService;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.encryption.EncryptedItemsStorage;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.guest.impl.CachingGuestLookupService;
import com.openexchange.guard.guest.impl.GuardGuestServiceImpl;
import com.openexchange.guard.guest.impl.GuestLookupServiceImpl;
import com.openexchange.guard.guest.metadata.storage.CachingGuardGuestEMailMetadataService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.CachingFileCacheStorage;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pgp.mail.PGPMimeService;

/**
 * {@link GuestActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuestActivator extends HousekeepingActivator {

    private final static Logger logger = LoggerFactory.getLogger(GuestActivator.class);

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { PGPMimeCryptoService.class, Storage.class, FileCacheStorage.class, PGPMimeService.class,
                             GuardKeyService.class, GuardConfigurationService.class, GuardGuestEMailMetadataService.class,
                             HelpFileService.class, EncryptedItemsStorage.class, GuardDatabaseService.class,
                             EmailStorage.class, GenericCacheFactory.class, GuestUpgradeStorageService.class };

    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class[] { CacheService.class, ActivityTrackingService.class };
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        GuardGuestEMailMetadataService guestEMailMetadataService = getService(GuardGuestEMailMetadataService.class);
        FileCacheStorage fileCacheStorage = getService(FileCacheStorage.class);
        GuestLookupService guestLookupService = new GuestLookupServiceImpl(
            getService(EmailStorage.class));
        final boolean caching = getService(GuardConfigurationService.class).getBooleanProperty(GuardProperty.guestCaching);
        if(caching) {
            //Setup caching
            guestEMailMetadataService  = new CachingGuardGuestEMailMetadataService(getService(GenericCacheFactory.class), guestEMailMetadataService);
            fileCacheStorage = new CachingFileCacheStorage(getService(GenericCacheFactory.class),fileCacheStorage);
            guestLookupService = new CachingGuestLookupService(getService(GenericCacheFactory.class), guestLookupService, getService(EmailStorage.class));
        }

        registerService(GuardGuestService.class, new GuardGuestServiceImpl(
            getService(PGPMimeCryptoService.class),
            getService(Storage.class),
            fileCacheStorage,
            getService(GuardConfigurationService.class),
            guestEMailMetadataService,
            getOptionalService(ActivityTrackingService.class),
            getService(GuestUpgradeStorageService.class)));

        registerService(GuestLookupService.class, guestLookupService);

    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(GuardGuestService.class);
        super.stopBundle();
    }

}
