/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.ImportSystemPublicKey;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.pgp.keys.parsing.KeyRingParserResult;
import com.openexchange.pgp.keys.parsing.PGPKeyRingParser;

/**
 * {@link ImportSystemPublicKeyImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class ImportSystemPublicKeyImpl implements ImportSystemPublicKey {

    private String LoadFile (String file) throws OXException {
        StringBuilder sb = new StringBuilder();
        Path path = Paths.get(file);
        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
              //process each line in some way
              sb.append(line);
              sb.append("\r");
            }
        } catch (IOException e) {
            throw OXException.general("Error loading file", e);
        }
        return sb.toString();
    }

    private String parseEmailAdress(String identity) throws AddressException, UnsupportedEncodingException {
        if (identity.contains("<")) {
            String name = identity.substring(0, identity.indexOf("<"));
            if (!name.contains("\"")) {
                identity = "\"" + name.trim() + "\" " + identity.substring(identity.indexOf("<"));
            }
        }

        InternetAddress[] recips = InternetAddress.parse(IDNUtil.aceEmail(identity), false);
        if (recips.length > 0) {
            return new InternetAddress(recips[0].getAddress(), recips[0].getPersonal(), "UTF-8").getAddress();
        }
        return null;
    }

    private void importKey (PGPPublicKeyRing publicKeyRing) throws OXException {
        ArrayList<String> keyEmailIdentities = new ArrayList<String>();
        ArrayList<String> keyHexIds = new ArrayList<String>();
        for (PGPPublicKey publicKey : publicKeyRing) {
            Iterator iter = publicKey.getUserIDs();
            while (iter.hasNext()) {
                try {
                    String address = parseEmailAdress((String) iter.next());
                    if (address != null) {
                        keyEmailIdentities.add(IDNUtil.decodeEmail(address));
                    }
                } catch (AddressException | UnsupportedEncodingException e) {
                    continue;
                }
            }
            keyHexIds.add(LongUtil.longToHexStringTruncated(publicKey.getKeyID()));
        }


        GuardRatifierService guardRatifierService = Services.getService(GuardRatifierService.class);

        if (!keyHexIds.isEmpty()) {
            for (String keyEmailIdentity : keyEmailIdentities) {
                guardRatifierService.validate(keyEmailIdentity);
                GuardKeys key = new GuardKeys();
                key.setPGPPublicKeyRing(publicKeyRing);
                key.setEmail(keyEmailIdentity);
                key.setContextid(0);
                key.setKeyid(publicKeyRing.getPublicKey().getKeyID());
                insertKey (key);
            }
        }
    }

    private void insertKey(GuardKeys key) throws OXException {
        KeyTableStorage keyStorage = Services.getService(KeyTableStorage.class);
        int next = keyStorage.getMinFromSystemTable() - 1;
        key.setUserid(next);
        keyStorage.insert(key, true);
        keyStorage.setCurrentKey(key);
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        ogEmailStorage.insertOrUpdate(key.getEmail(), key.getContextid(), key.getUserid(), 0);

        //Adding the new key to the public key index (lookup table)
        PGPKeysStorage keysStorage = Services.getService(PGPKeysStorage.class);
        keysStorage.addPublicKeyIndex(key.getContextid(), 0L, key.getEmail(), key.getPGPPublicKeyRing());
    }

    private void importKeyRings (String data) throws OXException {
        PGPKeyRingParser pgpKeyParser = Services.getService(PGPKeyRingParser.class);
        try {
            KeyRingParserResult parseResult = pgpKeyParser.parse(data);
            List<PGPPublicKeyRing> keyRings = parseResult.toPublicKeyRings();
            for (PGPPublicKeyRing ring : keyRings) {
                importKey (ring);
            }
        } catch (IOException ex) {
            throw OXException.general("Problem parsing keys", ex);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.ImportSystemPublicKey#importPublic(java.lang.String)
     */
    @Override
    public void importPublic(String file) throws OXException {
        String pgpPublicKeyData = LoadFile (file);
        importKeyRings (pgpPublicKeyData);
    }


}
