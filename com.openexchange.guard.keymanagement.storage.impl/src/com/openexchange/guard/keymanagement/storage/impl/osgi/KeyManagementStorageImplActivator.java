/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.osgi;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.impl.deletedkeys.DeletedKeysStorageImpl;
import com.openexchange.guard.keymanagement.storage.impl.keycache.KeyCacheStorageImpl;
import com.openexchange.guard.keymanagement.storage.impl.keytable.KeyTableStorageImpl;
import com.openexchange.guard.keymanagement.storage.impl.pgpkeys.OGPGPKeysStorageImpl;
import com.openexchange.guard.keymanagement.storage.impl.pgpkeys.PGPKeysStorageImpl;
import com.openexchange.guard.keymanagement.storage.impl.remotekeys.RemoteKeyCacheStorageImpl;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link KeyManagementStorageImplActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyManagementStorageImplActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {GuardDatabaseService.class, GuardShardingService.class, GuardRatifierService.class, KeySourceFactory.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {

        org.slf4j.LoggerFactory.getLogger(KeyManagementStorageImplActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);

        // Register storages
        registerService(KeyCacheStorage.class, new KeyCacheStorageImpl());
        registerService(KeyTableStorage.class, new KeyTableStorageImpl());
        registerService(OGPGPKeysStorage.class, new OGPGPKeysStorageImpl());
        registerService(PGPKeysStorage.class, new PGPKeysStorageImpl());
        registerService(DeletedKeysStorage.class, new DeletedKeysStorageImpl());
        registerService(RemoteKeyCacheStorage.class, new RemoteKeyCacheStorageImpl(getService(KeySourceFactory.class)));

        // Track storages
        trackService(KeyCacheStorage.class);
        trackService(KeyTableStorage.class);
        trackService(OGPGPKeysStorage.class);
        trackService(PGPKeysStorage.class);
        trackService(DeletedKeysStorage.class);
        trackService(RemoteKeyCacheStorage.class);

    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        final Logger logger = LoggerFactory.getLogger(KeyManagementStorageImplActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        unregisterService(KeyCacheStorage.class);
        unregisterService(KeyTableStorage.class);
        unregisterService(OGPGPKeysStorage.class);
        unregisterService(PGPKeysStorage.class);
        unregisterService(DeletedKeysStorage.class);
        unregisterService(RemoteKeyCacheStorage.class);

        logger.info("Key management storages unregistered.");
        Services.setServiceLookup(null);
        super.stopBundle();
    }

}
