/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.smime.impl.passwordManagementServices;

import static com.openexchange.java.Autoboxing.I;
import java.security.PrivateKey;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.auth.AuthUtils;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.auth.PasswordChangedResult;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardChangePasswordExceptionCodes;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SmimePasswordManagementService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimePasswordManagementService implements PasswordManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmimePasswordManagementService.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SmimePasswordManagementService}.
     *
     * @param services The {@link ServiceLookup} to use
     */
    public SmimePasswordManagementService(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Creates a {@link PasswordChangedResult}
     *
     * @param userSession The session to create the result for
     * @param key The key to create the result for
     * @param newPassword The new/changed password
     * @return The created {@link PasswordChangedResult}
     * @throws OXException
     */
    private PasswordChangedResult createPasswordChangedResult(GuardUserSession userSession, SmimeKeys key, String newPassword) throws OXException {
        GuardSessionService sessionService = services.getServiceSafe(GuardSessionService.class);
        final int userId = userSession.getUserId();
        final int contextId = userSession.getContextId();
        String token = sessionService.getToken(userSession.getGuardSession());
        if (token == null) {
            token = sessionService.newToken(userSession.getGuardSession(), userId, contextId);
        }

        //@formatter:off
        JsonObject json = AuthUtils.createAuthJson(userSession.getGuardUserId(),
            userSession.getGuardContextId(), 
            userSession.getUserId(),
            userSession.getContextId(), 
            key.getEmail(), 
            "encr_password", 
            newPassword, 
            null);
        //@formatter:on

        GuardCipherService cipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);
        //@formatter:off
        return new PasswordChangedResult(
            cipherService.encrypt(json.toString(), token))
            .withRecovery(key.hasPrivateKey() ? key.getPrivateKey().hasRecovery() : false);
        //@formatter:on
    }

    /**
     * Asserts that the given password has the configured minimum required password length
     * <p>
     * The minimum password length is defined with {@link GuardProperty#minPasswordLength}
     * </p>
     *
     * @param userSession The session to check the password length for
     * @param newpass The new password to check the length for
     * @return The newpass if it's length is sufficient
     * @throws OXException if newpass length is less than the configured {@link GuardProperty#minPasswordLength}
     */
    private String assertPasswordLengh(GuardUserSession userSession, String newpass) throws OXException {
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        int minLength = configService.getIntProperty(GuardProperty.minPasswordLength, userSession.getUserId(), userSession.getContextId());
        if (newpass.length() < minLength) {
            throw GuardChangePasswordExceptionCodes.BAD_LENGTH.create(I(minLength));
        }
        return newpass;
    }

    /**
     * Sends the reset email containing the new password to a given user after a password reset
     *
     * @param senderId The user id
     * @param senderCid The context id
     * @param newpass The new password
     * @param userEmail The email
     * @param secondEmail The secondary email
     * @param lang The language to use
     * @param templId The ID of the template to use
     * @param host The host
     * @param senderIp The IP of the sender
     * @throws OXException
     */
    private void sendResetEmail(int senderId, int senderCid, String newpass, String userEmail, String secondEmail, String lang, int templId, String host, String senderIp) throws OXException {
        MailCreatorService mailCreatorService = services.getServiceSafe(MailCreatorService.class);
        GuardNotificationService guardNotificationService = services.getServiceSafe(GuardNotificationService.class);
        List<String> fromEmail = mailCreatorService.getFromAddress(userEmail, userEmail, senderId, senderCid);
        JsonObject mail = mailCreatorService.getResetEmail(secondEmail, fromEmail, newpass, lang, templId, host, senderId, senderCid);
        LOGGER.debug("Sending guard notificatio mail: {}", mail);
        guardNotificationService.send(mail, senderId, senderCid, senderIp);
    }

    /**
     * Change the current password and updates the private key storage
     *
     * @param userSession The {@link GuardSession} to change the password for
     * @param oldPass The old password required for the password change
     * @param newPass The new password to set
     * @return The updated {@link SmimeKeys}
     * @throws OXException
     */
    private SmimeKeys changePassword(GuardUserSession userSession, String oldPass, String newPass) throws OXException {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        SmimeKeys key = keyService.getCurrentKey(userSession.getUserId(), userSession.getContextId());
        if (key == null || !key.hasPrivateKey()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create("");
        }
        // Check and decrypt using old password
        PrivateKey privateKey = SmimeCryptoKeyUtil.decryptPrivateKey(key, oldPass);
        // Re-Encrypt private key with the key salt and the new password 
        String encryptPrivateKey = SmimeCryptoKeyUtil.encryptPrivateKey(privateKey, newPass, key.getPrivateKey().getSalt());
        key.getPrivateKey().setEncryptedKeyData(encryptPrivateKey);
        // Update recovery
        keyService.updateRecovery(key, newPass, userSession.getUserId(), userSession.getContextId());
        // Store it
        keyService.storeKey(key, userSession.getUserId(), userSession.getContextId(), true);
        return key;
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.SMIME;
    }

    @Override
    public PasswordChangedResult changePassword(GuardUserSession userSession, String guestEmail, String oldpass, String newpass, String question, String answer, String pin) throws OXException {
        SmimeKeys newKeys = changePassword(userSession, oldpass, assertPasswordLengh(userSession, newpass));
        return createPasswordChangedResult(userSession, newKeys, newpass);
    }

    @Override
    public String getSecondaryEmail(GuardUserSession userSession) throws OXException {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        SmimePrivateKeys pKey = keyService.getCurrentPrivateKey(userSession.getUserId(), userSession.getContextId());
        if (pKey != null) {
            String secondaryEmail = pKey.getRecoveryEmail();
            return secondaryEmail != null ? secondaryEmail : pKey.getEmail();
        }
        throw SmimeExceptionCodes.KEY_NOT_FOUND.create("");
    }

    @Override
    public void changeSecondary(GuardUserSession userSession, String password, String email) throws OXException {
        services.getServiceSafe(GuardRatifierService.class).validate(email);
        final SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);

        SmimeKeys key = keyService.getCurrentKey(userSession.getUserId(), userSession.getContextId());
        if (key == null || !key.hasPrivateKey()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create("");
        }

        //Verify password
        SmimeCryptoKeyUtil.decryptPrivateKey(key, password);

        key.getPrivateKey().setRecoveryEmail(email);
        keyService.storeKey(key, userSession.getUserId(), userSession.getContextId(), true);
    }

    @Override
    public void deleteRecover(GuardUserSession userSession, String password) throws OXException {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        List<SmimeKeys> keys = keyService.getKeys(userSession.getUserId(), userSession.getContextId());
        if (keys.isEmpty()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create("");
        }
        boolean done = false;
        Iterator<SmimeKeys> it = keys.iterator();
        while (it.hasNext()) {
            SmimeKeys key = it.next();
            if (key.hasPrivateKey()) {
                SmimePrivateKeys privateKey = key.getPrivateKey();

                //Verify password
                SmimeCryptoKeyUtil.decryptPrivateKey(privateKey, password);

                if (privateKey.hasRecovery()) {
                    privateKey.clearRecovery();

                    keyService.storeKey(key, userSession.getUserId(), userSession.getContextId(), true);
                    done = true;

                } else {
                    throw SmimeExceptionCodes.RECOVERY_DISABLED.create();
                }
            }
        }
        if (!done) {
            throw SmimeExceptionCodes.CRYPTO_ERROR.create("Problem deleting");
        }
    }

    @Override
    public ResetPasswordDestination resetPass(GuardUserSession userSession, String lang, String hostname, String senderIp, boolean web, String email) throws OXException {
        int userid = userSession != null ? userSession.getUserId() : 0;
        int cid = userSession != null ? userSession.getContextId() : 0;

        int templid = 0;
        final SmimeKeyService mimeKeyService = services.getServiceSafe(SmimeKeyService.class);
        SmimeKeys key = null;
        if (email == null && userid > 0) {
            key = mimeKeyService.getCurrentKey(userid, cid);
        } else if (email != null) {
            // OK, search by email
            List<SmimeKeys> pubKeys = mimeKeyService.getKeys(email);
            // Get a public key that is local and uses that email address
            Optional<SmimeKeys> pubKey = pubKeys.stream().filter((p) -> p.isLocal() && p.isCurrent()).findFirst();
            if (pubKey.isPresent()) {
                key = pubKey.get();
            }
        }
        if (key == null || !key.hasPrivateKey()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create("");
        }
        if (!key.getPrivateKey().hasRecovery()) {
            throw SmimeExceptionCodes.RECOVERY_DISABLED.create();
        }

        userid = key.getUserId();
        cid = key.getContextId();

        // Generate new password
        GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
        String newpass = keyService.generatePassword(userid, cid);

        // Get new key updated with new password
        mimeKeyService.changePasswordWithRecovery(key.getPrivateKey(), newpass, userid, cid);
        mimeKeyService.updateRecovery(key.getPrivateKey(), newpass, userid, cid);

        // Get elements required for email
        GuardConfigurationService guardConfigService = services.getServiceSafe(GuardConfigurationService.class);
        templid = guardConfigService.getIntProperty(GuardProperty.templateID, userid, cid);
        boolean secondary = key.getPrivateKey().getRecoveryEmail() != null && !key.getPrivateKey().getRecoveryEmail().isEmpty();
        // Send it
        sendResetEmail(userid, cid, newpass, key.getPrivateKey().getEmail(), secondary ? key.getPrivateKey().getRecoveryEmail() : key.getPrivateKey().getEmail(), lang, templid, hostname, senderIp);

        // Store it
        mimeKeyService.storeKey(key, userid, cid, true);

        if (secondary) {
            if (!web) {
                System.out.println("Reset sent to recovery email");
            }
            return ResetPasswordDestination.SECONDARY;
        }
        if (!web) {
            System.out.println("Rest sent to the primary email address");
        }
        return ResetPasswordDestination.PRIMARY;
    }
}
