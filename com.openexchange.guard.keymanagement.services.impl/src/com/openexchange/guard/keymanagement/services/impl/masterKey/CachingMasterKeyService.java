/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.masterKey;

import java.security.Key;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.services.MasterKeyService;

import static com.openexchange.java.Autoboxing.I;

/**
 * Retrieve master keys from cache if available, otherwise use delegate.
 * Master keys with the recovery master keys are NOT cached.
 * {@link CachingMasterKeyService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.6
 */
public class CachingMasterKeyService extends AbstractMasterKeyService {

    private final Map<Integer, MasterKey> map;  // Simple cache of index to master key.  Will be small sized and fairly static
    private final MasterKeyService delegate;

    public CachingMasterKeyService(MasterKeyService delegate, GuardCipherService cipher) {
        super(cipher);
        this.map = new ConcurrentHashMap<Integer, MasterKey>();
        this.delegate = delegate;
    }

    @Override
    public MasterKey getMasterKey(int userId, int cid, boolean recovery) throws OXException {
        final int index = getIndexForUser(userId, cid);
        return getMasterKey(index, recovery);
    }

    @Override
    public int getIndexForUser(int userId, int cid) throws OXException {
        return delegate.getIndexForUser(userId, cid);
    }

    @Override
    public MasterKey getMasterKey(int index, boolean recovery) throws OXException {
        if (recovery) {  // We don't cache the password recovery key
            return delegate.getMasterKey(index, recovery);
        }
        if (map.containsKey(I(index))) {
            return map.get(I(index));
        }
        MasterKey key = delegate.getMasterKey(index, false);
        if (key != null) {
            map.put(I(index), key);
        }
        return key;
    }

    @Override
    public Key getDecryptedClientKey(MasterKey key) throws OXException {
        return delegate.getDecryptedClientKey(key);
    }

    @Override
    public String getRcEncryted(String data, String salt, int index) throws OXException {
        return encrypt(getMasterKey(index, false), data, salt);
    }

    @Override
    public String getRcDecrypted(String data, String salt, int index) throws OXException {
        return decrypt(getMasterKey(index, false), data, salt);
    }

    @Override
    public MasterKey createKey(int index) throws OXException {
        return delegate.createKey(index);
    }

    @Override
    public MasterKey createKey() throws OXException {
        return delegate.createKey();
    }
}
