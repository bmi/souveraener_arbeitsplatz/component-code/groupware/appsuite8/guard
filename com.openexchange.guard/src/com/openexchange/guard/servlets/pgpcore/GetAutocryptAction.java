/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link GetAutocryptAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class GetAutocryptAction extends GuardServletAction {

    private static final String USER_ID = "id";
    private static final String CONTEXT_ID = "cid";
    private static final String EMAIL = "email";



    /* (non-Javadoc)
     * Action to retrieve a autocrypt public key ring for user
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws OXException {

        RangeInputValidator<Integer> positiveValidator = new RangeInputValidator<Integer>(0, Integer.MAX_VALUE);
        int userId = positiveValidator.assertInput(ServletUtils.getIntParameter(request, USER_ID, true), "userId");
        int cid = positiveValidator.assertInput(ServletUtils.getIntParameter(request, CONTEXT_ID, true), "contextId");
        String email = ServletUtils.getStringParameter(request, EMAIL);

        AutoCryptService autoCrypt = Services.getService(AutoCryptService.class);

        String responseData = "";

        if (autoCrypt != null) {
            responseData = autoCrypt.getOutgoingHeaderString(userId, cid, userId, cid, email);
        }

        ServletUtils.sendObject(response, responseData);

    }

}
