/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import static com.openexchange.java.Autoboxing.L;

import java.io.InputStream;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignerDigestMismatchException;
import org.bouncycastle.cms.CMSVerifierCertificateNotValidException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.mail.smime.SMIMESignedParser;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.certificatemanagement.commons.SmimeUtil;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.impl.SMIMESignatureResult.SMIMESignatureResultBuilder;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SignatureVerifier} verifies smime signature
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SignatureVerifier {

    private static final String BC = BouncyCastleProvider.PROVIDER_NAME;
    private static final String DIGEST_MISMATCH = "mismatch";
    private static final String CERT_INVALID = "invalidCert";
    private static final String NO_SIG_FOUND = "novalidcert";
    private final ServiceLookup services;

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(SignatureVerifier.class);
    }

    /**
     * Initializes a new {@link SignatureVerifier}.
     *
     * @param services The service lookup
     */
    public SignatureVerifier(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Get the email associated with the certificate
     *
     * @param cert The certificate
     * @return
     */
    private String getCertEmail(X509Certificate cert) {
        return SmimeUtil.getEmail(cert);
    }


    /**
     *
     * Private class to verify the signature (assuming the cert is contained in the message)
     *
     * @param fromAddress From address, used to verify certificate pertains to the sender
     * @param s SMimeSignedParser
     * @param date Date the item was sent for certificate verification
     * @param userId
     * @param cid
     * @return List of SignatureVerificationResult
     * @throws Exception
     */
    private List<SignatureVerificationResult> verify(String fromAddress,
        SMIMESignedParser s, Date date, int userId, int cid)
        throws Exception
    {

        ArrayList<SignatureVerificationResult> results = new ArrayList<SignatureVerificationResult>();

        Store certs = s.getCertificates();

        SignerInformationStore  signers = s.getSignerInfos();

        Collection              c = signers.getSigners();
        Iterator                it = c.iterator();

        while (it.hasNext())
        {
            SignerInformation   signer = (SignerInformation)it.next();

            Collection          certCollection = certs.getMatches(signer.getSID());

            Iterator        certIt = certCollection.iterator();
            X509CertificateHolder certHolder = (X509CertificateHolder) certIt.next();
            X509Certificate cert = new JcaX509CertificateConverter().setProvider(BC).getCertificate(certHolder);

            String certEmail = getCertEmail(cert);
            // Make sure the signing certificate is same as From Address, otherwise skip this signature
            if (certEmail == null || certEmail.isEmpty() || !certEmail.toLowerCase().contains(fromAddress.toLowerCase())) {
                continue;
            }
            // verify certificate chain
            CertificateService certificateService = services.getServiceSafe(CertificateService.class);
            CertificateVerificationResult certResult = certificateService.verify(certs, certHolder, date, userId, cid);
            // If chain fails, then reject
            if (certResult.getResult() != Result.VERIFIED) {
                results.add(new SMIMESignatureResult(false, certResult.getResult().getVal()));
            } else {
                // OK, chain is complete.  Let's verify the data
                try {
                    if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider(BC).build(cert))) {
                        // All verified.  Let's check if we already have that public key
                        SmimeKeyService keyService = services.getOptionalService(SmimeKeyService.class);
                        if (keyService != null) {
                            if (!keyService.existsPublicKey(cert.getSerialNumber().toString(), certResult.getKeys().getEmail(), date)) {
                                keyService.storePublicKey(userId, cid, certResult.getKeys(), date);
                            }
                        }
                        // ALL GOOD
                        SMIMESignatureResultBuilder builder = new SMIMESignatureResultBuilder(true);
                        builder.setEmail(certEmail).setSerial(cert.getSerialNumber());
                        // Parse out the signing time
                        try {
                            ASN1Set signingTime = signer.getSignedAttributes().get(CMSAttributes.signingTime).getAttrValues();
                            if (signingTime.size() > 0) {
                                String time = signingTime.getObjectAt(0).toString();
                                if (time != null && time.endsWith("Z")) {
                                    DateFormat df = new SimpleDateFormat("yyMMddHHmmss");
                                    Date signedDate = df.parse(time.replace("Z", ""));
                                    builder.setSignedDate(L(signedDate.getTime()));
                                }
                            }
                        } catch (Exception e) {
                            // Log the error, but we want to continue
                            LoggerHolder.LOGGER.error("Error getting date from signer ", e);
                        }
                        results.add(builder.build());
                    } else {
                        // Signature failed
                        results.add(new SMIMESignatureResult(false));
                    }
                } catch (CMSException ex) {
                    if (ex instanceof CMSSignerDigestMismatchException) {
                        results.add(new SMIMESignatureResult(false, DIGEST_MISMATCH));
                        continue;
                    }
                    if (ex instanceof CMSVerifierCertificateNotValidException) {
                        results.add(new SMIMESignatureResult(false, CERT_INVALID));
                        continue;
                    }
                    results.add(new SMIMESignatureResult(false));
                }
            }

        }
        // If no valid signatures found, then return that sig expected, but none found
        if (results.isEmpty()) {
            results.add(new SMIMESignatureResult(false, NO_SIG_FOUND));
        }
        return results;
    }

    /**
     * Verify msg signature
     *
     * @param msg MimeMessage containing the signature
     * @param user The UserIdentity of the user
     * @return List of {@link SignatureVerificationResult}
     * @throws Exception
     */
    public List<SignatureVerificationResult> verify(MimeMessage msg, UserIdentity user) throws Exception
    {
        SMIMESignedParser s = null;
        if (msg.isMimeType("multipart/signed")) {
            s = new SMIMESignedParser(new JcaDigestCalculatorProviderBuilder().build(), (MimeMultipart) msg.getContent());
        } else if (msg.isMimeType("application/pkcs7-mime") || msg.isMimeType("application/x-pkcs7-mime")) {
            // in this case the content is wrapped in the signature block.
            s = new SMIMESignedParser(new JcaDigestCalculatorProviderBuilder().build(), msg);
        }
        if (s == null) {
            return Collections.emptyList();
        }

        // Parse out the from address to make sure signature matches
        Address[] from = msg.getFrom();
        String fromAddress = "";
        if (from != null && from.length > 0) {
            if (from[0] instanceof InternetAddress) {
                fromAddress = ((InternetAddress) from[0]).getAddress();
            }
        }

        // Verify
        List<SignatureVerificationResult> results = verify(fromAddress, s, msg.getSentDate(), user.getOXUser().getId(), user.getOXUser().getContextId());
        // Remove signature file
        try (InputStream in = s.getContent().getInputStream()) {
            GuardSmimeUtil.updateFromInputStream(msg, s.getContent().getInputStream(), s.getContent().getContentType());
        }
        return results;
    }
}
