/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;

/**
 * {@link OGPGPKeysStorage} provides access to the uploaded keys by a user
 */
public interface OGPGPKeysStorage {

    /**
     * Gets a list of uploaded PGP Keys for a given user
     *
     * @param userId the user's id to retrieve the uploaded keys for
     * @param contextId the user's context Id
     * @param A list of uploaded keys for the given user
     * @throws OXException due an Error
     */
    public List<OGPGPKey> getForUser(int userId, int contextId) throws OXException;

    /**
     * Gets a specific list of uploaded PGP Keys for a given user
     *
     * @param userId the user's id to retrieve the uploaded keys for
     * @param contextId the context id
     * @param emails a list of mails to get the keys for
     * @return A list of uploaded keys for the given user matching the given email addresses
     * @throws OXException
     */
    public List<OGPGPKey> getForUserByEmail(int userId, int contextId, List<String> emails) throws OXException;

    /**
     * Gets the uploaded PGP key containing the given email for a certain user
     *
     * @param userId the user to get the uploaded key for
     * @param contextId the context Id
     * @param email the email of the key to retrieve
     * @param keyIds the IDs of the key to retrieve
     * @return The uploaded key related to the given email, or null if no such key was found, or the user is not allowed to access the key
     * @throws OXException due an error
     */
    public OGPGPKey getForUserByEmailAndIds(int userId, int contextId, String email, List<String> keyIds) throws OXException;

    /**
     * Gets the uploaded PGP key containing the given Ids for a certain user
     * (If the key contains more than one email addresses, the storage will have an entry for each)
     *
     * @param userId the user to get the uploaded key for
     * @param contextId the context Id
     * @param keyIds the IDs of the key to retrieve
     * @return The uploaded key related to the given keyIds, or null if no such key was found, or the user is not allowed to access the key
     * @throws OXException due an error
     */
    public List<OGPGPKey> getForUserByIds(int userId, int contextId, List<String> keyIds) throws OXException;

    /**
     * Inserts or updates a uploaded PGP Key entry
     *
     * @param userId the id of the owner of the uploaded key
     * @param contextId the context id of the owner
     * @param email the email related to the public key
     * @param keyIds a list of IDs related to the public key
     * @param ascKeyData the ASCII armored public key data
     * @throws OXException due an error
     */
    public void insertOrUpdate(int userId, int contextId, String email, List<String> keyIds, String ascKeyData) throws OXException;

    /**
     * Deletes an uploaded key for a user by a list of keyIds
     *
     * @param userId the ID of the user to delete the public key data for
     * @param contextId the context Id
     * @param keyIds the IDs of the key to delete
     * @return The number of deleted key entries
     * @throws OXException due an error
     */
    public int delete(int userId, int contextId, List<String> keyIds) throws OXException;

    /**
     * Sets the share-level for an uploaded PGP key. The share-level can be 0 for "not shared" or 1 for "shared"
     *
     * @param userId the ID of the user who wants to change the share-level
     * @param contextId the context id
     * @param keyIds the IDs of the key to change the share-level for
     * @param shareLevel the new share-level
     * @throws OXException due an error
     */
    public void updateShareLevel(int userId, int contextId, List<String> keyIds, int shareLevel) throws OXException;

    /**
     * Sets the inline mode for an uploaded PGP key
     *
     * @param userId The user ID of the user who wants to change the inline mode
     * @param contextId the context id
     * @param keyIds the IDs of the key to change the inline mode for
     * @param useInline Whether to use PGP inline with the given key or not
     * @throws OXException due an error
     */
    public void updateInlineMode(int userId, int contextId, List<String> keyIds, boolean useInline) throws OXException;
}
