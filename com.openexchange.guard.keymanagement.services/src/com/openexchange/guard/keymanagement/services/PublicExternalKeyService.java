/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;

/**
 * {@link PublicExternalKeyService} handles external public keys uploaded by a user.
 * <p>
 * An OX Guard user is able to upload public keys for external recipients.
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public interface PublicExternalKeyService {

    /**
     * Gets a list of user uploaded public PGP key rings of external recipients.
     *
     * @param userId The ID of the user
     * @param contextId The context of the user
     * @param emails Filters the response based on a list of email addresses
     * @return A list of uploaded PGP keys
     * @throws OXException
     */
    List<OGPGPKeyRing> get(int userId, int contextId, List<String> emails) throws OXException;

    /**
     * Gets a list of user uploaded public PGP key rings of external recipients.
     *
     * @param userId The ID of the user
     * @param contextId The context of the user
     * @return A list of uploaded PGP keys
     * @throws OXException
     */
    List<OGPGPKeyRing> get(int userId, int contextId) throws OXException;

    /**
     * Gets the uploaded public PGP key ring with the specified id for the given user
     * @param userId The ID of the user
     * @param contextId The context of the user
     * @param hexid The identifier of the key ring to delete
     * @return The uploaded PGP key ring for the given user containing the given hexid, or null if no such key was found for the user.
     * @throws OXException
     */
    OGPGPKeyRing get(int userId, int contextId, String hexid) throws OXException;

    /**
     * Adds new public PGP key rings for an external recipient
     *
     * @param userId The ID who imports the keys
     * @param contextId The context ID of the user who imports the keys
     * @param publicKeyRings The keys to import.
     * @return  A list of imported PGP key rings
     * @throws OXException
     */
    List<OGPGPKeyRing> importPublicKeyRing(int userId, int contextId, PGPPublicKeyRing... publicKeyRings) throws OXException;

    /**
     * Deletes a public PGP key ring for an external recipient
     *
     * @param userId The ID of the user who wants to delete the public key ring
     * @param contextId The context ID of the user who wants to delete the public key ring
     * @param ids The identifier of the key ring to delete
     * @throws OXException
     */
    void delete(int userId, int contextId, String ids) throws OXException;

    /**
     * Sets the share mode for a specified public key ring
     *
     * @param userId The user ID
     * @param contextId The context ID
     * @param ids The identifier of the key ring
     * @param share True, to mark the key ring as "shared", false otherwise.
     * @throws OXException
     */
    void share(int userId, int contextId, String ids, boolean share) throws OXException;

    /**
     * Sets the inline mode for a specified public key ring
     *
     * @param userId The user ID
     * @param contextId The context ID
     * @param ids The identifier of the key ring
     * @param inline True, to mark the key ring as "inline", False otherwise
     * @throws OXException
     */
    void setInline(int userId, int contextId, String ids, boolean inline) throws OXException;
}
