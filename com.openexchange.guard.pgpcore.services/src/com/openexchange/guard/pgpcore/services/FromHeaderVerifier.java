/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.util.List;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link FromHeaderVerifier} - Helper class to verify FROM Headers of MimeMessages
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class FromHeaderVerifier {

    private static final Logger LOG = LoggerFactory.getLogger(FromHeaderVerifier.class);

    private static boolean issuerAllowed(Address[] fromAddresses, PGPSignatureVerificationResult result) throws AddressException {
        for (String pgpUserId : result.getIssuerUserIds()) {
            for (Address address : fromAddresses) {
                final InternetAddress fromAddress = (InternetAddress) address;
                // Remove username info from userId, may contain illegal internet address characters, etc.
                final String id = (pgpUserId.contains("<") ? pgpUserId.substring(pgpUserId.indexOf("<")) : pgpUserId);
                final InternetAddress pgpUserIdAddress = new InternetAddress(id);
                if (fromAddress.getAddress() != null && pgpUserIdAddress.getAddress() != null) {
                    if (fromAddress.getAddress().toLowerCase().trim().equals(pgpUserIdAddress.getAddress().toLowerCase().trim())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Verifies that the issuer of the PGPSignatureVerificationResults are in the actual FROM-Header for the given MimeMessage.
     * This sets the verification results to not verified if the issuer is not part of the message's FROM Header
     *
     * @param mimeMessage The MimeMessage
     * @param verificationResults The verification results
     * @throws OXException due Address parsing error
     */
    public static void verify(MimeMessage mimeMessage, List<SignatureVerificationResult> verificationResults) throws OXException {
        try {
            Address[] fromAddresses = mimeMessage.getFrom();
            if (fromAddresses == null) {
                //We do not found any from headers
                for (SignatureVerificationResult result : verificationResults) {
                    if (result instanceof PGPSignatureVerificationResult) {
                        ((PGPSignatureVerificationResult) result).setVerified(false);
                        ((PGPSignatureVerificationResult) result).setError("Unable to verify signature due missing FROM header in MIME message.");
                        LOG.error("Unable to verify signature due missing FROM header in MIME message.");
                    } else {
                        LOG.debug("Unable to handle result {}", result);
                    }
                }
            } else {
                for (SignatureVerificationResult result : verificationResults) {
                    if (result instanceof PGPSignatureVerificationResult) {
                        if (result.isVerified()) {
                            try {
                                boolean issuerInFromHeader = issuerAllowed(fromAddresses, ((PGPSignatureVerificationResult) result));
                                if (!issuerInFromHeader) {
                                    ((PGPSignatureVerificationResult) result).setVerified(false);
                                    ((PGPSignatureVerificationResult) result).setError("The issuer of the signature was not found in the FROM header for the MIME message.");
                                    LOG.debug("The issuer of the signature was not found in the FROM header for the MIME message.");
                                }
                            } catch (MessagingException e) { // Problem parsing/verifying email addresses.  Fail signature and add error message
                                ((PGPSignatureVerificationResult) result).setVerified(false);
                                ((PGPSignatureVerificationResult) result).setError(e.getMessage());
                            }
                        }
                    } else {
                        LOG.debug("Unable to handle result {}", result);
                    }
                }
            }
        } catch (MessagingException e) {
            throw PGPCoreServicesExceptionCodes.SIGNING_ERROR.create(e, e.getMessage());
        }
    }
}
