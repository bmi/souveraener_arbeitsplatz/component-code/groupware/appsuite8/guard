/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

/**
 * {@link MailSortField}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public enum MailSortField {
    /**
     * From
     */
    FROM,
    /**
     * To
     */
    TO,
    /**
     * Cc
     */
    CC,
    /**
     * Subject
     */
    SUBJECT,
    /**
     * Size
     */
    SIZE,
    /**
     * Sent date
     */
    SENT_DATE,
    /**
     * Received date
     */
    RECEIVED_DATE,
    /**
     * Color Label
     */
    COLOR_LABEL,
    /**
     * Flag \SEEN
     */
    FLAG_SEEN,
    /**
     * Account name
     */
    ACCOUNT_NAME,
    /**
     * Flag \ANSWERED
     */
    FLAG_ANSWERED,
    /**
     * Flag \FORWARDED
     */
    FLAG_FORWARDED,
    /**
     * Flag \DRAFT
     */
    FLAG_DRAFT,
    /**
     * Flag \FLAGGED
     */
    FLAG_FLAGGED,

    ;

}
