/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.util.Store;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;

/**
 * {@link CertificateService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public interface CertificateService {

    /**
     * Verify certificate chain
     *
     * @param certificatesStore Store containing the certificate chains
     * @param certFromSignedData Certificate from signed data
     * @param signDate Date for which the cert should be valid
     * @param userId id of the user
     * @param contextId The context id of the user
     * @return A {@link CertificateVerificationResult}
     * @throws Exception In case of error
     */
    CertificateVerificationResult verify(Store<X509CertificateHolder> certificatesStore, X509CertificateHolder certFromSignedData, Date signDate, int userId, int contextId) throws Exception;

    /**
     * Verify certificate against chain
     *
     * @param certToCheck The certificate to verify
     * @param additionalCerts List of certificates in chain
     * @param date Date during which the certificate should be valid
     * @param userId user identifier
     * @param cid The context ID of the user
     * @return The {@link Result}
     * @throws Exception In case of error
     */
    Result verify(X509Certificate certToCheck, Set<X509Certificate> additionalCerts, Date date, int userId, int cid) throws Exception;

}
