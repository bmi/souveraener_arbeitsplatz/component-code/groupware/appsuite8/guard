/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuardGuestService} - provides functionalities for OX Guard guest accounts
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface GuardGuestService {

    public static final String DEFAULT_FOLDER = "INBOX";

    /**
     * Gets a list of available item IDs for the specified guest
     *
     * @param guestUser The user to get a list of items for
     * @return A list of item IDs for the given guest
     * @throws OXException
     */
    List<String> getEmailItemIds(UserIdentity guestUser) throws OXException;

    /**
     * Queries a set of guest items for a given guest, from the underlying storage.
     *
     * @param guestUser The guest to get the items for
     * @param query A {@link MailQuery} object which describes which E-Mails to get; Use {@link MailQuery#EMPTY_QUERY} to query all emails for the given user.
     * @return A collection of emails from the given guest.
     * @throws OXException
     */
    Collection<GuardGuestEmail> getEmailItems(UserIdentity guestUser, MailQuery query) throws OXException;

    /**
     * Retrievals a guest item/email form the underlying storage.
     *
     * @param guestUser The guest to get an item for
     * @param itemId The ID of the item to fetch.
     * @return The guest item, or null, if no such item was found.
     * @throws OXException
     */
    GuardGuestEmail getEmailItem(UserIdentity guestUser, String itemId) throws OXException;

    /**
     * Retrievals and decrypts a guest item/email.
     *
     * @param guestUser The guest to get an item for
     * @param itemId The ID of the item to fetch.
     * @return The guest item, or null, if no such item was found.
     * @throws OXException
     */
    GuardGuestEmail getDecryptedEmailItem(UserIdentity guestUser, String itemId) throws OXException;

    /**
     * Updates a set of E-Mails
     *
     * @param guestUser The guest to update items for
     * @param mailUpdate An object defining which data to update
     */
    void updateMessages(UserIdentity guestUser, MailUpdate mailUpdate) throws OXException;

    /**
     * Moves a set of E-Mails to another folder
     *
     * @param guestUser The guest owning the mails to move
     * @param messageIds The IDs of the mails to move
     * @param newFolderId The new FolderID to move the mails into
     * @throws OXException
     */
    void moveMessages(UserIdentity guestUser, String[] messageIds, String newFolderId) throws OXException;

    /**
     * Moves all E-Mails related to the given folderId to a new folder
     *
     * @param guestUser The guest owning the mails to move
     * @param folderId The ID of the folder to move it's content
     * @param newFolderId The ID of the destination folder
     * @throws OXException
     */
    void moveAllMessages(UserIdentity guestUser, String folderId, String newFolderId) throws OXException;

    /**
     * Copies a set of E-Mails to another folder
     *
     * @param guestUser The guest owning the mails to copy
     * @param messageIds The IDs of the mails to copy
     * @param newFolderId The new FolderID to move the mails into
     * @return A set of corresponding new message Ids for the copied objects.
     * @throws OXException
     */
    Collection<GuardGuestEmail> copyMessages(UserIdentity guestUser, String[] messageIds, String newFolderId) throws OXException;

    /**
     * Appends a new message to the given folder
     *
     * @param guestUser The guest who wants to add a message to one of his folders
     * @param msg The message to add
     * @param folderId The ID of the folder to add the message to
     * @return The append message
     * @throws OXException
     */
    GuardGuestEmail appendMessage(UserIdentity guestUser, InputStream msg, String folderId) throws OXException;

    /**
     * Appends a new message to the given folder with a special ID assigned
     *
     * @param guestUser The guest who wants to add the message to one of his folders
     * @param msg The message to add
     * @param id The ID to set fo the new message
     * @param folderId The ID of the folder to add the message to
     * @return The append message
     * @throws OXException
     */
    GuardGuestEmail appendMessage(UserIdentity guestUser, InputStream msg, String itemId, String folderId) throws OXException;

    /**
     * Deletes a set of E-Mails
     *
     * @param guestUser The user owning the E-Mails
     * @param messageIds The message IDs of the emails to delete
     * @throws OXException
     */
    void deleteMessages(UserIdentity guestUser, String[] messageIds) throws OXException;

    /**
     * Deletes all E-Mails related to a given folder
     * @param guestUser The user owning the E-Mails
     * @param folderId The ID of the folder to delete
     * @throws OXException
     */
    void deleteAllMessages(UserIdentity guestUser, String folderId) throws OXException;
}

