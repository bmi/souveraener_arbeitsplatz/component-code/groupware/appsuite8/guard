/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link DefaultRecipKeyLookupStrategy} defines a concrete and the default {@link RecipKeyLookupStrategy} used by OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class DefaultRecipKeyLookupStrategy implements RecipKeyLookupStrategy {

    private final int senderUserId;
    private final int senderContextId;
    private final String hkpClientToken;
    private static final Logger logger = LoggerFactory.getLogger(DefaultRecipKeyLookupStrategy.class);

    /**
     * Initializes a new {@link DefaultRecipKeyLookupStrategy}.
     *
     * @param senderUserId
     * @param senderContextId
     */
    public DefaultRecipKeyLookupStrategy(int senderUserId, int senderContextId, String hkpClientToken) {
        this.hkpClientToken = hkpClientToken;
        this.senderUserId = senderUserId;
        this.senderContextId = senderContextId;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {

        RecipKey recipKey = new CompositeRecipKeyLookupStrategy(new RecipKeyLookupStrategy[] {
            new UserUploadedRecipKeyLookupStrategy(senderUserId, senderContextId),
            new GuardRecipKeyLookupStrategy(senderUserId, senderContextId),
            new MailResolverRecipKeyLookupStrategy(),
            new AutoCryptLookupStrategy(senderUserId, senderContextId),
            new WKSRecipKeyLookupStrategy(senderUserId, senderContextId),
            new HKPRecipKeyLookupStrategy(hkpClientToken, senderUserId, senderContextId),
            new GuestRecipKeyLookupStrategy(senderUserId, senderContextId),
        }).lookup(email, timeout);

        if(recipKey == null) {
            logger.info("Could not find a recipient for email {}", email);
        }
        else {
            if(recipKey.isNewKey()) {
                logger.info("Guard will create a new {} for email {}",
                    recipKey.isGuest() ? "guest key" : "key" ,
                    email);
            }
            else {
                logger.info("Found a recipient for email {}", email);
            }
        }
        return recipKey;
    }


}
