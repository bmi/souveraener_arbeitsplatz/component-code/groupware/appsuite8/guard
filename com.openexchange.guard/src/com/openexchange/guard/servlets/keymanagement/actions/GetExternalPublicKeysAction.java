/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.ExternalPublicKeyRingCollectionResponse;

/**
 * {@link GetExternalPublicKeysAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GetExternalPublicKeysAction extends GuardServletAction {

    private static final String KEY_TYPE_AUTOCRYPT = "autocrypt";

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        String type = ServletUtils.getStringParameter(request, Parameters.PARAM_KEY_TYPE, false);
        List<OGPGPKeyRing> keys = null;
        switch ((type == null ? "" : type)) {
            case KEY_TYPE_AUTOCRYPT:
                AutoCryptStorageService autocryptStorage = Services.getService(AutoCryptStorageService.class);
                keys = autocryptStorage.getAllKeys(userSession.getUserId(), userSession.getContextId());
                break;
            default:
                PublicExternalKeyService externalKeyService = Services.getService(PublicExternalKeyService.class);
                keys = externalKeyService.get(userSession.getUserId(), userSession.getContextId());
        }
        ServletUtils.sendObject(response, new ExternalPublicKeyRingCollectionResponse(keys));
    }
}
