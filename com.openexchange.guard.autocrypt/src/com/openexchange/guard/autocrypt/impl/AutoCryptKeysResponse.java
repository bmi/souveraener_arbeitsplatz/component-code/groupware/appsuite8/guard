/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.util.ArrayList;
import java.util.Iterator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;

/**
 * {@link AutoCryptKeysResponse}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptKeysResponse {

    private PGPPublicKeyRing ring;
    private boolean newKey;
    private boolean added;
    private String error;

    public AutoCryptKeysResponse (String error) {
        this.error = error;
        this.ring = null;
    }

    public AutoCryptKeysResponse (PGPPublicKeyRing ring, boolean newKey, boolean added) {
        this.ring = ring;
        this.newKey = newKey;
        this.added = added;
    }

    public boolean getNew() {
        return newKey;
    }

    public boolean getAdded() {
        return added;
    }

    public ArrayList<AutoCryptKeyResponse> getKeys() {
        if (ring == null) return null;
        ArrayList<AutoCryptKeyResponse> keys = new ArrayList<AutoCryptKeyResponse>();
        Iterator<PGPPublicKey> it = ring.getPublicKeys();
        while (it.hasNext()) {
            PGPPublicKey key = it.next();
            keys.add(new AutoCryptKeyResponse(key));
        }
        return keys;
    }

    public String getError() {
        return this.error;
    }

}
