/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.encryption;

public class RetrievedItem {

    private final int owner;

    private final long expiration;

    private final int type;

    private final String xml;

    private final String salt;

    private final String contentKey;

    public RetrievedItem(int owner, long expiration, int type, String xml, String salt, String contentKey) {
        this.owner = owner;
        this.expiration = expiration;
        this.type = type;
        this.xml = xml;
        this.salt = salt;
        this.contentKey = contentKey;
    }

    public int getOwner() {
        return owner;
    }

    public long getExpiration() {
        return expiration;
    }

    public int getType() {
        return type;
    }

    public String getXml() {
        return xml;
    }

    public String getSalt() {
        return salt;
    }

    public String getContentKey() {
        return contentKey;
    }
}
