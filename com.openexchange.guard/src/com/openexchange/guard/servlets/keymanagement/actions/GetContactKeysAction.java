/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_CONTACT_FOLDER;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_CONTACT_ID;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.services.ContactKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.ExternalPublicKeyRingCollectionResponse;

/**
 * {@link GetContactKeysAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GetContactKeysAction extends GuardServletAction {

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        final boolean mandatory = true;
        String contactFolder = ServletUtils.getStringParameter(request, PARAM_CONTACT_FOLDER, mandatory);
        String contactId = ServletUtils.getStringParameter(request, PARAM_CONTACT_ID, mandatory);
        ContactKeyService contactKeyService = Services.getService(ContactKeyService.class);

        List<OGPGPKeyRing> keys = contactKeyService.getKeysForContact(userSession.getUserId(), userSession.getContextId(), contactId, contactFolder);

        ServletUtils.sendObject(response, new ExternalPublicKeyRingCollectionResponse(keys));
    }
}
