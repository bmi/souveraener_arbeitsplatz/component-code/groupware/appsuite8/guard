/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;

/**
 * {@link SmimePrivateKeyStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public interface SmimePrivateKeyStorage {

    /**
     * Gets the {@link SmimePrivateKeys} for the given user and the specified serial number
     *
     * @param serial Serial number, should be BigInteger represented as string
     * @param userId The user identifier
     * @param cid The context identifier
     * @return The user's {@link SmimePrivateKeys} with the given serial number, or <code>null</code> if no such key was found
     * @throws OXException
     */
    SmimePrivateKeys getKeyById(String serial, int userId, int cid) throws OXException;

    /**
     * Get the {@link SmimePrivateKeys} which is marked as "current" for the given user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return The "current" {@link SmimePrivateKeys} for the given user, or <code>null</code> if no keys are present
     * @throws OXException
     */
    SmimePrivateKeys getCurrentPrivateKey(int userId, int cid) throws OXException;

    /**
     * Gets all {@link SmimePrivateKeys} for the user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return A list of {@link SmimePrivateKeys} for the given user
     * @throws OXException
     */
    List<SmimePrivateKeys> getPrivateKeys(int userId, int cid) throws OXException;

    /**
     * Store the private key
     *
     * @param keys {@link SmimeKeys} to store
     * @param userId The user identifier
     * @param cid The context identifier
     * @param replaceIfPresent Replace the key if already present with updated key or throw error
     * @throws OXException
     */
    void storePrivateKey(SmimeKeys keys, int userId, int cid, boolean replaceIfPresent) throws OXException;

    /**
     * Delete the {@link SmimePrivateKeys} associated with the user and serial number
     *
     * @param serial Serial number, should be BigInteger represented as string
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the key was deleted, <code>False</code> otherwise
     * @throws OXException
     */
    boolean deleteKeyById(String serial, int userId, int cid) throws OXException;

    /**
     * Deletes all known {@link SmimePrivateKeys} for the given user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if at least one {@link SmimePrivateKeys} was deleted, <code>False</code> if no {@link SmimePrivateKeys} were deleted.
     * @throws OXException
     */
    boolean deleteForUser(int userId, int cid) throws OXException;

    /**
     * Delete all {@link SmimePrivateKeys} associated with a context
     *
     * @param cid The context identifier
     * @return <code>True</code> if at least one {@link SmimePrivateKeys} was deleted, <code>False</code> if no {@link SmimePrivateKeys} were deleted.
     * @throws OXException
     */
    boolean deleteForContext(int cid) throws OXException;

    /**
     * Make the specified key marked as "current" for the given user
     *
     * @param serial The unique serial ID of the key which should be marked as "current"
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if a {@link SmimePrivateKeys} was marked as "current", <code>False</code> if no {@link SmimePrivateKeys} was found for the given serial ID and user identifier.
     * @throws OXException
     */
    boolean makeCurrent(String serial, int userId, int cid) throws OXException;

    /**
     * Update the recovery data for the key, if recovery is available/enabled for the given user
     *
     * @param key The {@link SmimePrivateKeys} to update if recovery is enabled
     * @param password The password of the private key
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the given {@link SmimePrivateKeys} instance was updated <code>False</code>, otherwise
     * @throws OXException
     */
    boolean updateRecovery(SmimePrivateKeys key, String password, int userId, int cid) throws OXException;

    /**
     * Change the password of a private key using the recovery-string in the given key (if available).
     *
     * @param key The {@link SmimePrivateKeys} to change, also containing the recovery-string to use. Changes are reflected to the given object.
     * @param newpass The new password to use
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the given {@link SmimePrivateKeys} instance was updated <code>False</code>, otherwise (i.e no recovery-string available)
     * @throws OXException
     */
    boolean changePasswordWithRecovery(SmimePrivateKeys key, String newPass, int userId, int cid) throws OXException;
}
