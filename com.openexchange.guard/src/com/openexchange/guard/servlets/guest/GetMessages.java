/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.guest.GuardGuestEmail;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.MailQuery;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.servlets.guest.responses.GuestEmailsResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GetMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GetMessages extends GuardGuestServletAction {

    private static final String JSON_DATA_FIELD_NAME  = "json";
    private static final String JSON_QUERY_FIELD_NAME = "query";
    private static final RangeInputValidator<Integer> rangeInputValidator = new RangeInputValidator<Integer>(1,Integer.MAX_VALUE);

    /**
     * Internal method to parse a {@link MailQuery} from the given json
     *
     * @param json The json to parse the {@link MailQuery} from
     * @return The parse {@link MailQuery}
     * @throws JSONException
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    private MailQuery parseMailQuery(JSONObject json) throws JSONException, JsonParseException, JsonMappingException, IOException {
        //Parse MailQuery object
        final JSONObject jsonQuery = json.getJSONObject(JSON_QUERY_FIELD_NAME);
        return new ObjectMapper().readValue(jsonQuery.toString(), MailQuery.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        //guest identity in the JSON body
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, true);
        UserIdentity guestIdentity = getAnyGuestIdentityFrom(json);
        if(guestIdentity != null) {
            final MailQuery mailQuery = parseMailQuery(json);

            if(mailQuery.getMailFlags() != null) {
                rangeInputValidator.assertIsValid(mailQuery.getMailFlags(), "mailFlags");
            }
            if(mailQuery.getNotMailFlags() != null) {
                rangeInputValidator.assertIsValid(mailQuery.getNotMailFlags(), "notMailFlags");
            }

            GuardGuestService guestService = Services.getService(GuardGuestService.class);
            final Collection<GuardGuestEmail> guestEmails = guestService.getEmailItems(guestIdentity, mailQuery);
            ServletUtils.sendObject(response, new GuestEmailsResponse(guestEmails));
        }
        else {
            ServletUtils.sendObject(response, new GuestEmailsResponse(Collections.emptyList()));
        }
    }
}
