/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.java.Strings;

/**
 * {@link CipherUtil}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public final class CipherUtil {

    private static final Logger logger = LoggerFactory.getLogger(CipherUtil.class);

    public static final String MD5 = "MD5";
    public static final String SHA_1 = "SHA-1";
    public static final String SHA_256 = "SHA-256";
    public static final String SHA_512 = "SHA-512";

    protected static final char[] encoding = "ybndrfg8ejkmcpqxot1uwisza345h769".toCharArray();

    /**
     * Get the SHA hash from the specified password.
     *
     * @param password The password to hash
     * @param salt The salt to use
     * @return The SHA hashed password or <code>null</code> if the given salt or password was null
     */
    public static String getSHA(String password, String salt) {
        try {
            if (password == null || salt == null) {
                return null;
            }
            MessageDigest md = MessageDigest.getInstance(SHA_256);
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] hashbytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            return Base64.encodeBase64String(hashbytes);
        } catch (Exception ex) {
            //TODO: throw exception?
            logger.error("Failure creating SHA digest", ex);
            return "";
        }
    }

    /**
     * Performs a hash operation on a given set of bytes
     *
     * @param algorithm The algorithm to use for hashign
     * @param bytes The bytes to has
     * @return The hash in hex-string representation
     * @throws NoSuchAlgorithmException
     */
    public static String getHash(String algorithm, byte[]...bytes) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        for (byte[] data : bytes) {
            if (data != null) {
                md.update(data);
            }
        }
        return Strings.asHex(md.digest());
    }

    /**
     * Get the MD5 hash from a string
     * @param data The string to get the has for
     * @return The MD5 hash of data
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String getMD5(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        StringBuffer ret = new StringBuffer();
        MessageDigest md = MessageDigest.getInstance(MD5);
        byte[] hashbytes = md.digest(data.getBytes(StandardCharsets.UTF_8));

        for (int i = 0; i < hashbytes.length; i++) {
            if ((0xff & hashbytes[i]) < 0x10) {
                ret.append("0" + Integer.toHexString((0xFF & hashbytes[i])));
            } else {
                ret.append(Integer.toHexString(0xFF & hashbytes[i]));
            }
        }
        return ret.toString();
    }

    /**
     * Generate a random UUID and return it as a string
     *
     * @return A random UUID as string
     */
    public static String getUUID() {
        UUID uid = java.util.UUID.randomUUID();
        return uid.toString();
    }

    /**
     * Function to generate random salt
     *
     * @return string of random salt
     */
    public static String generateSalt() {
        Random random = new SecureRandom();
        byte[] salt = new byte[20];
        random.nextBytes(salt);

        return Base64.encodeBase64String(salt);
    }

    public static String getEmailUserHash (String email) {
        if (email.contains("@")) {
            String username = email.substring(0, email.indexOf("@"));
            return getZbaseSHA (username);
        }
        return "";
    }


    /**
     * ZBase32 Encoding used in WebKey service
     * As described RFC6189, section 5.1.6
     * @param in
     * @return
     */
    public static String encodeZBase32(byte[] in) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < in.length; i += 5) {
            sb.append(
                    encoding[((in[i] & 0xff) >> 3) & 0x1f]
                );
            sb.append(
                    encoding[(((in[i] & 0xff) << 2) | ((in[i + 1] & 0xff) >> 6)) & 0x1f]
                );
            sb.append(
                    encoding[((in[i + 1] & 0xff) >> 1) & 0x1f]
                );
            sb.append(
                    encoding[(((in[i + 1] & 0xff) << 4) | ((in[i + 2] & 0xff) >> 4)) & 0x1f]
                );
            sb.append(
                    encoding[(((in[i + 2] & 0xff) << 1) | ((in[i + 3] & 0xff) >> 7)) & 0x1f]
                );
            sb.append(
                    encoding[((in[i + 3] & 0xff) >> 2) & 0x1f]
                );
            sb.append(
                    encoding[(((in[i + 3] & 0xff) << 3) | ((in[i + 4] & 0xff) >> 5)) & 0x1f]
                );
            sb.append(
                    encoding[(in[i + 4] & 0xff) & 0x1f]
                );
        }
        return (sb.toString());
    }

    public static String getZbaseSHA (String data) {
        MessageDigest crypt;
        try {
            crypt = MessageDigest.getInstance(SHA_1);
            crypt.update(data.toLowerCase().getBytes(StandardCharsets.UTF_8));
            return (encodeZBase32(crypt.digest()));
        } catch (NoSuchAlgorithmException e) {
            logger.error("Problem computing SHA hash", e);
            return null;
        }
    }
}
