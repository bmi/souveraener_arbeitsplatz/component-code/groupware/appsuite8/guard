/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.security.Key;
import java.security.PublicKey;

/**
 * {@link MasterKey}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.6
 */
public class MasterKey {

    private String rc, mc;
    private GuardKeys client;
    private GuardKeys masterEncr;
    private Key clientPrivateKey;
    private int index;

    /**
     * Initializes a new {@link MasterKey}.
     *
     * @param rc
     * @param mc
     * @param client Key used for client encryption for passwords, database
     * @param master Key used to encrypt the password recovery
     */
    public MasterKey(String rc, String mc, GuardKeys client, GuardKeys master, int index) {
        this.rc = rc;
        this.mc = mc;
        this.client = client;
        this.masterEncr = master;
        this.index = index;
    }

    public void setClientPrivateKey(Key key) {
        this.clientPrivateKey = key;
    }

    public Key getClientPrivateKey() {
        return clientPrivateKey;
    }

    /**
     * Gets the rc string
     * getRC
     *
     * @return
     */
    public String getRC() {
        return rc;
    }

    /**
     * Gets the mc string
     * getMC
     *
     * @return
     */
    public String getMC() {
        return mc;
    }

    /**
     * Check if the master password, used for password recovery, is avail
     * hasMaster
     *
     * @return
     */
    public boolean hasMaster() {
        return mc != null;
    }

    public String getMasterEncodedPrivate() {
        return masterEncr.getEncodedPrivate();
    }

    public PublicKey getMasterPublic() {
        return masterEncr.getPublicKey();
    }

    public String getMasterSalt() {
        return masterEncr.getSalt();
    }

    public String getClientEncodedPrivate() {
        return client.getEncodedPrivate();
    }

    public String getClientSalt() {
        return client.getSalt();
    }

    public PublicKey getClientPublic() {
        return client.getPublicKey();
    }

    public int getIndex() {
        return index;
    }

}
