/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.clt;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import com.openexchange.cli.AbstractRmiCLI;
import com.openexchange.guard.management.maintenance.GuardMaintenanceMBean;

/**
 * {@link AbstractGuardCLT}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public abstract class AbstractGuardCLT extends AbstractRmiCLI<Void> {

    private static final String serviceURL = "service:jmx:rmi:///jndi/rmi://";

    protected static final String DEFAULT_CONF_FOLDER = "/opt/open-xchange/etc";

    /**
     * Create JMX Environment
     *
     * @return
     */
    private static Map<String, Object> createEnvironment(String jmxUser, String jmxPassword) {
        Map<String, Object> environment = new HashMap<String, Object>();
        if (jmxUser != null && jmxPassword != null) {
            environment.put(JMXConnector.CREDENTIALS, new String[] { jmxUser, jmxPassword });
        }
        return environment;
    }

    protected MBeanServerConnection connect(String jmxHost, String jmxPort, String jmxUser, String jmxPassword) throws IOException {
        JMXServiceURL jmxServiceURL = new JMXServiceURL(serviceURL + jmxHost + ":" + jmxPort + "/server");
        JMXConnector jmxConnector = JMXConnectorFactory.connect(jmxServiceURL, createEnvironment(jmxUser, jmxPassword));
        return jmxConnector.getMBeanServerConnection();
    }

    protected ObjectName getObjectName() throws MalformedObjectNameException {
        return new ObjectName(GuardMaintenanceMBean.DOMAIN, GuardMaintenanceMBean.KEY, GuardMaintenanceMBean.VALUE);
    }

    protected GuardMaintenanceMBean createProxy(String jmxHost, String jmxPort, String jmxUser, String jmxPassword) throws MalformedObjectNameException, IOException {
        ObjectName objectName = getObjectName();
        MBeanServerConnection mbeanServerConnection = connect(jmxHost, jmxPort, jmxUser, jmxPassword);
        return MBeanServerInvocationHandler.newProxyInstance(mbeanServerConnection, objectName, GuardMaintenanceMBean.class, false);
    }

}
