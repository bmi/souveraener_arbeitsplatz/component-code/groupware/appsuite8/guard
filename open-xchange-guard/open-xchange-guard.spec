Name:          open-xchange-guard
BuildArch:     noarch
BuildRequires: ant
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: open-xchange-core
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GPL-2.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Open-Xchange Guard micro service
Autoreqprov:   no
Requires:      open-xchange >= 7.8.4
Requires:      open-xchange-core >= 7.8.4

%description
Guard - the Open-Xchange security solution. This package contains the Guard backend component.

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build
mkdir -p %{buildroot}/var/spool/open-xchange/guard/uploads
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}/opt/open-xchange/guard

%pre
. /opt/open-xchange/lib/oxfunctions.sh
if [ ${1:-0} -eq 2 ]; then
    # only when updating

    # SoftwareChange_Request-2933
    %if (0%{?rhel_version} && 0%{?rhel_version} >= 700) || (0%{?suse_version} && 0%{?suse_version} >= 1210)
        systemctl --no-reload disable open-xchange-guard.service > /dev/null 2>&1 || :
        systemctl stop open-xchange-guard.service > /dev/null 2>&1 || :
    %else
        /sbin/service open-xchange-guard stop >/dev/null 2>&1 || :
        /sbin/chkconfig --del open-xchange-guard || :
    %endif
fi

%post
. /opt/open-xchange/lib/oxfunctions.sh
if [ ${1:-0} -eq 2 ]; then
    # only when updating

    # prevent bash from expanding, see bug 13316
    GLOBIGNORE='*'

    # SoftwareChange_Request 2900, 2902, 2975, 3105, 3139
    # additionally backup old guard.properties file
    old_core_file=/opt/open-xchange/guard/etc/guard.properties
    old_core_file_back=/opt/open-xchange/etc/guard.properties.move
    new_core_file=/opt/open-xchange/etc/guard-core.properties
    key_prefix="com.openexchange.guard."
    core_names_22=( aesKeyLength backend_SSL badMinuteLock badPasswordCount cacheDays connectiontimeout cronHour databasePassword databaseUsername dbSchemaBaseName defaultLanguage exposedKeyDurationInHours externalEmailURL externalReaderPath guestSMTPPassword guestSMTPPort guestSMTPServer guestSMTPUsername keycachecheckinterval keyValidDays mailIdDomain mailresolverurl maxhttpconnections maxtotalconnections minpasswordlength newpasslength noDeletePrivate noDeleteRecovery noRecovery oxbackendidletime oxbackendpath OXBackendPort oxguardDatabaseHostname oxguardDatabaseRead oxguardShardDatabase oxguardShardRead passwordFromAddress passwordFromName pgpCacheDays publicKeyWhitelist publicPGPDirectory restApiHostname restApiPassword restApiUsername rsacache rsacachecount rsacertainty rsaKeyLength shardsize supportapipassword supportapiusername usestarttls productName )

    core_names_24=( aesKeyLength backendSSL badMinuteLock badPasswordCount cacheDays connectionTimeout cronHour databasePassword databaseUsername dbSchemaBaseName defaultLanguage exposedKeyDurationInHours externalEmailURL externalReaderPath guestSMTPPassword guestSMTPPort guestSMTPServer guestSMTPUsername keyCacheCheckInterval keyValidDays mailIdDomain mailResolverUrl maxHttpConnections maxTotalConnections minPasswordLength newPassLength noDeletePrivate noDeleteRecovery noRecovery oxBackendIdleTime oxBackendPath oxBackendPort oxGuardDatabaseHostname oxGuardDatabaseRead oxGuardShardDatabase oxGuardShardRead passwordFromAddress passwordFromName pgpCacheDays publicKeyWhitelist publicPGPDirectory restApiHostname restApiPassword restApiUsername rsaCache rsaCacheCount rsaCertainty rsaKeyLength shardSize supportApiPassword supportApiUsername useStartTLS productName )

    #if all needed files exist
    if [ -e $old_core_file ] && [ -e $new_core_file ]; then
      #make a backup of user changes
      cp $old_core_file $old_core_file_back
      last_index=$(( ${#core_names_22[@]}-1 ))
      for index in $(seq 0 $last_index); do
        old_name=${key_prefix}${core_names_22[$index]}
        new_name=${key_prefix}${core_names_24[$index]}
        #if property is uncommented
        if $(ox_exists_property ${old_name} ${old_core_file}); then
          value=$(ox_read_property ${old_name} ${old_core_file})
          #and property has a value
          if [ -n "${value}" ]; then
            ox_comment ${new_name} remove ${new_core_file}
            ox_set_property ${new_name} "${value}" ${new_core_file}
          fi
        fi
      done
    fi

    # SoftwareChange_Request-3307
    ox_add_property com.openexchange.guard.defaultAdvanced false /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3313
    ox_add_property com.openexchange.guard.trustedPGPDirectory "" /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.untrustedPGPDirectory "" /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3318
    ox_add_property com.openexchange.guard.mailResolverUrl.basicAuthUsername "" /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.mailResolverUrl.basicAuthPassword "" /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3357
    ox_add_property com.openexchange.guard.authLifeTime 1W /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3534
    ox_add_property com.openexchange.guard.temporaryTokenLifespan 48 /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3560
    ox_add_property com.openexchange.guard.newGuestsRequirePassword false /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3645
    ox_add_property com.openexchange.guard.dns.allowUnsignedSRVRecords true /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3646
    ox_add_property com.openexchange.guard.forceTLSForHKPSRV false /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-3647
    PFILE=/opt/open-xchange/etc/guard-core.properties
    if ! grep "com.openexchange.guard.fileUploadDirectory" >/dev/null $PFILE; then
        echo -e "\n# File Upload\n" >> $PFILE
        echo "# The directory used for buffering uploaded data." >> $PFILE
        echo "# Default: empty which falls back to use \"java.io.tmpdir\"" >> $PFILE
        echo -e "# com.openexchange.guard.fileUploadDirectory=\n" >> $PFILE
    fi
    if ! grep "com.openexchange.guard.fileUploadBufferThreshhold" >/dev/null $PFILE; then
        echo "# The threshold in bytes at which uploaded data will be buffered to disk." >> $PFILE
        echo "# NOTE: Guard does only buffer non sensitive or encrypted data to disk." >> $PFILE
        echo "# Default: 10240 = 10 Kb" >> $PFILE
        echo -e "# com.openexchange.guard.fileUploadBufferThreshhold=10240\n" >> $PFILE
    fi

    # SoftwareChange_Request-3736
    ox_add_property com.openexchange.guard.attachHelpFile true /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-4293
    ox_add_property com.openexchange.guard.guestSMTPMailFrom "" /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-4488
    pname=com.openexchange.guard.cacheDays
    pfile=/opt/open-xchange/etc/guard-core.properties
    [[ -n "$(ox_read_property $pname $pfile)" ]] && ox_remove_property $pname $pfile
    ox_add_property com.openexchange.guard.guestCleanedAfterDaysOfInactivity 365 $pfile

    # SoftwareChange_Request-80
    ox_add_property com.openexchange.guard.keySources.trustThreshold 4 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelGuard 5 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelGuardUserUploaded 4 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelGuardUserShared 3 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelHKPPublicServer 1 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelHKPTrustedServer 5 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelHKPSRVServer 4 /opt/open-xchange/etc/guard-core.properties
    ox_add_property com.openexchange.guard.keySources.trustLevelHKPSRVDNSSECServer 4 /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-105
    ox_add_property com.openexchange.guard.guestCaching true /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-152
    ox_add_property com.openexchange.guard.remoteKeyLookupTimeout 1000 /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-169
    ox_add_property com.openexchange.guard.pinEnabled false /opt/open-xchange/etc/guard-core.properties

    # SoftwareChange_Request-181
    ox_add_property com.openexchange.guard.failForMissingMDC false /opt/open-xchange/etc/guard-core.properties

    SCR=SCR-333
    if ox_scr_todo ${SCR}
    then
      pfile=/opt/open-xchange/etc/guard-core.properties
      ox_add_property com.openexchange.guard.autoCryptEnabled true ${pfile}
      ox_add_property com.openexchange.guard.autoCryptMutual true ${pfile}
      ox_add_property com.openexchange.guard.keySources.trustLevelAutoCrypt 2  ${pfile}
      ox_add_property com.openexchange.guard.keySources.trustLevelAutoCryptUserVerified 5 ${pfile}
      ox_scr_done ${SCR}
    fi

    SCR=SCR-998
    if ox_scr_todo ${SCR}
    then
        ox_remove_property com.openexchange.guard.keySources.trustLevelWKSSRVDNSSECServer /opt/open-xchange/etc/guard-core.properties
        ox_scr_done ${SCR}
    fi
fi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
/opt/open-xchange/templates/guard
/opt/open-xchange/i18n/*
/opt/open-xchange/bundles/com.openexchange.guard.*
/opt/open-xchange/lib/com.openexchange.guard.*
/opt/open-xchange/osgi/bundle.d/com.openexchange.guard.*
/opt/open-xchange/sbin/guard
/opt/open-xchange/sbin/smime
/opt/open-xchange/etc/meta/guard-default.yml
%config(noreplace) %attr(640, root, open-xchange) /opt/open-xchange/etc/guard-core.properties
%config(noreplace) /opt/open-xchange/etc/meta/guard.yml
%config(noreplace) %attr(640, root, open-xchange) /opt/open-xchange/etc/security/guard.list
/var/spool/open-xchange/guard
%dir %attr(750, open-xchange, root) /var/spool/open-xchange/guard/uploads
%dir %attr(750, open-xchange, root) /opt/open-xchange/guard

%changelog
* Thu Aug 11 2022 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.7 release
* Thu Aug 11 2022 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.7 release
* Mon Jul 04 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-07-11 (6145)
* Thu Jun 02 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-05-30 (6138)
* Wed Mar 16 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-03-21 (6116)
* Mon Feb 21 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-02-22 (6107)
* Mon Feb 14 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-02-15 (6099)
* Mon Jan 17 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-01-24
* Mon Nov 29 2021 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.6 release
* Fri Oct 22 2021 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.6 release
* Wed Jun 09 2021 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.6 release
* Wed Apr 21 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-04-27 (5984)
* Tue Mar 23 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-03-29 (5979)
* Thu Feb 11 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-02-22 (5954)
* Mon Feb 01 2021 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.10.5 release
* Fri Jan 15 2021 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.5 release
* Thu Dec 17 2020 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.5 release
* Mon Nov 30 2020 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.5 release
* Tue Oct 06 2020 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.5 release
* Tue Jul 28 2020 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.4 release
* Tue Jun 30 2020 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.4 release
* Wed May 20 2020 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.4 release
* Mon Feb 03 2020 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.4
* Thu Nov 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate of 2.10.3 release
* Thu Nov 21 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 2.10.3 release
* Thu Oct 17 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.3 release
* Wed Jun 19 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.3 release
* Fri May 10 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 2.10.2 release
* Wed May 01 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.2 release
* Thu Mar 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.2 release
* Mon Mar 11 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.2
* Fri Nov 23 2018 Marcus Klein <marcus.klein@open-xchange.com>
RC 1 for 2.10.1 release
* Mon Nov 05 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 2.10.1 release
* Fri Oct 12 2018 Marcus Klein <marcus.klein@open-xchange.com>
First preview for 2.10.1 release
* Mon Sep 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.1
* Mon Jun 25 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.10.0 release
* Mon Jun 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.0 release
* Fri May 18 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 2.10.0 release
* Thu Apr 19 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 2.10.0 release
* Tue Apr 03 2018 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 2.10.0 release
* Tue Feb 20 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.0 release
* Fri Feb 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.0 release
* Wed Jan 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.0 release
* Tue May 16 2017 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.8.0 release
* Thu May 04 2017 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.8.0 release
* Mon Apr 03 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.8.0 release
* Fri Dec 02 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.8.0 release
* Fri Nov 25 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second release candidate for 2.6.0 release
* Thu Nov 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First release candidate for 2.6.0 release
* Tue Nov 15 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 2.6.0 release
* Sat Oct 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.6.0 release
* Fri Oct 14 2016 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.6.0 release
* Wed Oct 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.6.0 release
* Tue Jul 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.2 release
* Wed Jul 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.2 release
* Wed Jun 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 2.4.2 release
* Thu Jun 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.2 release
* Thu Jun 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.4.2
* Wed Mar 30 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.0 release
* Thu Mar 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.0 release
* Wed Mar 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 2.4.0 release
* Fri Mar 04 2016 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 2.4.0 release
* Sat Feb 20 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third candidate for 2.4.0 release
* Fri Feb 05 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.0 release
* Fri Feb 05 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.0 release
* Sun Dec 06 2015 Greg <greg@ubuntu>
Second candidate for 2.2.1 release
* Mon Nov 09 2015 Greg <greg@ubuntu>
First candidate for 2.2.1 release
* Mon Nov 09 2015 Greg <greg@ubuntu>
First release 2.2.1
* Fri Oct 23 2015 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.4.0 release
* Mon Oct 05 2015 Marcus Klein <marcus.klein@open-xchange.com>
Sixth candidate release for 2.2.0
* Sun Sep 27 2015 Marcus Klein <marcus.klein@open-xchange.com>
Fifth candidate release for 2.2.0
* Mon Sep 21 2015 Marcus Klein <marcus.klein@open-xchange.com>
Fourth candidate release 2.2.0
* Mon Sep 14 2015 Greg Hill <greg.hill@open-xchange.com>
Build for patch 2015-09-24 (2756)
* Mon Sep 14 2015 Marcus Klein <marcus.klein@open-xchange.com>
Third candidate release 2.2.0
* Mon Aug 24 2015 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.2.0 release
* Mon Aug 10 2015 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.2.0 release
* Mon Aug 10 2015 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.2.0 release
* Wed Jul 29 2015 Greg Hill <greg.hill@open-xchange.com>
Build for patch 2015-07-27 (2626)
* Wed Jul 08 2015 Greg Hill <greg.hill@open-xchange.com>
Build for patch 2015-07-09 (2612)
* Tue Jun 30 2015 Greg Hill <greg.hill@open-xchange.com>
Final release candidate for Guard 2.0
* Mon Jun 29 2015 Greg Hill <greg.hill@open-xchange.com>
Fifth candidate for 2.0
* Sun Jun 28 2015 Greg Hill <greg.hill@open-xchange.com>
Fourth candidate for 2.0 release
* Sun Jun 21 2015 Greg Hill <greg.hill@open-xchange.com>
Third candidate for 2.0 release
* Sun Jun 14 2015 Greg Hill <greg.hill@open-xchange.com>
Second candidate for 2.0 release
* Sun Jun 07 2015 Greg Hill <greg.hill@open-xchange.com>
First candidate for 2.0 release
* Mon Feb 09 2015 Greg Hill <greg.hill@open-xchange.com>
prepare for 2.0.0 release
* Sun Jan 04 2015 Greg Hill <greg.hill@open-xchange.com>
Third candidate for 1.2.0 release
* Wed Dec 10 2014 Greg Hill <greg.hill@open-xchange.com>
Second candidate for 1.2.0 release
* Thu Oct 30 2014 Greg Hill <greg.hill@open-xchange.com>
Build for 1.2
* Fri Oct 10 2014 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2014-10-09
* Wed Sep 03 2014 Marcus Klein <marcus.klein@open-xchange.com>
Third candidate for 1.0.0 release
* Tue Sep 02 2014 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 1.0.0 release
* Thu Aug 28 2014 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 1.0.0 release
* Thu Aug 28 2014 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 1.0.0
