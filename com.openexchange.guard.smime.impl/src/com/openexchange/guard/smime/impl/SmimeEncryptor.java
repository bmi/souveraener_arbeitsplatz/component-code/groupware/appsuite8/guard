/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.activation.DataHandler;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.capabilities.Capability;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.RecipCertificateService;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.smime.storage.SmimeCapabilityStorage;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SmimeEncryptor}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimeEncryptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmimeEncryptor.class);

    private final ServiceLookup services;

    /**
     *
     * Initializes a new {@link SmimeEncryptor}.
     *
     * @param services The service lookup
     */
    public SmimeEncryptor(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Check if user has Guard-mail capability. Throw error if not
     * checkGuardMailCapable
     *
     * @param userId
     * @param contextId
     * @throws OXException Thrown if user doesn't have guard-mail
     */
    private void checkCapabilities(int userId, int contextId) throws OXException {
        CapabilityService capabilities = services.getServiceSafe(CapabilityService.class);
        if (!capabilities.getCapabilities(userId, contextId).contains(new Capability("smime"))) {
            throw SmimeExceptionCodes.NOT_AUTHORIZED.create();
        }
    }

    /**
     * Get the signing key for the message
     *
     * @param userId User Id
     * @param contextId Context Id of the user
     * @param password Password used to decrypt private key
     * @param fromEmail Email address of the sender
     * @return The SmimeKey containing the private key used to sign the email
     * @throws OXException
     */
    private SmimeKeys getSigningKey(int userId, int contextId, String password, String fromEmail) throws OXException {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        List<SmimeKeys> keys = keyService.getKeys(userId, contextId);
        if (keys.isEmpty()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create(fromEmail);
        }
        // Find a singing key.  Should be valid, marked for signing, and have the email address match the from address
        //@formatter:off
        List<SmimeKeys> keysAvailable = keys.stream().filter((k) -> 
            !k.isExpired() && 
            k.isForSigning() && 
            Strings.isNotEmpty(k.getEmail()) 
            && k.getEmail().toLowerCase().equals(fromEmail.toLowerCase())
        ).collect(Collectors.toList());
        //@formatter:on
        if (keysAvailable.isEmpty()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND_FOR_SIGNING.create();
        }

        SmimeKeys foundKey = null;
        // If only one matching key found, use that
        if (keysAvailable.size() == 1) {
            foundKey = keysAvailable.get(0);  // only one key found matching email
        } else {
            // See if any are marked as current and use the password sent
            Optional<SmimeKeys> current = keysAvailable.stream().filter((k) -> {
                try {
                    return (k.isCurrent() && SmimeCryptoKeyUtil.canDecryptPrivateKey(k, password));
                } catch (OXException e1) {
                    LOGGER.debug("Error while checking certificate", e1);
                    return false;
                }
            }).findAny();
            if (current.isPresent()) {  // Use if present
                foundKey = current.get();
            } else { // Otherwise just take first match that uses the correct password
                Optional<SmimeKeys> decryptable = keysAvailable.stream().filter((k) -> {
                    try {
                        return SmimeCryptoKeyUtil.canDecryptPrivateKey(k, password);
                    } catch (OXException e) {
                        LOGGER.debug("Error while decoding", e);
                        return false;
                    }
                }).findFirst();
                if (decryptable.isPresent()) {
                    foundKey = decryptable.get();
                }
            }
        }
        if (foundKey == null) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND_FOR_SIGNING.create();
        }
        // Check password is correct
        SmimeCryptoKeyUtil.decryptPrivateKey(foundKey, password);
        return foundKey;
    }

    /**
     * Find the sender certificate to use for encryption.
     * Returns the users selected "current" key, or if none marked, one that matches email address
     *
     *
     * @param userId
     * @param contextId
     * @param fromEmail
     * @return Certificate to use
     * @throws OXException
     */
    private X509Certificate findSenderCertificate(int userId, int contextId, String fromEmail) throws OXException {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        // First, let's add the senders key marked as current
        X509Certificate senderCert = null;
        SmimeKeys currentKey = keyService.getCurrentKey(userId, contextId);
        if (currentKey != null) {
            senderCert = currentKey.getCertificate();
        }
        // If that fails, search keys for one that matches email address
        if (senderCert == null) {
            List<SmimeKeys> senderKeys = keyService.getKeys(userId, contextId);
            for (SmimeKeys key : senderKeys) {
                if (key.hasPrivateKey() && key.getPrivateKey().getEmail().equals(fromEmail)) {
                    senderCert = key.getCertificate();
                    break;
                }
            }
        }
        // Can't find match.  Throw error
        if (senderCert == null) {
            throw SmimeExceptionCodes.UNABLE_TO_FIND_SENDER.create();
        }
        return senderCert;
    }

    /**
     * Signing plaintext email
     *
     * @param output Output stream to write
     * @param userId User Id
     * @param contextId Context of the user
     * @param password Password for the private key
     * @param msg GuardParsedMimeMessage
     * @throws Exception
     */
    public void doSigning(OutputStream output, int userId, int contextId, String password, GuardParsedMimeMessage msg) throws Exception {
        checkCapabilities(userId, contextId);
        sign(userId, contextId, password, msg).writeTo(output);
    }

    private MimeMessage sign(int userId, int contextId, String password, GuardParsedMimeMessage msg) throws Exception {
        MimeMessage toSign = msg.getMessage();
        String fromEmail = msg.getFromAddress().getAddress();
        MimeBodyPart bp = new MimeBodyPart();
        bp.setDataHandler(new DataHandler(new ByteArrayDataSource(new SignatureStream(toSign.getInputStream()), toSign.getContentType())));

        MimeMultipart result = SMimeSigner.signMime(bp, getSigningKey(userId, contextId, password, fromEmail), password);
        MimeMessage signedMime = new MimeMessage(Session.getDefaultInstance(new Properties()));
        copyHeaders(toSign, signedMime);
        signedMime.setContent(result, signedMime.getContentType());
        signedMime.saveChanges();
        return signedMime;
    }

    /**
     * Perform SMIME encryption with or without signing
     *
     * @param output Output stream to write
     * @param userId User Id
     * @param contextId Context of the user
     * @param password Password for the private key
     * @param msg GuardParsedMimeMessage
     * @throws Exception
     */
    public void doEncryption(OutputStream output, int userId, int contextId, String password, GuardParsedMimeMessage msg) throws Exception {
        checkCapabilities(userId, contextId);
        MimeMessage message;
        if (msg.isSign()) {
            message = sign(userId, contextId, password, msg);
        } else {
            message = new MimeMessage(Session.getDefaultInstance(new Properties()));
            copyHeaders(msg.getMessage(), message);
            try (InputStream in = msg.getMessage().getInputStream()) {
                GuardSmimeUtil.updateFromInputStream(message, in, msg.getMessage().getContentType());
            }

        }
        SMIMEEnvelopedGenerator encrypter = new SMIMEEnvelopedGenerator();
        // Add sender certificate
        X509Certificate senderCert = findSenderCertificate(userId, contextId, msg.getFromAddress().getAddress());
        encrypter.addRecipientInfoGenerator(new JceKeyTransRecipientInfoGenerator(senderCert).setProvider("BC"));
        JSONArray recips = msg.getRecipients();
        ArrayList<X509Certificate> recipientCertificates = new ArrayList<>(recips.length());
        RecipCertificateService recipCertificateService = services.getServiceSafe(RecipCertificateService.class);
        for (int i = 0; i < recips.length(); i++) {
            try {
                InternetAddress emailAddress = new InternetAddress(IDNUtil.aceEmail(((JSONObject) recips.get(i)).getString("Email")));
                RecipCertificate recipientCertificate = recipCertificateService.getRecipCertificate(userId, contextId, emailAddress.getAddress());
                if (recipientCertificate == null) {
                    throw SmimeExceptionCodes.KEY_NOT_FOUND.create(emailAddress.getAddress());
                }
                encrypter.addRecipientInfoGenerator(new JceKeyTransRecipientInfoGenerator(recipientCertificate.getSmimeKeys().getCertificate()).setProvider("BC"));
                recipientCertificates.add(recipientCertificate.getSmimeKeys().getCertificate());

            } catch (AddressException | JSONException e) {
                throw OXException.general(e.getMessage());
            }
        }
        //@formatter:off
        ASN1ObjectIdentifier symmetricEncryptionAlgorithm = new SMIMECapabilityHandler()
            .setSymmetricFallback(NISTObjectIdentifiers.id_aes128_CBC) //S/MIME v3.2 default
            .setCapabilityStorage(services.getOptionalService(SmimeCapabilityStorage.class))
            .getSymmetricEncryptionAlgorithm(recipientCertificates);
        //@formatter:on
        LOGGER.debug("Using the following symmetric algorithm for S/MIME content encryption: " + symmetricEncryptionAlgorithm);

        MimeBodyPart encryptedPart = encrypter.generate(message, new JceCMSContentEncryptorBuilder(symmetricEncryptionAlgorithm).setProvider("BC").build());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        encryptedPart.writeTo(out);
        MimeMessage encrMime = new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(out.toByteArray()));
        copyHeaders(message, encrMime);
        //encrMime.setContent(encryptedPart, encryptedPart.getContentType());
        encrMime.saveChanges();
        encrMime.writeTo(output);

    }

    /**
     * Copy Headers from original mail to new
     *
     * @param from
     * @param to
     */
    private void copyHeaders(MimeMessage from, MimeMessage to) {
        Enumeration<Header> headers;
        try {
            headers = from.getAllHeaders();
            while (headers.hasMoreElements()) {
                Header h = headers.nextElement();
                if (!h.getName().contains("Content-")) {
                    to.addHeader(h.getName(), h.getValue());
                }
            }
        } catch (MessagingException e) {
            LOGGER.debug("Unable to copy header", e);
        }
    }

}
