/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.init;

import com.openexchange.database.AbstractCreateTableImpl;
import com.openexchange.database.CreateTableService;

/**
 *
 * {@link CreateTableService} to add guard related tables to ox user schemata.<br>
 * <br>
 * In addition these statements are used to create new guest shards.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public final class GuardCreateTableService extends AbstractCreateTableImpl {

    private static final String KEY_TABLE = "og_KeyTable";

    private static final String KEY_TABLE_CREATE_STMT = "CREATE TABLE " + KEY_TABLE + " (\n" +
        "  id int(11) NOT NULL AUTO_INCREMENT,\n" +
        "  PGPSecret text COLLATE utf8_unicode_ci,\n" +
        "  PGPPublic mediumtext COLLATE utf8_unicode_ci,\n" +
        "  RSAPrivate varchar(6000) COLLATE utf8_unicode_ci,\n" +
        "  RSAPublic varchar(2000) COLLATE utf8_unicode_ci,\n" +
        "  Recovery varchar(2000) COLLATE utf8_unicode_ci,\n" +
        "  Salt varchar(40) COLLATE utf8_unicode_ci,\n" +
        "  cid int(11) NOT NULL DEFAULT '0',\n" +
        "  email varchar(250) COLLATE utf8_unicode_ci NOT NULL,\n" +
        "  lastMod datetime DEFAULT NULL,\n" +
        "  question varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  answer varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  misc varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  lang  varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  settings varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  current bit(1) NOT NULL DEFAULT b'1',\n" +
        "  keyid bigint(20) NOT NULL DEFAULT '0',\n" +
        "  version int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',\n" +
        "  inline bit(1) NOT NULL DEFAULT b'0',\n" +
        "  mKeyIndex int(11) NOT NULL DEFAULT '0',\n" +
        "  PRIMARY KEY (id, cid, keyid),\n" +
        "  KEY email (email)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

    private static final String OG_PGP_KEYS_TABLE = "og_pgp_keys";

    private static final String OG_PGP_KEYS_TABLE_CREATE_STMT = "CREATE TABLE " + OG_PGP_KEYS_TABLE + " (\n" +
        "  ids varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',\n" +
        "  Email varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',\n" +
        "  PGPPublic mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,\n" +
        "  userid int(11) NOT NULL DEFAULT '0',\n" +
        "  cid int(11) NOT NULL DEFAULT '0',\n" +
        "  share_level int(11) DEFAULT NULL,\n" +
        "  inline bit(1) DEFAULT b'0',\n" +
        "  PRIMARY KEY (Email,cid,userid),\n" +
        "  KEY id (ids)\n" +
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

    private static final String ENCRYPTED_ITEMS_TABLE = "og_encrypted_items";

    private static final String ENCRYPTED_ITEMS_TABLE_CREATE_STMT = "CREATE TABLE " + ENCRYPTED_ITEMS_TABLE + " (\n" +
        "  Id varchar(38) NOT NULL,\n"+
        "  Owner int(11) DEFAULT NULL,\n" +
        "  Expiration bigint(20) DEFAULT NULL,\n" +
        "  Type int(11) DEFAULT NULL,\n" +
        "  XML varchar(10000) DEFAULT NULL,\n" +
        "  Owner_cid int(11) DEFAULT NULL,\n"+
        "  Salt varchar(50) DEFAULT NULL,\n" +
        "  PRIMARY KEY (Id(36))\n" +
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"; //TODO fehlt hier nicht das COLLATE=utf...?

    private static final String MAILVELOPE_TABLE = "og_mailvelope";

    private static final String CREATE_MAILVELOPE_TABLE = "CREATE TABLE og_mailvelope (" +
            "`id`  int NOT NULL , " +
            "`cid`  int NOT NULL , " +
            "`keydata`  mediumtext NULL , "+
            "PRIMARY KEY (`id`, `cid`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

    /**
     * Gets the table names.
     *
     * @return The table names.
     */
    public static String[] getTablesToCreate() {
        return new String[] { ENCRYPTED_ITEMS_TABLE, KEY_TABLE, OG_PGP_KEYS_TABLE, MAILVELOPE_TABLE };
    }

    /**
     * Gets the CREATE-TABLE statements.
     *
     * @return The CREATE statements
     */
    public static String[] getCreateStmts() {
        return new String[] { ENCRYPTED_ITEMS_TABLE_CREATE_STMT, KEY_TABLE_CREATE_STMT, OG_PGP_KEYS_TABLE_CREATE_STMT, CREATE_MAILVELOPE_TABLE };
    }

    /**
     * Initializes a new {@link GuardCreateTableService}.
     */
    public GuardCreateTableService() {
        super();
    }

    @Override
    public String[] requiredTables() {
        return NO_TABLES;
    }

    @Override
    public String[] tablesToCreate() {
        return getTablesToCreate();
    }

    @Override
    protected String[] getCreateStatements() {
        return getCreateStmts();
    }

}
