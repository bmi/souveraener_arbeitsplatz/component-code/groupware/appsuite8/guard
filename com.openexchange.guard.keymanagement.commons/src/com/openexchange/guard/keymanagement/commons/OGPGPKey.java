/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.util.List;

/**
 * {@link OGPGPKey} represents an PGP key which has been uploaded by a user
 *
 * Users are able to upload public PGP keys from other external, non OX Guard users, in order to securely interact with them
 */
public class OGPGPKey {

    private int userId;
    private int contextId;
    private String email;
    private List<String> keyIds;
    private String publicPGPAscData;
    private int shareLevel;
    private boolean inline;

    /**
     * Initializes a new {@link OGPGPKey}.
     *
     * @param userId The uploader's user ID
     * @param contextId the uploader's context ID
     * @param email the email related to the key
     * @param keyIds The key IDs related to the key
     * @param publicPGPAscData the key's ASCII armored PGP data
     * @param shareLevel the level of share
     * @param inline whether OX Guard should use PGP inline while using this key, or not
     */
    public OGPGPKey(int userId, int contextId, String email, List<String> keyIds, String publicPGPAscData, int shareLevel, boolean inline) {
        this.userId = userId;
        this.contextId = contextId;
        this.email = email;
        this.keyIds = keyIds;
        this.publicPGPAscData = publicPGPAscData;
        this.shareLevel = shareLevel;
        this.inline = inline;
    }

    /**
     * @return the user's/uploader's Id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the user's/uploader's Id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the user's/uploader's Id
     */
    public int getContextId() {
        return contextId;
    }

    /**
     * @param contextId the user's/uploader's Id
     */
    public void setContextId(int contextId) {
        this.contextId = contextId;
    }

    /**
     * @return The email related to the key
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email the email related to the key
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the PGP key IDs related to the public key
     */
    public List<String> getKeyIds() {
        return keyIds;
    }

    public static String keyIdsAsString(List<String> keyIds) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyIds.size(); i++) {
            if(i > 0) {
                sb.append(" ");
            }
            sb.append(keyIds.get(i));
        }
        return sb.toString();
    }

    public String getKeyIdsAsString() {
        return keyIdsAsString(this.keyIds);
    }

    /**
     * @param keyIds the PGP key IDs related to the public key
     */
    public void setKeyIds(List<String> keyIds) {
        this.keyIds = keyIds;
    }

    /**
     * @return the public key's ASCII armored PGP data
     */
    public String getPublicPGPAscData() {
        return publicPGPAscData;
    }

    /**
     * @param publicPGPAscData the public key's ASCII armored PGP data
     */
    public void setPublicPGPAscData(String publicPGPAscData) {
        this.publicPGPAscData = publicPGPAscData;
    }

    /**
     * @return the key's share level
     */
    public int getShareLevel() {
        return shareLevel;
    }

    /**
     * @param shareLevel the key's new share level
     */
    public void setShareLevel(int shareLevel) {
        this.shareLevel = shareLevel;
    }

    /**
     * The inline mode which should be used for this key
     *
     * @return whether PGP inline should be used while using this key or not
     */
    public boolean isInline() {
        return inline;
    }

    /**
     * The inline mode which should be used for this key
     *
     * @param inline whether PGP inline should be used while using this key or not
     */
    public void setInline(boolean inline) {
        this.inline = inline;
    }
}
