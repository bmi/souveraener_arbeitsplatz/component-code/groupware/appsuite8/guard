/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.secondfactor.impl;

import com.openexchange.exception.OXException;
import com.openexchange.guard.secondfactor.SecondFactorService;

/**
 * {@link DisabledPinImplementation}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class DisabledPinImplementation implements SecondFactorService {

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#hasSecondFactor(int, int)
     */
    @Override
    public boolean hasSecondFactor(int userid, int cid, int guardId, int guardCid) throws OXException {
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#addSecondFactor(int, int, java.lang.String)
     */
    @Override
    public boolean addSecondFactor(int userid, int cid, int guardUserId, int guardCid, String token) throws OXException {
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#verifySecondFactor(int, int, java.lang.String)
     */
    @Override
    public void verifySecondFactor(int userid, int cid, int guardUserId, int guardCid, String token) throws OXException {
        return;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#removeSecondFactor(int, int)
     */
    @Override
    public void removeSecondFactor(int userid, int cid) throws OXException {
        return;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#removeSecondFactorIfSingleUse(int, int)
     */
    @Override
    public void removeSecondFactorIfSingleUse(int userid, int cid) throws OXException {
        return;
    }

}
