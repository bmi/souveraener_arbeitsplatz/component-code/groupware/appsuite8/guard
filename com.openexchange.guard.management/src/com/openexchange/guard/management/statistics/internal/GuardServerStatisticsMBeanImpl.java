/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.statistics.internal;

import java.lang.management.ManagementFactory;
import java.text.NumberFormat;
import javax.management.MBeanException;
import javax.management.NotCompliantMBeanException;
import javax.management.StandardMBean;
import com.openexchange.exception.OXException;
import com.openexchange.guard.management.osgi.Services;
import com.openexchange.guard.management.statistics.GuardServerStatisticsMBean;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * MBean statistic implementation
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardServerStatisticsMBeanImpl extends StandardMBean implements GuardServerStatisticsMBean {

    //private final HttpPoolingStats stats;

    /**
     * Initialises a new {@link GuardServerStatisticsMBeanImpl}.
     *
     * @throws NotCompliantMBeanException
     */
    public GuardServerStatisticsMBeanImpl() throws NotCompliantMBeanException {
        super(GuardServerStatisticsMBean.class);
    }

    @Override
    public synchronized String getPoolInfo() throws MBeanException {
        HttpConnectionPoolService poolService = getHttpConnectionPoolService();
        return ("HTTP pool connections- In Pool : " + poolService.getStats().getAvailable() + " Active: " + poolService.getStats().getLeased() + " Hold: " + poolService.getStats().getPending());
    }

    @Override
    public synchronized String getMemoryInfo() {
        return ("Total: " + NumberFormat.getIntegerInstance().format(getTotalMemory()) + " Used: " + NumberFormat.getIntegerInstance().format(getUsedMemory()) + " Free: " + NumberFormat.getIntegerInstance().format(getFreeMemory()) + " Max: " + NumberFormat.getIntegerInstance().format(getMaxMemory()));
    }

    @Override
    public synchronized int getPoolCount() throws MBeanException {
        HttpConnectionPoolService poolService = getHttpConnectionPoolService();
        return (poolService.getStats().getAvailable());
    }

    @Override
    public synchronized int getPoolActive() throws MBeanException {
        HttpConnectionPoolService poolService = getHttpConnectionPoolService();
        return (poolService.getStats().getLeased());
    }

    @Override
    public synchronized int getPoolWaiting() throws MBeanException {
        HttpConnectionPoolService poolService = getHttpConnectionPoolService();
        return (poolService.getStats().getPending());
    }

    @Override
    public synchronized long getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.totalMemory() - runtime.freeMemory());
    }

    @Override
    public synchronized long getFreeMemory() {
        return (Runtime.getRuntime().freeMemory());
    }

    @Override
    public synchronized long getMaxMemory() {
        return (Runtime.getRuntime().maxMemory());
    }

    @Override
    public synchronized long getTotalMemory() {
        return (Runtime.getRuntime().totalMemory());
    }

    @Override
    public int getThreadCount() {
        return (ManagementFactory.getThreadMXBean().getThreadCount());
    }

    private HttpConnectionPoolService getHttpConnectionPoolService() throws MBeanException {
        try {
            return Services.getService(HttpConnectionPoolService.class);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }
}
