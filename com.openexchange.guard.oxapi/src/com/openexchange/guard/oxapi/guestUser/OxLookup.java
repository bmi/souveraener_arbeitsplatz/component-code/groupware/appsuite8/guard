/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.guestUser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.AbstractOXCalls;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionCodes;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * Looks up any OX userId associated with context and email
 * {@link OxLookup}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.5
 */
public class OxLookup extends AbstractOXCalls {

    private static Logger LOG = LoggerFactory.getLogger(OxLookup.class);
    private static final String SESSION_REST_PATH = "/preliminary/guard/guest/v1/getoxid";
    private static final String MIME_JSON = "application/json";
    private static final String RESULT_ID = "id";

    /**
     * Internal method to create a share link/guest
     *
     * @param url The full URL
     * @return The result object
     * @throws OXException
     * @throws UnsupportedEncodingException
     * @throws JSONException
     */
    private JSONObject sendRequest(String url) throws OXException, UnsupportedEncodingException, JSONException {
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();
        HttpGet get = new HttpGet(url);
        //get.addHeader("Content-Type", MIME_JSON);
        get.addHeader("accept", MIME_JSON);
        get.setHeader("User-Agent", Api.USER_AGENT);
        get.addHeader(getAuthenticationHeader());
        HttpResponse response = null;
        try {
            response = httpClient.execute(get);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8))) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpServletResponse.SC_OK) {
                    return new JSONObject(reader);
                } else {
                    LOG.error(response.toString());
                    throw OXApiExceptionCodes.WRONG_ERROR_CODE.create(statusCode);
                }
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            get.releaseConnection();
        }
    }

    /**
     * Creates the URL to send
     * createURL
     *
     * @param email Email address of the guest
     * @param cid ContextID of the guest
     * @return
     * @throws OXException
     */
    private String createURL(String email, int cid) throws OXException {
        return getUrl() + "/" + cid + "/" + email;
    }

    /**
     * Creates the full URL to call
     *
     * @return The URL to call
     * @throws OXException
     */
    private String getUrl() throws OXException {
        return getMainURI() + SESSION_REST_PATH;
    }

    public int getId(String email, int cid) {
        int id = 0;
        try {
            JSONObject json = sendRequest(createURL(email, cid));
            if (json.has(RESULT_ID)) {
                id = json.getInt(RESULT_ID);
            }
        } catch (UnsupportedEncodingException | OXException | JSONException e) {
            LOG.error("Error gettingg guest ID: ", e);
        }
        return id;
    }

}
