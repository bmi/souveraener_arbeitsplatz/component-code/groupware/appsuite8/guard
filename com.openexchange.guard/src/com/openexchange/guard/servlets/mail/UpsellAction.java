/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.mail;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.translation.GuardTranslationService;

/**
 * {@link UpsellAction} retrievals the upsell-template for a user
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class UpsellAction extends GuardServletAction {

    private static final String PARAMETER_LANGUAGE = "lang";
    private static final String UPSELL_TEMPLATE_NAME = "upsell.html";

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        int userId = userSession.getUserId();
        int cid = userSession.getContextId();
        String language = ServletUtils.getStringParameter(request, PARAMETER_LANGUAGE);
        int templateId = 0;

        GuardConfigurationService configurationService = Services.getService(GuardConfigurationService.class);
        templateId = configurationService.getIntProperty(GuardProperty.templateID, userId, cid);

        String template = "";
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        try {
            template = translationService.getTranslation(UPSELL_TEMPLATE_NAME, language, templateId);

            if (template != null) {
                template = template.replace("%id", Integer.toString(userId)).replace("%cid", Integer.toString(cid));
            } else {
                template = "";
            }
        } catch (OXException ex) {
            if (ex.getPrefix().equals("GRD-TRN") && ex.getCode() == 1) {  // OK if no upsell template found.
                return;
            }
            throw ex;
        }

        ServletUtils.sendHtmlOK(response, template);
    }
}
