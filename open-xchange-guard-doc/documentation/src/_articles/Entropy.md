---
title: Entropy
description: Entropy handling

---
# Entropy

In order to create encryption keys, a linux system requires entropy, and unpredictable input of randomness. 
In general, Linux systems use keyboard and mouse input randomness to generate entropy. 
On standalone servers, this input is not available. Other activity, such as disk access, is used, but the amount of entropy available can become an issue.

Guard uses the entropy to generate its encryption keys. 

```
cat /proc/sys/kernel/random/entropy_avail
```

Ideally in a guard system, the number should be high enough.

If entropy becomes a problem, we would recommend installing the package *“haveged”*. 
This uses the hardware volatile entropy gathering and expansion of the rhythm. 
This will repopulate the entropy available in a Linux system by monitoring other hardware actions.

## Debian

To install, on a Debian system, type the following:


```
apt-get install haveged
```

the configuration file is located in */etc/default/haveged* you may want to check that the following arguments are present in the config file:

```
DAEMON_ARGS="-w 1024"
```

likewise, to make sure that *haveged* starts upon restart, type the following

```
update-rc.d haveged defaults
```

## REHL

On a REHL system, to install type the following

```
Yum install haveged
```

and to make sure it starts when restarted

```
chkconfig haveged on
```

Once it’s installed, you can again check the amount of entropy available. The number should be significantly higher. 
