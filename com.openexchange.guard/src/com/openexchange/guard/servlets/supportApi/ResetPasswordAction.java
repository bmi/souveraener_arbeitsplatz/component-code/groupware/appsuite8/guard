/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.supportApi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.support.PasswordReseter.EmailTarget;
import com.openexchange.guard.support.SupportService;
import com.openexchange.guard.support.SupportServiceImpl;

/**
 * Resets the password for a user's current key
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class ResetPasswordAction extends GuardServletAction {

    private final SupportService supportService;

    /**
     * Initializes a new {@link ResetPasswordAction}.
     */
    public ResetPasswordAction() {
        supportService = new SupportServiceImpl();
    }

    @Override
    public void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        //Parsing parameters
        final String email = new EmailValidator().assertInput(ServletUtils.getStringParameter(request, "email", true /* mandatory */), "email");
        String language = ServletUtils.getStringParameter(request, "language", false /* optional */);
        if (language == null || language.isEmpty()) {
            GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
            language = configService.getProperty(GuardProperty.defaultLanguage, userSession.getUserId(), userSession.getContextId());
        }

        //Resetting the user's password
        EmailTarget target = supportService.resetPassword(email, language);
        ServletUtils.sendHtmlOK(response, target.name());
    }
}
