/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import com.openexchange.guard.translation.GuardTranslator;
import com.openexchange.i18n.Translator;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;

/**
 * {@link PGPSignatureVerificationResultCollectionResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPSignatureVerificationResultCollectionResponse {

    private final Collection<PGPSignatureVerificationResult> signatureVerificationResults;
    private final GuardTranslator translator;

    /**
     * Initializes a new {@link PGPSignatureVerificationResultCollectionResponse}.
     *
     * @param signatureVerificationResults The verification results
     * @param translator The {@link Translator} used for translating textual values
     */
    public PGPSignatureVerificationResultCollectionResponse(Collection<PGPSignatureVerificationResult> signatureVerificationResults,GuardTranslator translator) {
        this.signatureVerificationResults = Objects.requireNonNull(signatureVerificationResults,"signatureVerificationResults must not be null");
        this.translator = Objects.requireNonNull(translator, "translator must not be null");
    }

    public PGPSignatureVerificationResultResponse[] getSignatures() {
        ArrayList<PGPSignatureVerificationResultResponse> ret = new ArrayList<>(signatureVerificationResults.size());
        signatureVerificationResults.forEach( r -> ret.add(new PGPSignatureVerificationResultResponse(r, translator)));
        return ret.toArray(new PGPSignatureVerificationResultResponse[ret.size()]);
    }
}
