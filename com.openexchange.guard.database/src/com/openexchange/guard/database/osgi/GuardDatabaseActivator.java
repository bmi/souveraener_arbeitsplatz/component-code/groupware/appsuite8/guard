/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.osgi;

import com.openexchange.database.AssignmentFactory;
import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.JdbcProperties;
import com.openexchange.database.migration.DBMigrationExecutorService;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.init.GuardCreateTableService;
import com.openexchange.guard.database.internal.GuardDatabaseServiceImpl;
import com.openexchange.guard.database.internal.GuardShardingIDManagement;
import com.openexchange.guard.database.internal.Services;
import com.openexchange.guard.database.update.GuardCreateTableTask;
import com.openexchange.guard.database.update.shardUpdate.ShardUpdaterService;
import com.openexchange.guard.database.update.shardUpdate.ShardUpdaterServiceImpl;
import com.openexchange.guard.database.update.shardUpdate.updates.AutoIncrement;
import com.openexchange.guard.updater.AddMKeyIndexUpdateTask;
import com.openexchange.guard.updater.ChangeOGKeyTableSizes;
import com.openexchange.guard.updater.ChangePGPPublicKeySize;
import com.openexchange.guard.updater.ChangePasswordTimeStampUpdateTask;
import com.openexchange.guard.updater.CreateMailvelopeTableTask;
import com.openexchange.guard.updater.SetDBEngineUpdateTask;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardDatabaseActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class GuardDatabaseActivator extends HousekeepingActivator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { DatabaseService.class, JdbcProperties.class, AssignmentFactory.class, GuardConfigurationService.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(GuardDatabaseActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        DatabaseService databaseService = getService(DatabaseService.class);

        // Register create table services for user schema
        registerService(CreateTableService.class, new GuardCreateTableService());

        // Register update tasks for user schema
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new GuardCreateTableTask(databaseService)));
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new ChangePGPPublicKeySize(),
            new CreateMailvelopeTableTask(databaseService),
            new ChangeOGKeyTableSizes(),
            new SetDBEngineUpdateTask(),
            new ChangePasswordTimeStampUpdateTask(),
            new AddMKeyIndexUpdateTask()));

        GuardConfigurationService guardConfigService = getService(GuardConfigurationService.class);
        GuardDatabaseService guardDatabaseService = new GuardDatabaseServiceImpl(databaseService, guardConfigService, getService(AssignmentFactory.class));

        // Shard updating
        ShardUpdaterService shardUpdates = new ShardUpdaterServiceImpl(this, guardDatabaseService,
            new AutoIncrement(new GuardShardingIDManagement(guardDatabaseService)));

        track(DBMigrationExecutorService.class,
            new DBMigrationServiceTracker(context,
                guardDatabaseService,
                guardConfigService,
                shardUpdates,
                getService(AssignmentFactory.class)));
        trackService(GuardDatabaseService.class);
        trackService(GuardShardingService.class);
        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(GuardDatabaseActivator.class).info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());

        Services.setServiceLookup(null);

        super.stopBundle();
    }
}
