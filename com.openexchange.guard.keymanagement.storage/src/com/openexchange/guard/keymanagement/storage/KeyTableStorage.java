/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage;

import java.sql.Connection;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * Provides access to stored PGP Keys
 * {@link KeyTableStorage}
 */
public interface KeyTableStorage {

    /**
     * Gets a list of keys for a given user
     *
     * @param userId the user's id
     * @param contextId the context id
     * @return a list of OX Guard keys for the given user
     * @throws OXException due an error
     */
    public List<GuardKeys> getKeysForUser(int userId, int contextId) throws OXException;

    /**
     * Gets a list of keys for a given user
     *
     * @param userId the user's id
     * @param contextId the context id
     * @param shard the shard to use
     * @return a list of OX Guard keys for the given user
     * @throws OXException due an error
     */
    public List<GuardKeys> getKeysForUser(int userId, int contextId, int shard) throws OXException;

    /**
     * Gets a specific user key
     *
     * @param userId the user id
     * @param contextId the context id
     * @param keyId the id of the key to get
     * @return the key with the given key id, or null if no such key was found
     * @throws OXException due an error
     */
    public GuardKeys getKeyForUserById(int userId, int contextId, String keyid) throws OXException;

    /**
     * Gets a specific user key from a shard
     *
     * @param userId the user id
     * @param contextId the context id
     * @param keyId the id of the key to get
     * @param shardId the sharding id
     * @return the key with the given key id, or null if no such key was found
     * @throws OXException due an error
     */
    public GuardKeys getKeyForUserById(int userId, int contextId, int shardId, String keyId) throws OXException;

    /**
     * Gets the key with the highest version for a user.
     *
     * @param userId
     * @param contextId
     * @return The user's keys with the highest version.
     * @throws OXException due an error
     */
    public GuardKeys getHighestVersionKeyForUser(int userId, int contextId) throws OXException;

    /**
     * Gets the user's key which is marked as "current" for a specific email
     *
     * @param userId the user id
     * @param contextId the context id
     * @param email the email to get the current key for
     * @return the key for the user which is marked as current, or null if no such key was found
     * @throws OXException due an error
     */
    public GuardKeys getCurrentKeyForUser(int userId, int contextId, String email) throws OXException;

    /**
     * Gets the user's key which is marked as current
     *
     * @param userId the user id
     * @param contextId the context id
     * @return the key for the user
     * @throws OXException due an error
     */
    public GuardKeys getCurrentKeyForUser(int userId, int contextId) throws OXException;

    /**
     * Gets the user's key which is marked as "current" from a specific shard
     *
     * @param userId the user id
     * @param contextId the context id
     * @param shardId the sharding id
     * @return the key for the user which is marked as current, or null if no such key was found
     * @throws OXException due an error
     */
    public GuardKeys getCurrentKeyForUser(int userId, int contextId, int shardId) throws OXException;

    /**
     * Gets a list of keys for a given context
     *
     * @param contextId the context id
     * @return a list of OX Guard keys for the given context
     * @throws OXException due an error
     */
    public List<GuardKeys> getKeysForContext(int contextId) throws OXException;

    /**
     * Gets the guard keys for a given email in a context
     *
     * @param email the email
     * @param contextid the context
     * @return the GuardKey for the given email and context
     * @throws OXException due an error
     */
    public GuardKeys getKeyForEmailAndContext(String email, int contextid) throws OXException;

    /**
     * Gets the guard keys for a given email in a context
     *
     * @param email the email
     * @param the shard where the key is located
     * @return the GuardKey for the given email and context
     * @throws OXException due an error
     */
    public GuardKeys getKeyForGuest(String email, int shard) throws OXException;

    /**
     * Gets the guard keys for a given email in a context
     *
     * @param email the email
     * @param userId the id of the user to get the keys fors
     * @param contextId the context
     * @param the shard where the key is located
     * @return the GuardKey for the given email and context
     * @throws OXException due an error
     */
    public GuardKeys getKeyForEmailAndContext(String email, int userId, int contextId, int shard) throws OXException;

    /**
     * Gets the guard keys for a given email in a context
     *
     * @param email the email
     * @param contextid the context
     * @param the shard where the key is located
     * @return the GuardKey for the given email and context
     * @throws OXException due an error
     */
    public GuardKeys getKeyForEmailAndContext(String email, int contextid, int shard) throws OXException;

    /**
     * Inserts a new GuardKey
     *
     * @param key the key to insert into
     * @param setLastMod true to set lastMod to NOW(), false to set it to NULL
     * @throws OXException due an exception
     */
    public void insert(GuardKeys key, boolean setLastMod) throws OXException;

    /**
     * Inserts a new GuardKey using the given connection
     *
     * @param connection the connection to the db
     * @param key the key to insert into
     * @param setLastMod true to set lastMod to NOW(), false to set it to NULL
     * @throws OXException due an exception
     */
    public void insert(Connection connection, GuardKeys key, boolean setLastMod) throws OXException;

    /**
     * Inserts a new GuardKey
     *
     * @param key the key to insert into
     * @param setLastMod true to set lastMod to NOW(), false to set it to NULL
     * @param shard if guest user.
     * @throws OXException due an exception
     */
    public void insert(GuardKeys key, boolean setLastMod, int shard) throws OXException;

    /**
     * Updates the public part of a Guard key
     *
     * @param key the guard key to update the public part for
     * @throws OXException due an error
     */
    public void updatePublicKey(GuardKeys key) throws OXException;

    /**
     * Updates the public and private part of a Guard key
     *
     * @param keys the guard key to update the public and private part for
     * @throws OXException due an error
     */
    public void updatePublicAndPrivateKey(GuardKeys keys) throws OXException;

    /**
     * Updates the email address for a user
     * @param key
     * @param newEmail
     * @throws OXException
     */
    public void updateEmailAddress(GuardKeys key, String newEmail) throws OXException;

    /**
     * Updates the PGPSecret, RSAPrivate and the Recovery
     *
     * @param key the key containing the new password values
     * @param the recovery to set
     * @param setLastMod true to set lastMod to NOW(), false to set it to NULL
     * @throws OXException due an error
     */
    public void updatePassword(GuardKeys key, String recovery, boolean setLastMod) throws OXException;

    /**
     * Updates the Key's pin which can be used as additional security token when sending emails to guest users
     *
     * @param userId the ID of the user to update the pin for
     * @param contextId the contextId
     * @param newPin the new pin to set, or NULL to delete the pin
     */
    public void updatePin(int userId, int contextId, String newPin) throws OXException;

    /**
     * Updates an existing Guard Key (Updates PGPSecret,PGPPublic,Recovery and Salt)
     *
     * @param key the new key data (PGPSecret, PGPPublic..)
     * @throws OXException due an error
     */
    public void updateDuplicate(GuardKeys key) throws OXException;

    /**
     * Updates the question and answer for all key's of a given user
     *
     * @param userId the user's id
     * @param contextId the context id
     * @param question the new question
     * @param answer the new answer
     * @param keyId the id of the key
     * @throws OXException
     */
    public void updateQuestionForKey(int userId, int contextId, String question, String answer, long keyId) throws OXException;

    /**
     * Updates the version of a key
     *
     * @param key the key to update
     * @param newVersion the new version to set
     * @throws OXException due an error
     */
    public void updateKeyVersion(GuardKeys key, int newVersion) throws OXException;

    /**
     * Updates the settings for each key of a user
     *
     * @param userId the user id
     * @param contextId the context id
     * @param settings the settings to set
     * @throws OXException due an exception
     */
    public void updateSettingsForUser(int userId, int contextId, String settings) throws OXException;

    /**
     * Marks the key as "current"
     *
     * @param key the key to mark as current
     */
    public void setCurrentKey(GuardKeys key) throws OXException;

    /**
     * Marks all keys of the user as _not_ current
     *
     * @param userId the user's id
     * @param contextId the context id
     * @throws OXException due an exception
     */
    public void unsetCurrentFlag(int userId, int contextId) throws OXException;

    /**
     * Deletes a specific key
     *
     * @param key the key to delete
     */
    public void delete(GuardKeys key) throws OXException;

    /**
     * Deletes all keys related to a given user
     *
     * @param userId the user id
     * @param contextId the context id
     * @throws OXException due an error
     */
    public void deleteAllForUser(int userId, int contextId) throws OXException;

    /**
     * Deletes all keys related to a given user and connection
     *
     * @param connection the connection to the DB
     * @param userId the user id
     * @param contextId the context id
     * @throws OXException due an error
     */
    public void deleteAllForUser(Connection connection, int userId, int contextId) throws OXException;

    /**
     * Deletes all keys related for a given context
     *
     * @param contextId the context id
     * @throws OXException due an error
     */
    public void deleteAllForContext(int contextId) throws OXException;

    /**
     * Removes the recovery
     *
     * @param key the key to remove the recovery from
     * @throws OXException due an error
     */
    public void deleteRecovery(GuardKeys key) throws OXException;

    /**
     * Sets the auto increment within a guest shard
     *
     * @param shardId identifier of the shard database
     * @param value the new auto increment value
     * @throws OXException due an error
     */
    public void updateAutoIncrementInShard(int shardId, int value) throws OXException;

    /**
     * Retrieve keys from Mater KeyTable for the specified user
     * (Still needed?)
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The guard key
     * @throws OXException
     */
    GuardKeys getOldGuardKeys(int userId, int contextId) throws OXException;

    /**
     * Whether the underlying storage exists or not
     *
     * @param userId The userid used for checking the underlaying storage
     * @param contextId The context-id used for checking the underlaying storage
     * @param shardId the ShardID used for checking the underlaying storage
     * @return True, if the underlying storage exists, false otherwise
     * @throws OXException
     */
    public boolean exists(int userId, int contextId, int shardId) throws OXException;

    /**
     * Gets the lowest existing key number for guard.og_KeyTable
     * @return
     * @throws OXException
     */
    int getMinFromSystemTable() throws OXException;

    /**
     * Updates the misc data for a key
     * @param key
     * @param misc
     * @throws OXException
     */
    void updateMisc(GuardKeys key, String misc) throws OXException;

    /**
     * Updates the locale for a key
     * @param key
     * @throws OXException
     */
    void updateLocale(GuardKeys key) throws OXException;
}
