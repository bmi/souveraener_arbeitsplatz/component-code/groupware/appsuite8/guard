/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.idn.IDNA;

/**
 * {@link EmailValidator} provides simple length validation of email addresses.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 * @see https://www.owasp.org/index.php/Input_Validation_Cheat_Sheet#Email_Address_Validation
 */
public class EmailValidator extends AbstractInputValidator<String> {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.inputvalidation.InputValidator#isValid(java.lang.Object)
     */
    private static final int MAX_LOCAL_LENGTH = 64;
    private static final int MAX_DOMAIN_LENGTH = 255;

    @Override
    public boolean isValid(String input) {
        //Email validation with regex is quite prone to false positives.
        //According to OWASP Input Validation Sheet we do just a simple check
        //https://www.owasp.org/index.php/Input_Validation_Cheat_Sheet#Email_Address_Validation
        if (input != null && input.contains("@")) {
            String[] parts = input.split("@");
            if (parts.length == 2) {
                String localPart = parts[0];
                String domainPart = parts[1];
                //Simple length check
                if ((localPart.length() > 0 && localPart.length() <= MAX_LOCAL_LENGTH) &&
                    (domainPart.length() > 0 && domainPart.length() <= MAX_DOMAIN_LENGTH)) {
                    try {
                        //additionally using InternetAddress to perform some basic checks
                        String aceConformMailAddress = IDNA.toACE(new String(input));
                        InternetAddress address = new InternetAddress(aceConformMailAddress);
                        address.validate();
                        return true;
                    } catch (AddressException e1) {
                        return false;
                    }

                }
            }
        }
        return false;
    }
}
