/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.utils;

import java.sql.Connection;

/**
 * {@link GuardConnectionWrapper}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public interface GuardConnectionWrapper {

    /**
     * Returns the shard identifier the {@link Connection} available via GuardConnectionWrapper.getConnection() is associated to
     * 
     * @return int with the id of the shard
     */
    int getShardId();

    /**
     * Returns the user identifier the {@link Connection} available via GuardConnectionWrapper.getConnection() is associated to
     * 
     * @return int with the id of the user
     */
    int getUserId();

    /**
     * Returns the context identifier the {@link Connection} available via GuardConnectionWrapper.getConnection() is associated to
     * 
     * @return int with the id of the context
     */
    int getContextId();

    /**
     * Returns the {@link Connection} that is associated to the given parameters
     * 
     * @return {@link Connection} to the database based on the given shard, user and context ids
     */
    Connection getConnection();
}
