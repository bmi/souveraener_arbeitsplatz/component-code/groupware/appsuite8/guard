/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import com.openexchange.exception.OXException;

/**
 * {@link InputValidator} validates a given input
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public interface InputValidator<T> {

    /**
     * Validates that the given input is valid
     *
     * @param input The input to validate
     * @return True, if the given input is valid, False otherwise
     */
    boolean isValid(T input);

    /**
     * Returns the input value, if it is valid. Allows a more fluent like validation.
     *
     * @param input The input value.
     * @return input if it is valid, null if it is not valid
     */
    T getInput(T input);

    /**
     * Validates that the given input is valid and throws an Exception if not.
     *
     * @param input The input to validate
     * @throws OXException If the given input is not valid
     */
    void assertIsValid(T input) throws OXException;

    /**
     * Validates that the given input is valid and throws an Exception if not.
     *
     * @param input The input to validate
     * @param inputFieldName The name of the input field which will be included in the Exception
     * @throws OXException If the given input is not valid
     */
    void assertIsValid(T input, String inputFieldName) throws OXException;

    /**
     * Returns the value, if it is valid. Allows a more fluent like validation.
     *
     * @param input The input value
     * @return input if it is valid
     * @throws OXException If the given input is not valid
     */
    T assertInput(T input) throws OXException;

    /**
     * Returns the value, if it is valid. Allows a more fluent like validation.
     *
     * @param input The input value
     * @return input if it is valid
     * @param inputFieldName The name of the input field which will be included in the Exception
     * @throws OXException If the given input is not valid
     */
    T assertInput(T input, String inputFieldName) throws OXException;
}
