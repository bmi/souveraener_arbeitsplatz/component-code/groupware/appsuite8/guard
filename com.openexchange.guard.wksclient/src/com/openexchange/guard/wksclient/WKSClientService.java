/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient;

import java.util.Collection;
import java.util.Optional;
import com.openexchange.exception.OXException;

/**
 * {@link WKSClientService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public interface WKSClientService {

    /**
     * Tries to find all (including expired and revoked) public-key rings for the given email address by performing a WKS lookup.
     * <br>
     * <br>
     * The query is performed on the host obtained from the domain part of the given email address.
     * <br>
     * <br>
     * Note that the keys may be revoked or expired - it is up to the client to handle such conditions.
     *
     * @param email The email
     * @param userId the Sender userId
     * @param cid the Sender context Id
     * @param timeout Procedure should timeout after specified milliseconds
     * @return A list of public keys, or an empty list if no keys were found for the given email
     * @throws OXException
     */
    Collection<WKSResult> findAll(String email, int userId, int cid, int timeout) throws OXException;

    /**
     * Tries to find all (including expired and revoked) public-key rings for the given email address by performing a WKS lookup on a specified, alternative, host/port.
     * <br>
     * <br>
     * The query is performed on the host obtained from the domain part of the given email address.
     * <br>
     * <br>
     * Note that the keys may be revoked or expired - it is up to the client to handle such conditions.
     *
     * @param email The email
     * @param userId the Sender userId
     * @param cid the Sender context Id
     * @param timeout Procedure should timeout after specified milliseconds
     * @param host The alternative host to perform the key lookup on
     * @param port The alternative port to use
     * @return A list of public keys, or an empty list if no keys were found for the given email
     * @throws OXException
     */
    Collection<WKSResult> findAll(String email, int userId, int cid, int timeout, String host, int port) throws OXException;

    /**
     * Tries to find the most usable (non revoked, non expired) public-key ring for the given email address by performing a WKS lookup.
     *
     * @param email The email
     * @param userId the Sender userId
     * @param cid the Sender context Id
     * @param timeout Procedure should timeout after specified milliseconds
     * @return The first suitable key found, or an empty Optional if no suitable key was found
     * @throws OXException
     */
    Optional<WKSResult> find(String email, int userId, int cid, int timeout) throws OXException;

    /**
     * Tries to find the most usable (non revoked, non expired) public-key ring for the given email address by performing a WKS lookup on the specified host/port.
     *
     * @param email The email
     * @param timeout Procedure should timeout after specified milliseconds
     * @param userId the Sender userId
     * @param cid the Sender context Id
     * @param host The alternative host to perform the key lookup on
     * @param port The alternative port to use
     * @return The first suitable key found, or an empty Optional if no suitable key was found
     * @throws OXException
     */
    Optional<WKSResult> find(String email, int userId, int cid, int timeout, String host, int port) throws OXException;
}
