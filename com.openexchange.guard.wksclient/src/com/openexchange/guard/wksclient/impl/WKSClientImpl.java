/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient.impl;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.wksclient.WKSClientService;
import com.openexchange.guard.wksclient.WKSResult;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.server.ServiceLookup;
import static com.openexchange.java.Autoboxing.I;

/**
 * {@link WKSClientImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class WKSClientImpl implements WKSClientService {

    private static Logger LOG = LoggerFactory.getLogger(WKSClientImpl.class);
    private static int TLS_PORT = 443;
    ServiceLookup services;


    public WKSClientImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to extract the host name form the given email address
     *
     * @param email The email
     * @return The host part of the email
     */
    private String getHostFromEmail(String email) {
        return email.substring(email.indexOf("@") + 1);
    }

    /**
     * Filter only those results which contains the given email (See WKS-RFC 3.1)
     *
     * @param email The email
     * @param results The results
     * @return Only those results which contain a uid package with the given email
     */
    private Collection<WKSResult> filterForEmail(final String email, Collection<WKSResult> results) {
        return results.stream().filter(r -> PGPKeysUtil.containsUID(r.getPublicKeyRing(), email)).collect(Collectors.toList());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#findAll(java.lang.String, int)
     */
    @Override
    public Collection<WKSResult> findAll(String email, int userId, int cid, int timeout) throws OXException {
        return findAll(email, userId, cid, timeout, getHostFromEmail(email), TLS_PORT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#findAll(java.lang.String, int, java.lang.String, int)
     */
    @Override
    public Collection<WKSResult> findAll(String email, int userId, int cid, int timeout, String host, int port) throws OXException {
        final String verifiedEmail = new EmailValidator().assertInput(email);
        WKSClient wksClient = new WKSClient(services);

        Collection<WKSResult> results;
        try {
            //Performing an "advanced" search
            results = wksClient.findAdvanced(host, verifiedEmail, port, timeout);
            LOG.debug("An advanced web key search for \"{}\" yielded {} result(s).", email, I(results.size()));
            results = filterForEmail(verifiedEmail, results);
        } catch (@SuppressWarnings("unused") UnknownHostException e) {
            //Advanced WKS Subdomain might not exist; trying direct mode
            results = wksClient.findDirect(host, verifiedEmail, port, timeout);
            LOG.debug("A direct web key search for \"{}\" yielded {} result(s).", email, I(results.size()));
        }
        return results;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#find(java.lang.String, int)
     */
    @Override
    public Optional<WKSResult> find(String email, int userId, int cid, int timeout) throws OXException {
        return find(email, userId, cid, timeout, getHostFromEmail(email), TLS_PORT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#find(java.lang.String, int, java.lang.String, int)
     */
    @Override
    public Optional<WKSResult> find(String email, int userId, int cid, int timeout, String host, int port) throws OXException {
        Collection<WKSResult> results = findAll(email, userId, cid, timeout, host, port);
        //Do not return required or revoked keys and filter keys which do not contain the email (See WKS-RFC 3.1)

        Optional<WKSResult> key = results.stream().filter(r ->
            r.hasValidKey()).findFirst();
        return key;
    }
}
