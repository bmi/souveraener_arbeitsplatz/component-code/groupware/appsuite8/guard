/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.cipher.exceptions.GuardCipherExceptionCodes;

/**
 * {@link GuardCipherFactoryServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardCipherFactoryServiceImpl implements GuardCipherFactoryService {

    private final Map<GuardCipherAlgorithm, GuardCipherService> cipherServices;

    /**
     * Initialises a new {@link GuardCipherFactoryServiceImpl}.
     * 
     * @throws OXException If one of the cipher algorithms does not exist, or if the key length of one of the specified algorithms exceeds the maximum defined
     */
    public GuardCipherFactoryServiceImpl() throws OXException {
        super();

        // Register available cipher algorithms
        Map<GuardCipherAlgorithm, GuardCipherService> c = new HashMap<GuardCipherAlgorithm, GuardCipherService>(3);
        c.put(GuardCipherAlgorithm.AES_CBC, new GuardAESCipherServiceImpl(GuardCipherAlgorithm.AES_CBC));
        c.put(GuardCipherAlgorithm.AES_GCM, new GuardAESCipherServiceImpl(GuardCipherAlgorithm.AES_GCM));
        c.put(GuardCipherAlgorithm.RSA, new GuardRSACipherServiceImpl(GuardCipherAlgorithm.RSA));
        cipherServices = Collections.unmodifiableMap(c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.cipher.GuardCipherFactory#getCipherService(com.openexchange.guard.cipher.GuardCipherAlgorithm)
     */
    @Override
    public GuardCipherService getCipherService(GuardCipherAlgorithm cipher) throws OXException {
        GuardCipherService cipherService = cipherServices.get(cipher);
        if (cipherService == null) {
            throw GuardCipherExceptionCodes.UNKNOWN_CIPHER_SERVICE.create(cipher.getTransformation());
        }
        return cipherService;
    }

}
