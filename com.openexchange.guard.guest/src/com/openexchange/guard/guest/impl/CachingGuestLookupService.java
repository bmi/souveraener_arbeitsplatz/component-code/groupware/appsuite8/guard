/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.io.Serializable;
import java.util.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.email.storage.ogEmail.EmailStorageListener;
import com.openexchange.guard.guest.GuestLookupService;

/**
 * {@link CachingGuestLookupService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class CachingGuestLookupService implements GuestLookupService, EmailStorageListener {

    private final GenericCache<Email> cache;
    private final GenericCache<Integer> idCache;
    private final GuestLookupService delegate;

    public CachingGuestLookupService(GenericCacheFactory cacheFactory,
                                     GuestLookupService delegate,
                                     EmailStorage emailStorage) {
        this.cache = Objects.requireNonNull(cacheFactory, "cacheFactory must not be null").createCache("GuestLookup");
        this.idCache = cacheFactory.createCache("Guest2OXLookup");
        this.delegate = Objects.requireNonNull(delegate, "delegate must not be null");
        Objects.requireNonNull(emailStorage, "emailStorage must not be null").addListener(this);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuestLookupService#lookupGuardGuest(int, int)
     */
    @Override
    public Email lookupGuardGuest(int userId, int contextId) throws OXException {
        final Serializable cacheKey = cache.createKey(contextId, userId);
        Email email = cache.get(cacheKey);
        if (email == null) {
            email = delegate.lookupGuardGuest(userId, contextId);
            if (email != null) {
                cache.put(cacheKey, email);
            }
        }
        return email;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorageListener#deleted(int, int)
     */
    @Override
    public void deleted(int userId, int contextId) throws OXException  {
        //clearing the whole cache; no mapping from provided guard userId/cid to Appsuite userId/cid
        cache.clear();
        idCache.clear();
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.email.storage.ogEmail.EmailStorageListener#deleted(int)
     */
    @Override
    public void deleted(int contextId) throws OXException {
        cache.clear();
        idCache.clear();
  }

    @Override
    public int lookupOxUserId(String email, int contextId) throws OXException {
        final Serializable cacheKey = idCache.createKey(email, contextId);
        Integer id = idCache.get(cacheKey);
        if (id == null) {
            id = delegate.lookupOxUserId(email, contextId);
            if (id != 0) {
                idCache.put(cacheKey, id);
            }
        }
        return id;
    }

}
