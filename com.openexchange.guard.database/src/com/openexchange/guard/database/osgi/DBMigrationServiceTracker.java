/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.osgi;

import java.sql.Connection;
import java.sql.SQLException;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import com.openexchange.database.AssignmentFactory;
import com.openexchange.database.migration.DBMigration;
import com.openexchange.database.migration.DBMigrationConnectionProvider;
import com.openexchange.database.migration.DBMigrationExecutorService;
import com.openexchange.database.migration.resource.accessor.BundleResourceAccessor;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.DatabaseMaintenanceService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.dao.SchemaCandidate;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.internal.DatabaseMaintenanceServiceImpl;
import com.openexchange.guard.database.internal.GuardDatabaseServiceImpl;
import com.openexchange.guard.database.internal.GuardShardingServiceImpl;
import com.openexchange.guard.database.update.shardUpdate.ShardUpdaterService;
import com.openexchange.guard.database.utils.DBUtils;

/**
 *
 * {@link DBMigrationServiceTracker}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class DBMigrationServiceTracker implements ServiceTrackerCustomizer<DBMigrationExecutorService, DBMigrationExecutorService> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DBMigrationServiceTracker.class);

    public static final String GUARDDB_CHANGE_LOG = "/liquibase/guarddbChangeLog.xml";
    public static final String SHARDDB_CHANGE_LOG = "/liquibase/sharddbChangeLog.xml";

    private final BundleContext context;

    private final GuardDatabaseService guardDatabaseService;
    private final GuardConfigurationService guardConfigurationService;
    private DatabaseMaintenanceService databaseMaintenanceService;
    private GuardShardingService guardShardingService;
    private final AssignmentFactory assignmentFactory;
    private final ShardUpdaterService shardUpdates;

    public DBMigrationServiceTracker(BundleContext context, GuardDatabaseService guardDatabaseService, GuardConfigurationService guardConfigurationService, ShardUpdaterService shardUpdates, AssignmentFactory assignmentFactory) {
        this.context = context;
        this.guardDatabaseService = guardDatabaseService;
        this.guardConfigurationService = guardConfigurationService;
        this.assignmentFactory = assignmentFactory;
        this.shardUpdates = shardUpdates;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DBMigrationExecutorService addingService(ServiceReference<DBMigrationExecutorService> serviceRef) {
        DBMigrationExecutorService dbMigrationExecutorService = context.getService(serviceRef);

        databaseMaintenanceService = new DatabaseMaintenanceServiceImpl(guardDatabaseService, guardConfigurationService, assignmentFactory);
        context.registerService(DatabaseMaintenanceService.class, databaseMaintenanceService, null);

        guardShardingService = new GuardShardingServiceImpl(guardDatabaseService, dbMigrationExecutorService, databaseMaintenanceService, shardUpdates);
        context.registerService(GuardShardingService.class, guardShardingService, null);

        try {
            String guardDbSchemaName = guardConfigurationService.getProperty(GuardProperty.dbSchemaBaseName);
            handleGuardSchema(dbMigrationExecutorService, guardDbSchemaName);
            handleInitialShardSchema(1);
            handleAllShardSchemas();
            shardUpdates.doUpdates();
            //Only if above is successful: register the GuardDatabaseService
            context.registerService(GuardDatabaseService.class, guardDatabaseService, null);
        } catch (OXException oxException) {
            LOG.error("Unable to init (create or update) guard schema", oxException);
        }
        return dbMigrationExecutorService;
    }

    private void handleAllShardSchemas() {
        try {
            if (guardShardingService != null) {
                guardShardingService.updateAllShardSchemas();
            } else {
                LOG.error("Unable to update all existing shard schemas as required GuardShardingService is null.");
            }
        } catch (OXException oxException) {
            LOG.error("Unable to init shard schema", oxException);
        }
    }

    private void handleInitialShardSchema(int shardId) {
        try {
            createShardSchema(shardId);
        } catch (OXException oxException) {
            LOG.error("Unable to init (create or update) shard schema", oxException);
        }
    }

    private void createShardSchema(int shardId) throws OXException {
        String shardDbSchema = guardDatabaseService.getShardName(shardId);

        Connection shardMasterInitConnection = guardDatabaseService.getWritableForShardInit();
        String shardSlaveHost = guardConfigurationService.getProperty(GuardProperty.oxGuardShardRead);

        SchemaCandidate shardSchemaCandidates = new SchemaCandidate(guardDatabaseService, shardMasterInitConnection, shardDbSchema, shardSlaveHost);
        try {
            shardSchemaCandidates.start();
            databaseMaintenanceService.createAndRegisterSchemaIfNotExistent(shardSchemaCandidates);
            shardSchemaCandidates.commit();
        } finally {
            shardSchemaCandidates.finish();
        }
    }

    private void handleGuardSchema(DBMigrationExecutorService service, String guardDbSchema) throws OXException {
        createGuardSchema(guardDbSchema);
        updateGuardSchema(service, guardDbSchema);
    }

    /**
     * @param guardDbSchema
     * @throws OXException
     * @throws SQLException
     */
    private void createGuardSchema(String guardDbSchema) throws OXException {
        Connection guardMasterInitConnection = guardDatabaseService.getWritableForGuardInit();
        String guardSlaveHost = guardConfigurationService.getProperty(GuardProperty.oxGuardDatabaseRead);

        SchemaCandidate masterSchemaCandidates = new SchemaCandidate(guardDatabaseService, guardMasterInitConnection, guardDbSchema, guardSlaveHost);
        try {
            masterSchemaCandidates.start();
            databaseMaintenanceService.createAndRegisterSchemaIfNotExistent(masterSchemaCandidates);
            masterSchemaCandidates.commit();
        } finally {
            masterSchemaCandidates.finish();
        }
    }

    /**
     * @param service
     * @throws OXException
     */
    private void updateGuardSchema(DBMigrationExecutorService service, String guardDbSchema) throws OXException {
        Bundle dbBundle = FrameworkUtil.getBundle(GuardDatabaseServiceImpl.class);
        if (dbBundle == null) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create("Missing database bundle");
        }

        //checking the connection, (server registered etc..) before scheduling the migration provider
        Connection test = null;
        try {
            test = guardDatabaseService.getWritableForGuardNoTimeout();
        }
        finally {
            if (test != null) {
                guardDatabaseService.backWritableAfterReadingForGuardNoTimeout(test);
            }
        }

        BundleResourceAccessor bundleResourceAccessor = new BundleResourceAccessor(dbBundle);
        DBMigration dbMigration = new DBMigration(new DBMigrationConnectionProvider() {

            @SuppressWarnings("synthetic-access")
            @Override
            public Connection get() throws OXException {
                return guardDatabaseService.getWritableForGuardNoTimeout();
            }

            @SuppressWarnings("synthetic-access")
            @Override
            public void back(Connection connection) {
                DBUtils.autocommit(connection);
                guardDatabaseService.backWritableForGuardNoTimeout(connection);
            }

            @SuppressWarnings("synthetic-access")
            @Override
            public void backAfterReading(Connection connection) {
                DBUtils.autocommit(connection);
                guardDatabaseService.backWritableAfterReadingForGuardNoTimeout(connection);
            }
        }, GUARDDB_CHANGE_LOG, bundleResourceAccessor, guardDbSchema);
        service.scheduleDBMigration(dbMigration);
    }

    @Override
    public void modifiedService(ServiceReference<DBMigrationExecutorService> serviceRef, DBMigrationExecutorService migrationService) {
        // nothing to do
    }

    @Override
    public void removedService(ServiceReference<DBMigrationExecutorService> serviceRef, DBMigrationExecutorService migrationService) {
        context.ungetService(serviceRef);
    }
}
