/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.osgi;

import com.openexchange.database.CreateTableService;
import com.openexchange.database.DatabaseService;
import com.openexchange.groupware.update.DefaultUpdateTaskProviderService;
import com.openexchange.groupware.update.UpdateTaskProviderService;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.autocrypt.database.CreateAutoCryptTable;
import com.openexchange.guard.autocrypt.database.CreateAutoCryptTableTask;
import com.openexchange.guard.autocrypt.database.impl.AutoCryptStorageServiceImpl;
import com.openexchange.guard.autocrypt.impl.AutoCryptServiceImpl;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.jslob.JSlobService;
import com.openexchange.osgi.HousekeepingActivator;

public class Activator extends HousekeepingActivator {


    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] { GuardConfigurationService.class, GuardKeyService.class,
            DatabaseService.class, GuardDatabaseService.class, GuardNotificationService.class,
            GuardTranslationService.class, MailCreatorService.class, GuardRatifierService.class };
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {

        final DatabaseService databaseService = getService(DatabaseService.class);

        final AutoCryptStorageService storageService = new AutoCryptStorageServiceImpl(this.getService(GuardDatabaseService.class));

        registerService(AutoCryptStorageService.class, storageService);

        registerService(AutoCryptService.class, new AutoCryptServiceImpl(this));

        // Register service to create autoCrypt table
        registerService(UpdateTaskProviderService.class, new DefaultUpdateTaskProviderService(new CreateAutoCryptTableTask(databaseService)));
        registerService(CreateTableService.class, new CreateAutoCryptTable());
        trackService(JSlobService.class);
        openTrackers();

    }

    @Override
    protected void stopBundle() throws Exception {
        unregisterService (AutoCryptService.class);
        super.stopBundle();
    }

}
