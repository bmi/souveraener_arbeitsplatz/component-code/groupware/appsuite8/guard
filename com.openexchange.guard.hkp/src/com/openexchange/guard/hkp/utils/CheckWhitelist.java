/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.utils;

import org.apache.commons.net.util.SubnetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.hkp.osgi.Services;

/**
 * {@link CheckWhitelist}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class CheckWhitelist {

    private static final Logger LOG = LoggerFactory.getLogger(CheckWhitelist.class);

    /**
     * Check if the ip address is within the range of whitelist configured
     *
     * @param ip
     * @return
     * @throws OXException
     */
    public static boolean isWhiteListed(String ip) throws OXException {
        String whitlist = Services.getService(GuardConfigurationService.class).getProperty(GuardProperty.publicKeyWhitelist);
        if(whitlist == null || whitlist.isEmpty() ) {
            return false;
        }
        String[] splitByComma = Strings.splitByComma(whitlist);
        try {
            for (int i = 0; i < splitByComma.length; i++) {
                String whiteListIp = splitByComma[i];
                if(!whiteListIp.contains("/")){
                    //Single IP
                    if(whiteListIp.trim().equals(ip)){
                        return true;
                    }
                }
                else if(whiteListIp.contains("/")){
                    //CDIR notation
                    //TODO: support ipv6
                    SubnetUtils util = new SubnetUtils(splitByComma[i]);
                    if (util.getInfo().isInRange(ip)) {
                        return true;
                    }
                    if (util.getInfo().getAddress().equals(ip)) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            LOG.error("Problem checking pgp whitelist", e);
            return false;
        }
    }
}
