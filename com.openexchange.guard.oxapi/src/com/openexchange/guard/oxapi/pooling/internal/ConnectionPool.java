/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.pooling.internal;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.ConnPoolControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.oxapi.osgi.Services;

/**
 * Provides a pool of HTTP connections
 */
class ConnectionPool {

    private final Logger logger = LoggerFactory.getLogger(ConnectionPool.class);
    private CloseableHttpClient httpClient;
    private PoolingHttpClientConnectionManager cm;
    private boolean resetting = false;
    private IdleConnectionMonitor monitor;
    private final CookieHandlingMode cookieHandlingMode;
    private final String name;

    /**
     * The cookie handling mode for a connection pool
     */
    public enum CookieHandlingMode {
        /**
         * Default cookie handling,
         */
        CookieHandlingEnabled,

        /**
         * Disable cookie handling
         */
        CookieHandligDissabled
    };

    /**
     * Constructor
     *
     * @param name the name of the connection pool
     * @param cookieHandlingMode the cookie handling mode to use for the pool
     * @throws OXException
     */
    ConnectionPool(String name, CookieHandlingMode cookieHandlingMode) throws OXException {
        this.name = name;
        this.cookieHandlingMode = cookieHandlingMode;
        initialize();
    }

    /**
     * Internal method to initialize the connection pool
     *
     * @throws OXException
     */
    private void initialize() throws OXException {
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);

        cm = new PoolingHttpClientConnectionManager(10, TimeUnit.MINUTES);
        cm.setMaxTotal(guardConfigService.getIntProperty(GuardProperty.maxTotalConnections));
        cm.setDefaultMaxPerRoute(guardConfigService.getIntProperty(GuardProperty.maxHttpConnections));

        HttpHost ox = new HttpHost(guardConfigService.getProperty(GuardProperty.restApiHostname), guardConfigService.getIntProperty(GuardProperty.oxBackendPort));
        cm.setMaxPerRoute(new HttpRoute(ox), guardConfigService.getIntProperty(GuardProperty.maxHttpConnections));
        httpClient = buildHttpClient(cookieHandlingMode);
        monitor = new IdleConnectionMonitor(cm, cm, name);
        monitor.start();
        try {
            monitor.join(1000);
        } catch (InterruptedException e) {
            logger.info("HTTP Monitor thread shutdown");
        }
    }

    /**
     * Internal method to build the HttpClient-based connection pool
     *
     * @param cookieHandlingMode the cookie handling mode
     * @return A HTTClient representing the connection pool
     * @throws OXException
     */
    private CloseableHttpClient buildHttpClient(CookieHandlingMode cookieHandlingMode) throws OXException {
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);

        int connectionTimeout = guardConfigService.getIntProperty(GuardProperty.connectionTimeout);
        RequestConfig requestConfig =
            RequestConfig.custom().setStaleConnectionCheckEnabled(true).setConnectTimeout(connectionTimeout).setSocketTimeout(connectionTimeout).build();

        return cookieHandlingMode == CookieHandlingMode.CookieHandligDissabled ?
            HttpClients.custom().setDefaultRequestConfig(requestConfig).disableCookieManagement().disableAutomaticRetries().setConnectionManager(cm).build() :
            HttpClients.custom().setDefaultRequestConfig(requestConfig).disableAutomaticRetries().setConnectionManager(cm).build();
    }

    /**
     * Closes the connection pool
     */
    public void close() {
        monitor.shutdown();
        cm.shutdown();
    }

    /**
     * Resets the connection pool
     */
    public void reset() {
        try {
            if (resetting) {
                return;
            }
            logger.debug("Resetting connection pool");
            resetting = true;
            close();
            initialize();
            Timer resetDelay = new Timer();
            resetDelay.schedule(new TimerTask() {

                @Override
                public void run() {
                    resetting = false;
                    logger.debug("end reset delay");
                }
            }, 30000);
        } catch (Exception ex) {
            logger.error("Error resetting connection pool ", ex);
        }
    }

    /**
     * Gets a connection from the pool
     *
     * @return
     */
    public HttpClient getHttpClient() {
        return this.httpClient;
    }

    /**
     * Gets the runtime control of the connection pool
     *
     * @return The runtime control of the connection pool
     */
    public ConnPoolControl<HttpRoute> getPoolControl() {
        ConnPoolControl<HttpRoute> x;
        return cm;
    }
}
