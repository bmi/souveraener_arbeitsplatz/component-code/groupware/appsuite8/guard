/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.util.List;
import javax.mail.internet.MimeMessage;
import com.openexchange.pgp.core.MDCVerificationResult;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link PGPMimeDecryptionResult} represents the result of decrypting a PGP/MIME message.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPMimeDecryptionResult {

    private final MimeMessage                          mimeMessage;
    private final List<SignatureVerificationResult> signatureVerificationResults;
    private final MDCVerificationResult mdcVerificationResult;

    /**
     * Initializes a new {@link PGPMimeDecryptionResult}.
     *
     * @param mimeMessage The decrypted mimeMessage.
     * @param signatureVerificationResults The results of the signature verificaiton.
     */
    public PGPMimeDecryptionResult(MimeMessage mimeMessage, List<SignatureVerificationResult> signatureVerificationResults, MDCVerificationResult mdcVerificationResult) {
        this.mimeMessage = mimeMessage;
        this.signatureVerificationResults = signatureVerificationResults;
        this.mdcVerificationResult = mdcVerificationResult;
    }

    /**
     * Gets the decrypted mimeMessage
     *
     * @return The decrypted mimeMessage
     */
    public MimeMessage getMimeMessage() {
        return mimeMessage;
    }

    /**
     * Gets the signature verification results.
     *
     * @return The results
     */
    public List<SignatureVerificationResult> getSignatureVerificationResults() {
        return signatureVerificationResults;
    }

    public MDCVerificationResult getMDCVerifiacionResult() {
        return this.mdcVerificationResult;
    }
}
