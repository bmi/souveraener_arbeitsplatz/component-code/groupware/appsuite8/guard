/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.internal;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;

/**
 * {@link GuardRSACipherServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardRSACipherServiceImpl extends AbstractGuardCipherService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardAESCipherServiceImpl.class);

    /**
     * Initializes a new {@link GuardRSACipherServiceImpl}.
     *
     * @param cipherAlgorithm The cipher algorithm
     * @throws OXException If the specified cipher algorithm does not exist, or if the key length of the specified algorithm exceeds the maximum defined
     */
    public GuardRSACipherServiceImpl(GuardCipherAlgorithm cipherAlgorithm) throws OXException {
        super(cipherAlgorithm);
    }

    @Override
    public String encrypt(String data, Key secretKey) {
        byte[] cipherText = null;
        try {
            // Get an RSA cipher object
            final Cipher cipher = getCipher();

            // Encrypt the plain text using the public key
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            cipherText = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            LOG.error("Error RSA encrypt: " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return (Base64.encodeBase64String(cipherText));
    }

    @Override
    public String encrypt(String data, String base64Key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String encrypt(String data, String password, String salt) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String decrypt(String data, String password, String salt, int keyLength) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String decrypt(String data, String password, String salt) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String decrypt(String data, String base64Key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String decrypt(String data, Key secretKey) {
        // Passed bad private key
        if (secretKey == null) {
            //TODO: throw illegal argument exception?
            return null;
        }
        byte[] text = Base64.decodeBase64(data);
        byte[] dectyptedText = null;
        try {
            // Get an RSA cipher object and print the provider
            final Cipher cipher = getCipher();

            // Decrypt the text using the secretKey
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            dectyptedText = cipher.doFinal(text);
            return new String(dectyptedText, StandardCharsets.UTF_8);

        } catch (Exception ex) {
            //TODO throw exception
            LOG.error("Problem decoding RSA: " + ex.getMessage(), ex);
            return null;
        }

    }

    @Override
    public String getRandomKey() {
        throw new UnsupportedOperationException();
    }
}
