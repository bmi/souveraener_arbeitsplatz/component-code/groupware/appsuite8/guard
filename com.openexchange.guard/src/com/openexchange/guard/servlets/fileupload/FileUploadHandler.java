/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Objects;
import javax.mail.util.SharedFileInputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.FileCleanerCleanup;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.DeferredFileOutputStream;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link FileUploadHandler} is a simple wrapper around Apache Commons FileUpload.
 * <p>
 * It provides basic Apache Commons FileUpload functionality configured to be used within OX Guard.
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class FileUploadHandler {

    private static final String GUARD_FILE_SUFFIX = ".grd";
    private static final String GUARD_FILE_PREFIX = "guard-";
    private static final GuardCipherAlgorithm ENCRYPTION_ALGORITHM = GuardCipherAlgorithm.AES_CBC;
    public static final String REQUEST_TRACKER = "grd_tracker";

    private final GuardConfigurationService configurationService;
    private final boolean encrypted;

    /**
     * Initializes a new {@link FileUploadHandler}.
     *
     * @param configurationService The configuration to use
     */
    public FileUploadHandler(GuardConfigurationService configurationService) {
        this.configurationService = Objects.requireNonNull(configurationService, "configurationService must not be null");
        this.encrypted = false;
    }

    /**
     * Initializes a new {@link FileUploadHandler}.
     *
     * @param configurationService The configuration to use
     * @param encrypted True, in order to handle file uploads encrypted when buffered to disk, false to buffer as plain text
     */
    public FileUploadHandler(GuardConfigurationService configurationService, boolean encrypted) {
        this.configurationService = Objects.requireNonNull(configurationService, "configurationService must not be null");
        this.encrypted = encrypted;
    }

    /**
     * Internal method to get the directory in which the files should be stored temporary.
     *
     * @return The directory in which the files should be stored.
     */
    private File getUploadDirectory() {
        String directoryValue = configurationService.getProperty(GuardProperty.fileUploadDirectory);
        if (directoryValue != null && !directoryValue.trim().isEmpty()) {
            return new File(directoryValue);
        }
        return new File(System.getProperty("java.io.tmpdir"));
    }

    /**
     * Internal method to get the threshold value at which OX Guard begins to buffer uploaded files to disk before processing them.
     *
     * @return The threshold value
     */
    private int getThreshold() {
        return configurationService.getIntProperty(GuardProperty.fileUploadBufferThreshhold);
    }

    /**
     * Internal method to get the MAX_UPLOAD_SIZE properties from the infostore.properties file
     *
     * @return The configured MAX_UPLOAD_SIZE in infostore.properties which can be a value >0 in bytes, 0 (in case of unlimited) or -1 (in order to fallback to server.properties' MAX_UPLOAD_SIZE)
     */
    private long getInfostoreMaxUploadSize() {
        //Getting the maximum allowed upload size
        String maxSizeProperty = configurationService.getPropertyFromFile("infostore.properties", "MAX_UPLOAD_SIZE");
        long maxSize = -1L;
        if (maxSizeProperty != null) {
            maxSize = Long.parseLong(maxSizeProperty);
        }
        return maxSize;
    }

    /**
     * Internal method to get the MAX_UPLOAD_SIZE properties from the server.properties file
     *
     * @return The configured MAX_UPLOAD_SIZE in infostore.properties which can be a value >0 in bytes or 0 (in case of unlimited)
     */
    private long getServerMaxUploadSize() {
        //Getting the maximum allowed upload size
        String maxSizeProperty = configurationService.getPropertyFromFile("server.properties", "MAX_UPLOAD_SIZE");
        long maxSize = 0L;
        if (maxSizeProperty != null) {
            maxSize = Long.parseLong(maxSizeProperty);
        }
        return maxSize;
    }

    /**
     * Internal method to obtain the maxium allowed size of an uploaded file.
     * <br><br>
     * Relies on <code>MAX_UPLOAD_SIZE</code> from <code>infostore.properties</code> and <code>server.properties</code>.
     * @return The maximum allowed size, in bytes,  of a file upload, or -1 if unlimited
     */
    private long getConfiguredMaxUploadSize() {
        //get MAX_UPLOAD_SIZE from infostore.properties
        long maxSize = getInfostoreMaxUploadSize();
        if (maxSize == -1) {
            maxSize = getServerMaxUploadSize();
            //fallback to server.properties
        }

        if (maxSize == 0) {
            //unrestricted means "-1" in Apache's fileupload
            maxSize = -1;
        }
        return maxSize;
    }

    /**
     * Creates the {@link ServletFileUpload}.
     *
     * @param request The request to create the {@link ServletFileUpload} for
     * @param maxUploadSize The max. allowed upload size
     * @return The configured, ready to use {@link ServletFileUpload}.
     * @throws OXException
     */
    private ServletFileUpload createFileUpload(HttpServletRequest request, long maxUploadSize) throws OXException {
        DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory(getThreshold(), getUploadDirectory());
        diskFileItemFactory.setFileCleaningTracker(FileCleanerCleanup.getFileCleaningTracker(request.getServletContext()));
        FileItemFactory fileItemFactory;
        if (encrypted) {
            try {
                fileItemFactory = new EncryptedFileItemFactory(diskFileItemFactory, ENCRYPTION_ALGORITHM);
            } catch (NoSuchAlgorithmException e) {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        } else {
            fileItemFactory = new TrackingFileItemFactory(diskFileItemFactory);
        }
        ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
        fileUpload.setFileSizeMax(maxUploadSize);
        fileUpload.setSizeMax(maxUploadSize);
        request.getServletContext().setAttribute(REQUEST_TRACKER, fileItemFactory);  // Store with context for cleanup
        return fileUpload;
    }

    /**
     * Creates a new {@link DeferredFileOutputStream},configured to be used with OX Guard, which can be used to buffer output before serving it to clients
     *
     * @return A new DeferredFileOutputStream
     */
    public DeferredFileOutputStream createOutputStream() {
        return new DeferredFileOutputStream(getThreshold(), GUARD_FILE_PREFIX, GUARD_FILE_SUFFIX, getUploadDirectory());
    }

    /**
     * Parses all {@link FileItem} instances from a given request with the default configured max. allowed upload size
     *
     * @param request The request
     * @return A collection of parsed FileItems.
     * @throws FileUploadException
     */
    public Collection<FileItem> parseItems(HttpServletRequest request) throws OXException {
        try {
            return createFileUpload(request, getConfiguredMaxUploadSize()).parseRequest(request);
        } catch (FileUploadException e) {
            throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parses all {@link FileItem} instances from a given request with a max. upload size
     *
     * @param request The request
     * @param maxUploadSize The max. allowed upload size
     * @return A collection of parsed FileItems.
     * @throws FileUploadException
     */
    public Collection<FileItem> parseItems(HttpServletRequest request, long maxUploadSize) throws OXException {
        try {
            return createFileUpload(request, maxUploadSize).parseRequest(request);
        } catch (FileUploadException e) {
            throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parses a {@link FileItemIterator} from the given request with the default configured max. allowed upload size
     *
     * @param request The request
     * @return The {@link FileItemIterator} parsed from the given request.
     * @throws FileUploadException
     * @throws IOException
     */
    public FileItemIterator parseIterator(HttpServletRequest request) throws OXException {
        try {
            return createFileUpload(request, getConfiguredMaxUploadSize()).getItemIterator(request);
        } catch (FileUploadException e) {
            throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Gets a specific InputStream for a {@link FileItem} from a parsed collection of fileItems.
     *
     * @param fileItems The parsed collection of FileItems
     * @param name The name of the item to get the stream for
     * @param Whether the FileItem is mandatory or not
     * @return The InputStream to the FileItem with the given name, or null if no such FileItem was found and mandatory is set to false.
     * @throws IOException
     * @throws OXException If mandatory is set to true and the given FileItem was not found.
     */
    public InputStream getFileItemStreamFrom(Collection<FileItem> fileItems, String name, boolean mandatory) throws OXException {
        try {
            for (FileItem fileItem : fileItems) {
                if (fileItem.getFieldName().equals(name)) {
                    if (!fileItem.isInMemory()) {
                        File tmpFile = null;
                        if (fileItem instanceof DiskFileItem) {
                            tmpFile = ((DiskFileItem) fileItem).getStoreLocation();
                            return new SharedFileInputStream(tmpFile);
                        } else if (fileItem instanceof EncryptedFileItem) {
                            EncryptedFileItem encryptedFileItem = (EncryptedFileItem) fileItem;
                            return encryptedFileItem.getSharedInputStream();
                        }
                    }
                    return fileItem.getInputStream();
                }
            }
            if (mandatory) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
            }
            return null;
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }
}
