/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.trust;

import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link KeySourceFactory} provides functionality for creating {@link KeySource} objects.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public interface KeySourceFactory {

    /**
     * Creates instances of {@link KeySource} with a trust level reflected by the given configuration property.
     *
     * @param name The name of the KeySource to create.
     * @param trustLevelConfigurationProperty The configuration property which reflects the trust level of the key source.
     * @return A new {@link KeySource} created with the given name and a trust level value obtained from the given {@link GuardProperty}
     */
    public KeySource create(String name, GuardProperty trustLevelConfigurationProperty);

    /**
     * Creates a {@link KeySource} from a given name. The trust level is obtained from previous created key sources with the same name.
     *
     * @param name The name of the {@link KeySource} to add.
     * @return A new {@link KeySource} created with the given name and a trust level value obtained from previous created instances with the same name.
     */
    public KeySource create(String name) ;

}
