/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.lang.ArrayUtils;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.GuardResult;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.osgi.Services;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.SecuritySettings;
import com.openexchange.mail.dataobjects.compose.ComposedMailMessage;
import com.openexchange.mail.dataobjects.compose.ContentAwareComposedMailMessage;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.mail.service.EncryptedMailService;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link EncryptedMailServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class EncryptedMailServiceImpl extends AbstractGuardAccess implements EncryptedMailService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(EncryptedMailServiceImpl.class);

    /**
     * Simple test if PGP type. Currently the only draft email type supported
     *
     * @param type
     * @return
     */
    // TODO   Make this configurable or method to register
    private boolean handles(String type) {
        if (type == null || "pgp".equals(type.toLowerCase()))
            return true;
        return false;
    }

    private User getUserFrom(Session session) throws OXException {
        UserService userService = Services.getService(UserService.class);
        if (userService == null) {
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(UserService.class);
        }
        return userService.getUser(session.getUserId(), session.getContextId());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.mail.service.EncryptedMailService#encryptDraftEmail(com.openexchange.mail.dataobjects.compose.ComposedMailMessage)
     */
    @Override
    public ComposedMailMessage encryptDraftEmail(ComposedMailMessage draft, Session session, String cryptoAuthentication) throws OXException {

        // If not encrypting, then don't send to Guard
        // Not signing drafts
        if (!draft.getSecuritySettings().isEncrypt() || !handles(draft.getSecuritySettings().getType())) {
            draft.removeHeader("X-Security");
            draft.addHeader("X-Security", getSecurityHeader(draft.getSecuritySettings()));
            return draft;
        }

        if (cryptoAuthentication != null) {
            draft.getSecuritySettings().setAuthentication(cryptoAuthentication);
        }

        Message msg = null;
        if (draft.getClass().equals(ContentAwareComposedMailMessage.class)) {
            msg = (Message) draft.getContent();
        } else {
            msg = MimeMessageConverter.convertComposedMailMessage(draft);
        }

        GuardApiImpl guardApi = requireGuardApi(CRYPTO_ENDPOINT);

        GuardAuthenticationToken authToken = cryptoAuthentication == null ? new AuthenticationTokenHandler()
            .getForSession(session, CryptoType.PROTOCOL.PGP.getValue()) : GuardAuthenticationToken.fromString(cryptoAuthentication);

        final User user = getUserFrom(session);
        // Do we want to add keys for all recips here?  Or just the from address?  Only draft
        Address[] recips = (Address[]) ArrayUtils.addAll(draft.getFrom(), draft.getTo());
        recips = (Address[]) ArrayUtils.addAll(recips, draft.getCc());

        GuardResult processed = guardApi.processMimeMessage(msg, recips, draft.getSecuritySettings(),
            GuardApis.mapFor("action", "process_mime",
                "user", Integer.toString(session.getUserId()),
                "context", Integer.toString(session.getContextId()),
                "fromName", user.getDisplayName(),
                "email", user.getMail(),
                "draft", "true",
                "isGuest", Boolean.toString(user.isGuest())),
            session,
            authToken);

        MimeMessage encrypted = processed.getMimeMessage();
        try {
            encrypted.removeHeader("X-Security");  // Remove any old headers
            encrypted.addHeader("X-Security", getSecurityHeader(draft.getSecuritySettings()));
            encrypted.saveChanges();
        } catch (MessagingException e) {
            LOG.error("Problem saving draft security settings ", e);
        }
        ContentAwareComposedMailMessage encryptedMessage = new ContentAwareComposedMailMessage(encrypted, session, session.getContextId());
        encryptedMessage.setMsgref(draft.getMsgref());  // keep the message reference
        encryptedMessage.setFlags(draft.getFlags());
        return encryptedMessage;
    }

    private boolean isEncryptedPart(BodyPart part) throws IOException, MessagingException {
        part = Objects.requireNonNull(part, "part must not be null");
        return (part.getContent() instanceof InputStream && part.getContentType() != null && part.getContentType().contains("application/pgp-encrypted")) ||
               (part.getFileName() != null && part.getFileName().contains("encrypted.asc"));
    }

    /**
     * Internal method to remove some old PGP attachments for autodraft
     *
     * @param multipart The part to remove the PGP attachments from
     * @throws MessagingException
     * @throws IOException
     */
    private void removePGPAttachments(Multipart multipart) throws MessagingException, IOException {
        final int count = multipart.getCount();
        final List<BodyPart> partsToRemove = new ArrayList<BodyPart>();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = multipart.getBodyPart(i);
            if(bodyPart != null && (isEncryptedPart(bodyPart))) {
                partsToRemove.add(bodyPart);
            }
        }
        for (BodyPart p : partsToRemove) {
            multipart.removeBodyPart(p);
        }
    }

    /**
     * Return string representation of Security Settings to save to Draft header
     *
     * @param settings
     * @return
     */
    private String getSecurityHeader(SecuritySettings settings) {
        StringBuilder sb = new StringBuilder();
        sb.append("sign=" + (settings.isSign() ? "true" : "false") + "; ");
        sb.append("encrypt=" + (settings.isEncrypt() ? "true" : "false") + "; ");
        sb.append("pgpInline=" + (settings.isPgpInline() ? "true" : "false") + "; ");
        sb.append("type=" + (settings.getType() == null ? "pgp" : settings.getType()) + ";");
        return (sb.toString());
    }


    /* (non-Javadoc)
     * @see com.openexchange.mail.service.EncryptedMailService#encryptAutosaveDraftEmail(com.openexchange.mail.dataobjects.MailMessage, com.openexchange.session.Session, com.openexchange.mail.dataobjects.SecuritySettings)
     */
    @Override
    public MailMessage encryptAutosaveDraftEmail(MailMessage message, Session session, SecuritySettings securitySettings) throws OXException {

        // If not encrypting, then don't send to Guard
        // Not signing drafts
        if (!securitySettings.isEncrypt() || !handles(securitySettings.getType())) {
            message.removeHeader("X-Security");
            message.addHeader("X-Security", getSecurityHeader(securitySettings));
            return message;
        }

        Message msg = MimeMessageConverter.convertMailMessage(message);

        GuardApiImpl guardApi = requireGuardApi(CRYPTO_ENDPOINT);
        GuardAuthenticationToken authToken = securitySettings.getAuthentication() == null ? new AuthenticationTokenHandler()
            .getForSession(session, CryptoType.PROTOCOL.PGP.getValue()) : GuardAuthenticationToken.fromString(securitySettings.getAuthentication());

        try {
            Object content = msg.getContent();
            if (content instanceof Multipart || content instanceof MimeMultipart) {
                //We need to remove previous encrypted autodraft content which gets overwritten with the new content
                removePGPAttachments((Multipart) content);
                msg.saveChanges();
            }

            Address[] recips = new Address[] {};
            recips = (Address[]) ArrayUtils.addAll(recips, msg.getFrom());
            recips = (Address[]) ArrayUtils.addAll(recips, msg.getRecipients(RecipientType.TO));
            recips = (Address[]) ArrayUtils.addAll(recips, msg.getRecipients(RecipientType.CC));

            final User user = getUserFrom(session);
            GuardResult processed = guardApi.processMimeMessage(msg, recips, securitySettings,
                GuardApis.mapFor("action", "process_mime",
                    "user", Integer.toString(session.getUserId()),
                    "context", Integer.toString(session.getContextId()),
                    "fromName", user.getDisplayName(),
                    "email", user.getMail(),
                    "draft", "true",
                    "isGuest", Boolean.toString(user.isGuest())),
                session,
                authToken);

            MimeMessage encrypted = processed.getMimeMessage();

            encrypted.removeHeader("X-Security");  // Remove any old headers
            encrypted.addHeader("X-Security", getSecurityHeader(securitySettings));
            encrypted.setFlags(msg.getFlags(), true);
            encrypted.saveChanges();

            MailMessage ret = MimeMessageConverter.convertMessage(encrypted);
            ret.setFlags(message.getFlags());
            return ret;

        } catch (Exception e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, "Error processing message recipients");
        }
    }
}
