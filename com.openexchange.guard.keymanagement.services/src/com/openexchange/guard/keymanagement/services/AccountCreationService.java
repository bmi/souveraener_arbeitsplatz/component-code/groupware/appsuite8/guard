/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;

/**
 * {@link AccountCreationService} provides high level functionality for creating OX Guard user and guest accounts.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public interface AccountCreationService {

    /**
     * Creates the initial OX Guard keys ({@link GuardKeys}) for a given recipient
     *
     * @param recipKey The {@link RecipKey} to create an initial OX Guard key for
     * @return The updated {@link RecipKey} populated with information from the new created initial OX Guard key
     * @throws OXException
     */
    RecipKey createUserFor(RecipKey recipKey) throws OXException;
}
