/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.pgpcore.services.FromHeaderVerifier;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;
import com.openexchange.pgp.core.PGPSignatureVerifier;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link PGPMimeSigningServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class PGPMimeSigningServiceImpl implements MimeSignatureVerificationService {

    private static final Logger LOG = LoggerFactory.getLogger(PGPMimeSigningServiceImpl.class);
    private final GuardKeyService guardKeyService;
    private final OGPGPKeysStorage keyStorage;
    private final AutoCryptStorageService autocryptService;
    private final GuardConfigurationService configurationService;
    private final HKPClientService hkpClient;
    private static final String BEGIN_PGP_SIGNATURE_MARKER = "-----BEGIN PGP SIGNED MESSAGE-----";

    /**
     * Initializes a new {@link PGPMimeSigningServiceImpl}.
     *
     * @param guardKeyService The {@link GuardKeyService} instance
     * @param keyStorage The {@link OGPGPKeysStorage} instance
     * @param autocryptService The {@link AutoCryptStorageService} instance
     */
    public PGPMimeSigningServiceImpl(GuardKeyService guardKeyService,
        OGPGPKeysStorage keyStorage,
        AutoCryptStorageService autocryptService,
        GuardConfigurationService configurationService,
        HKPClientService hkpClient) {

        super();
        this.guardKeyService = Objects.requireNonNull(guardKeyService, "guardKeyService must not be null");
        this.keyStorage = Objects.requireNonNull(keyStorage, "keyStorage must not be null");
        this.autocryptService = Objects.requireNonNull(autocryptService, "autocryptService must not be null");
        this.configurationService = Objects.requireNonNull(configurationService, "configurationService must not be null");
        this.hkpClient = Objects.requireNonNull(hkpClient, "hkpClient must not be null");
    }

    private PGPKeyRetrievalStrategy getKeyRetrievalStrategyFor(UserIdentity user) {
        int timeout = this.configurationService.getIntProperty(GuardProperty.remoteKeyLookupTimeout,
            user.getOXUser().getId(), user.getOXUser().getContextId());

        return new CompositePGPKeyRetrievalStrategy(

            //Look for public keys in Guard
            new DefaultPGPKeyRetrievalStrategy(guardKeyService,
                keyStorage,
                autocryptService,
                user.getOXUser().getId(),
                user.getOXUser().getContextId()),

            //..Look for public keys on _trusted_ HKP servers
            new HKPPGPKeyRetrievalStrategy(hkpClient,
                user.getOXUser().getId(),
                user.getOXUser().getContextId(),
                timeout,
                HKPPGPKeyRetrievalStrategy.TrustMode.TRUSTED_ONLY)

        );
    }

    private <T> T requireSignedDataNotNull(T data) throws OXException {
        if (data == null) {
            throw PGPCoreServicesExceptionCodes.SIGNATURE_ERROR_NO_SIGNED_DATA_IN_EMAIL.create();
        }
        return data;
    }

    private <T> T requireSignatureNotNull(T signature) throws OXException {
        if (signature == null) {
            throw PGPCoreServicesExceptionCodes.SIGNATURE_ERROR_NO_SIGNATURE_IN_EMAIL.create();
        }
        return signature;
    }

    private String decodeBase64(String data) {
        if (data.contains("\r\n\r\n")) {
            data = data.substring(data.indexOf("\r\n\r\n"));
        }
        return new String(Base64.decode(data.trim()), StandardCharsets.UTF_8);
    }

    private String normalize(String content) {
        StringBuilder sb = new StringBuilder();
        String[] lines = content.split("\n");
        for (String line : lines) {
            if (line.startsWith("- -")) {
                line = line.substring(2);
            }
            sb.append(StringUtils.stripEnd(line, null) + "\r\n");
        }
        return (sb.toString());
    }

    private String getDecoded(InputStream stream) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(stream, out);
        stream.close();
        return new String(out.toByteArray(), StandardCharsets.UTF_8);
    }

    private InputStream extractSignedDataFromMime(MimeMessage message) throws IOException, MessagingException {
        if (message.getContent() instanceof Multipart) {
            Multipart mp = (Multipart) message.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                // Check that the subsequent mime part is the signature
                if ((i < mp.getCount() - 1) && mp.getBodyPart(i + 1).getContentType().contains("application/pgp-signature")) {
                    return new SignatureStream(mp.getBodyPart(i));
                }
            }
        }
        return null;
    }

    private InputStream asStream(String data) {
        return new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
    }

    private String getContentInlineBody(String data, boolean convert) {
        int i = data.indexOf("----BEGIN PGP SIGNATURE");
        if (i > -1) {
            data = data.substring(0, i - 1);
            int j = data.indexOf("Hash:");
            if (j > -1) {
                j = data.indexOf("\n", j) + 3;
                data = data.substring(j);
            }
            if (convert) {
                data = data.replace("\n", "<br>");
            }
        }
        return (data);
    }

    private InputStream extractMimeSignatureFromMime(MimeMessage message) throws IOException, MessagingException {
        if (message.getContent() instanceof Multipart) {
            Multipart mp = (Multipart) message.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                ByteArrayOutputStream writer = new ByteArrayOutputStream();
                mp.getBodyPart(i).writeTo(writer);
                String partData = writer.toString("UTF-8");
                if (mp.getBodyPart(i).getContentType().contains("application/pgp-signature")) {
                    if (partData.contains("Encoding: base64")) {
                        return asStream(decodeBase64(partData));
                    }
                    return asStream(partData);
                }
            }
        }
        return null;
    }

    private InputStream extractInlineSignatureFromInline(String message) {
        final String regex = "(-----BEGIN PGP SIGNATURE-----[\\s\\S]*?-----END PGP SIGNATURE-----)";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(message);
        boolean signatureFound = matcher.find();
        if (signatureFound) {
            return asStream(cleanupSignature(matcher.group(1)));
        }
        return null;
    }

    private InputStream extractInlineMessageFromInline(String message) {
        if (message.contains("<br>")) {
            message = cleanupHTML(message);
        }
        final String regex = "-----BEGIN PGP SIGNED MESSAGE-----.*?\nHash:.*?\n.*?\n(.*).\\s-----BEGIN PGP SIGNATURE-----";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(message);
        boolean messageFound = matcher.find();
        if (messageFound) {
            return asStream(matcher.group(1));
        }
        return null;
    }

    private String cleanupSignature(String sig) {
        StringBuilder sb = new StringBuilder();
        String[] lines = sig.split("\r");
        for (String line : lines) {
            sb.append(line.trim().replaceAll("<br>", "").replaceAll("[^\\x20-\\x7E]", ""));
            sb.append("\r");
        }
        sb.append("\r");
        return (sb.toString());
    }

    private String cleanupHTML(String data) {
        StringBuilder sb = new StringBuilder();
        String[] lines = data.split("\r");
        int indentation = 0;  // HTML often indented
        for (String line : lines) {
            if (line.contains("-----BEGIN")) {
                indentation = line.indexOf("-----BEGIN");
            }
            line = line.replaceAll("<br>", "").replaceAll("[\\xA0]", " ");  // A0 is space in extended ASCII
            line = line.substring(indentation > line.length() ? line.length() : indentation);
            if (line.startsWith("- -")) {
                line = line.substring(2);
            }
            sb.append(line);
            sb.append("\r\n");
        }
        return (sb.toString());
    }

    private List<SignatureVerificationResult> verifyMimeEmailSignature(MimeMessage message, UserIdentity userIdentity) throws OXException {
        InputStream signedData = null;
        InputStream signature = null;
        try {
            signedData = requireSignedDataNotNull(extractSignedDataFromMime(message));
            signature = requireSignatureNotNull(extractMimeSignatureFromMime(message));
        } catch (IOException | MessagingException e) {
            IOUtils.closeQuietly(signedData);  // If signature was null, close signedData
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
        ;

        try {
            return new PGPSignatureVerifier(getKeyRetrievalStrategyFor(userIdentity))
                .verifySignatures(signedData, signature);
        } catch (Exception e) {
            LOG.error("Error verifying signature", e);
            return Arrays.asList(new PGPSignatureVerificationResult(e.getMessage()));
        } finally {
            IOUtils.closeQuietly(signature);
            IOUtils.closeQuietly(signedData);
        }
    }

    private List<SignatureVerificationResult> verifyInlineEmailSignature(String inlineData, UserIdentity userIdentity) throws OXException {
        InputStream signedData = requireSignedDataNotNull(extractInlineMessageFromInline(inlineData));
        InputStream signature = requireSignatureNotNull(extractInlineSignatureFromInline(inlineData));
        try {
            return new PGPSignatureVerifier(getKeyRetrievalStrategyFor(userIdentity))
                .verifySignatures(signedData, signature);
        } catch (Exception e) {
            return Arrays.asList(new PGPSignatureVerificationResult(e.getMessage()));
        } finally {
            IOUtils.closeQuietly(signature);
            IOUtils.closeQuietly(signedData);
        }
    }

    private boolean wasVerified(List<SignatureVerificationResult> results) {
        if (results != null && results.size() > 0) {
            for (SignatureVerificationResult result : results) {
                if (!result.isVerified()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private List<SignatureVerificationResult> verifyInlineMessage(MimeMessage msg, UserIdentity userIdentity) throws OXException {
        try {
            List<SignatureVerificationResult> ret = new ArrayList<SignatureVerificationResult>();
            Object content = msg.getContent();
            if (content instanceof String) {  // Simple text only email
                String data = getDecoded(msg.getInputStream());
                ret = verifyInlineEmailSignature(normalize(data), userIdentity);
                if (wasVerified(ret)) {  // If verified, clean up content
                    msg.setContent(getContentInlineBody((String) content, false), msg.getContentType());
                }
            } else {  // Multipart.  Check for any messages for display.  All must have signatures
                if (content instanceof MimeMultipart) {
                    MimeMultipart mp = (MimeMultipart) content;
                    // Create a new messsage to return with the signature markup removed
                    MimeMultipart newpart = new MimeMultipart(mp.getContentType().contains("alternative") ? "alternative" : "mixed");
                    for (int i = 0; i < mp.getCount(); i++) {
                        BodyPart bp = mp.getBodyPart(i);
                        if (bp.getContent() instanceof String && (bp.getDisposition() == null || bp.getDisposition().contains("inline"))) {
                            String[] encoding = bp.getHeader("Content-Transfer-Encoding");
                            String data = "";
                            if (encoding != null) {
                                data = normalize(getDecoded(bp.getInputStream()));
                            } else {
                                data = (String) bp.getContent();
                            }
                            try {
                                List<SignatureVerificationResult> partResult = verifyInlineEmailSignature(data, userIdentity);
                                ret.addAll(partResult);
                                if (wasVerified(partResult)) {  // If verified, clean up content
                                    bp.setContent(getContentInlineBody((String) bp.getContent(), false), bp.getContentType());
                                    newpart.addBodyPart(bp);
                                } else {
                                    if (bp.getContentType() != null && bp.getContentType().toLowerCase().contains("text/plain")) {  // If the plaintext fails, we'll just fail the entire thing and return
                                        msg.saveChanges();
                                        return ret;
                                    }
                                }
                            } catch (OXException ex) {
                                LOG.error("Problem checking inline signature in email", ex);
                                return Collections.singletonList(new PGPSignatureVerificationResult("Unable to verify signature"));
                            }
                        } else {
                            // If not a text part or attachment, just add it back to the multipart
                            newpart.addBodyPart(bp);
                        }
                    }
                    msg.setContent(newpart);
                }
            }
            msg.saveChanges();
            return ret;
        } catch (MessagingException | IOException e) {
            return Collections.emptyList();
        }
    }

    private boolean isPGPMime(MimeMessage message) throws OXException {
        try {
            return message.getContentType().toLowerCase().contains("pgp-signature");
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Peak through upper 2k only of an email to see if finds signed data
     * isPGPSignedObject
     *
     * @param message
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    private static boolean isPGPSignedObject(MimeMessage message) throws IOException, MessagingException {
        InputStream stream = message.getInputStream();
        if (stream == null) {
            return false;
        }
        byte[] peekedData = new byte[2048];
        int read = stream.read(peekedData);
        stream.close();
        if (read > 0) {
            String peekedContent = new String(peekedData, StandardCharsets.UTF_8);
            return peekedContent.contains(BEGIN_PGP_SIGNATURE_MARKER);
        }
        return false;
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.PGP;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPMimeSigningService#verify(javax.mail.internet.MimeMessage, com.openexchange.guard.user.UserIdentity)
     */
    @Override
    public List<SignatureVerificationResult> verify(MimeMessage mimeMessage, UserIdentity user, boolean decrypted) throws OXException {
        List<SignatureVerificationResult> verificationResults;
        if (isPGPMime(mimeMessage)) {
            verificationResults = verifyMimeEmailSignature(mimeMessage, user);
        } else {
            try {
                if (isPGPSignedObject(mimeMessage)) {  // Check if inline signed data found
                    verificationResults = verifyInlineMessage(mimeMessage, user);
                } else {
                    verificationResults = Collections.emptyList();
                }
            } catch (IOException | MessagingException e) {
                throw OXException.general("Error checking signature", e);
            }
        }

        //We need to compare the FROM header of the Message with the actual User ID of the signature's issuer in order ensure that the signature was really created by the sender of the email.
        if (!decrypted)
            FromHeaderVerifier.verify(mimeMessage, verificationResults);
        return verificationResults;
    }

}
