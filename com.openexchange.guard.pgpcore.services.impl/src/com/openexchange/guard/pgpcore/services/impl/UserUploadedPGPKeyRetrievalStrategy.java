/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;

/**
 * {@link UserUploadedPGPKeyRetrievalStrategy}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class UserUploadedPGPKeyRetrievalStrategy implements PGPKeyRetrievalStrategy {

    final private OGPGPKeysStorage keyStorage;
    private final int userId;
    private final int contextId;

    /**
     * Initializes a new {@link UserUploadedPGPKeyRetrievalStrategy}.
     *
     * @param userId The user id
     * @param contextId The context id of the user
     */
    public UserUploadedPGPKeyRetrievalStrategy(OGPGPKeysStorage keyStorage, int userId, int contextId) {
        this.keyStorage = Objects.requireNonNull(keyStorage, "keyStorage must not ne null");
        this.userId = userId;
        this.contextId = contextId;
    }

    /**
     * Gets the PGPPublicKey with the given id from the {@link OGPGPKey} object.
     *
     * @param ogpgpKey The object to extract the PGPPublicKey from.
     * @param keyId The ID of the public key to extract.
     * @return The PGPPublicKey with the given ID, or null if no such key was found.
     * @throws IOException
     */
    private PGPPublicKey getPublicKey(OGPGPKey ogpgpKey, long keyId) throws IOException {
        PGPPublicKeyRing keyRing = PGPPublicKeyRingFactory.create(ogpgpKey.getPublicPGPAscData());
        return keyRing.getPublicKey(keyId);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getSecretKey(long, java.lang.String, char[])
     */
    @Override
    public PGPPrivateKey getSecretKey(long keyId, String userIdentity, char[] password) throws Exception {
        /* user does only upload public keys for external recipients */
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getPublicKey(long)
     */
    @Override
    public PGPPublicKey getPublicKey(long keyId) throws Exception {
        List<OGPGPKey> publicKeys = keyStorage.getForUser(userId, contextId);
        for(OGPGPKey keyRing : publicKeys) {
            PGPPublicKey publicKey = getPublicKey(keyRing, keyId);
            if(publicKey != null) {
                return publicKey;
            }
        }
        return null;
    }
}
