/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.InputStream;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.cms.KeyTransRecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipient;
import org.bouncycastle.mail.smime.SMIMEEnveloped;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SMIMEDecryptor}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SMIMEDecryptor {

    ServiceLookup services;

    public SMIMEDecryptor(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Decrypt MimeMessage
     *
     * @param msg MimeMessage to decrypt
     * @param pKeys The private keys of the user
     * @param password Password to decrypt the private keys
     * @return Decrypted MimeMessage
     * @throws Exception
     */
    public MimeMessage decryptPart(MimeMessage msg, int userId, int contextId, String password) throws Exception {
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        List<SmimePrivateKeys> pkeys = keyService.getPrivateKeys(userId, contextId);
        if (pkeys == null || pkeys.isEmpty()) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create();
        }
        SMIMEEnveloped smime = new SMIMEEnveloped(msg);
        RecipientInformationStore recipientInfo = smime.getRecipientInfos();
        Collection<RecipientInformation> recipients = recipientInfo.getRecipients();
        for (RecipientInformation recip : recipients) {
            PrivateKey pkey = null;
            KeyTransRecipientId rid = (KeyTransRecipientId) recip.getRID();
            for (SmimePrivateKeys key : pkeys) {
                if (rid.getSerialNumber().toString().equals(key.getSerial())) {
                    try {
                        pkey = SmimeCryptoKeyUtil.decryptPrivateKey(key, password);
                    } catch (OXException e) {
                        //continue if the password was wrong
                        if (!e.similarTo(GuardAuthExceptionCodes.BAD_PASSWORD)) {
                            throw e;
                        }
                    }
                }
            }
            if (pkey == null) {
                continue;
            }
            JceKeyTransRecipient pKeyRecp = new JceKeyTransEnvelopedRecipient(pkey);
            MimeBodyPart res = SMIMEUtil.toMimeBodyPart(recip.getContent(pKeyRecp));
            MimeMessage newResult = new MimeMessage(msg);
            removeHeaders(newResult);   // get rid of all the content-type headers
            if (res.getContent() instanceof Multipart) {
                Multipart part = (Multipart) res.getContent();
                newResult.setContent(part);
            } else {
                try (InputStream in = res.getInputStream()) {
                    GuardSmimeUtil.updateFromInputStream(newResult, in, res.getContentType());
                }
            }
            newResult.saveChanges();
            return newResult;
        }
        return null;
    }

    /**
     * Remove any headers that relate to content. These will be replaced with decrypted data
     * removeHeaders
     *
     * @param msg
     * @throws OXException
     */
    private static void removeHeaders(MimeMessage msg) throws OXException {
        try {
            Enumeration<Header> headers = msg.getAllHeaders();
            while (headers.hasMoreElements()) {
                Header header = headers.nextElement();
                if (header.getName().contains("Content")) {
                    msg.removeHeader(header.getName());
                }
            }
        } catch (MessagingException e) {
            LoggerFactory.getLogger(SMIMEDecryptor.class).error("Unable to remove header", e);
            throw SmimeExceptionCodes.SMIME_ERROR.create(e.getMessage());
        }
    }

}
