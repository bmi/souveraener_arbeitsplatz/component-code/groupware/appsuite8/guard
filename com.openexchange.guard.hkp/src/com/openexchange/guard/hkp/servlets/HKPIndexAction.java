/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.hkp.internal.HKPIndex;

/**
 * {@link HKPIndexAction} Handles incoming HKP index actions
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class HKPIndexAction {

    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private final String search;

    /**
     * Initializes a new {@link HKPIndexAction}.
     * 
     * @param request The request
     * @param response The response
     * @param search HKP search value
     */
    public HKPIndexAction(HttpServletRequest request, HttpServletResponse response, String search) {
        this.request = request;
        this.response = response;
        this.search = search;
    }

    /**
     * Performs the index action
     * 
     * @throws OXException
     * @throws Exception
     */
    public void doAction() throws OXException, Exception {
        String ip = ServletUtils.getClientIP(request);
        String result = new HKPIndex().index(ip, search);
        if (result != null) {
            ServletUtils.sendOK(response, "application/pgp-keys", result);
        } else {
            ServletUtils.sendResponse(response, HttpServletResponse.SC_NOT_FOUND, "No keys found");
        }
    }
}
