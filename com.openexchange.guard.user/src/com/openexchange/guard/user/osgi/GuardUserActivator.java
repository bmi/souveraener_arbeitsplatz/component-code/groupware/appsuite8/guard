/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user.osgi;

import org.slf4j.Logger;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.internal.OXUserServiceImpl;
import com.openexchange.guard.user.internal.Services;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardUserActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardUserActivator extends HousekeepingActivator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardDatabaseService.class, GuardConfigurationService.class, EmailStorage.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(GuardUserActivator.class);
        logger.info("Starting bundle: {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);
        registerService(OXUserService.class,
            new OXUserServiceImpl(getService(GuardConfigurationService.class), getService(EmailStorage.class)));
        openTrackers();
        logger.info("GuardUserService registered.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(GuardUserActivator.class);
        logger.info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(OXUserService.class);
        Services.setServiceLookup(null);
        logger.info("GuardUserService unregistered.");
        super.stopBundle();
    }
}
