/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.updater;

import java.sql.Connection;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;
import com.openexchange.guard.database.update.GuardCreateTableTask;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.tools.update.Column;
import com.openexchange.tools.update.Tools;

/**
 * {@link AddMKeyIndexUpdateTask}
 *
 * Updates the og_KeyTable to add the master key index
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.6
 */
public class AddMKeyIndexUpdateTask extends UpdateTaskAdapter {

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#perform(com.openexchange.groupware.update.PerformParameters)
     */
    @Override
    public void perform(PerformParameters params) throws OXException {
        final Logger log = LoggerFactory.getLogger(AddMKeyIndexUpdateTask.class);
        log.info("Performing update task {}", AddMKeyIndexUpdateTask.class.getSimpleName());
        Connection connection = params.getConnection();
        boolean rollback = false;
        try {
            connection.setAutoCommit(false);
            rollback = true;

            Column column = new Column("mKeyIndex", "int DEFAULT 0");
            Tools.checkAndAddColumns(connection, "og_KeyTable", column);

            connection.commit();
            rollback = false;
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        }
        finally {
            if(rollback) {
               DBUtils.rollback(connection);
            }
            DBUtils.autocommit(connection);
        }
        log.info("{} successfully performed.", AddMKeyIndexUpdateTask.class.getSimpleName());
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#getDependencies()
     */
    @Override
    public String[] getDependencies() {
        return new String[] { GuardCreateTableTask.class.getName() };
    }

}
