/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;


/**
 * {@link PGPCoreServicesExceptionCodes}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public enum PGPCoreServicesExceptionCodes implements DisplayableOXExceptionCode {

    RECIPIENT_KEY_NOT_FOUND("Unable to retrieve recipient's key (%1$s)",PGPCoreServicesExceptionMessages.RECIPIENT_KEY_NOT_FOUND, Category.CATEGORY_ERROR, 1),
    SIGNING_KEY_NOT_FOUND("Unable to retrieve signing key (%1$s)", PGPCoreServicesExceptionMessages.SIGNING_KEY_NOT_FOUND, Category.CATEGORY_ERROR, 2),
    ENCRYPTION_ERROR("An PGP encryption error occurred: %1$s", PGPCoreServicesExceptionMessages.ENCRYPTION_ERROR, Category.CATEGORY_ERROR,3),
    DECRYPTION_ERROR("An PGP decryption error occurred: %1$s", PGPCoreServicesExceptionMessages.DECRYPTION_ERROR, Category.CATEGORY_ERROR,4),
    SIGNING_ERROR("An PGP signing error occured: %1$s", PGPCoreServicesExceptionMessages.SIGNING_ERROR,Category.CATEGORY_ERROR,5),
    SIGNATURE_VERIFICATION_ERROR("An PGP signature verification error occured: %1$s",PGPCoreServicesExceptionMessages.SIGNATURE_VERIFICATION_ERROR, Category.CATEGORY_ERROR,6),
    UNEXPECTED_ERROR("Unexpected error occured: %1$s", PGPCoreServicesExceptionMessages.UNEXPECTED_ERROR, CATEGORY_ERROR,7),
    RECIPIENT_PARSING_ERROR("Unable to parse the given recipient (%1$s)",PGPCoreServicesExceptionMessages.RECIPIENT_PARSING_ERROR,Category.CATEGORY_ERROR,8),
    NOT_A_MULTIPART_MSG_ERROR("The provided message is not a valid multipart message", PGPCoreServicesExceptionMessages.NOT_A_MULTIPART_MSG_ERROR, Category.CATEGORY_ERROR, 9),
    SIGNATURE_ERROR_NO_SIGNED_DATA_IN_EMAIL("No signed data found in E-Mail", PGPCoreServicesExceptionMessages.SIGNATURE_ERROR_NO_SIGNED_DATA_MSG, Category.CATEGORY_ERROR, 10),
    SIGNATURE_ERROR_NO_SIGNATURE_IN_EMAIL("No signature found in E-Mail", PGPCoreServicesExceptionMessages.SIGNATURE_ERROR_NO_SIGNATURE_MSG, Category.CATEGORY_ERROR, 11)


    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    private PGPCoreServicesExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private PGPCoreServicesExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-SVS";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
