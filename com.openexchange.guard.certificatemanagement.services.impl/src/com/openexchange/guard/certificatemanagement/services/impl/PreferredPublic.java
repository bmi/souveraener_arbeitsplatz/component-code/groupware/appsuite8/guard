/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.certificatemanagement.services.impl;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PreferredPublic}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class PreferredPublic {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(PreferredPublic.class);
    }

    /**
     * Get the key marked as current/preferred from local private key list
     *
     * @param keys public keys to compare
     * @param services
     * @return Key marked as current, null if none of them marked current
     * @throws OXException
     */
    private static SmimeKeys getValidLocalKey(List<SmimeKeys> keys, ServiceLookup services) throws OXException {
        final SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        for (SmimeKeys key : keys) {
            SmimePrivateKeys pKey = keyService.getPrivateKey(key.getSerial().toString(), key.getUserId(), key.getContextId());
            if (pKey != null && pKey.isCurrent()) {
                if (!key.isExpired() && key.isForEncryption())
                    return key;
            }
        }
        return null;
    }

    /**
     * Check key still valid
     *
     * @param key
     * @param verifier
     * @param userId
     * @param contextId
     * @return
     */
    private static boolean verify(SmimeKeys key, CertificateService verifier, int userId, int contextId) {
        try {
            return verifier.verify(key.getCertificate(), new HashSet<X509Certificate>(key.getChain()), new Date(), userId, contextId).equals(Result.VERIFIED);
        } catch (Exception e) {
            LoggerHolder.LOGGER.debug("Unable to verify", e);
            return false;
        }
    }

    /**
     * Find the preferred public key. Will verify the keys are marked as encryption, not expired, and verify
     * If local keys, will return the one marked as current
     * If multiple valid keys present, will return the key most recently seen.
     *
     * @param keys List of public keys
     * @param services
     * @param userId
     * @param contextId
     * @return Preferred key
     * @throws OXException
     */
    public static SmimeKeys findPreferredEncrypting(List<SmimeKeys> keys, ServiceLookup services, int userId, int contextId) throws OXException {
        CertificateService verifier = services.getServiceSafe(CertificateService.class);
        if (keys.size() == 1) {
            SmimeKeys key = keys.get(0);
            if (key.isForEncryption() && verify(key, verifier, userId, contextId)) {
                return key;
            }
            return null;
        }
        boolean hasLocal = keys.stream().anyMatch((k) -> k.isLocal());
        if (hasLocal) {
            SmimeKeys key = getValidLocalKey(keys, services);
            if (key != null && verify(key, verifier, userId, contextId)) {
                return key;
            }
        }
        SmimeKeys mostRecent = null;
        for (SmimeKeys key : keys) {
            if (!key.isExpired() && key.isForEncryption()) {
                if (verify(key, verifier, userId, contextId)) {
                    if (mostRecent == null) {
                        mostRecent = key;
                    } else {
                        if (key.getLastUpdate() > mostRecent.getLastUpdate()) {
                            mostRecent = key;
                        }
                    }
                }
            }
        }
        return mostRecent;
    }

}
