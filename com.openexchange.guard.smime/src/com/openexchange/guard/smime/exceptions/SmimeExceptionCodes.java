/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * {@link SmimeExceptionCodes}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public enum SmimeExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * <li>Duplicate SMIME private key
     * <li>{@value SmimeExceptionCodeMessages#DUPLICATE_PRIVATE}
     */
    DUPLICATE_PRIVATE_KEY("Duplicate SMIME private key", SmimeExceptionCodeMessages.DUPLICATE_PRIVATE, Category.CATEGORY_ERROR, 1),

    /**
     * <li>Certificate failed verification
     * <li>{@value SmimeExceptionCodeMessages#VERIFICATION_FAILED}
     */
    CERTIFICATE_VERIFICATION_FAIL("Certificate failed verification", SmimeExceptionCodeMessages.VERIFICATION_FAILED, Category.CATEGORY_ERROR, 2),
    /**
     * <li>Key not found %1$s
     * <li>{@value SmimeExceptionCodeMessages#KEY_NOT_FOUND}
     */
    KEY_NOT_FOUND("Key not found %1$s", SmimeExceptionCodeMessages.KEY_NOT_FOUND, Category.CATEGORY_ERROR, 3),
    /**
     * <li>Not authorized
     * <li>{@value SmimeExceptionCodeMessages#NOT_AUTHORIZED}
     */
    NOT_AUTHORIZED("Not authorized", SmimeExceptionCodeMessages.NOT_AUTHORIZED, Category.CATEGORY_ERROR, 4),
    /**
     * <li>Error processing SMIME: '%1$s'
     * <li>{@value SmimeExceptionCodeMessages#SMIME_ERROR}
     */
    SMIME_ERROR("Error processing SMIME: '%1$s'", SmimeExceptionCodeMessages.SMIME_ERROR, Category.CATEGORY_ERROR, 5),
    /**
     * <li>SMIME Cryptographic error: '%1$s'
     * <li>{@value SmimeExceptionCodeMessages#CRYPTO_GRAPH_ERROR}
     */
    CRYPTO_ERROR("SMIME Cryptographic error: '%1$s'", SmimeExceptionCodeMessages.CRYPTO_GRAPH_ERROR, Category.CATEGORY_ERROR, 6),
    /**
     * <li>Key not found for signing
     * <li>{@value SmimeExceptionCodeMessages#KEY_NOT_FOUND_FOR_SIGNING}
     */
    KEY_NOT_FOUND_FOR_SIGNING("Key not found for signing", SmimeExceptionCodeMessages.KEY_NOT_FOUND_FOR_SIGNING, Category.CATEGORY_ERROR, 7),
    /**
     * <li>Password recovery is disabled
     * <li>{@value SmimeExceptionCodeMessages#RECOVERY_DISABLED}
     */
    RECOVERY_DISABLED("Password recovery is disabled", SmimeExceptionCodeMessages.RECOVERY_DISABLED, Category.CATEGORY_ERROR, 8),
    /**
     * <li>Unable to decrypt using the exiting keys. If multiple keys, possible wrong password for the key needed. Possible missing key.
     * <li>{@value SmimeExceptionCodeMessages#UNABLE_TO_DECRYPT}
     */
    UNABLE_TO_DECRYPT("Unable to decrypt using the exiting keys. If multiple keys, possible wrong password for the key needed. Possible missing key.", SmimeExceptionCodeMessages.UNABLE_TO_DECRYPT, Category.CATEGORY_ERROR, 9),
    /**
     * <li>Unable to find encryption certificate for sender.
     * <li>{@value SmimeExceptionCodeMessages#UNABLE_TO_FIND_SENDER}
     */
    UNABLE_TO_FIND_SENDER("Unable to find encryption certificate for sender.", SmimeExceptionCodeMessages.UNABLE_TO_FIND_SENDER, Category.CATEGORY_ERROR, 9),
    /**
     * <li>The certificate was revoked.
     * <li>{@value SmimeExceptionCodeMessages#REVOKED}
     */
    CERTIFICATE_REVOKED("The certificate was revoked.", SmimeExceptionCodeMessages.REVOKED, Category.CATEGORY_ERROR, 10),

    /**
     * <li>Another account is using this private key. Only one account can own a private key
     *
     */
    PRIVATE_KEY_ALREADY_EXISTS("This private key already exists in another account and cannot be imported", SmimeExceptionCodeMessages.PRIVATE_KEY_ALREADY_EXISTS, Category.CATEGORY_ERROR, 11),

    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    /**
     * Default constructor.
     *
     * @param message message.
     * @param category category.
     * @param number detail number.
     */
    private SmimeExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private SmimeExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "SMIME";
    }

    /**
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    /**
     * @return the category
     */
    @Override
    public Category getCategory() {
        return category;
    }

    /**
     * @return the number
     */
    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
