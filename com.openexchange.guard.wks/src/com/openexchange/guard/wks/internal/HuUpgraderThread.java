/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.internal;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.wks.osgi.Services;

/**
 * {@link HuUpgraderThread}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class HuUpgraderThread implements Runnable {

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            EmailStorage emails = Services.getService(EmailStorage.class);
            boolean running = true;
            while (emails.getMissingHuCount() > 0 && running) {
                List<Email> missingEmails = emails.getMissingHu(1000);
                for (Email email : missingEmails) {
                    String addr = email.getEmail();
                    if (addr.contains("@")) {
                        String hash = CipherUtil.getEmailUserHash(addr);
                        running = emails.updateHu(addr, hash);
                    }
                }
            }
        } catch (OXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
