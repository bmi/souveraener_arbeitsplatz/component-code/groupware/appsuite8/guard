/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.io.IOException;
import javax.mail.Message;
import javax.mail.MessagingException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.file.storage.PGPSessionKeyExtractor;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.session.Session;

/**
 * {@link PGPMailSessionKeyExtractor} uses an OX Guard endpoint to extract "Public-Key Encrypted Session Key Packets" from a given PGP Mail object.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class PGPMailSessionKeyExtractor {

    public String[] getEncryptedSession(MailMessage mailMessage, Session session) throws OXException {
        try {
            final boolean clone = true;
            Message originalMimeMessage = MimeMessageConverter.convertMailMessage(mailMessage, clone);
            return new PGPSessionKeyExtractor().getEncryptedSession(originalMimeMessage.getInputStream(), session);
        } catch (IOException e) {
            throw MailExceptionCode.IO_ERROR.create(e, e.getMessage());
        } catch (MessagingException e) {
            throw MailExceptionCode.MESSAGING_ERROR.create(e, e.getMessage());
        }
    }
}
