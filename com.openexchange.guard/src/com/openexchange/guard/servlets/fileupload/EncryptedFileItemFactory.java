/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.fileupload;

import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;

/**
 * {@link EncryptedFileItemFactory} - Enhances {@link FileItemFactory} in order to provide symmetric encryption
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.3
 */
public class EncryptedFileItemFactory extends AbstractTrackingFileItemFactory implements FileItemFactory {

    private final GuardCipherAlgorithm algorithm;
    private final FileItemFactory delegate;
    private final SecretKey secretKey;

    /**
     * Initializes a new {@link EncryptedFileItemFactory}.
     *
     * @param delegate The {@link FilItemFactory} to enhance with encryption
     * @param algorithm The algorithm to use
     * @throws NoSuchAlgorithmException
     */
    public EncryptedFileItemFactory(FileItemFactory delegate, GuardCipherAlgorithm algorithm) throws NoSuchAlgorithmException {
        this.delegate = delegate;
        this.algorithm = algorithm;
        KeyGenerator keyGen = KeyGenerator.getInstance(algorithm.getAlgorithmName());
        keyGen.init(256);
        this.secretKey = keyGen.generateKey();
    }

    @Override
    public FileItem createItem(String fieldName, String contentType, boolean isFormField, String fileName) {
        FileItem fileItem = new EncryptedFileItem(delegate.createItem(fieldName, contentType, isFormField, fileName), algorithm, secretKey);
        fileItems.add(fileItem);
        return fileItem;
    }
}
