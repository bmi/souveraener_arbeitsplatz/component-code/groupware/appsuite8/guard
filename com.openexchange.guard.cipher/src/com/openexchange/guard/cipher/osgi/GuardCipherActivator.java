/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.internal.GuardCipherFactoryServiceImpl;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.configuration.validation.ConfigurationValidator;
import com.openexchange.guard.configuration.validation.InvalidValueHandler;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.inputvalidation.WhiteListInputValidator;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardCipherActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardCipherActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(GuardCipherActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);
        registerService(GuardCipherFactoryService.class, new GuardCipherFactoryServiceImpl());

        //Validating OX Guard Key configuration

        ConfigurationValidator validator = new ConfigurationValidator(this.getService(GuardConfigurationService.class));
        //Validate that the configured AES key length is a valid AES key length
        validator.assertValue(GuardProperty.aesKeyLength,
            Integer.class,
            new WhiteListInputValidator<>(new Integer[] { 128, 192, 256 }),
            new InvalidValueHandler<Integer>() {

                @Override
                public void handle(Integer invalidValue) {
                    LoggerFactory.getLogger(GuardCipherActivator.class).error(
                        "OX Guard is unable to operate with invalid configured AES key length of {} (From property {}). Valid values are 128, 192, 256.",
                        invalidValue,
                        GuardProperty.aesKeyLength.getFQPropertyName());
                }
            });

        //Validates that the configured RSA key length is at least 1024 bit. In theory shorter RSA keys are possible but RSA can only encrypt data which is
        //about up to the key length and OX Guard requires at least about 1024 bit.
        validator.assertValue(GuardProperty.rsaKeyLength,
            Integer.class,
            new RangeInputValidator<Integer>(1024, Integer.MAX_VALUE),
            new InvalidValueHandler<Integer>() {

                @Override
                public void handle(Integer invalidValue) {
                    LoggerFactory.getLogger(GuardCipherActivator.class).error(
                        "OX Guard is unable to operate with invalid configured RSA key length of {} (From property {}). The RSA key length must be at least 1024.",
                        invalidValue,
                        GuardProperty.rsaKeyLength.getFQPropertyName());
                }
            });

        logger.info("Guard Cipher Algorithms registered.");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardCipherActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(null);

        unregisterService(GuardCipherFactoryService.class);

        logger.info("Unregistering Guard Cipher Algorithms");

        super.stopBundle();
    }
}
