package com.openexchange.guard.sharing.osgi;

import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.sharing.SharingService;
import com.openexchange.guard.sharing.impl.SharingServiceImpl;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.user.UserService;

public class ShareActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { UserService.class, GuardKeyService.class, AccountCreationService.class, OXUserService.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        Services.setServiceLookup(this);
        registerService(SharingService.class, new SharingServiceImpl());
        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        unregisterService(SharingService.class);
        super.stopBundle();
    }

}

