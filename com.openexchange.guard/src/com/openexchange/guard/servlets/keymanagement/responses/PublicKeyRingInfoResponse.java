/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link PublicKeyRingInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class PublicKeyRingInfoResponse {

    private final PGPPublicKeyRing keyRing;
    private final PGPSecretKeyRing pgpSecretKeyRing;

    /**
     * Initializes a new {@link PublicKeyRingInfoResponse}.
     *
     * @param pgpring
     */
    public PublicKeyRingInfoResponse(PGPPublicKeyRing keyRing) {
        this(keyRing, null);
    }

    /**
     * Initializes a new {@link PublicKeyRingInfoResponse}.
     *
     * @param keyRing
     * @param pgpSecretKeyRing
     */
    public PublicKeyRingInfoResponse(PGPPublicKeyRing keyRing, PGPSecretKeyRing pgpSecretKeyRing) {
        this.keyRing = keyRing;
        this.pgpSecretKeyRing = pgpSecretKeyRing;
    }

    public String getRing() throws OXException {
        return KeyExportUtil.export(this.keyRing);
    }

    public String getHash() throws NoSuchAlgorithmException, IOException, OXException {
        return CipherUtil.getHash(CipherUtil.SHA_512, getRing().getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Checks if the given public key has a corresponding, not empty, private key
     *
     * @param publicKey The public key to check for a corresponding private key.
     * @return true, if the public key has a corresponding, not empty, private key, false otherwise.
     */
    private boolean checkHasPrivate(PGPPublicKey publicKey) {
        if (pgpSecretKeyRing != null) {
            PGPSecretKey secretKey = pgpSecretKeyRing.getSecretKey(publicKey.getKeyID());
            return secretKey != null && !secretKey.isPrivateKeyEmpty();
        }
        return false;
    }

    public PublicKeyInfoResponse[] getKeys() {
        ArrayList<PublicKeyInfoResponse> list = new ArrayList<PublicKeyInfoResponse>();
        Iterator<PGPPublicKey> iter = this.keyRing.getPublicKeys();
        PGPPublicKey keyUsedForEncryption = PGPKeysUtil.getEncryptionKey(keyRing);
        while (iter.hasNext()) {
            PGPPublicKey publicKey = iter.next();
            final boolean hasPrivateKey = checkHasPrivate(publicKey);
            final boolean usedForEncryption = keyUsedForEncryption != null &&
                keyUsedForEncryption.getKeyID() == publicKey.getKeyID();
            list.add(new PublicKeyInfoResponse(publicKey,hasPrivateKey, usedForEncryption));
        }
        return list.toArray(new PublicKeyInfoResponse[list.size()]);
    }
}
