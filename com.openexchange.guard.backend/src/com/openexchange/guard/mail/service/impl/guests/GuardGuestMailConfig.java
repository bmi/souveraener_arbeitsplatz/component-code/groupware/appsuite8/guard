/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.net.URI;
import java.net.URISyntaxException;
import javax.mail.internet.idn.IDNA;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.mail.api.IMailProperties;
import com.openexchange.mail.api.MailCapabilities;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mail.api.UrlInfo;
import com.openexchange.tools.net.URIDefaults;
import com.openexchange.tools.net.URIParser;

/**
 * {@link GuardGuestMailConfig}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailConfig extends MailConfig {

    private int port;
    private String server;
    private boolean secure;
    private IMailProperties mailProperties;

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#getCapabilities()
     */
    @Override
    public MailCapabilities getCapabilities() {
       return MailCapabilities.EMPTY_CAPS;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#getPort()
     */
    @Override
    public int getPort() {
        return this.port;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#getServer()
     */
    @Override
    public String getServer() {
        return server;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#isSecure()
     */
    @Override
    public boolean isSecure() {
        return this.secure;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#setPort(int)
     */
    @Override
    public void setPort(int port) {
        this.port = port;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#setSecure(boolean)
     */
    @Override
    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#setServer(java.lang.String)
     */
    @Override
    public void setServer(String server) {
        this.server = server == null ? null : IDNA.toUnicode(server);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#getMailProperties()
     */
    @Override
    public IMailProperties getMailProperties() {
        return this.mailProperties;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#setMailProperties(com.openexchange.mail.api.IMailProperties)
     */
    @Override
    public void setMailProperties(IMailProperties mailProperties) {
        this.mailProperties = mailProperties;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailConfig#parseServerURL(com.openexchange.mail.api.UrlInfo)
     */
    @Override
    protected void parseServerURL(UrlInfo urlInfo) throws OXException {
        final URI uri;
        try {
            uri = URIParser.parse(urlInfo.getServerURL(), URIDefaults.NULL);
            if (uri == null) {
                throw GuardExceptionCodes.URI_PARSE_FAILED.create(urlInfo.getServerURL());
            }
        } catch (URISyntaxException e) {
            throw GuardExceptionCodes.URI_PARSE_FAILED.create(e, urlInfo.getServerURL());
        }
        this.server = uri.getHost();
        this.port = uri.getPort();
    }

}
