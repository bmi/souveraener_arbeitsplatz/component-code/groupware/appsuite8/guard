/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.guard.oxapi.pooling.internal.HttpConnectionPoolServiceImpl;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * 
 * {@link OXAPIActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class OXAPIActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class };
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(OXAPIActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        // Register the connection pool service
        HttpConnectionPoolService httpConnectionPoolService = new HttpConnectionPoolServiceImpl();
        registerService(HttpConnectionPoolService.class, httpConnectionPoolService);
        trackService(HttpConnectionPoolService.class);

        logger.info("HttpConnectionPoolService registered.");

        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(OXAPIActivator.class);
        logger.info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(HttpConnectionPoolService.class);
        logger.info("HttpConnectionPoolService unregistered.");
        Services.setServiceLookup(null);

        super.stopBundle();
    }
}
