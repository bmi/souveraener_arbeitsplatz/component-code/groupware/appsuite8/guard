/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.passwordManagementServices.responses;

import static com.openexchange.java.Autoboxing.L;
import java.util.Date;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * {@link KeyInformationResponse}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class KeyInformationResponse {

    private SmimeKeys keys;

    /**
     * 
     * Initializes a new {@link KeyInformationResponse}.
     *
     * @param keys
     */
    public KeyInformationResponse(SmimeKeys keys) {
        this.keys = keys;
    }

    public String getId() {
        return keys.getEmail();
    }

    public Long getSerial() {
        return L(null == keys.getSerial() ? 0l : keys.getSerial().longValue());
    }

    public Date getValidUntil() {
        return null == keys.getCertificate() ? null : keys.getCertificate().getNotAfter();
    }
}
