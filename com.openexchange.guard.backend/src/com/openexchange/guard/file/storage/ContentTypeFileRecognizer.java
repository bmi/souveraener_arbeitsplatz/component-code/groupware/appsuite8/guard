/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.ArrayList;
import java.util.Collection;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;

/**
 * {@link ContentTypeFileRecognizer} recognizes a file as encrypted based on it's content-type.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class ContentTypeFileRecognizer implements EncryptedFileRecognizer {

    private static final String PGP_CONTENT_TYPE = "application/pgp-encrypted";
    private static final String ORIGINAL_MIME_TYPE = "OrigMime";

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#isEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public boolean isEncrypted(File file) {
        if(file != null) {
            return PGP_CONTENT_TYPE.equals(file.getFileMIMEType());
        }
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markEncrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        if(!isEncrypted(file)) {

            //Saving original content-type into meta-data
            if (!file.getMeta().containsKey(ORIGINAL_MIME_TYPE)) {
                file.getMeta().put(ORIGINAL_MIME_TYPE, file.getFileMIMEType());
                ret.add(Field.META);
            }

            //Setting content-type
            file.setFileMIMEType(PGP_CONTENT_TYPE);
            ret.add(Field.FILE_MIMETYPE);
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markEncrypted(Document document) {
        return markEncrypted(document.getFile());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markDecrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        if (file.getMeta().containsKey(ORIGINAL_MIME_TYPE)) {
            //Get the file's original content-type back from the meta-data
            String originalMimeType = (String)file.getMeta().get(ORIGINAL_MIME_TYPE);
            file.getMeta().remove(ORIGINAL_MIME_TYPE);
            ret.add(Field.META);
            //..and restore it
            file.setFileMIMEType(originalMimeType);
            ret.add(Field.FILE_MIMETYPE);
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markDecrypted(Document document) {
        return markDecrypted(document.getFile());
    }

}
