/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardMaintenanceExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public final class GuardMaintenanceExceptionMessages implements LocalizableStrings {

    /** Failed resetting the password for '%1$s' */
    public static final String FAILED_RESETTING_PASSWORD = "Failed resetting the password for '%1$s'";

    /** Failed upgrading to OX account for e-mail '%1$s' */
    public static final String FAILED_UPGRADING_TO_OX_ACCOUNT = "Failed upgrading to OX account for e-mail '%1$s'";

    /** Unable to resolve the e-mail address '%1$s' to a local user. */
    public static final String UNABLE_TO_RESOLVE_MAIL_ADDRESS = "Unable to resolve the e-mail address '%1$s' to a local user.";

    private GuardMaintenanceExceptionMessages() {}
}
