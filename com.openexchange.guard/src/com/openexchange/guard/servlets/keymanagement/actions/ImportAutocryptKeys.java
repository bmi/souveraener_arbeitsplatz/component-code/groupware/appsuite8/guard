/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.google.gson.JsonObject;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.KeyImportService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.ApiResponse;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link ImportAutocryptKeys}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class ImportAutocryptKeys extends GuardServletAction {

    private final String ID_PARAM = "id";
    private final String FOLDER_PARAM = "folder";
    private final String ATTACHMENT_PARAMT = "attachment";
    private final String PASSCODE = "startkey";
    private final String OLD_PASSWORD = "password";
    private final String NEW_PASSWORD = "newpassword";

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        JsonObject json = Services.getService(ResponseHandler.class).getJsonAndDecodeEncryptedPasswords(request, userSession.getUserId(), userSession.getContextId());
        if (!json.has(PASSCODE) || !json.has(OLD_PASSWORD) || !json.has(NEW_PASSWORD)) {
            throw new IllegalArgumentException("Missing password");
        }
        String passcode = json.get(PASSCODE).getAsString();
        String oldpass = json.get(OLD_PASSWORD).getAsString();
        String newpass = json.get(NEW_PASSWORD).getAsString();
        Api api = new Api(new OxCookie(request.getCookies()), request);
        final String email = new EmailValidator().assertInput(api.getPrimary(), "email");
        String id = ServletUtils.getStringParameter(request, ID_PARAM, true);
        String folder = ServletUtils.getStringParameter(request, FOLDER_PARAM, true);
        String att = ServletUtils.getStringParameter(request, ATTACHMENT_PARAMT, true);
        ApiResponse resp = api.getPlainAttachment(id, att, folder);
        try {
            String content = new String(resp.readContent(), StandardCharsets.UTF_8);
            PasswordChangeService passwordService = Services.getService(PasswordChangeService.class);
            passwordService.checkPasswordRequirements(newpass, userSession.getUserId(), userSession.getContextId());
            AutoCryptService autoCryptService = Services.getService(AutoCryptService.class);
            PGPSecretKeyRing secKeyRing = autoCryptService.getKeyFromAttachment(content, passcode);
            if (secKeyRing != null) {
                KeyImportService keyImportService = Services.getService(KeyImportService.class);
                Collection<GuardKeys> importedKeyRings = keyImportService.importPrivateKeyRing(userSession.getGuardUserId(),
                    userSession.getGuardContextId(),
                    email,
                    userSession.getLocale(),
                    oldpass,
                    newpass,
                    secKeyRing);
                if (importedKeyRings != null && importedKeyRings.size() > 0) {
                    ServletUtils.sendObject(response, "Success");
                    return;
                }
            }
        } finally {
            resp.close();
        }
        ServletUtils.sendError(response, "Unable to import key");

    }

}
