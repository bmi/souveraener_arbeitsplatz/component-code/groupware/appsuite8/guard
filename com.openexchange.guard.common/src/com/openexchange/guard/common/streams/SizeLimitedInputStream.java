/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.streams;

import java.io.IOException;
import java.io.InputStream;

/**
 * Size limiting stream. If more bytes are read than configured maxSize, and IOException is
 * thrown
 * {@link SizeLimitedInputStream}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.5
 */
public class SizeLimitedInputStream extends InputStream {

    private InputStream in;
    private long maxSize;
    private long count;

    /**
     * Initializes a new {@link SizeLimitedInputStream}.
     * 
     * @param in InputSTream to use
     * @param maxSize Max size for input stream before throwing error
     */
    public SizeLimitedInputStream(InputStream in, long maxSize) {
        this.in = in;
        this.maxSize = maxSize;
        count = 0;
    }

    /**
     * Check read count hasn't exceeded max limit
     * checkLimit
     *
     * @throws IOException
     */
    private void checkLimit() throws IOException {
        if (maxSize == 0)
            return;  // 0 is unlimited
        if (count > maxSize) {
            throw new IOException("Exceeds max size");
        }
    }

    @Override
    public int read() throws IOException {
        int result = in.read();
        if (result != -1) {
            count++;
            checkLimit();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.InputStream#available()
     */
    @Override
    public int available() throws IOException {
        return in.available();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.InputStream#close()
     */
    @Override
    public void close() throws IOException {
        in.close();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.InputStream#mark(int)
     */
    @Override
    public synchronized void mark(int readlimit) {
        in.mark(readlimit);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.InputStream#reset()
     */
    @Override
    public synchronized void reset() throws IOException {
        in.reset();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.InputStream#markSupported()
     */
    @Override
    public boolean markSupported() {
        return in.markSupported();
    }

}
