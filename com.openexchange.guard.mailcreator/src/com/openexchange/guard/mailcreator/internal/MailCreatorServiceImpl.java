/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mailcreator.internal;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProductName;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.translation.ConditionalVissibleTemplateTransformation;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.translation.TemplateTransformation;
import com.openexchange.user.UserService;

/**
 * {@link MailCreatorServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class MailCreatorServiceImpl implements MailCreatorService {

    private static Logger LOG = LoggerFactory.getLogger(MailCreatorServiceImpl.class);

    public static final String OUTSITE_TEMPLATE = "";

    public static final String PASSWORD_EMAIL_TEMPLATE = "";

    @Override
    public JsonObject createBlankMail() {
        String blank = " {\"data\":{\"from\":\"\",\"to\":\"\",\"cc\":\"\",\"bcc\":\"\",\"reply_to\":\"\",\"subject\":\"\",\"priority\":\"3\",\"vcard\":\"0\",\"attachments\":[{\"content\":\"\",\"content_type\":\"ALTERNATIVE\"}],\"nested_msgs\":[],\"sendtype\":\"0\"},\"mode\":\"compose\",\"format\":\"html\",\"signature\":\"\",\"files\":[]} ";
        Gson g = new Gson();
        JsonObject json = g.fromJson(blank, JsonObject.class);
        return (json);
    }

    @Override
    public JsonObject createBlankOutMail(String lang, int templId) throws OXException {
        JsonObject email = createBlankMail();
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        String content = translationService.getTranslation("oxtempl.html", lang, templId);
        if (content == null) {
            return (null);
        }
        return (setContent(email, content));
    }

    @Override
    public JsonObject createBlankGuestMail(String lang, int templId, String host, int userid, int cid, String guestMessage) throws OXException {
        return createBlankMail (lang, templId, host, userid, cid, "guesttempl.html", guestMessage);
    }

    @Override
    public JsonObject createBlankMail (String lang, int templId, String host, int userid, int cid, String template, String guestmessage) throws OXException {
        JsonObject email = createBlankMail();
        ArrayList<TemplateTransformation> transformations = new ArrayList<TemplateTransformation>();
        //"guestmessage" is a conditional part of the template file. We only show it, if the user entered a guest message
        transformations.add(new ConditionalVissibleTemplateTransformation("guestmessage", guestmessage == null || guestmessage.isEmpty()));
        transformations.add(new ConditionalVissibleTemplateTransformation("!guestmessage", guestmessage != null && !guestmessage.isEmpty()));
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        String content = translationService.getTranslation(template, lang, templId, transformations);
        if (content == null) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Missing email template");
        }
        content = setProductName (content, host, userid, cid);
        if (content == null) {
            return (null);
        }
        content = addDateVars(content, userid, cid);
        return (setContent(email, content));
    }

    @Override
    public JsonObject createBlankGuestMail(String lang, int templId, String messageID, String host, int userid, int cid, String guestMessage) throws OXException {
        JsonObject mail = createBlankGuestMail(lang, templId, host, userid, cid, guestMessage);
        JsonObject header = new JsonObject();
        header.addProperty("Message-ID", messageID);
        mail.get("data").getAsJsonObject().add("headers", header);
        return (mail);
    }

    @Override
    public JsonObject addPlainText(JsonObject email, String data) {
        Gson gson = new Gson();
        String emailText = gson.toJson(email);
        if (data == null) {
            data = "";
        }
        emailText = emailText.replace("$plaintext", data.replace("\n", "<br>"));
        Gson g = new Gson();
        return (g.fromJson(emailText, JsonObject.class));
    }

    @Override
    public JsonObject addURL(JsonObject email, String data) {
        Gson gson = new Gson();
        String emailText = gson.toJson(email);
        emailText = emailText.replace("$url", data);
        Gson g = new Gson();
        return (g.fromJson(emailText, JsonObject.class));
    }

    @Override
    public JsonObject addTo(JsonObject mail, String name, String email, RecipientType type) {
        if (type == RecipientType.TO) {
            return (addTo(mail, name, email));
        }
        if (type == RecipientType.CC) {
            return (addCC(mail, name, email));
        }
        if (type == RecipientType.BCC) {
            return (addBCC(mail, name, email));
        }
        return (mail);
    }

    // Add flattened to address to mail
    @Override
    public JsonObject addTo(JsonObject mail, String name, String email) {
        String to = mail.getAsJsonObject("data").get("to").getAsString();
        if (to.length() > 0) {
            to = to + ", ";
        }
        if (name.length() > 0) {
                to = to + flatten(name, email);
        } else {
            to = to + email;
        }
        mail.getAsJsonObject("data").add("to", new JsonPrimitive(to));
        return (mail);
    }

    // Add flattened to address to mail
    @Override
    public JsonObject addCC(JsonObject mail, String name, String email) {
        String to = mail.getAsJsonObject("data").get("cc").getAsString();
        if (to.length() > 0) {
            to = to + ", ";
        }
        if (name.length() > 0) {
            to = to + flatten(name, email);
        } else {
            to = to + email;
        }
        mail.getAsJsonObject("data").add("cc", new JsonPrimitive(to));
        return (mail);
    }

    // Add flattened to address to mail
    @Override
   public JsonObject addBCC(JsonObject mail, String name, String email) {
        String to = mail.getAsJsonObject("data").get("bcc").getAsString();
        if (to.length() > 0) {
            to = to + ", ";
        }
        if (name.length() > 0) {
            to = to + flatten(name, email);
        } else {
            to = to + email;
        }
        mail.getAsJsonObject("data").add("bcc", new JsonPrimitive(to));
        return (mail);
    }

    // Add flattened from address to mail
    @Override
    public JsonObject addFrom(JsonObject mail, String name, String email) {
        String from = email;
        if (name != null && name.length() > 0) {
            from = flatten(name, email);
        }
        mail.getAsJsonObject("data").add("from", new JsonPrimitive(from));
        return (mail);
    }

    private String flatten (String name, String email) {
        if (name.contains(",")) {  // If contains comma, enclose in quotes
            name = "\"" + name + "\"";
        }
        return (name + "<" + email + ">");
    }

    @Override
    public JsonObject addSubject(JsonObject mail, String subject) {
        mail.getAsJsonObject("data").add("subject", new JsonPrimitive(subject));
        return (mail);
    }

    @Override
    public JsonObject setContent(JsonObject mail, String content) {
        mail.getAsJsonObject("data").getAsJsonArray("attachments").get(0).getAsJsonObject().add("content", new JsonPrimitive(content));
        return (mail);
    }

    @Override
    public JsonObject noSave(JsonObject email) {
        email.getAsJsonObject("data").add("copy2Sent", new JsonPrimitive("false"));
        return (email);
    }

    @Override
    public JsonObject getOxPasswordEmail(String to, String sendername, String from, String from2, String password, String lang, int templId, String host, int userid, int cid) throws OXException {
        return getPasswordEmail(to, sendername, from, from2, password, lang, templId, "", "oxpasswordtempl.html", host, userid, cid);
    }

    @Override
    public JsonObject getPasswordEmail(String to, String sendername, String from, String from2, String password, String lang, int templId, String guestmessage, String host, int userid, int cid) throws OXException {
        return getPasswordEmail(to, sendername, from, from2, password, lang, templId, guestmessage, "passwordtempl.html", host, userid, cid);
    }

    /**
     *
     * @param to email of recipient
     * @param sendername Full name of the sender
     * @param from  Full name of the sender
     * @param from2 Email of the sender
     * @param password password to send
     * @param lang
     * @param templId
     * @param guestmessage
     * @param templateName
     * @param host the senders hostname
     * @param userid the recipient's user id
     * @param cid the recipient's context id
     * @return
     * @throws OXException
     */
    private JsonObject getPasswordEmail(String to, String sendername, String from, String from2, String password, String lang, int templId, String guestmessage, String templateName, String host, int userid, int cid) throws OXException {
        if(guestmessage == null) {
            guestmessage = "";
        }
        JsonObject email = createBlankMail();
        email = addTo(email, to, to);
        email = addFrom(email, from, from2);
        email.getAsJsonObject("data").add("copy2Sent", new JsonPrimitive("false"));// Do not copy this to sent folder

        ArrayList<TemplateTransformation> transformations = new ArrayList<TemplateTransformation>();
        //"guestmessage" is a conditional part of the template file. We only show it, if the user entered a guest message
        transformations.add(new ConditionalVissibleTemplateTransformation("guestmessage", guestmessage == null || guestmessage.isEmpty()));
        transformations.add(new ConditionalVissibleTemplateTransformation("!guestmessage", guestmessage != null && !guestmessage.isEmpty()));

        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        String templ = translationService.getTranslation(templateName, lang, templId, transformations);
        if (templ == null) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Password email template not found");
        }
        templ = templ.replace("$from", sendername != null ? sendername : from2);
        templ = templ.replace("$password", password);
        templ = templ.replace("$plaintext", guestmessage);
        templ = setProductName (templ, host, userid, cid);
        if (templ.contains("Subject:")) {
            String subject = templ.substring(templ.indexOf("Subject:"), templ.indexOf("\n")).replace("Subject:", "");
            email = addSubject(email, subject);
        } else {
            LOG.error("Missing subject in password template! Make sure starts with 'Subject:'");
        }
        if (templ.contains("Body:")) {
            templ = templ.substring(templ.indexOf("Body:") + 5);
        } else {
            LOG.error("Missing body in password template! Make sure starts with 'Body:'");
        }
        templ = addDateVars(templ, userid, cid);
        email = setContent(email, templ);
        return (email);
    }

    @Override
    public JsonObject getResetEmail(String to, List<String> from, String password, String lang, int templId, String host, int userid, int cid) throws OXException {
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        String templ = translationService.getTranslation("resettempl.html", lang, templId).replace("$from", from.get(0)).replace("$password", password);
        return (templToEmail (templ, to, from, host, userid, cid));
    }

    @Override
    public JsonObject getGuestResetEmail(String to, String resetId, String lang, int templId, String host, int userid, int cid) throws OXException {
        List<String> from = getFromAddress(to, to, userid, cid);
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        String templ = translationService.getTranslation("guestresettempl.html", lang, templId).replace("$resetid", resetId).replace("$url", resetId);
        return (templToEmail (templ, to, from, host, userid, cid));
    }

    @Override
    public JsonObject templToEmail (String templ, String to, List<String> from, String host, int userid, int cid) throws OXException {
        JsonObject email = createBlankMail();
        email = addTo(email, to, to);
        email = addFrom(email, from.get(0), from.get(1));
        email.getAsJsonObject("data").add("copy2Sent", new JsonPrimitive("false"));// Do not copy this to sent folder
        templ = setProductName (templ, host, userid, cid);
        if (templ.contains("Subject:")) {
            String subject = templ.substring(templ.indexOf("Subject:"), templ.indexOf("\n", templ.indexOf("Subject:"))).replace("Subject:", "");
            email = addSubject(email, subject);
        } else {
            LOG.error("Missing subject in password template! Make sure starts with 'Subject:'");
        }
        if (templ.contains("Body:")) {
            templ = templ.substring(templ.indexOf("Body:") + 5);
        } else {
            LOG.error("Missing body in password template! Make sure starts with 'Body:'");
        }
        templ = addDateVars(templ, userid, cid);
        email = setContent(email, templ);
        return (email);
    }

    /**
     * Populate the date/time variables
     * addDateVars
     *
     * @param templ The template
     * @param userId UserId for getting locale
     * @param cid ContextId of the user
     * @return
     * @throws OXException
     */
    private static String addDateVars(String templ, int userId, int cid) throws OXException {
        UserService userService = Services.getService(UserService.class);
        Locale local = userService == null ? Locale.getDefault() : userService.getUser(userId, cid).getLocale();
        Calendar date = Calendar.getInstance(local);
        templ = templ.replaceAll("\\$year", Integer.toString(date.get(Calendar.YEAR)));
        templ = templ.replaceAll("\\$time", DateFormat.getTimeInstance(DateFormat.MEDIUM, local).format(date.getTime()));
        templ = templ.replaceAll("\\$datetime", DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, local).format(date.getTime()));
        return templ;
    }

    /**
     * Gets sender address from configuration cascade
     *
     * @param name
     * @param email
     * @param userid
     * @param cid
     * @return Arraylist, index 0 is name, index 1 is email address
     */
    @Override
    public ArrayList<String> getFromAddress(String name, String email, int userid, int cid) throws OXException {
        ArrayList<String> answer = new ArrayList<String>();
        // Check if Backend has a from address for this person in config cascade
        String fromemail = null;
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
        if (userid != 0) {
            fromemail = guardConfigService.getProperty(GuardProperty.fromEmail, userid, cid);
        }
        // If no config cascade
        if (fromemail == null || fromemail.isEmpty()) {
            // Check if we have global configuration
            String fromAddress = guardConfigService.getProperty(GuardProperty.passwordFromAddress, userid, cid);
            String fromName = guardConfigService.getProperty(GuardProperty.passwordFromName, userid, cid);
            if (Strings.isNotEmpty(fromAddress)) {
                if (Strings.isNotEmpty(fromName)) {
                    answer.add(fromName);
                } else {
                    answer.add(fromAddress);
                }
                answer.add(fromAddress);
                return (answer);
            }
            // If no global, just use the sender info
            answer.add(name);
            answer.add(email);
            if (name == null || email == null) {
                LOG.error("Problem resolving from email " + name + " " + email);
            }
            return (answer);
        }
        try {
            // If config cascade exists, validate and use
            InternetAddress addr = new InternetAddress(fromemail);
            addr.validate();
            answer.add(addr.getPersonal() == null ? addr.getAddress() : addr.getPersonal());
            answer.add(addr.getAddress());
            return (answer);
        } catch (AddressException e) {
            // Validation failed, use sender info
            answer.add(name);
            answer.add(email);
            return (answer);
        }

    }

    /**
     * Replace variable productName with configured Guard product name
     * @param content
     * @param host
     * @param id
     * @param cid
     * @return
     * @throws OXException
     */
    public String setProductName (String content, String host, int id, int cid) throws OXException {
        String name = GuardProductName.getProductName(host, cid, id);
        content = content.replace("$productName", name);
        return (content);
    }
}
