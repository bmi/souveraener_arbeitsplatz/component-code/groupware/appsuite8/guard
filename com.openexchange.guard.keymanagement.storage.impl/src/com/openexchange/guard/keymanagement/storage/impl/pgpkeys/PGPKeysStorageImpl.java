/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.pgpkeys;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.ListUtil;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.PGPKeys;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;

public class PGPKeysStorageImpl implements PGPKeysStorage {

    private final static String VALUES_LIST = "{@VALUES}";

    @Override
    public List<PGPKeys> getInternalByEmail(String email) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.SELECT_BY_EMAIL_STMT);
            stmt.setString(1, email);

            resultSet = stmt.executeQuery();
            ArrayList<PGPKeys> ret = new ArrayList<PGPKeys>();
            while (resultSet.next()) {
                ret.add(new PGPKeys(resultSet.getLong("id"), resultSet.getString("email"), resultSet.getString("hexid"), resultSet.getInt("cid"), resultSet.getBoolean("local"), resultSet.getLong("keyid")));
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public PGPKeys getByHexId(String hexId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.SELECT_BY_HEXID_STMT);
            stmt.setString(1, hexId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new PGPKeys(resultSet.getLong("id"), resultSet.getString("email"), resultSet.getString("hexid"), resultSet.getInt("cid"), resultSet.getBoolean("local"), resultSet.getLong("keyid"));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public PGPKeys getById(long id) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.SELECT_BY_ID_STMT);
            stmt.setLong(1, id);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new PGPKeys(resultSet.getLong("id"), resultSet.getString("email"), resultSet.getString("hexid"), resultSet.getInt("cid"), resultSet.getBoolean("local"), resultSet.getLong("keyid"));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public List<PGPKeys> getByIds(List<Long> ids) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            String sql = PGPKeysSql.SELECT_BY_IDS_STMT;
            stmt = connection.prepareStatement(sql.replace(VALUES_LIST, ListUtil.listToDbValuesList(ids.size())));

            int i = 1;
            for (long id : ids) {
                stmt.setLong(i, id);
                i++;
            }
            resultSet = stmt.executeQuery();
            List<PGPKeys> ret = new ArrayList<PGPKeys>();
            while (resultSet.next()) {
                ret.add(new PGPKeys(resultSet.getLong("id"), resultSet.getString("email"), resultSet.getString("hexid"), resultSet.getInt("cid"), resultSet.getBoolean("local"), resultSet.getLong("keyid")));
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public PGPKeys getByKeyIdInContext(int contextId, long keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.SELECT_BY_ID_IN_CONTEXT_STMT);
            stmt.setInt(1, contextId);
            stmt.setLong(2, keyId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new PGPKeys(resultSet.getLong("id"), resultSet.getString("email"), resultSet.getString("hexid"), resultSet.getInt("cid"), resultSet.getBoolean("local"), resultSet.getLong("keyid"));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void updateContextIdByKeyId(long keyId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();
        try {
            updateContextIdByKeyId(connection, keyId, contextId);
        } finally {
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void updateContextIdByKeyId(Connection connection, long keyId, int contextId) throws OXException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.UPDATE_CONTEXT_ID_BY_KEY_ID_STMT);
            stmt.setLong(1, contextId);
            stmt.setLong(2, keyId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    @Override
    public void insertOrUpdate(long id, String email, String hexId, int contextId, long keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.INSERT_OR_UPDATE_STMT);
            stmt.setLong(1, id);
            stmt.setString(2, email);
            stmt.setString(3, hexId);
            stmt.setInt(4, contextId);
            stmt.setInt(5, 1);//always create as "local"
            stmt.setLong(6, keyId);
            stmt.setInt(7, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void deleteByKeyId(long keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.DELETE_BY_KEYID_STMT);
            stmt.setLong(1, keyId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void deleteByEmail(String email) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.DELETE_BY_EMAIL_STMT);
            stmt.setString(1, email);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void deleteByKeyIdInContext(long keyId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.DELETE_BY_KEYID_IN_CONTEXT_STMT);
            stmt.setInt(1, contextId);
            stmt.setLong(2, keyId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.storage.PGPKeysStorage#addPublicKeyIndex(int, long, java.lang.String, org.bouncycastle.openpgp.PGPPublicKeyRing)
     */
    @Override
    public void addPublicKeyIndex(int cid, long keyId, String email, PGPPublicKeyRing publicKeyRing) throws OXException {
        Iterator<PGPPublicKey> it = publicKeyRing.getPublicKeys();
        while (it.hasNext()) {
            PGPPublicKey pubkey = it.next();
            if (keyId == 0) {
                keyId = pubkey.getKeyID();
            }
            insertOrUpdate(pubkey.getKeyID(), email, LongUtil.longToHexStringTruncated(pubkey.getKeyID()), cid, keyId);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.storage.PGPKeysStorage#changeEmailAddress(java.lang.String, java.lang.String, int)
     */
    @Override
    public void changeEmailAddress(String oldEmail, String newEmail, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(PGPKeysSql.UPDATE_EMAIL_ADDRESS);
            stmt.setString(1, newEmail);
            stmt.setString(2, oldEmail);
            stmt.setInt(3, contextId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }

    }
}
