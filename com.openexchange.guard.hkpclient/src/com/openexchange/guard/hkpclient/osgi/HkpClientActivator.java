/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkpclient.osgi;

import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.dns.DNSService;
import com.openexchange.guard.hkpclient.services.CachingHKPClientService;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.hkpclient.services.HKPKeySources;
import com.openexchange.guard.hkpclient.services.RemoteHKPClientService;
import com.openexchange.guard.hkpclient.services.SRVHKPClientService;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link HkpClientActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class HkpClientActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { HttpConnectionPoolService.class, GuardConfigurationService.class, DNSService.class,
                                RemoteKeyCacheStorage.class, KeySourceFactory.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(HkpClientActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        HKPKeySources.initialize(getService(KeySourceFactory.class));

        //Register HKP client services
        //Setting up HKP client query logic in a decorator style
        GuardConfigurationService guardConfigService = getService(GuardConfigurationService.class);
        boolean forceTLSForHKPSRV = guardConfigService.getBooleanProperty(GuardProperty.forceTLSForHKPSRV);
        RemoteHKPClientService remoteHKPClientService = new RemoteHKPClientService(this);
        registerService(HKPClientService.class, new CachingHKPClientService(new SRVHKPClientService(remoteHKPClientService, forceTLSForHKPSRV, this)));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(HkpClientActivator.class).info("Stopping bundle: {}", context.getBundle().getSymbolicName());
        unregisterService(HKPClientService.class);
        Services.setServiceLookup(null);
        super.stopBundle();
    }

}
