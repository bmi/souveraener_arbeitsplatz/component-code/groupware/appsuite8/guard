/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.util.Objects;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;

/**
 * {@link CachedKey} represents a cached remote PGP key.
 * e*
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class CachedKey {

    private final PGPPublicKeyRing publicKeyRing;
    private final KeySource        keySource;

    /**
     * Initializes a new {@link CachedKey}.
     *
     * @param publicKeyRing The public key ring to cache
     * @param keySource The original source of the key ring
     */
    public CachedKey(PGPPublicKeyRing publicKeyRing, KeySource keySource) {
        this.publicKeyRing = Objects.requireNonNull(publicKeyRing, "publicKeyRing must not be null");
        this.keySource = Objects.requireNonNull(keySource, "keySource must not be null");
    }

    /**
     * Gets the publicKeyRing
     *
     * @return The publicKeyRing
     */
    public PGPPublicKeyRing getPublicKeyRing() {
        return publicKeyRing;
    }

    /**
     * Gets the keySource
     *
     * @return The keySource
     */
    public KeySource getKeySource() {
        return keySource;
    }
}
