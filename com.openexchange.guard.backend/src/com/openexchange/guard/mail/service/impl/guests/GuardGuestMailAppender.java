/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.compose.ContentAwareComposedMailMessage;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailAppender} appends messages to an OX Guard guest mail folder
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailAppender extends AbstractGuardAccess {

    /**
     * Internal method to convert {@link MailMessage} objects to {@link Message} objects
     *
     * @param messages The messages to convert
     * @return The converted messages
     * @throws OXException
     */
    private List<Message> toMimeMessage(MailMessage... messages) throws OXException {
        ArrayList<Message> ret = new ArrayList<Message>(messages.length);
        for (MailMessage message : messages) {
            if(message instanceof ContentAwareComposedMailMessage){
                Object content = message.getContent();
                if(content instanceof MimeMessage) {
                   ret.add((MimeMessage)content);
                }
            }
            else {
                ret.add(MimeMessageConverter.convertMailMessage(message, true));
            }
        }
        return ret;
    }

    private Object createAppend(String folderId) {
        return new JSONObject().putSafe("folderId", folderId);
    }

    /**
     * Internal method to read a JSONObject from the given InputStream
     *
     * @param response The inputStream
     * @return The JSONObject read from the steam
     * @throws OXException
     */
    private JSONObject toJSON(InputStream response) throws OXException {
        try {
            return new JSONObject(new InputStreamReader(response, StandardCharsets.UTF_8));
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    private String[] parseResponse(InputStream response) throws OXException {
        MailMessage[] messages = new MailMessageFactory().createFrom(toJSON(response));
        return Arrays.stream(messages).map( message -> message.getMailId()).toArray(String[]::new);
    }

    /**
     * Appends a set of messages to a specific guard guest folder
     *
     * @param session The session
     * @param authToken The auth token
     * @param folderId The ID of the folder to add the messages to
     * @param messages The messages to add to the folder
     * @return The IDs of the messages added to the folder
     * @throws OXException
     */
    public String[] appendMessage(Session session, GuardAuthenticationToken authToken, String folderId, MailMessage[] messages) throws OXException {
        List<Message> mimeMessage = toMimeMessage(messages);

        GuardApiImpl guardApi = requireGuardApi(GUEST_ENDPOINT) ;

        final JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
        final JSONObject json = new JSONObject()
            .putSafe("auth", authJson)
            .putSafe("append", createAppend(folderId));
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        try {
            LinkedHashMap<String, InputStream> multipartParameters = new LinkedHashMap<>();
            multipartParameters.put("json", jsonStream);
            int i = 0;
            for (Message msg : mimeMessage) {
                multipartParameters.put("msg_" + i++, MimeMessageUtility.getStreamFromPart(msg));
            }
            try(InputStream result = guardApi.processResources(
                GuardApis.mapFor("action", "appendmessage", "respondWithJSON", "true" /* forcing guard to render errors in JSON */),
                multipartParameters, null, session)){
                return parseResponse(result);
            }
        }
        catch(IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }
}
