/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.session.Session;

/**
 * {@link SessionKeyExtractionHandler} extracts encrypted PGP session keys from a PGP object
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface SessionKeyExtractionHandler {

    /**
     * Checks whether or not the handler can handle the given JSON request.
     *
     * @param json The request's  JSON data
     * @return True if a handler can handle the given data, false otherwise
     * @throws JSONException
     */
    public boolean canHandle(JSONObject json) throws JSONException;

    /**
     * Extracts an encrypted PGP Session key from an PGP object described in the given JSON data.
     *
     * @param json The JSON data describing the object to extract the encrypted PGP session key packet from.
     * @param session The user's session
     * @return An base64 encoded representation of the requested PGP session key packet.
     * @throws OXException
     * @throws JSONException
     */
    public String[] handle(JSONObject json, Session session) throws OXException, JSONException;
}
