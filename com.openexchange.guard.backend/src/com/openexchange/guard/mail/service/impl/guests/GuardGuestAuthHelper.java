/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.util.Objects;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.AuthenticationTokenUtils;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.osgi.Services;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link GuardGuestAuthHelper} contains some helper methods for creating a guest authentication object for accessing OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
class GuardGuestAuthHelper {

    /**
     * Internal Method to convert a {@link GuardAuthenticationToken} to a JSONObject
     *
     * @param authToken The Auth Token to convert.
     * @return The JSON representation of the given authToken
     */
    static JSONObject toAuthJSON(GuardAuthenticationToken authToken) {
        if (authToken == null) {
            return null;
        }
        return AuthenticationTokenUtils.toJsonObject(authToken);
    }

    /**
     * Internal helper method to convert a User object in a password less authentication object
     *
     * @param user The user to get a password-less authentication object for
     * @return The passwordless auth object
     */
    static JSONObject toAuthJSON(User user) {
        user = Objects.requireNonNull(user, "user must not be null");
        final JSONObject jsonUser = new JSONObject();
        jsonUser.putSafe("email", user.getMail());
        final JSONObject json = new JSONObject();
        json.putSafe("user", jsonUser);
        return json;
    }

    /**
     * Internal helper method get an authentication object from the given objects
     * @param authToken
     * @param session
     * @return An authentication object suitable for OX Guard guest access
     * @throws OXException
     */
     static JSONObject toAuthJSON(GuardAuthenticationToken authToken, Session session) throws OXException {
         JSONObject auth = toAuthJSON(authToken);
         if(auth != null) {
             return auth;
         }
        UserService userService = Services.getService(UserService.class);
        User user = userService.getUser(session.getUserId(), session.getContextId());
        return toAuthJSON(user);
     }
}
