/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONValue;
import org.slf4j.Logger;
import com.google.common.collect.ImmutableSet;
import com.openexchange.ajax.AJAXServlet;
import com.openexchange.ajax.container.ThresholdFileHolder;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.Cookie;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApiExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenUtils;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.osgi.GuardHttpClientConnetionConfiguration;
import com.openexchange.guard.osgi.Services;
import com.openexchange.java.Charsets;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.java.util.UUIDs;
import com.openexchange.log.LogProperties;
import com.openexchange.mail.config.MailProperties;
import com.openexchange.mail.dataobjects.SecuritySettings;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.mail.mime.MimeMailException;
import com.openexchange.mail.mime.converters.FileBackedMimeMessage;
import com.openexchange.mail.mime.utils.MimeMessageUtility;
import com.openexchange.mail.transport.listener.Reply;
import com.openexchange.mail.utils.IpAddressRenderer;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.ManagedHttpClient;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.session.Session;
import com.openexchange.user.UserService;

/**
 * {@link GuardApiImpl}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach></a>
 * @since v7.8.0
 */
public class GuardApiImpl implements GuardApi {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(GuardApiImpl.class);

    public static Logger getLogger() {
        return LOGGER;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    /** The status code policy to obey */
    public static interface StatusCodePolicy {

        /**
         * Examines given status line
         *
         * @param httpResponse The HTTP response
         * @throws OXException If an Open-Xchange error is yielded from status
         * @throws HttpResponseException If status is interpreted as an error
         */
        void handleStatusCode(HttpResponse httpResponse) throws OXException, HttpResponseException;
    }

    /** The default status code policy; accepting greater than/equal to <code>200</code> and lower than <code>300</code> */
    public static final StatusCodePolicy STATUS_CODE_POLICY_DEFAULT = new StatusCodePolicy() {

        @Override
        public void handleStatusCode(HttpResponse httpResponse) throws OXException, HttpResponseException {
            final StatusLine statusLine = httpResponse.getStatusLine();
            final int statusCode = statusLine.getStatusCode();
            if (statusCode < 200 || statusCode >= 300) {
                if (404 == statusCode) {
                    throw GuardApiExceptionCodes.NOT_FOUND_SIMPLE.create();
                }
                String reason = statusLine.getReasonPhrase();
                String code;
                HttpResponseException exception = null;
                try {
                    JSONObject jsonObject = null;
                    String jsonBody = getJsonString(httpResponse);
                    if (jsonBody == null) {
                        jsonBody = "";  // No reason for error
                    }
                    if (jsonBody.startsWith("{")) {
                        jsonObject = new JSONObject(jsonBody);
                        if (jsonObject.has("code") && jsonObject.has("error_desc")) {
                            code = jsonObject.getString("code");
                            reason = code + ": " + jsonObject.getString("error_desc");
                            exception = new ApiResponseException(statusCode, reason, jsonObject);
                        } else {
                            reason = jsonBody;
                        }
                    }
                } catch (final IOException | JSONException e) {
                    getLogger().error("Problem parsing error response from Guard MW", e);
                    reason = statusLine.getReasonPhrase();
                }
                if (exception == null) {
                    exception = new HttpResponseException(statusCode, reason);
                }
                throw exception;
            }
        }
    };

    /**
     * Parse out JSON from HTTP response
     *
     * @param httpResponse
     * @return
     * @throws ParseException
     * @throws IOException
     */
    static String getJsonString(HttpResponse httpResponse) throws ParseException, IOException {
        String jsonBody = EntityUtils.toString(httpResponse.getEntity(), Charsets.UTF_8);
        if (jsonBody != null && jsonBody.contains("{") && jsonBody.contains("}")) {
            jsonBody = jsonBody.substring(jsonBody.indexOf("{"));
            jsonBody = jsonBody.substring(0, jsonBody.lastIndexOf("}") + 1);
        }
        return jsonBody;
    }

    /** The status code policy; accepting greater than/equal to <code>200</code> and lower than <code>300</code> while ignoring <code>404</code> */
    public static final StatusCodePolicy STATUS_CODE_POLICY_IGNORE_NOT_FOUND = new StatusCodePolicy() {

        @Override
        public void handleStatusCode(HttpResponse httpResponse) throws HttpResponseException {
            final StatusLine statusLine = httpResponse.getStatusLine();
            final int statusCode = statusLine.getStatusCode();
            if ((statusCode < 200 || statusCode >= 300) && statusCode != 404) {
                String reason = statusLine.getReasonPhrase();
                String code;
                HttpResponseException exception = null;
                try {
                    JSONObject jsonObject = null;
                    String jsonBody = getJsonString(httpResponse);
                    if (jsonBody == null) {
                        jsonBody = "";  // No reason for error
                    }
                    if (jsonBody.startsWith("{")) {
                        jsonObject = new JSONObject(jsonBody);
                        if (jsonObject.has("code") && jsonObject.has("error_desc")) {
                            code = jsonObject.getString("code");
                            reason = code + ": " + jsonObject.getString("error_desc");
                            exception = new ApiResponseException(statusCode, reason, jsonObject);
                        } else {
                            reason = jsonBody;
                        }
                    }
                } catch (final IOException | JSONException e) {
                    getLogger().error("Problem parsing error response from Guard MW", e);
                    reason = statusLine.getReasonPhrase();
                }
                if (exception == null) {
                    exception = new HttpResponseException(statusCode, reason);
                }
                throw exception;
            }
        }
    };

    // -------------------------------------------------------------------------------------------------------------- //

    private final String authLogin;
    private final String authPassword;
    private final URI uri;
    private final HttpHost targetHost;
    private final BasicHttpContext localcontext;

    private final HttpClientService httpClientService;

    /**
     * Initializes a new {@link GuardApiImpl}.
     *
     * @param endPoint The end-point
     * @param httpClientService the {@link HttpClientService} to use
     * @param configurationService the {@link ConfigurationService} to use
     * @throws OXException If initialization fails
     */
    public GuardApiImpl(String endPoint, HttpClientService httpClientService, ConfigurationService configurationService) throws OXException {
        super();

        this.httpClientService = Objects.requireNonNull(httpClientService, "httpClientService must not be null");
        configurationService = Objects.requireNonNull(configurationService, "configurationService must not be null");

        final String authLogin = configurationService.getProperty("com.openexchange.rest.services.basic-auth.login");
        final String authPassword = configurationService.getProperty("com.openexchange.rest.services.basic-auth.password");
        if (Strings.isEmpty(authLogin) || Strings.isEmpty(authPassword)) {
            LOGGER.error("Denied initialization due to unset Basic-Auth configuration. Please set properties 'com.openexchange.rest.services.basic-auth.login' and 'com.openexchange.rest.services.basic-auth.password' appropriately.");
            throw ServiceExceptionCode.absentService(ConfigurationService.class);
        }
        this.authLogin = authLogin.trim();
        this.authPassword = authPassword.trim();

        final String sUrl = endPoint;
        try {
            uri = new URI(sUrl);
            final HttpHost targetHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
            this.targetHost = targetHost;
        } catch (final URISyntaxException e) {
            throw GuardApiExceptionCodes.INVALID_GUARD_URL.create(null == sUrl ? "<empty>" : sUrl);
        }

        // Generate BASIC scheme object and stick it to the local execution context
        final BasicHttpContext context = new BasicHttpContext();
        final BasicScheme basicAuth = new BasicScheme();
        context.setAttribute("preemptive-auth", basicAuth);
        this.localcontext = context;
    }

    private ManagedHttpClient getHttpClient() throws IllegalStateException, OXException {
        ManagedHttpClient httpClient = this.httpClientService.getHttpClient(GuardHttpClientConnetionConfiguration.HTTP_CLIENT_ID);
        return httpClient;
    }

    private String getJsonString(Address[] recipients, SecuritySettings securitySettings, Session session) throws JSONException {
        JSONObject data = new JSONObject();
        if (securitySettings != null) {
            data.put("security", securitySettings.getJSON());
        }
        if (recipients == null || recipients.length == 0) {
            return (data.toString());
        }
        JSONArray recips = new JSONArray();
        for (int i = 0; i < recipients.length; i++) {
            JSONObject recip = new JSONObject();
            recip.put("Name", ((InternetAddress) recipients[i]).getPersonal());
            recip.put("Email", ((InternetAddress) recipients[i]).getAddress());
            recips.add(i, recip);
        }
        data.put("recipients", recips);
        data.put("senderSessionId", session.getSessionID());
        data.put("senderIp", getClientIPAddress(session));
        return (data.toString());
    }

    /**
     * Pull IP address from session.
     * If localhost, try grizzly remote address from Log
     *
     * @param session
     * @return
     */
    private String getClientIPAddress(final Session session) {
        IpAddressRenderer renderer = MailProperties.getInstance().getIpAddressRenderer();
        /*
         * Get IP from session
         */
        String localIp = session.getLocalIp();
        if (isLocalhost(localIp)) {
            String clientIp = LogProperties.getLogProperty(LogProperties.Name.GRIZZLY_REMOTE_ADDRESS);
            clientIp = clientIp == null ? localIp : clientIp;
            return (null == renderer ? clientIp : renderer.render(clientIp));
        } else {
            return (null == renderer ? localIp : renderer.render(localIp));
        }
    }

    private static final Set<String> LOCAL_ADDRS = ImmutableSet.of("127.0.0.1", "localhost", "::1");

    static boolean isLocalhost(final String localIp) {
        return LOCAL_ADDRS.contains(localIp);
    }

    public static String getJsonString(GuardAuthenticationToken token) {
        JSONObject data = AuthenticationTokenUtils.toJsonObject(token);
        return data.toString();
    }

    /**
     * Processes the specified MIME message.
     *
     * @param mimeMessage The MIME message to process
     * @param parameters The request parameters
     * @return The processed MIME message
     * @throws OXException If processing the MIME message fails
     */
    public GuardResult processMimeMessage(final Message mimeMessage, Address[] recipients, SecuritySettings securitySettings, Map<String, String> parameters, Session session, GuardAuthenticationToken authenticationToken) throws OXException {
        HttpPost request = null;
        InputStream msgSrc = null;
        try {
            request = new HttpPost(buildUri(toQueryString(parameters)));
            setLanguage(request, session);
            final InputStream data = MimeMessageUtility.getStreamFromPart(mimeMessage);

            final String boundary = UUIDs.getUnformattedStringFromRandom();
            final MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charsets.UTF_8) {

                @Override
                public Header getContentType() {
                    return new BasicHeader(HTTP.CONTENT_TYPE, generateContentType(boundary, null));
                }
            };
            try {
                Charset chars = StandardCharsets.UTF_8;
                multipartEntity.addPart("json", new StringBody(getJsonString(recipients, securitySettings, session), chars));

                if (authenticationToken != null) {
                    multipartEntity.addPart("guardAuth", new StringBody(getJsonString(authenticationToken), chars));
                }

            } catch (JSONException e) {
                throw GuardApiExceptionCodes.JSON_ERROR.create(e, e.getMessage());
            }
            multipartEntity.addPart("file", new InputStreamBody(data, "message/rfc882", "mail.eml"));
            request.setEntity(multipartEntity);

            msgSrc = handleHttpResponse(execute(request, getHttpClient(), session), InputStream.class);

            final ThresholdFileHolder sink = new ThresholdFileHolder();
            boolean closeSink = true;
            try {
                sink.write(msgSrc);
                msgSrc = null;

                final File tempFile = sink.getTempFile();
                MimeMessage tmp;
                if (null == tempFile) {
                    tmp = new MimeMessage(MimeDefaultSession.getDefaultSession(), sink.getStream()) {

                        @Override
                        public Date getReceivedDate() throws MessagingException {
                            Date receivedDate = mimeMessage.getReceivedDate();
                            return null == receivedDate ? super.getReceivedDate() : receivedDate;
                        }
                    };
                } else {
                    final FileBackedMimeMessage fbm = new FileBackedMimeMessage(MimeDefaultSession.getDefaultSession(), tempFile, mimeMessage.getReceivedDate());
                    tmp = fbm;
                }
                Address[] newRecips = processRecipients(tmp, recipients);
                tmp.removeHeader("Remove-recip");

                GuardResult result = new GuardResult(tmp, newRecips, Reply.ACCEPT);
                closeSink = false;
                return result;
            } catch (final MessagingException e) {
                throw MimeMailException.handleMessagingException(e);
            } finally {
                if (closeSink) {
                    sink.close();
                }
            }
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Streams.close(msgSrc);
            reset(request);
        }
    }

    /**
     * Find any headers with Remove-recip. If found, remove the recipient specified from the address list
     *
     * @param tmp
     * @param recipients
     * @return
     * @throws OXException
     */
    private Address[] processRecipients(MimeMessage tmp, Address[] recipients) throws OXException {
        try {
            String[] removeList = tmp.getHeader("Remove-recip");
            if (removeList == null) {
                return recipients;
            }
            ArrayList<Address> newRecips = new ArrayList<Address>();
            for (Address recip : recipients) {
                boolean found = false;
                for (int i = 0; i < removeList.length; i++) {
                    if (recip.toString().contains(removeList[i])) {
                        found = true;
                    }
                }
                if (!found) {
                    newRecips.add(recip);
                }
            }
            return newRecips.toArray(new Address[newRecips.size()]);
        } catch (MessagingException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }

    }

    @Override
    public <R> R doCallGet(Map<String, String> parameters, Class<? extends R> clazz, Session session) throws OXException {
        HttpGet request = null;
        try {
            final URI uri = buildUri(toQueryString(parameters));
            request = new HttpGet(uri);
            setLanguage(request, session);
            LOGGER.debug("Executing GET using \"{}\"", uri);

            return handleHttpResponse(execute(request, getHttpClient(), session), clazz);
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public <R> R doCallPost(Map<String, String> parameters, Map<String, String> bodyParameters, Class<? extends R> clazz, Session session) throws OXException {
        HttpPost request = null;
        try {
            final URI uri = buildUri(toQueryString(parameters));
            request = new HttpPost(uri);

            List<NameValuePair> urlEncodedParameters = toQueryString(bodyParameters);
            request.setEntity(new UrlEncodedFormEntity(urlEncodedParameters));
            setLanguage(request, session);
            LOGGER.debug("Executing POST using \"{}\"", uri);
            return handleHttpResponse(execute(request, getHttpClient(), session), clazz);
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public <R> R doCallPut(Map<String, String> parameters, JSONValue jsonBody, Class<? extends R> clazz, Session session) throws OXException {
        HttpPut request = null;
        try {
            final URI uri = buildUri(toQueryString(parameters));
            request = new HttpPut(uri);
            request.setEntity(asHttpEntity(jsonBody));
            setLanguage(request, session);
            LOGGER.debug("Executing PUT using \"{}\" with body \"{}\"", uri, jsonBody);
            return handleHttpResponse(execute(request, getHttpClient(), session), clazz);
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final JSONException e) {
            throw GuardApiExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public InputStream requestResource(Map<String, String> parameters, Session session) throws OXException {
        HttpGet request = null;
        try {
            final URI uri = buildUri(toQueryString(parameters));
            request = new HttpGet(uri);
            setLanguage(request, session);
            LOGGER.debug("Executing GET using \"{}\"", uri);
            final InputStream data = handleHttpResponse(execute(request, getHttpClient(), session), InputStream.class);
            final ResourceReleasingInputStream in = new ResourceReleasingInputStream(data, request);
            request = null; // Avoid preliminary reset
            return in;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public InputStream processResource(Map<String, String> parameters, InputStream resource, String contentType, String name, Session session) throws OXException {
        HashMap<String, InputStream> resources = new HashMap<>();
        resources.put(name == null ? "file" : name, resource);
        return processResources(parameters, resources, contentType, session);
    }

    @Override
    public InputStream processResources(Map<String, String> parameters, Map<String, InputStream> resources, String contentType, Session session) throws OXException {
        HttpPost request = null;
        InputStream processedResource = null;
        try {
            request = new HttpPost(buildUri(toQueryString(parameters)));
            setLanguage(request, session);
            final String boundary = UUIDs.getUnformattedStringFromRandom();
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charsets.UTF_8) {

                @Override
                public Header getContentType() {
                    return new BasicHeader(HTTP.CONTENT_TYPE, generateContentType(boundary, null));
                }
            };
            String mimeType = null == contentType ? "application/octet-stream" : contentType;
            for (Map.Entry<String, InputStream> resource : resources.entrySet()) {
                multipartEntity.addPart(resource.getKey(), new InputStreamBody(resource.getValue(), mimeType, resource.getKey()));
            }
            request.setEntity(multipartEntity);

            LOGGER.debug("Executing POST using \"{}\" with binary content of type \"{}\"", uri, mimeType);
            processedResource = handleHttpResponse(execute(request, getHttpClient(), session), InputStream.class);
            ResourceReleasingInputStream in = new ResourceReleasingInputStream(processedResource, request);
            processedResource = null; // Avoid preliminary close
            request = null; // Avoid preliminary reset
            return in;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Streams.close(resources.values());
            Streams.close(processedResource);
            reset(request);
        }
    }

    @Override
    public <R> R doCallSessionSensitiveGet(Map<String, String> parameters, Class<? extends R> clazz, Session session, List<Cookie> cookies, List<com.openexchange.guard.api.Header> headers) throws OXException {
        HttpGet request = null;
        try {
            List<NameValuePair> queryString = toQueryString(parameters);
            if (null == queryString) {
                queryString = new ArrayList<>(1);
            }
            queryString.add(new BasicNameValuePair(AJAXServlet.PARAMETER_SESSION, session.getSessionID()));

            final URI uri = buildUri(queryString);
            request = new HttpGet(uri);

            if (null != headers && !headers.isEmpty()) {
                for (final com.openexchange.guard.api.Header header : headers) {
                    request.setHeader(header.getName(), header.getValue());
                }
            }

            CookieStore cookieStore = null;
            if (null != cookies && !cookies.isEmpty()) {
                cookieStore = new BasicCookieStore();

                final StringBuilder sCookies = new StringBuilder(64);
                boolean first = true;
                for (final Cookie cookie : cookies) {
                    final BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    clientCookie.setPath("/");
                    cookieStore.addCookie(clientCookie);

                    if (first) {
                        first = false;
                    } else {
                        sCookies.append("; ");
                    }
                    sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
                }

                request.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build());
                request.setHeader("Cookie", sCookies.toString());
            }

            final BasicHttpContext context = new BasicHttpContext();
            final BasicScheme basicAuth = new BasicScheme();
            context.setAttribute("preemptive-auth", basicAuth);
            if (null != cookieStore) {
                context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            }

            LOGGER.debug("Executing GET using \"{}\" including cookies \"{}\" and headers \"{}\"", uri, prettPrint(cookies), prettyPrint(headers));
            final R retval = handleHttpResponse(execute(adoptUserAgent(request, headers), getHttpClient(), context, session), clazz);
            if (retval instanceof InputStream) {
                final ResourceReleasingInputStream in = new ResourceReleasingInputStream((InputStream) retval, request);
                request = null; // Avoid preliminary reset
                return (R) in;
            }
            return retval;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public InputStream processSessionSensitiveResource(Map<String, String> parameters, InputStream resource, String contentType, String name, Session session, List<Cookie> cookies, List<com.openexchange.guard.api.Header> headers) throws OXException {
        HttpPost request = null;
        InputStream processedResource = null;
        try {
            List<NameValuePair> queryString = toQueryString(parameters);
            if (null == queryString) {
                queryString = new ArrayList<>(1);
            }
            queryString.add(new BasicNameValuePair(AJAXServlet.PARAMETER_SESSION, session.getSessionID()));

            URI uri = buildUri(queryString);
            request = new HttpPost(uri);

            if (null != headers && !headers.isEmpty()) {
                for (final com.openexchange.guard.api.Header header : headers) {
                    request.setHeader(header.getName(), header.getValue());
                }
            }

            final String boundary = UUIDs.getUnformattedStringFromRandom();
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charsets.UTF_8) {

                @Override
                public Header getContentType() {
                    return new BasicHeader(HTTP.CONTENT_TYPE, generateContentType(boundary, null));
                }
            };
            String mimeType = null == contentType ? "application/octet-stream" : contentType;
            multipartEntity.addPart("file", new InputStreamBody(resource, mimeType, null == name ? "file.dat" : name));
            request.setEntity(multipartEntity);

            CookieStore cookieStore = null;
            if (null != cookies && !cookies.isEmpty()) {
                cookieStore = new BasicCookieStore();

                final StringBuilder sCookies = new StringBuilder(64);
                boolean first = true;
                for (final Cookie cookie : cookies) {
                    final BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    clientCookie.setPath("/");
                    cookieStore.addCookie(clientCookie);

                    if (first) {
                        first = false;
                    } else {
                        sCookies.append("; ");
                    }
                    sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
                }

                request.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build());
                request.setHeader("Cookie", sCookies.toString());
            }
            final BasicHttpContext context = new BasicHttpContext();
            final BasicScheme basicAuth = new BasicScheme();
            context.setAttribute("preemptive-auth", basicAuth);
            if (null != cookieStore) {
                context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            }

            LOGGER.debug("Executing POST using \"{}\" with binary content of type \"{}\" including cookies \"{}\" and headers \"{}\"", uri, mimeType, prettPrint(cookies), prettyPrint(headers));
            processedResource = handleHttpResponse(execute(adoptUserAgent(request, headers), getHttpClient(), session), InputStream.class);
            ResourceReleasingInputStream in = new ResourceReleasingInputStream(processedResource, request);
            processedResource = null; // Avoid preliminary close
            request = null; // Avoid preliminary reset
            return in;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            Streams.close(resource, processedResource);
            reset(request);
        }
    }

    @Override
    public <R> R doCallSessionSensitivePost(Map<String, String> parameters, JSONValue jsonBody, Class<? extends R> clazz, Session session, List<Cookie> cookies, List<com.openexchange.guard.api.Header> headers) throws OXException {
        HttpPost request = null;
        try {
            List<NameValuePair> queryString = toQueryString(parameters);
            if (null == queryString) {
                queryString = new ArrayList<>(1);
            }
            queryString.add(new BasicNameValuePair(AJAXServlet.PARAMETER_SESSION, session.getSessionID()));

            URI uri = buildUri(queryString);
            request = new HttpPost(uri);

            if (null != headers && !headers.isEmpty()) {
                for (final com.openexchange.guard.api.Header header : headers) {
                    request.setHeader(header.getName(), header.getValue());
                }
            }

            // Set request entity
            request.setEntity(asHttpEntity(jsonBody));

            CookieStore cookieStore = null;
            if (null != cookies && !cookies.isEmpty()) {
                cookieStore = new BasicCookieStore();

                final StringBuilder sCookies = new StringBuilder(64);
                boolean first = true;
                for (final Cookie cookie : cookies) {
                    final BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    clientCookie.setPath("/");
                    cookieStore.addCookie(clientCookie);

                    if (first) {
                        first = false;
                    } else {
                        sCookies.append("; ");
                    }
                    sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
                }

                request.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build());
                request.setHeader("Cookie", sCookies.toString());
            }
            final BasicHttpContext context = new BasicHttpContext();
            final BasicScheme basicAuth = new BasicScheme();
            context.setAttribute("preemptive-auth", basicAuth);
            if (null != cookieStore) {
                context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            }

            LOGGER.debug("Executing POST using \"{}\" including cookies \"{}\" and headers \"{}\"", uri, prettPrint(cookies), prettyPrint(headers));
            final R retval = handleHttpResponse(execute(adoptUserAgent(request, headers), getHttpClient(), context, session), clazz);
            if (retval instanceof InputStream) {
                final ResourceReleasingInputStream in = new ResourceReleasingInputStream((InputStream) retval, request);
                request = null; // Avoid preliminary reset
                return (R) in;
            }
            return retval;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (JSONException e) {
            throw GuardApiExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public <R> R doCallSessionSensitivePut(Map<String, String> parameters, JSONValue jsonBody, Class<? extends R> clazz, Session session, List<Cookie> cookies, List<com.openexchange.guard.api.Header> headers) throws OXException {
        HttpPut request = null;
        try {
            List<NameValuePair> queryString = toQueryString(parameters);
            if (null == queryString) {
                queryString = new ArrayList<>(1);
            }
            queryString.add(new BasicNameValuePair(AJAXServlet.PARAMETER_SESSION, session.getSessionID()));

            URI uri = buildUri(queryString);
            request = new HttpPut(uri);

            if (null != headers && !headers.isEmpty()) {
                for (final com.openexchange.guard.api.Header header : headers) {
                    request.setHeader(header.getName(), header.getValue());
                }
            }

            // Set request entity
            request.setEntity(asHttpEntity(jsonBody));

            CookieStore cookieStore = null;
            if (null != cookies && !cookies.isEmpty()) {
                cookieStore = new BasicCookieStore();

                final StringBuilder sCookies = new StringBuilder(64);
                boolean first = true;
                for (final Cookie cookie : cookies) {
                    final BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    clientCookie.setPath("/");
                    cookieStore.addCookie(clientCookie);

                    if (first) {
                        first = false;
                    } else {
                        sCookies.append("; ");
                    }
                    sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
                }

                request.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build());
                request.setHeader("Cookie", sCookies.toString());
            }
            final BasicHttpContext context = new BasicHttpContext();
            final BasicScheme basicAuth = new BasicScheme();
            context.setAttribute("preemptive-auth", basicAuth);
            if (null != cookieStore) {
                context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            }

            LOGGER.debug("Executing PUT using \"{}\" with body \"{}\" including cookies \"{}\" and headers \"{}\"", uri, jsonBody, prettPrint(cookies), prettyPrint(headers));
            final R retval = handleHttpResponse(execute(adoptUserAgent(request, headers), getHttpClient(), context, session), clazz);
            if (retval instanceof InputStream) {
                final ResourceReleasingInputStream in = new ResourceReleasingInputStream((InputStream) retval, request);
                request = null; // Avoid preliminary reset
                return (R) in;
            }
            return retval;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final JSONException e) {
            throw GuardApiExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    @Override
    public InputStream requestSessionSensitiveResource(Map<String, String> parameters, Session session, List<Cookie> cookies, List<com.openexchange.guard.api.Header> headers) throws OXException {
        HttpGet request = null;
        try {
            List<NameValuePair> queryString = toQueryString(parameters);
            if (null == queryString) {
                queryString = new ArrayList<>(1);
            }
            queryString.add(new BasicNameValuePair(AJAXServlet.PARAMETER_SESSION, session.getSessionID()));

            final URI uri = buildUri(queryString);
            //request = new HttpGet(uri.toASCIIString().replace("+", "%2b"));
            request = new HttpGet(uri);

            if (null != headers && !headers.isEmpty()) {
                for (final com.openexchange.guard.api.Header header : headers) {
                    request.setHeader(header.getName(), header.getValue());
                }
            }

            CookieStore cookieStore = null;
            if (null != cookies && !cookies.isEmpty()) {
                cookieStore = new BasicCookieStore();

                final StringBuilder sCookies = new StringBuilder(64);
                boolean first = true;
                for (final Cookie cookie : cookies) {
                    final BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                    clientCookie.setPath("/");
                    cookieStore.addCookie(clientCookie);

                    if (first) {
                        first = false;
                    } else {
                        sCookies.append("; ");
                    }
                    sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
                }

                request.setConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build());
                request.setHeader("Cookie", sCookies.toString());
            }
            final BasicHttpContext context = new BasicHttpContext();
            final BasicScheme basicAuth = new BasicScheme();
            context.setAttribute("preemptive-auth", basicAuth);
            if (null != cookieStore) {
                context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            }

            LOGGER.debug("Executing GET using \"{}\" including cookies \"{}\" and headers \"{}\"", uri, prettPrint(cookies), prettyPrint(headers));
            final InputStream data = handleHttpResponse(execute(adoptUserAgent(request, headers), getHttpClient(), context, session), InputStream.class);
            final ResourceReleasingInputStream in = new ResourceReleasingInputStream(data, request);
            request = null; // Avoid preliminary reset
            return in;
        } catch (final HttpResponseException e) {
            if (400 == e.getStatusCode() || 401 == e.getStatusCode()) {
                // Authentication failed -- recreate token
                throw GuardApiExceptionCodes.AUTH_ERROR.create(e, e.getMessage());
            }
            throw handleHttpResponseError(null, e);
        } catch (final IOException e) {
            throw handleIOError(e);
        } catch (final RuntimeException e) {
            throw GuardApiExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } finally {
            reset(request);
        }
    }

    // ----------------------------------------------------------------------------------------------------------- //

    /**
     * Builds the URI from given arguments
     *
     * @param queryString The query string parameters
     * @return The built URI string
     * @throws IllegalArgumentException If the given string violates RFC 2396
     */
    protected URI buildUri(List<NameValuePair> queryString) {
        try {
            final URIBuilder builder = new URIBuilder();
            builder.setScheme(uri.getScheme()).setHost(uri.getHost()).setPort(uri.getPort()).setPath(uri.getPath()).setQuery(null == queryString ? null : URLEncodedUtils.format(queryString, StandardCharsets.UTF_8));
            return builder.build();
        } catch (final URISyntaxException x) {
            throw new IllegalArgumentException("Failed to build URI", x);
        }
    }

    /**
     * Turns specified JSON value into an appropriate HTTP entity.
     *
     * @param jValue The JSON value
     * @return The HTTP entity
     * @throws JSONException If a JSON error occurs
     * @throws IOException If an I/O error occurs
     */
    protected InputStreamEntity asHttpEntity(JSONValue jValue) throws JSONException, IOException {
        if (null == jValue) {
            return null;
        }

        ThresholdFileHolder sink = null;
        boolean error = true;
        try {
            sink = new ThresholdFileHolder();
            final OutputStreamWriter osw = new OutputStreamWriter(sink.asOutputStream(), Charsets.UTF_8);
            jValue.write(osw);
            osw.flush();
            final InputStreamEntity entity = new InputStreamEntity(sink.getStream(), sink.getLength(), ContentType.APPLICATION_JSON);
            error = false;
            return entity;
        } catch (final OXException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new IOException(null == cause ? e : cause);
        } finally {
            if (error && null != sink) {
                Streams.close(sink);
            }
        }
    }

    /**
     * Gets the appropriate query string for given parameters
     *
     * @param parameters The parameters
     * @return The query string
     */
    protected List<NameValuePair> toQueryString(Map<String, String> parameters) {
        if (null == parameters || parameters.isEmpty()) {
            return null;
        }
        final List<NameValuePair> l = new LinkedList<>();
        for (final Map.Entry<String, String> e : parameters.entrySet()) {
            l.add(new BasicNameValuePair(e.getKey(), e.getValue()));
        }
        return l;
    }

    /**
     * Sets the Authorization header.
     *
     * @param request The HTTP request
     * @param login The login
     * @param password The password
     */
    protected void setAuthorizationHeader(HttpRequestBase request, String login, String password) {
        String encodedCredentials = Base64.getEncoder().encodeToString((login + ":" + password).getBytes(Charsets.UTF_8));
        request.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encodedCredentials);
    }

    /**
     * Executes specified HTTP method/request using given HTTP client instance.
     *
     * @param method The method/request to execute
     * @param ManagedHttpClient The HTTP client to use
     * @return The HTTP response
     * @throws ClientProtocolException If client protocol error occurs
     * @throws IOException If an I/O error occurs
     */
    protected HttpResponse execute(HttpRequestBase method, ManagedHttpClient httpClient, Session session) throws ClientProtocolException, IOException {
        return execute(method, httpClient, localcontext, session);
    }

    /**
     * Executes specified HTTP method/request using given HTTP client instance.
     *
     * @param method The method/request to execute
     * @param httpClient The HTTP client to use
     * @param context The context
     * @return The HTTP response
     * @throws ClientProtocolException If client protocol error occurs
     * @throws IOException If an I/O error occurs
     */
    protected HttpResponse execute(HttpRequestBase method, ManagedHttpClient httpClient, BasicHttpContext context, Session session) throws ClientProtocolException, IOException {
        if (session != null) {
            if (session.getParameter("user-agent") != null) {
                method.addHeader("User-Agent", session.getParameter("user-agent").toString());
            }
            method.addHeader("X-Forwarded-For", session.getLocalIp());
            String host = session.getParameter(session.PARAM_HOST_NAME) == null ? null : (String) session.getParameter(session.PARAM_HOST_NAME);
            if (host != null) {
                method.addHeader("X-Host-Name", (String) session.getParameter(session.PARAM_HOST_NAME));
            }
        }
        setAuthorizationHeader(method, authLogin, authPassword);
        return httpClient.execute(targetHost, method, context);
    }

    /**
     * Resets given HTTP request
     *
     * @param request The HTTP request
     */
    protected static void reset(HttpRequestBase request) {
        if (null != request) {
            try {
                request.reset();
            } catch (Exception e) {
                // Ignore
            }
        }
    }

    /**
     * Closes the supplied response resources silently.
     *
     * @param response The HTTP response to consume and close
     */
    @SuppressWarnings("unused")
    protected static void close(HttpResponse response) {
        if (null != response) {
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                try {
                    EntityUtils.consumeQuietly(entity);
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
    }

    /**
     * Closes the supplied HTTP request / response resources silently.
     *
     * @param request The HTTP request to reset
     * @param response The HTTP response to consume and close
     */
    protected static void closeAndReset(HttpRequestBase request, HttpResponse response) {
        close(response);
        reset(request);
    }

    /**
     * Handles given HTTP response while expecting <code>200 (Ok)</code> status code.
     *
     * @param httpResponse The HTTP response
     * @param clazz The class of the result object
     * @return The result object
     * @throws OXException If an Open-Xchange error occurs
     * @throws ClientProtocolException If a client protocol error occurs
     * @throws IOException If an I/O error occurs
     */
    protected <R> R handleHttpResponse(HttpResponse httpResponse, Class<R> clazz) throws OXException, ClientProtocolException, IOException {
        return handleHttpResponse(httpResponse, STATUS_CODE_POLICY_DEFAULT, clazz);
    }

    /**
     * Handles given HTTP response while expecting given status code.
     *
     * @param httpResponse The HTTP response
     * @param policy The status code policy to obey
     * @param clazz The class of the result object
     * @return The result object
     * @throws OXException If an Open-Xchange error occurs
     * @throws ClientProtocolException If a client protocol error occurs
     * @throws IOException If an I/O error occurs
     * @throws IllegalStateException If content stream cannot be created
     */
    @SuppressWarnings({ "unchecked", "resource" })
    protected <R> R handleHttpResponse(HttpResponse httpResponse, StatusCodePolicy policy, Class<R> clazz) throws OXException, ClientProtocolException, IOException {
        boolean close = true;
        try {
            policy.handleStatusCode(httpResponse);
            // OK, continue
            if (Void.class.equals(clazz)) {
                return null;
            }
            if (InputStream.class.equals(clazz)) {
                R retval = (R) httpResponse.getEntity().getContent();
                close = false;
                return retval;
            }
            try {
                InputStream in = httpResponse.getEntity().getContent();
                R retval = (R) new JSONObject(new InputStreamReader(in, Charsets.UTF_8));
                return retval;
            } catch (final JSONException e) {
                throw GuardApiExceptionCodes.JSON_ERROR.create(e, e.getMessage());
            }
        } finally {
            if (close) {
                close(httpResponse);
            }
        }
    }

    /**
     * Handles given I/O error.
     *
     * @param e The I/O error
     * @return The resulting exception
     */
    protected OXException handleIOError(IOException e) {
        LOGGER.warn("An IOError occurred while communicating with the guard server: {}", e.getMessage());
        final Throwable cause = e.getCause();
        if (cause instanceof AuthenticationException) {
            return GuardApiExceptionCodes.AUTH_ERROR.create(cause, cause.getMessage());
        }
        return GuardApiExceptionCodes.IO_ERROR.create(e, e.getMessage());
    }

    /** Status code (401) indicating that the request requires HTTP authentication. */
    private static final int SC_UNAUTHORIZED = 401;

    /** Status code (404) indicating that the requested resource is not available. */
    private static final int SC_NOT_FOUND = 404;

    /**
     * Handles given HTTP response error.
     *
     * @param identifier The optional identifier for associated Microsoft OneDrive resource
     * @param e The HTTP error
     * @return The resulting exception
     */
    protected OXException handleHttpResponseError(String identifier, HttpResponseException e) {
        LOGGER.warn("An error occurred while communicating with the guard server: {}", e.getMessage());
        if (null != identifier && SC_NOT_FOUND == e.getStatusCode()) {
            return GuardApiExceptionCodes.NOT_FOUND.create(e, identifier);
        }
        if (SC_UNAUTHORIZED == e.getStatusCode()) {
            return GuardApiExceptionCodes.AUTH_ERROR.create();
        }
        if (e.getMessage() == null || e.getMessage().isEmpty()) {
            return GuardApiExceptionCodes.GUARD_SERVER_ERROR.create(e, Integer.valueOf(e.getStatusCode()), "No error message provided");
        }
        if (e instanceof ApiResponseException) {
            return new ApiDynamicExceptionCodes(((ApiResponseException) e).getJson(), e.getMessage()).create();
        }
        return GuardApiExceptionCodes.GUARD_SERVER_ERROR.create(e, Integer.valueOf(e.getStatusCode()), e.getMessage());
    }

    private static String getUserAgent(List<com.openexchange.guard.api.Header> headers) {
        if (null == headers || headers.isEmpty()) {
            return null;
        }
        for (final com.openexchange.guard.api.Header header : headers) {
            if ("User-Agent".equalsIgnoreCase(header.getName())) {
                return header.getValue();
            }
        }
        return null;
    }

    private static HttpRequestBase adoptUserAgent(HttpRequestBase request, List<com.openexchange.guard.api.Header> headers) {
        final String userAgent = getUserAgent(headers);
        if (Strings.isNotEmpty(userAgent)) {
            request.setHeader(HttpHeaders.USER_AGENT, userAgent);
        }
        return request;
    }

    /**
     * Add locale cookie to request
     *
     * @param request
     * @param session
     */
    private static void setLanguage(HttpRequestBase request, Session session) {
        if (session == null) {
            return;
        }
        UserService userService = Services.getService(UserService.class);
        try {
            String locale = userService.getUser(session.getUserId(), session.getContextId()).getLocale().toString();
            setLanguage(request, locale);
        } catch (OXException e) {
            LOGGER.error("Problem setting language for api call", e);
        }
    }

    /**
     * Adds locale cookie to request
     *
     * @param request
     * @param language
     */
    private static void setLanguage(HttpRequestBase request, String language) {
        if (Strings.isEmpty(language)) {
            return;
        }
        Header cookies = request.getFirstHeader("Cookie");
        StringBuilder sCookies = new StringBuilder();
        if (cookies != null) {
            String existing = cookies.getValue();
            if (Strings.isNotEmpty(existing) && existing.contains("locale")) {

                return;
            }
            sCookies.append(existing);
            sCookies.append(";");
        }
        Cookie cookie = new Cookie("locale", language);
        sCookies.append(cookie.getName()).append('=').append(cookie.getValue());
        request.setHeader("Cookie", sCookies.toString());
    }

    /**
     * An <code>InputStream</code> that takes care of shutting-down associated <code>HttpClient</code> instance when closed.
     */
    private static class ResourceReleasingInputStream extends InputStream {

        private final InputStream in;
        private final HttpRequestBase request;

        ResourceReleasingInputStream(InputStream in, HttpRequestBase request) {
            super();
            this.in = in;
            this.request = request;
        }

        @Override
        public int read() throws IOException {
            return in.read();
        }

        @Override
        public int read(byte[] b) throws IOException {
            return in.read(b);
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {
            return in.read(b, off, len);
        }

        @Override
        public long skip(long n) throws IOException {
            return in.skip(n);
        }

        @Override
        public String toString() {
            return in.toString();
        }

        @Override
        public int available() throws IOException {
            return in.available();
        }

        @Override
        public void close() throws IOException {
            try {
                in.close();
            } finally {
                GuardApiImpl.reset(request);
            }
        }

        @Override
        public void mark(int readlimit) {
            in.mark(readlimit);
        }

        @Override
        public void reset() throws IOException {
            in.reset();
        }

        @Override
        public boolean markSupported() {
            return in.markSupported();
        }
    } // End of class ClientReleasingInputStream

    private static Object prettyPrint(final List<com.openexchange.guard.api.Header> headers) {
        return new Object() {

            @Override
            public String toString() {
                if (null == headers || headers.isEmpty()) {
                    return "none";
                }

                final Iterator<com.openexchange.guard.api.Header> iter = headers.iterator();
                final StringBuilder sb = new StringBuilder(headers.size() << 2);

                com.openexchange.guard.api.Header hdr = iter.next();
                sb.append(hdr.getName()).append('=').append(hdr.getName());
                while (iter.hasNext()) {
                    hdr = iter.next();
                    sb.append(", ").append(hdr.getName()).append('=').append(hdr.getName());
                }
                return sb.toString();
            }
        };
    }

    private static Object prettPrint(final List<Cookie> cookies) {
        return new Object() {

            @Override
            public String toString() {
                if (null == cookies || cookies.isEmpty()) {
                    return "none";
                }

                final Iterator<Cookie> iter = cookies.iterator();
                final StringBuilder sb = new StringBuilder(cookies.size() << 2);

                Cookie cookie = iter.next();
                sb.append(cookie.getName()).append('=').append(cookie.getName());
                while (iter.hasNext()) {
                    cookie = iter.next();
                    sb.append(", ").append(cookie.getName()).append('=').append(cookie.getName());
                }
                return sb.toString();
            }
        };
    }

}
