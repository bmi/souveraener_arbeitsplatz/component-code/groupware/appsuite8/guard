---
title: Smime
description: Using Smime with OX Guard
---

# Overview

As of Guard version 2.10.7, users can now be configured to use SMIME for secure email communications.  The user can have only SMIME as an option, or can be configured to have the option of PGP or SMIME.

OX Guard was originally designed to support PGP only for both Email and File encryption.  Starting 2.10.7, support for SMIME Email encryption is now available.

Configuring the users options is done through capabilities.  Capability of com.openexchange.capability.smime is added.  This can be enabled alone, or in conjunction with the other Guard capabilities.

# Installation

There are no additional installation steps required for SMIME support.  Guard installation and setup must be completed as normal (initialization of database and files as per the normal setup).

To enable smime, set com.openexchange.capability.smime=true for the user.  This can be configured system wide, context wide, or user wide as normal.

# Configuration

## Supported encryption types

The most significant configuration is deciding if the user should have only one option for encryption, or if the user should have the choice between PGP and SMIME.  SMIME requires all users to have uploaded keys created from a certificate authority.  As a result, the number of people that can receive encrypted emails is much more limited.  PGP, however, can create keys on the fly.  Hence PGP can support Guest accounts which allows many more people to receive encrypted emails.

A user with only one choice of encryption will have a very straight-forward experience.  Having a choice of PGP and SMIME would add potential confusion for users if they don't understand the difference.  In addition, there may be different encryption passwords for the different types of encryption.

Due to this potential confusion when both types enabled, a setting in the user settings has been added to enable SMIME.  If the user does not check this, then he/she will be treated like they have a PGP only setup.

If the user has both capabilities (smime and guard-mail), and have enabled smime in their settings, they will have to actively choose which encryption to use.  Password prompts will specify the type of encryption being used.

## Certificate Authorities

By default, Guard will trust the certificate authorities in the default JAVA keystore.  A different keystore can be specified in the startup script if a new, more restricted list is created.

Additional certificate authorities can be added to Guard.  This is useful if a company uses their own certificate authority, or a third party authority that is not trusted by the default keystore is used.

Imported certificate authorities may not necessarily be trusted system wide.  One company may want their own certificate authority, but another company will likely not want to trust those certificates by default.

com.openexchange.smime.caGroupId

specifies a group number that the user should belong to.  When a certificate authority certificate is imported using the smime clt, a group ID is specified.  Users with this group ID configured will trust the imported certificate authority.

/opt/open-xchange/sbin/smime -A admin -P password -a /path/to/pemFile -g grpId

## Other configurations

com.openexchange.smime.checkCRL

Enabled checking certificate revokation lists when verifying certificates.  This can significantly slow down Guard response time, as there is an HTTP request sent for the revokation list to each certificate authority.  This request is cached for the length of time the response is valid, but can add several seconds to signature verifications and email encryption.



