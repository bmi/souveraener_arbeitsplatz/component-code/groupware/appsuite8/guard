/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.mime.services.PGPMimeAttachmentExtractor;
import com.openexchange.guard.pgpcore.services.SignatureVerificationResultUtil;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.server.ServiceLookup;

/**
 * {@link SmimeMimeEncryptionService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimeMimeEncryptionService implements MimeEncryptionService {

    private static final String SMIME_CAPABILITY = "smime";

    private final ServiceLookup services;
    private final SMIMESigatureVerificationService signatureVerificationService;

    /**
     * Initializes a new {@link SmimeMimeEncryptionService}.
     *
     * @param signatureVerificationService the {@link SMIMESigatureVerificationService} to use
     * @param services The service lookup
     */
    public SmimeMimeEncryptionService(SMIMESigatureVerificationService signatureVerificationService, ServiceLookup services) {
        this.signatureVerificationService = signatureVerificationService;
        this.services = services;
        // init mailcap
        MailcapCommandMap mailcap = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mailcap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        mailcap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        mailcap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        mailcap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        mailcap.addMailcap("multipart/signed;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");

        CommandMap.setDefaultCommandMap(mailcap);
    }

    /**
     * Internal method to check for "smime" capability
     *
     * @param userId The user ID
     * @param contextId The context ID
     * @throws OXException if the user does not have the "smime" capability assigned
     */
    private void checkCapabilities(int userId, int contextId) throws OXException {
        CapabilityService caps = services.getServiceSafe(CapabilityService.class);
        if (!caps.getCapabilities(userId, contextId).contains(SMIME_CAPABILITY)) {
            throw OXException.noPermissionForModule(SMIME_CAPABILITY);
        }
    }

    /**
     * Internal method to decrypt and verify a message
     *
     * @param msg The {@link MimeMessage} to decrypt
     * @param userIdentity The {@link UserIdentity} to use for decrypting the given message
     * @param output The {@link OutputStream} to write the decrypted data to
     * @throws OXException
     */
    private void doDecryption(MimeMessage msg, UserIdentity userIdentity, OutputStream output) throws OXException {
        try {
            checkCapabilities(userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId());
            if (msg.getContentType().contains("enveloped-data")) {

                MimeMessage decrypted = new SMIMEDecryptor(services).decryptPart(msg, userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId(), new String(userIdentity.getPassword()));
                if (decrypted == null) {
                    throw SmimeExceptionCodes.UNABLE_TO_DECRYPT.create();
                }
                verify(decrypted, userIdentity);
                decrypted.writeTo(output);
            }
        } catch (Exception e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Internal method to verify a given {@link MimeMessage}
     *
     * @param msg The {@link MimeMessage} to verify
     * @param userIdentity The {@link UserIdentity} to use for verifying the given message
     * @throws OXException
     */
    private void verify(MimeMessage msg, UserIdentity userIdentity) throws OXException {
        checkCapabilities(userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId());
        if (signatureVerificationService != null) {
            List<SignatureVerificationResult> results = signatureVerificationService.verify(msg, userIdentity, true);
            String[] signatures = SignatureVerificationResultUtil.toHeaders(results);
            for (String sig : signatures) {
                try {
                    msg.addHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT, sig);
                } catch (MessagingException e) {
                    throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
                }
            }
        }
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.SMIME;
    }

    @Override
    public void doEncryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, String password, OutputStream output) throws OXException {
        checkCapabilities(userId, contextId);
        SmimeEncryptor encryptor = new SmimeEncryptor(services);
        try {
            if (mimeMessage.isSign() && !mimeMessage.isEncrypt() && !mimeMessage.isDraft()) {  // not signing draft messages
                encryptor.doSigning(output, userId, contextId, password, mimeMessage);
                return;
            }
            encryptor.doEncryption(output, userId, contextId, password, mimeMessage);
        } catch (Exception e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void doDecryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, UserIdentity userIdentity, OutputStream output) throws OXException {
        doDecryption(mimeMessage.getMessage(), userIdentity, output);
    }

    @Override
    public MimeMessage doDecryption(MimeMessage mimeMessage, UserIdentity userIdentity) throws OXException {
        checkCapabilities(userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        doDecryption(mimeMessage, userIdentity, out);
        try {
            return new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(out.toByteArray()));
        } catch (MessagingException e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public Part doDecryption(MimeMessage mimeMessage, String attachmentName, UserIdentity userIdentity) throws OXException {
        mimeMessage = doDecryption(mimeMessage, userIdentity);
        try {
            return new PGPMimeAttachmentExtractor().getAttachmentFromMessage(mimeMessage, attachmentName);
        } catch (MessagingException | IOException e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
        }
    }
}
