
package com.openexchange.guard.cron;

import java.util.Date;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.osgi.Services;

/**
 * 
 * {@link ResetExposedKeysTask} resets the "exposed" flag of deleted keys which have been exposed longer since a given time
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class ResetExposedKeysTask implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ResetExposedKeysTask.class);
    private final int exposedKeyDurationInHours;

    /**
     * Initializes a new {@link ResetExposedKeysTask}.
     * 
     * @param exposedKeyDurationInHours the amount of hours specifying how long a deleted and exposed key will be marked as "exposed"
     */
    public ResetExposedKeysTask(int exposedKeyDurationInHours) {
        this.exposedKeyDurationInHours = exposedKeyDurationInHours;
    }

    /**
     * Calculating boundary date for resetting exposed keys
     *
     * @param the duration in hours how long a key can be marked as "exposed"
     * @return the boundary date before exposed keys can be reseted to "not exposed"
     */
    private Date CalculateBoundaryDate(int exposedDurationHours) {
        DateTime now = new DateTime();
        Date before = now.minusHours(exposedDurationHours).toDate();
        return before;
    }

    @Override
    public void run() {
        try {
            DeletedKeysStorage deletedKeysStorage = Services.getService(DeletedKeysStorage.class);
            //Getting the boundary date before which we reset the exposed flag for exposed keys
            Date before = CalculateBoundaryDate(this.exposedKeyDurationInHours);

            //Resetting the exposed flag
            LOG.debug(String.format("Unexposing exposed/deleted keys which have been exposed before %s ...", before.toString()));
            int updatedKeyCount = deletedKeysStorage.setAllUnexposed(before);
            LOG.debug(String.format("Unexposed %d exposed/deleted keys which have been exposed before %s", updatedKeyCount, before.toString()));
        } catch (OXException e) {
            LOG.error("Error while doing schedued cleanup of exposed keys", e);
        }
    }
}
