/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage;

import java.sql.Connection;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.PGPKeys;

/**
 * {@link OGPGPKeysStorage} provides access to the stored mappings from userid and contextId to a PGP Key ID
 *
 */
public interface PGPKeysStorage {

    /**
     * Gets the mapping for internal user, which means no guest user's are considered
     *
     * @param email the email to get the mappings for
     * @return mappings for the the given email
     * @throws OXException due an error
     */
    List<PGPKeys> getInternalByEmail(String email) throws OXException;

    /**
     * Gets the mapping for a given hexId
     *
     * @param hexId the hexId to get the mapping for
     * @return the mapping for the given hexId, or null if no such mapping was found
     * @throws OXException due an error
     */
    PGPKeys getByHexId(String hexId) throws OXException;

    /**
     * Gets the mapping for a given id
     *
     * @param id the Id to get the mapping for
     * @return the mapping for the given Id, or null if no such mapping was found
     * @throws OXException due an error
     */
    PGPKeys getById(long id) throws OXException;

    /**
     * Gets the mapping for the given ids
     *
     * @param ids the ids of the mappings to get
     * @return the mappings related to the given ids
     * @throws OXException due an error
     */
    List<PGPKeys> getByIds(List<Long> ids) throws OXException;

    /**
     * Gets the mapping for the given key-id in the given context
     *
     * @param contextId the context id
     * @param keyId the key id to get the mapping for
     * @return the mappings related to the given ids
     * @throws OXException due an error
     */
    PGPKeys getByKeyIdInContext(int contextId, long keyId) throws OXException;

    /**
     * Updates the context id for the specified key id
     *
     * @param keyId the key id
     * @param contextId the new contextId
     * @throws OXException due an error
     */
    void updateContextIdByKeyId(long keyId,int contextId) throws OXException;

    /**
     * Updates the context id for the specified key id
     *
     * @param connection the connection to use
     * @param keyId the key id
     * @param contextId the new contextId
     * @throws OXException due an error
     */
    void updateContextIdByKeyId(Connection connection, long keyId,int contextId) throws OXException;

    /**
     * Creates a mapping or updates an existing one
     *
     * @param id the id
     * @param email the mail
     * @param hexId the hexId
     * @param contextId the context ID
     * @param keyId the key id
     * @throws OXException due an error
     */
    void insertOrUpdate(long id, String email, String hexId, int contextId, long keyId) throws OXException;

    /**
     * Deletes a mapping entry for a key
     *
     * @param keyId the id of the key to delete the mapping entry for
     * @throws OXException due an error
     */
    void deleteByKeyId(long keyId) throws OXException;

    /**
     * Deletes all mappings for a given email
     *
     * @param email the email to delete the mappings for
     * @throws OXException due an error
     */
    void deleteByEmail(String email) throws OXException;

    /**
     * Deletes a mapping entry for a key in a given context
     *
     * @param keyId the id of the key to delete the mapping entry for
     * @param contextId the context id
     * @throws OXException due an error
     */
    void deleteByKeyIdInContext(long keyId, int contextId) throws OXException;

    /**
     * Adds all keys from the specified key ring
     *
     * @param cid The context identifier
     * @param keyId The key identifier
     * @param email The e-mail address
     * @param publicKeyRing The public key ring
     * @throws OXException
     */
    void addPublicKeyIndex(int cid, long keyId, String email, PGPPublicKeyRing publicKeyRing) throws OXException;

    /**
     * Update the email address for list of keys
     */
    void changeEmailAddress (String oldEmail, String newEmail, int contextId) throws OXException;
}
