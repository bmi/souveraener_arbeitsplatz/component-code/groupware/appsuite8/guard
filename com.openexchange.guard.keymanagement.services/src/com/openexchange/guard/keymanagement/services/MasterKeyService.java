/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import java.security.Key;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.MasterKey;

/**
 * {@link MasterKeyService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.6
 */
public interface MasterKeyService {

    /**
     * Gets the master key for a specific user index
     * getMasterKey
     *
     * @param userId id of the user
     * @param cid cid of the user
     * @return
     * @throws OXException
     */
    default MasterKey getMasterKey(int userId, int cid) throws OXException {
        return getMasterKey(userId, cid, false);
    }

    /**
     * Gets the master key for a specific user index
     * getMasterKey
     *
     * @param index of the key
     * @param recovery True if keys to decrypt password recovery required
     * @return
     * @throws OXException
     */
    public MasterKey getMasterKey(int index, boolean recovery) throws OXException;

    /**
     * Gets the master key for a specific user index
     * getMasterKey
     *
     * @param userId id of the user
     * @param cid cid of the user
     * @param recovery True if keys to decrypt password recovery required
     * @return
     * @throws OXException
     */
    public MasterKey getMasterKey(int userId, int cid, boolean recovery) throws OXException;

    /**
     * Gets the index for a specific user
     * getIndexForUser
     *
     * @param userId
     * @param cid
     * @return
     * @throws OXException
     */
    public int getIndexForUser(int userId, int cid) throws OXException;

    /**
     * getDecryptedClientKey
     *
     * @param key
     * @return
     * @throws OXException
     */
    Key getDecryptedClientKey(MasterKey key) throws OXException;

    /**
     * Encrypts data with the AES rc pass
     * getRcEncryted
     *
     * @param data
     * @param salt
     * @param index
     * @return
     * @throws OXException
     */
    String getRcEncryted(String data, String salt, int index) throws OXException;

    /**
     * Decrypts data with the AES rc pass
     * getRcDecrypted
     *
     * @param data
     * @param salt
     * @param index
     * @return
     * @throws OXException
     */
    String getRcDecrypted(String data, String salt, int index) throws OXException;

    /**
     * Creates a new masterKey pair and stores it in the implementation specific key storage.
     *
     * @param index The index to create the key for
     * @return The {@link MasterKey} The created and stored key
     * @throws OXException due an error while creating or storing the key
     */
    MasterKey createKey() throws OXException;

    /**
     * Creates a new masterKey pair for the specified index
     * and stores it in the implementation specific key storage.
     *
     * @param index The index to create the key for
     * @return The {@link MasterKey} The created and stored key
     * @throws OXException due an error while creating or storing the key
     */
    MasterKey createKey(int index) throws OXException;
}
