/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.osgi;

import java.util.concurrent.TimeUnit;
import org.osgi.service.http.HttpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigTools;
import com.openexchange.context.ContextService;
import com.openexchange.guard.GuardMaintenanceService;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.auth.AuthenticationService;
import com.openexchange.guard.auth.AuthenticationServiceRegistry;
import com.openexchange.guard.auth.AuthenticationServiceRegistryImpl;
import com.openexchange.guard.auth.PGPAuthenticationService;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.certificatemanagement.RecipCertificateService;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.cron.CleanAuthenticationTokenTask;
import com.openexchange.guard.cron.CleanRemoteKeyCacheTask;
import com.openexchange.guard.cron.DailyTaskScheduler;
import com.openexchange.guard.cron.GuestCleanupTask;
import com.openexchange.guard.cron.PopulateKeyCacheTask;
import com.openexchange.guard.cron.ResetExposedKeysTask;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.dns.DNSService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.encryption.EncryptedItemsStorage;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guestreset.GuestResetService;
import com.openexchange.guard.guestupgrade.GuestUpgradeService;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.handler.impl.ResponseHandlerImpl;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.internal.GuardMaintenanceServiceImpl;
import com.openexchange.guard.internal.GuardVersion;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.ContactKeyService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.ImportSystemPublicKey;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.KeyImportService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.PGPKeySigningService;
import com.openexchange.guard.pgpcore.services.PGPPacketService;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.servlets.GuardServlets;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.settings.GlobalSettingsStorage;
import com.openexchange.guard.sharing.SharingService;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.update.GuardUpdateService;
import com.openexchange.guard.update.internal.GuardKeysUpdater;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pgp.keys.parsing.PGPKeyRingParser;
import com.openexchange.startup.SignalHttpApiAvailabilityService;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * The activator for the OX Guard bundle
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardServiceActivator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(GuardServiceActivator.class);
    private DailyTaskScheduler dailyJobScheduler;
    private ScheduledTimerTask scheduledKeyCacheTask;

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { HttpService.class, SignalHttpApiAvailabilityService.class, CryptoManager.class,
            GuardConfigurationService.class, MasterKeyService.class, TimerService.class, GuardTranslationService.class,
            GuardRatifierService.class, GuardCipherFactoryService.class, GuardDatabaseService.class, GuardSessionService.class,
            KeyCacheStorage.class, RecipKeyService.class, RecipCertificateService.class, MailCreatorService.class, DNSService.class, GlobalSettingsStorage.class,
            GuardNotificationService.class, GuardAntiAbuseService.class, GuardKeyService.class, EmailStorage.class,
            GuardShardingService.class, KeyTableStorage.class, PGPKeysStorage.class, EncryptedItemsStorage.class, OGPGPKeysStorage.class,
            DeletedKeysStorage.class, Storage.class, GuardUpdateService.class, GuestUpgradeService.class, HKPClientService.class,
            PGPCryptoService.class, PGPSigningService.class, TokenAuthenticationService.class, FileCacheStorage.class, RemoteKeyCacheStorage.class,
            GuestResetService.class, KeyCreationService.class, OXUserService.class, KeyRecoveryService.class,
            PasswordChangeService.class, AccountCreationService.class, PublicExternalKeyService.class, KeyImportService.class, ContextService.class,
            PGPKeyRingParser.class, ImportSystemPublicKey.class, PGPPacketService.class, GuardGuestService.class,
            PGPKeySigningService.class, ContactKeyService.class, SharingService.class, GuardGuestEMailMetadataService.class,
            SecondFactorService.class, GuestLookupService.class, GuardKeysUpdater.class,
            AutoCryptService.class, AutoCryptStorageService.class };
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class[] { ActivityTrackingService.class, JSlobServiceRegistry.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);

        //Loading the Guard version information
        GuardVersion.loadGuardVersion();

        // Register the maintenance service
        GuardMaintenanceService maintenanceService = new GuardMaintenanceServiceImpl(this);
        registerService(GuardMaintenanceService.class, maintenanceService);
        trackService(GuardMaintenanceService.class);

        trackService(HttpConnectionPoolService.class);
        registerService(ResponseHandler.class, new ResponseHandlerImpl(this));
        trackService(ResponseHandler.class);

        AuthenticationServiceRegistryImpl registry = new AuthenticationServiceRegistryImpl();
        registerService(AuthenticationServiceRegistry.class, registry);
        track(AuthenticationService.class, new AuthenticationTracker(context, registry));
        registerService(AuthenticationService.class, new PGPAuthenticationService());

        trackService(MimeEncryptionService.class);
        trackService(SmimeKeyService.class);

        openTrackers();

        Services.getService(GuardUpdateService.class).createMasterKeysIfNecessary();

        GuardConfigurationService guardConfigurationService = getService(GuardConfigurationService.class);

        //Intialize key cache task
        if (guardConfigurationService.getBooleanProperty(GuardProperty.rsaCache)) {
            GuardCipherService AES_CBC = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
            PopulateKeyCacheTask populateKeyCacheTask = new PopulateKeyCacheTask(guardConfigurationService,
                getService(KeyCacheStorage.class),
                AES_CBC,
                getService(MasterKeyService.class));
            TimerService timerService = getService(TimerService.class);
            this.scheduledKeyCacheTask = timerService.scheduleAtFixedRate(populateKeyCacheTask,
                0,
                guardConfigurationService.getIntProperty(GuardProperty.keyCacheCheckInterval),
                TimeUnit.SECONDS);
        }
        // Initializing daily cron jobs
        dailyJobScheduler = new DailyTaskScheduler(guardConfigurationService.getIntProperty(GuardProperty.cronHour));
        dailyJobScheduler.register(new CleanRemoteKeyCacheTask(guardConfigurationService.getIntProperty(GuardProperty.pgpCacheDays)));
        dailyJobScheduler.register(new CleanAuthenticationTokenTask(ConfigTools.parseTimespan(guardConfigurationService.getProperty(GuardProperty.authLifeTime))));
        dailyJobScheduler.register(new GuestCleanupTask(guardConfigurationService.getIntProperty(GuardProperty.guestCleanedAfterDaysOfInactivity),
            Services.getServiceLookup().getOptionalService(ActivityTrackingService.class),
            Services.getService(FileCacheStorage.class),
            Services.getService(GuardGuestEMailMetadataService.class)));
        int exposedKeyDurationInHours = guardConfigurationService.getIntProperty(GuardProperty.exposedKeyDurationInHours);
        if (exposedKeyDurationInHours > 0) {
            dailyJobScheduler.register(new ResetExposedKeysTask(exposedKeyDurationInHours));
        }

        //Registering the OX Guard servlets
        GuardServlets.registerServlets(this);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        LOG.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        HttpConnectionPoolService poolService = Services.optService(HttpConnectionPoolService.class);
        if (poolService != null) {
            poolService.closeAll();
        }
        if (scheduledKeyCacheTask != null) {
            scheduledKeyCacheTask.cancel();
        }
        if (dailyJobScheduler != null) {
            dailyJobScheduler.unregisterAll();
        }

        //Unregistering the OX Guard servlets
        GuardServlets.unregisterServlets(getService(HttpService.class));
        super.stopBundle();
    }
}
