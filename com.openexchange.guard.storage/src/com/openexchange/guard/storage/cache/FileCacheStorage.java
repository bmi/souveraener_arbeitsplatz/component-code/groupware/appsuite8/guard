/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.cache;

import java.util.List;
import com.openexchange.exception.OXException;

/**
 * {@link FileCacheStorage} provides access to stored FileCache items
 */
public interface FileCacheStorage {

    /**
     * Gets an item by id
     *
     * @param itemId the item id
     * @return the FileCacheItem for the given itemId
     * @throws OXException due an error
     */
    FileCacheItem getById(String itemId) throws OXException;

    /**
     * Gets an item by ID for a specific user
     *
     * @param itemId The item id
     * @param userId The ID of the user to fetch the item for
     * @return The FileCacheItem, or null if there is no such item for the given user
     * @throws OXException
     */
    FileCacheItem getByIdForUser(String itemId, int userId) throws OXException;

    /**
     * Gets a list of items for a given user
     * @param userId The user ID
     * @param cid The contextId
     * @param offset The offset from where to return the items (depends on the underlying sorting)
     * @param maxCount the maximum amount of returned items
     * @return A list of items for the given user which contains not more than <code>maxCount</code> items.
     */
    List<FileCacheItem> getForUser(int userId, int cid, int offset, int maxCount) throws OXException;

    /**
     * Creates a new FileCache item
     *
     * @param itemId the unique id to use for the new item
     * @param userId the owner of the item
     * @param the location of the item on the disk
     * @throws due an error
     */
    void insert(String itemId, int userId, String path) throws OXException;

    /**
     * Updates the last access of a FileCache item to "NOW"
     *
     * @param itemId the id of the item to update
     * @throws due an error
     */
    void updateLastDate(String itemId) throws OXException;

    /**
     * Deletes a FileItem from the cache
     *
     * @param idemId the id of the item to delete
     * @throws OXException due an error
     */
    void delete(String itemId) throws OXException;

    /**
     * Finds FileCache items which have not been accessed since x days
     *
     * @param days the amount of days
     * @return A list of items which have not been accessed since the given amount of days
     * @throws OXException due an error
     */
    List<FileCacheItem> findOld(int days) throws OXException;

    /**
     * Finds all filecache items for a given guest user
     * @param userId
     * @return
     * @throws OXException
     */
    List<FileCacheItem> findAllForUser (int userId, int cid) throws OXException;

}
