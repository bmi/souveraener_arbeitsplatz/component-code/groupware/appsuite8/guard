/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;

/**
 * {@link ContentTypeDetectionUtil} is a helper class for guessing the content-type of binary data.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class ContentTypeDetectionUtil {

    public final static int READ_SIZE = 10 * 1024;

    /**
     * Internal implementation of content-type guessing.
     *
     * @param data The data to guess the content-type for
     * @return The guessed content-type for the data, or null if it could not be guessed.
     */
    private static String getContentType(byte[] data) {
        final ContentInfoUtil util = new ContentInfoUtil();
        final ContentInfo contentInfo = util.findMatch(data);
        return contentInfo != null ? contentInfo.getMimeType() : null;
    }

    /**
     * Guesses the content-type of an {@link InputStream}.
     *
     * The inputStream must support the <code>mark</code> and <code>reset</code> methods.
     *
     * @param inputStream The {@link InputStream} to guess the content-type for.
     * @return The guessed content-type, or null if no bytes could be read from the stream
     * @throws UnsupportedOperationException if the given inputStream does not support <code>mark</code> and <code>reset</code> methods.
     * @throws IOException
     */
    public static String guessContentType(InputStream inputStream) throws IOException {

        if (!inputStream.markSupported()) {
            throw new UnsupportedOperationException("The input stream does not support mark and reset methods.");
        }
        byte[] bytes = new byte[READ_SIZE];
        inputStream.mark(READ_SIZE);
        final int numRead = inputStream.read(bytes);
        if (numRead < 0) {
            return null;
        }
        if (numRead < bytes.length) {
            // move the bytes into a smaller array
            bytes = Arrays.copyOf(bytes, numRead);
        }

        final String contentType = getContentType(bytes);
        inputStream.reset();
        return contentType;
    }
}
