/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailMover} moves a guest mail item in OX Guard to another folder
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailMover extends AbstractGuardAccess {

    /**
     * Internal method to create a move object
     *
     * @param mailIds The IDs of the guest E-Mails to move, or null to move all IDs in the specified folder
     * @param folderId The ID of the folder to move the guest E-Mails from (source)
     * @param newFolderId The ID of the folder to move the guest E-Mails to (destination)
     * @return The created move object ready to include in the request
     */
    private JSONObject createMove(String[] mailIds, String folderId, String newFolderId) {
        JSONObject jsonObject = new JSONObject();

        if(mailIds != null) {
            jsonObject.putSafe("mailIds", new JSONArray(Arrays.asList(mailIds)));
        }

        if(folderId != null) {
            jsonObject.putSafe("folderId", folderId);
        }

        jsonObject.putSafe("newFolderId", newFolderId);
        return jsonObject;
    }

    /**
     * Internal method to move a set of messages to another folder
     *
     * @param session The session
     * @param authToken The Guard authentication token
     * @param mailIds The IDs of the messages to move
     * @param folderId The folder to move the messages from (source)
     * @param newFolderId The folder to move the messages to (destination)
     * @throws OXException
     */
    private void moveMessagesInternal(Session session, GuardAuthenticationToken authToken, String[] mailIds, String folderId, String newFolderId) throws OXException {

        final GuardApiImpl guardGuestApi = requireGuardApi(GUEST_ENDPOINT);
        final JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
        final JSONObject json = new JSONObject()
            .putSafe("auth", authJson)
            .putSafe("move", createMove(mailIds, folderId, newFolderId));
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        try (InputStream result = guardGuestApi.processResource(GuardApis.mapFor("action", "movemessages"),
            jsonStream,
            null,
            "json",
            session)) {
            //no-op
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Moves a set of messages to another folder
     *
     * @param session The session
     * @param authToken The Guard authentication token
     * @param mailIds The IDs of the messages to move
     * @param newFolderId The folder to move the messages to
     * @throws OXException
     */
    public void moveMessages(Session session, GuardAuthenticationToken authToken, String[] mailIds, String newFolderId) throws OXException {
        final String folderId = null; //destination not needed because we move specific mails by ID
        moveMessagesInternal(session,authToken,mailIds,folderId, newFolderId);
    }

    /**
     * Moves all messages from a folder to another
     *
     * @param session The session
     * @param authToken The Guard authentication token
     * @param folderId The source folder
     * @param newFolderId The destination folder
     * @throws OXException
     */
    public void moveAllMessages(Session session, GuardAuthenticationToken authToken, String folderId, String newFolderId) throws OXException {
        final String[] mailIDs = null; //Mail IDs not needed because we move all mails from the folder to another
        moveMessagesInternal(session, authToken, mailIDs, folderId, newFolderId);
    }
}
