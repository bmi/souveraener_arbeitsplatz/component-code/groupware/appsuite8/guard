/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database;

import java.sql.Connection;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.dao.SchemaCandidate;

/**
 * The {@link DatabaseMaintenanceService} handles everything related to the creation of databases (existence, register in configdb, adding tables, ...)
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public interface DatabaseMaintenanceService {

    /**
     * Registers the schema (provided by the given {@link SchemaCandidate}) as a database within the configdb. If no slave URL is provided within the {@link SchemaCandidate} only master setup will be registered.
     * 
     * @param candidate {@link SchemaCandidate} instance holding all relevant information about master/slave handling
     * @throws OXException
     */
    void register(SchemaCandidate candidate) throws OXException;

    /**
     * Creates the schema based on the given information encapsulated within the {@link SchemaCandidate}.<br>
     * <br>
     * Note: This happens without prior checking of schema existence (see com.openexchange.guard.database.DatabaseMaintenanceService.exists(Connection, String))
     * 
     * @param candidate {@link SchemaCandidate} instance holding all relevant information about master/slave handling
     * @throws OXException
     */
    void createSchema(SchemaCandidate candidate) throws OXException;

    /**
     * Checks if the schema with the given name already exists.
     * 
     * @param connection The connection that should be used to check the existence
     * @param schemaName The name of the schema the existence should be checked for
     * @return boolean <code>true</code>, if the schema already exists; otherwise <code>false</code>
     * @throws OXException
     */
    boolean exists(Connection connection, String schemaName) throws OXException;

    /**
     * Creates schemas provided as {@link SchemaCandidate}s if they do not exist.<br>
     * <br>
     * Master/Slave schemas have to be created in one action. If no slave URL is provided within the {@link SchemaCandidate} only master setup will be registered.
     * 
     * @param candidates {@link List} of all {@link SchemaCandidate}s that should be created if not existent and registered within the configdb
     * @throws OXException if the schema already exists or there was an error while creation/registration
     */
    void createAndRegisterSchemaIfNotExistent(SchemaCandidate... candidates) throws OXException;
}
