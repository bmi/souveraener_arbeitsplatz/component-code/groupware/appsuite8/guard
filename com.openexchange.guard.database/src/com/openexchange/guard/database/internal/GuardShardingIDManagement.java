/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;

/**
 * {@link GuardShardingIDManagement}  Class for keeping guest shard Id autoincrements up to date and
 * correct
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.3
 */
public class GuardShardingIDManagement {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(GuardShardingIDManagement.class);

    GuardDatabaseService guardDatabaseService;
    public GuardShardingIDManagement (GuardDatabaseService guardDatabaseService) {
        this.guardDatabaseService = guardDatabaseService;
    }

    private static final String SELECT_HIGHEST_GUEST_USERID_STMT = "SELECT MAX(ID) FROM og_email WHERE cid < 0";
    private static final String ALTER_AUTOINCREMENT_STMT = "ALTER TABLE `og_KeyTable` AUTO_INCREMENT=?";
    private static final String GET_AUTOINCREMENT_STMT = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ? AND TABLE_NAME = 'og_KeyTable';";
    private static final String LOCK_TABLE = "LOCK TABLES `og_KeyTable` WRITE;";
    private static final String UNLOCK = "UNLOCK TABLES";

    /**
     * Lock to og_KeyTable while autoincrement is being set.
     *
     * @param con  Database connection
     * @throws OXException
     */
    private void lockTables (Connection con) throws OXException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(LOCK_TABLE);
            stmt.execute();
        } catch (SQLException e) {
                throw GuardDatabaseExceptionCodes.DB_ERROR.create(e,e.getMessage());
            } finally {
                DBUtils.closeSQLStuff(stmt);
            }
    }

    /**
     * Unlock tables
     *
     * @param con  Database connection
     * @throws OXException
     */
    private void unLockTables (Connection con) throws OXException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UNLOCK);
            stmt.execute();
        } catch (SQLException e) {
                throw GuardDatabaseExceptionCodes.DB_ERROR.create(e,e.getMessage());
            } finally {
                DBUtils.closeSQLStuff(stmt);
            }
    }

    /**
     * Update the shard's autoincrement value to the highest Guest Id
     *
     * @param shardId
     * @throws OXException
     */
    public void updateAutoIncrement (int shardId) throws OXException {
        Connection connection = guardDatabaseService.getWritableForShard(shardId);
        try {
            // First, lock the og_KeyTable.  The getMaxGuest id may take a moment, and we don't
            // want other writes
            lockTables(connection);
            // Update the autoincrement with max Guest + 1
            updateAutoIncrement(connection, shardId, getMaxGuestId() + 1);
        } finally {
            try {
                // Unlock
                unLockTables(connection);
            } catch (OXException e) {
                LOG.error("Error unlocking Guest shard og_KeyTable ", e);
            }
            guardDatabaseService.backReadOnlyForGuard(connection);
        }

    }

    /**
     * Gets the highest Guest Id from the og_email table
     *
     * @return Highest guest ID
     * @throws OXException
     */
    private int getMaxGuestId () throws OXException {
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(SELECT_HIGHEST_GUEST_USERID_STMT);
            resultSet = stmt.executeQuery();

            int max = 1;
            if (resultSet.next()) {
                max = resultSet.getInt(1);
            }
            return max;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /**
     * Update the autoincrement value of the og_KeyTable for a shard
     *
     * @param shardId  Id of the shard
     * @param value  New value
     * @throws OXException
     */
    private void updateAutoIncrement(Connection connection, int shardId, int value) throws OXException {
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(ALTER_AUTOINCREMENT_STMT);
            stmt.setInt(1, value);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

}
