/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.smime;

import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import com.openexchange.annotation.Nullable;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;

/**
 * {@link SmimeKeyService} handles {@link SmimeKeys}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface SmimeKeyService {

    /**
     * Checks if a public key exists
     *
     * @param serial The ID of the public key to check
     * @param email The email
     * @param fromDate The date that the key was used (from date on email, or current date if actively added)
     * @return <code>True</code> if the public key exists, <code>False</code> otherwise
     * @throws OXException
     */
    boolean existsPublicKey(String serial, String email, Date fromDate) throws OXException;

    /**
     * Gets a list of all full {@link SmimeKeys} for a given user
     *
     * @param userId The user ID to get the keys for
     * @param contextId The context ID
     * @return A list of all full {@link SmimeKeys} for the given user
     * @throws OXException
     */
    List<SmimeKeys> getKeys(int userId, int contextId) throws OXException;

    /**
     * Gets a list of all full {@link SmimeKeys} associated with a given email address
     *
     * @param email The email address to get all {@link SmimeKeys} for
     * @return A list of {@link SmimeKeys} for the given email address
     * @throws OXException
     */
    List<SmimeKeys> getKeys(String email) throws OXException;

    /**
     * Gets a list of all {@link SmimeKeys}, only containing the public key/certificate
     *
     * @return A list of all {@link SmimeKeys} with the private key information being omitted
     * @throws OXException
     */
    List<SmimeKeys> getPublicKeys() throws OXException;

    /**
     * Gets a list of {@link SmimeKeys}, only containing the public key/certificate, associated with a given email address
     *
     * @param email The email address to get the public {@link SmimeKeys} for
     * @return A list of {@link SmimeKeys} for the given email address, with the private key information being omitted
     * @throws OXException
     */
    List<SmimeKeys> getPublicKeys(String email) throws OXException;

    /**
     * Get the {@link SmimeKeys} associated with the user and with specified serial number
     *
     * @param serial serial number represented as string
     * @param userId The ID of the user
     * @param cid The ID of the context
     * @return The {@link SmimeKeys} with the given serial ID for the given user, or null if no such key was found
     * @throws OXException
     */
    public SmimeKeys getKey(String serial, int userId, int cid) throws OXException;

    /**
     * Gets a list of {@link SmimePrivateKeys} for a given user
     *
     * @param userId The user ID to get the keys for
     * @param contextId The context ID
     * @return A list of all {@link SmimePrivateKeys} for the given user
     * @throws OXException
     */
    public List<SmimePrivateKeys> getPrivateKeys(int userId, int cid) throws OXException;

    /**
     * Get the {@link SmimePrivateKeys} associated with the user and with specified serial number
     *
     * @param serial serial number represented as string
     * @param userId The ID of the user
     * @param cid The ID of the context
     * @return The {@link SmimePrivateKeys} with the given serial ID for the given user, or null if no such key was found
     * @throws OXException
     */
    public SmimePrivateKeys getPrivateKey(String serial, int userId, int cid) throws OXException;

    /**
     * Get the full {@link SmimeKeys} which is marked as "current" for the given user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return The "current" {@link SmimeKeys} for the given user, or <code>null</code> if no keys are present
     * @throws OXException
     */
    public SmimeKeys getCurrentKey(int userId, int cid) throws OXException;

    /**
     * Get only the {@link SmimePrivateKeys} which is marked as "current" for the given user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return The "current" {@link SmimePrivateKeys} for the given user, or <code>null</code> if no keys are present
     * @throws OXException
     */
    public SmimePrivateKeys getCurrentPrivateKey(int userId, int cid) throws OXException;

    /**
     * Make the specified key marked as "current" for the given user
     *
     * @param serial The unique serial ID of the key which should be marked as "current"
     * @param userId The user identifier
     * @param cid The context identifier
     * @throws OXException
     */
    public void setCurrentKey(String serial, int userId, int cid) throws OXException;

    /**
     * Creates, but does not store, a new {@link SmimeKeys} using the given X509 certificate, raw {@link PrivateKey}, password, and user info.
     *
     * @param certificate Public key certificate to use
     * @param certificates The additional certificates
     * @param privateKey Private key to use
     * @param password Password used to encrypt the private key
     * @param userId The user identifier
     * @param cid The context identifier
     * @param current If the key is marked current
     * @return The new {@link SmimePrivateKeys}, not yet persistently stored
     * @throws OXException
     */
    public SmimeKeys createKey(X509Certificate certificate, List<X509Certificate> certificates, PrivateKey privateKey, String password, int userId, int cid, boolean current) throws OXException;

    /**
     * Stores the given {@link SmimeKeys}
     *
     * @param keys The key to store
     * @param userId The user identifier
     * @param cid The context identifier
     * @param replaceIfPresent Replace the key if already present with updated key or throw error
     * @throws OXException
     */
    public void storeKey(SmimeKeys keys, int userId, int cid, boolean replaceIfPresent) throws OXException;

    /**
     * Stores only the public part of the given {@link SmimeKeys}
     *
     * @param keys The key to store the public part of
     * @param userId The user identifier
     * @param cid The context identifier
     * @param replaceIfPresent Replace the key if already present with updated key or throw error
     * @throws OXException
     */
    public void storePublicKey(int userId, int cid, SmimeKeys keys, Date fromDate) throws OXException;

    /**
     * Deletes as specific {@link SmimeKeys} for a give user
     *
     * @param serial The serial ID of the certificate for which the keys should be deleted
     * @param userId The user's id
     * @param contextId The context ID
     * @param password The password of the certificate's private key
     * @throws OXException
     */
    void deleteKey(String serial, int userId, int contextId, String password) throws OXException;

    /**
     * Deletes all keys ({@link SmimeKeys} and corresponding {@link SmimePrivateKeys} for a given user
     *
     * @param userId The user's id
     * @param contextId The context ID
     * @throws OXException
     */
    void deleteKeys(int userId, int contextId) throws OXException;

    /**
     * Deletes all keys ({@link SmimeKeys} and corresponding {@link SmimePrivateKeys} within a given context
     *
     * @param contextId The context ID
     * @throws OXException
     */
    void deleteKeys(int contextId) throws OXException;

    /**
     * Imports a new certificate including it's keys
     *
     * @param data The data stream to import the certificate from
     * @param userId The user's ID
     * @param contextId The context ID
     * @param password The password of the certificate's private key
     * @param newPassword The new password to set while importing
     * @return A parsed and imported instance of {@link SmimeKeys}
     * @throws OXException
     */
    SmimeKeys importKey(InputStream data, int userId, int contextId, String password, String newPassword) throws OXException;

    /**
     * Exports a specified key as PKCS12 binary data
     *
     * @param serial The serial The unique serial ID of the key to export, or <code>null</code> in order to export the "current" key
     * @param userId The user's ID
     * @param contextId The context ID
     * @param password The password of the certificate's private key
     * @param newPassword The new password used to protect the export
     * @return An {@link SmimeKeysExport} object containing the PKCS12 data
     * @throws OXException
     */
    SmimeKeysExport exportKey(@Nullable String serial, int userId, int contextId, String password, String newPassword) throws OXException;

    /**
     * Update the recovery data for the key
     * 
     * <p>
     * This is a convenience method for calling {@link #updateRecovery(SmimePrivateKeys, String, int, int)} with
     * {@link SmimeKeys#getPrivateKey()} as key argument.
     * </P>
     * @param key The {@link SmimeKeys} to update it's {@link SmimePrivateKeys} recovery
     * @param password The password of the private key
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the included {@link SmimePrivateKeys} instance was updated <code>False</code>, otherwise
     * @throws NullPointerException if the given {@link SmimeKeys} does not contain a {@link SmimePrivateKeys} to update the recovery for
     * @throws OXException
     */
    boolean updateRecovery(SmimeKeys key, String password, int userId, int cid) throws OXException;

    /**
     * Update the recovery data for the {@link SmimePrivateKey}
     *
     * @param key The {@link SmimePrivateKey} to update
     * @param password The password of the private key
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the {@link SmimePrivateKeys} instance was updated <code>False</code>, otherwise
     * @throws OXException
     */
    boolean updateRecovery(SmimePrivateKeys privateKey, String password, int userId, int cid) throws OXException;

    /**
     * Change the password of a {@link SmimePrivateKey} key using recovery
     *
     * @param key The {@link SmimePrivateKey} to change
     * @param newpass The new password to use
     * @param userId The user identifier
     * @param cid The context identifier
     * @return <code>True</code> if the given {@link SmimePrivateKeys} instance was updated <code>False</code>, otherwise
     * @throws OXException
     */
    boolean changePasswordWithRecovery(SmimePrivateKeys key, String newpass, int userId, int cid) throws OXException;
}
