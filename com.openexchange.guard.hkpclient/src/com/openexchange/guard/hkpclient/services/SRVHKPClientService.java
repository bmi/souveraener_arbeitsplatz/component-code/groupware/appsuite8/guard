
package com.openexchange.guard.hkpclient.services;

import java.util.Collection;
import java.util.Collections;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.SRVRecord;
import com.openexchange.exception.OXException;
import com.openexchange.guard.dns.DNSResult;
import com.openexchange.guard.dns.DNSService;
import com.openexchange.guard.dns.ValidationResult;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkpclient.client.HKPClient;
import com.openexchange.guard.hkpclient.client.HKPClient.TLSMODE;
import com.openexchange.guard.hkpclient.osgi.Services;
import com.openexchange.guard.hkpclient.util.TimeTracker;
import com.openexchange.guard.inputvalidation.DomainNameValidator;
import com.openexchange.server.ServiceLookup;

/**
 *
 * {@link SRVHKPClientService} Service for querying DNS SRV configured HKP Servers
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class SRVHKPClientService implements HKPClientService {

    private static final Logger logger = LoggerFactory.getLogger(SRVHKPClientService.class);
    private static final Collection<PGPPublicKeyRing> EMPTY_LIST = Collections.emptyList();
    private static final String HKPS_SERVICE_NAME = "_hkps._tcp.";
    private static final String HKP_SERVICE_NAME = "_hkp._tcp.";
    private final HKPClientService delegate;
    private final boolean forceTLS;
    private final ServiceLookup services;

    private class QueryResult {

        private final Collection<PGPPublicKeyRing> result;
        private final ValidationResult validationResult;

        public QueryResult(Collection<PGPPublicKeyRing> result, ValidationResult validationResult) {
            this.result = Collections.unmodifiableCollection(result);
            this.validationResult = validationResult;
        }

        public Collection<PGPPublicKeyRing> getResult() {
            return result;
        }

        public ValidationResult getValidationResult() {
            return validationResult;
        }
    }

    /**
     *
     * Initializes a new {@link SRVHKPClientService}.
     */
    public SRVHKPClientService(ServiceLookup services) {
        this(null, false, services);
    }

    /**
     *
     * Initializes a new {@link SRVHKPClientService}.
     *
     * @param delegate The service to delegate search queries to in a public key ring was not found
     * @param foceTLS True to only allow TLS connection to the HKP Server, false otherwise
     */
    public SRVHKPClientService(HKPClientService delegate, boolean forceTLS, ServiceLookup services) {
        this.delegate = delegate;
        this.forceTLS = forceTLS;
        this.services = services;
    }

    /**
     * Internal method to determine the TLS mode.
     * <br>
     * TLS is used if forced by the caller, or if the <code>serviceName</code> parameter is equals {@link #HKPS_SERVICE_NAME}
     *
     * @param serviceName The service name used to query keys
     * @return True if the usage of TLS is mandatory, false if not mandatory.
     */
    private boolean isForceTLS(String serviceName) {
        if (forceTLS || (serviceName != null && serviceName.equals(HKPS_SERVICE_NAME))) {
            return true;
        }
        return false;
    }

    /**
     * Internal method to query HKP servers which were configured in the email-domain's DNS SRV record
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email the email to query
     * @return a list of found PGP key rings for the given email, an empty collection if no keys were found or the email's domain has no HKP SRV set
     * @throws Exception
     */
    private QueryResult findForSRVRecords(String serviceName, String clientToken, String email, int userId, int cid, int timeout) throws Exception {
        DNSService dnsService = Services.getService(DNSService.class);
        DNSResult result = dnsService.getSrvs(serviceName, email, userId, cid);

        Collection<PGPPublicKeyRing> rings;
        int current = 0;
        SRVRecord nextcheck;
        while ((nextcheck = dnsService.findNext(current, result.getResultRecords())) != null) {
            String target = nextcheck.getTarget().toString();
            if (target.endsWith(".")) {
                target = target.substring(0, target.length() - 1);
            }
            if (new DomainNameValidator().isValid(target) && !DomainNameValidator.isLocalAddress(target)) {  // Confirm target is a proper domain name
                int port = nextcheck.getPort();
                rings = new HKPClient(clientToken, isForceTLS(serviceName) ? TLSMODE.TLS : TLSMODE.BY_PORT, services).findKeys(target, port, email, timeout);
                if (rings != null) {
                    return new QueryResult(rings, result.getValidationResult());
                }
            }
            current = nextcheck.getPriority();
        }
        return new QueryResult(EMPTY_LIST, null);
    }

    /**
     * Internal method to query SRV records
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email The email
     * @return A list of public keys for the email
     * @throws Exception
     */
    private Collection<RemoteKeyResult> findAllInternal(String clientToken, String email, int userId, int cid, int timeout) throws Exception {

        TimeTracker timer = new TimeTracker(timeout);
        //Searching DNS SRV records
        try {
            QueryResult result = findForSRVRecords(HKPS_SERVICE_NAME, clientToken, email, userId, cid, timeout);
            if (result.getResult().isEmpty()) {
                result = findForSRVRecords(HKP_SERVICE_NAME, clientToken, email, userId, cid, timeout);
            }

            if (result != null && result.getResult() != null && result.getResult().size() > 0) {
                logger.debug("Remote keys found through SRV");
                return RemoteKeyResult.createCollectionFrom(result.getResult(),
                    result.getValidationResult() != null && result.getValidationResult()
                        .isValidated() ? HKPKeySources.SRV_HKP_DNSSEC_REMOTE_SERVER : HKPKeySources.SRV_HKP_REMOTE_SERVER);
            }
        } catch (Exception e) {
            logger.error("Error querying remote HKP server form SRV record", e);
        }
        if (delegate != null) {
            return delegate.find(clientToken, email, userId, cid, timer.getRemaining());
        }
        return null;
    }

    /**
     * Internal method to find key by id
     * Searches based on the from email domain and queries for the keyid
     *
     * @param clientToken
     * @param id
     * @param email
     * @return
     * @throws Exception
     */
    private RemoteKeyResult findInternal(String clientToken, Long id, String email, int userId, int cid, int timeout) throws Exception {
        // To do SRV lookup, we have to search by email address to get domain
        TimeTracker timer = new TimeTracker(timeout);
        if (email != null) {
            Collection<RemoteKeyResult> res = findAllInternal(clientToken, email, userId, cid, timeout);
            if (res != null && !res.isEmpty()) {  //If results, then search all to see if has the key by id
                for (PGPPublicKeyRing ring : RemoteKeyResult.getRingsFrom(res)) {
                    PGPPublicKey found = ring.getPublicKey(id);
                    if (found != null) {
                        return new RemoteKeyResult(ring, HKPKeySources.SRV_HKP_REMOTE_SERVER);
                    }
                }
            }
        }
        RemoteKeyResult result = null;
        if (delegate != null) {
            result = delegate.find(clientToken, email, id, userId, cid, timer.getRemaining());
        }
        return result;

    }

    @Override
    public Collection<RemoteKeyResult> find(String clientToken, String email, int userId, int cid, int timeout) throws OXException {
        try {
            return findAllInternal(clientToken, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public RemoteKeyResult find(String clientToken, String email, Long id, int userId, int cid, int timeout) throws OXException {
        try {
            return findInternal(clientToken, id, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }
}
