
package com.openexchange.guard.hkpclient.services;

import java.util.Collection;
import com.openexchange.exception.OXException;

/**
 * {@link HKPClientService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public interface HKPClientService {

    /**
     * Tries to find all public PGP keys for the given email
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email The email to get the public keys for
     * @param id The ID of the key
     * @param userId ID of the sender
     * @param timeout Procedure should timeout after specified milliseconds
     * @return A list of public keys found for the given email
     * @throws OXException
     */
    public Collection<RemoteKeyResult> find(String clientToken, String email, int userId, int cid, int timeout) throws OXException;

    /**
     * Tries to find a specific key by email and ID
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email The email to get the public keys for
     * @param id The ID of the key
     * @param userId ID of the sender
     * @param cid the Context of the sender
     * @param timeout Procedure should timeout after specified milliseconds
     * @return The public key for the given email and id
     * @throws OXException
     */
    public RemoteKeyResult find(String clientToken, String email, Long id, int userId, int cid, int timeout) throws OXException;

}
