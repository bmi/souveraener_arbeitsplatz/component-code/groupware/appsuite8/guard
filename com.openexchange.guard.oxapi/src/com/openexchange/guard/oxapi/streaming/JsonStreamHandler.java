/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import org.apache.commons.fileupload.util.Streams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * Processes a JSON stream
 */
public class JsonStreamHandler implements StreamHandler {

    private final JsonObject additionalObject;
    private final String additionalObjectName;

    /**
     * Constructor
     */
    public JsonStreamHandler(){
        this.additionalObject = null;
        this.additionalObjectName = null;
    }

    /**
     * Constructor
     * @param additionalObjectName The name of the JSON object to add to the streams' JSON
     * @param additionalObject The JSON object to add to the stream's JSON
     */
    public JsonStreamHandler(String additionalObjectName,
                             JsonObject additionalObject) {
        this.additionalObject = additionalObject;
        this.additionalObjectName = additionalObjectName;
    }

    @Override
    public void processStream(InputStream inputStream,
        OutputStream outputStream,
        int suggestedBufferSize) throws IOException {

        //Reading the JSON from the input stream and
        Gson gson = new GsonBuilder().create();
        String jsonString = Streams.asString(inputStream);
        JsonObject json = gson.fromJson(jsonString, JsonObject.class);

        //Adding additional object to the JSON
        if (additionalObject != null &&
            additionalObjectName != null) {
            json.add(additionalObjectName, additionalObject);
        }

        //Writing the JSON to the output stream
        outputStream.write(
            json.toString().getBytes(Charset.defaultCharset()));
    }

    @Override
    public String getNewContentType(String oldContentType, String fileName) {
        return oldContentType;
    }

    @Override
    public String getNewName(String oldName) {
        return oldName;
    }

}
;