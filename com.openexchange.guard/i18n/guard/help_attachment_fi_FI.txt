Tämän sähköpostin lähettämisessä käytettiin PGP-salausta. Käytä salaukseen ja sähköpostin hallintaan tarkoitettua webmail-järjestelmää lukeaksesi tämän sähköpostin. 

Jos et tiedä kuinka kirjoittautua webmail-sähköpostiisi, kokeile tätä linkkiä %webmail%



