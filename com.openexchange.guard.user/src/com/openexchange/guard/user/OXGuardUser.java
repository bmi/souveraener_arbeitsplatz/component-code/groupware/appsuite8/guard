/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user;

/**
 * {@link OXGuardUser} represents OX Guard related user information
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class OXGuardUser {

    private final int id;
    private final int contextId;

    /**
     * Initializes a new {@link OXGuardUser}.
     *
     * @param id The user ID
     * @param contextId The context ID
     */
    public OXGuardUser(int id, int contextId) {
        this.id = id;
        this.contextId = contextId;
    }

    /**
     * Gets the user's id
     *
     * @return The user's id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the context ID
     *
     * @return The context ID
     */
    public int getContextId() {
        return contextId;
    }

    /**
     * Checks whether the user is an OX Guard guest or not
     *
     * @return True, if the user is an OX Guard guest, false if a regular Guard user.
     */
    public boolean isGuest() {
        //Guest have negative context ID
        return contextId < 0;
    }
}
