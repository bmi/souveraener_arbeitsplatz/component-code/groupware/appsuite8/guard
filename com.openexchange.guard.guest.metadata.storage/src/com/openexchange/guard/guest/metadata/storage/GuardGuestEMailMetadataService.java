/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage;

import java.util.Collection;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;

/**
 * {@link GuardGuestEMailMetadataService} provides access to the meta data storage of OX Guard guest E-Mails.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface GuardGuestEMailMetadataService {

    /**
     * Gets the {@link GuardGuestEmailMetadata} for a Guest E-Mail
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param id The ID of the Guest E-Mail to get the meta data for
     * @return The {@link GuardGuestEmailMetadata} for the specified ID or null, if no meta data could be found for the given ID.
     */
    GuardGuestEmailMetadata get(int contextId, int userId, String id) throws OXException;

    /**
     * Gets all {@link GuardGuestEmailMetadata} from a specific folder
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param folderId The ID of the folder to get the items for
     * @return A collection of items for the given guest and folder
     * @throws OXException
     */
    Collection<GuardGuestEmailMetadata> getForFolder(int contextId, int userId, String folderId) throws OXException;

    /**
     * Inserts the {@link GuardGuestEmailMetadata} for a guest E-Mail
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param guestEmailMetaData The {@link GuardGuestEmailMetadata} to insert.
     * @throws OXException
     */
    void insert(int contextId, int userId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException;

    /**
     * Updates the message flags of the given item
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param itemId The ID of the given item to update
     * @param messageFlags The message flags to set
     */
    void updateMessageFlags(int contextId, int userId, String itemId, int messageFlags) throws OXException;

    /**
     * Updates the color label of the the given item
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param itemId The item to update
     * @param colorLabel The new color label
     * @throws OXException
     */
    void updateColorLabel(int contextId, int userId, String itemId, int colorLabel) throws OXException;

    /**
     * Updates the folder ID of the given item
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest related to the item
     * @param itemId The item to update
     * @param folderId The new folder Id
     * @throws OXException
     */
    void updateFolderId(int contextId, int userId, String itemId, String folderId) throws OXException;

    /**
     * Updates the folder ID of ALL messages of a guest related to the given folder
     *
     * @param contextId The context ID of the guest related to the item
     * @param userId The ID of the guest
     * @param folderId The ID of the folder containing the items to update
     * @param newFolderId The new folder ID to set for the items
     * @throws OXException
     */
    void updateAllFolderIdsInFolder(int contextId, int userId, String folderId, String newFolderId) throws OXException;

    /**
     * Deletes the {@link GuardGuestEmailMetadata} from the storage
     *
     * @param shardId The ID of the guest shard which contains the item to delete
     * @param guestEmailMetaData The item to delete
     * @throws OXException
     * @return The number of deleted entries
     */
    int delete(int shardId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException;

    /**
     * Deletes all meta data associated with the Guest user
     * @param userId
     * @param contextId
     * @return
     * @throws OXException
     */
    int deleteUserMetaData (int userId, int contextId) throws OXException;

    /**
     * Creates a GuardGuestEmailMetaData from message
     * @param itemId
     * @param folderId
     * @param message
     * @param defaultMessageFlags
     * @return
     * @throws MessagingException
     */
    GuardGuestEmailMetadata createMetaDataFrom(String itemId, String folderId, MimeMessage message, int defaultMessageFlags) throws MessagingException;
}
