/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.certificatemanagement.actions;

import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.MANDATORY;
import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.PARAM_EMAIL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.certificatemanagement.RecipCertificateService;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.certificatemanagement.responses.RecipCertificateResponse;

/**
 * {@link GetRecipCertificateAction} searches for other user's S/MIME certificates
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class GetRecipCertificateAction extends GuardServletAction {

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        //Email parameter
        final String email = ServletUtils.getStringParameter(request, PARAM_EMAIL, MANDATORY);
        Services.getService(GuardRatifierService.class).validate(email);

        //@formatter:off
        //Search certificate by email
        final RecipCertificate recipCertificate = Services.getService(RecipCertificateService.class).getRecipCertificate(
            userSession.getUserId(), 
            userSession.getContextId(), 
            email);
        //@formatter:on
        
        ServletUtils.sendObject(response, recipCertificate != null ? new RecipCertificateResponse(recipCertificate) : null);
    }
}