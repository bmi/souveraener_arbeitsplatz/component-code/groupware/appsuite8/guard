/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.util;

import java.util.Iterator;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link PGPUtil}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public final class PGPUtil {

    /**
     * Decodes the {@link PGPPrivateKey} from the given {@link PGPSecretKey} using a password.
     *
     * @param secretKey The secret key to decode the private key from
     * @param password The password
     * @return The decoded private key
     * @throws PGPException
     */
    public static PGPPrivateKey decodePrivate(PGPSecretKey secretKey, String password) throws PGPException {
        return decodePrivate(secretKey, password, null);
    }

    /**
     * Salts a password with the given salt
     *
     * @param password The password
     * @param salt The salt
     * @return The salted password
     */
    public static String saltPassword(String password, String salt) {
        char[] pw = salt == null ? password.toCharArray() : CipherUtil.getSHA(password, salt).toCharArray();
        return new String(pw);
    }

    /**
     * Decodes the {@link PGPPrivateKey} from the given {@link PGPSecretKey} using a salted password.
     *
     * @param secretKey The secret key to decode the private key from
     * @param password The password
     * @param salt The salt
     * @return The decoded private key
     * @throws PGPException
     */
    public static PGPPrivateKey decodePrivate(PGPSecretKey secretKey, String password, String salt) throws PGPException {
        char[] pw = salt == null ? password.toCharArray() : CipherUtil.getSHA(password, salt).toCharArray();
        PBESecretKeyDecryptor extractor = new BcPBESecretKeyDecryptorBuilder(new BcPGPDigestCalculatorProvider()).build(pw);
        return secretKey.extractPrivateKey(extractor);
    }

    /**
     * Decodes a {@link PGPPrivateKey} from the given {@link PGPSecretKeyRing} using a password.
     *
     * It is preferred to not return the master. Only return master if no other keys found.
     *
     * @param PGPSecretKeyRing The secret key ring to decode a private key from.
     * @param password The password
     * @return The decoded private key
     * @throws PGPException
     */
    public static PGPPrivateKey decodePrivate(PGPSecretKeyRing secretKeyRing, String password) throws PGPException {
        return decodePrivate(secretKeyRing, password, null);
    }

    /**
     * Decodes a {@link PGPPrivateKey} from the given {@link PGPSecretKeyRing} using a salted password.
     *
     * It is preferred to not return the master. Only return master if no other keys found.
     *
     * @param PGPSecretKeyRing The secret key ring to decode a private key from.
     * @param password The password
     * @param salt The salt
     * @return The decoded private key
     * @throws PGPException
     */
    public static PGPPrivateKey decodePrivate(PGPSecretKeyRing secretKeyRing, String password, String salt) throws PGPException {
        PGPSecretKey sec_key = null;
        if (secretKeyRing == null || password == null || salt == null) {
            return null;
        }
        Iterator<PGPSecretKey> it = secretKeyRing.getSecretKeys();
        PGPSecretKey master = null;
        while (sec_key == null && it.hasNext()) {
            PGPSecretKey key = it.next();
            if (!key.isMasterKey()) { // We prefer to not return the master.  Only return master if no other encr keys found
                return decodePrivate(key, password, salt);
            } else {
                master = key;
            }
        }
        if (master != null) {
            return decodePrivate(master, password, salt);
        }
        return null;
    }

    /**
     * Verifies the password for the given {@link PGPSecretKey}
     *
     * @param secretKey The secret key to verify the password against.
     * @param password The password
     * @return True if able to decode the secret key using the given password, false otherwise
     */
    public static boolean verifyPassword(PGPSecretKey secretKey, String password) {
        return verifyPassword(secretKey, password, null);
    }

    /**
     * Verifies the password and salt for the given {@link PGPSecretKey}
     *
     * @param secretKey The secret key to verify the password against.
     * @param password The password
     * @param salt The salt
     * @return True if able to decode the secret key using the given password and salt, false otherwise
     */
    public static boolean verifyPassword(PGPSecretKey secretKey, String password, String salt) {
        try {
            decodePrivate(secretKey, password, salt);
            return true;
        } catch (PGPException e) {
            return false;
        }
    }

    /**
     * Verifies the password for the given {@link PGPSecretKeyRing}
     *
     * @param secretKeyRing The secret key ring to verify the password against.
     * @param password The password *
     * @return True if able to decode a secret key from the key ring using the given password, false otherwise
     */
    public static boolean verifyPassword(PGPSecretKeyRing secretKeyRing, String password) {
        return verifyPassword(secretKeyRing, password, null);
    }

    /**
     * Verifies the password and salt for the given {@link PGPSecretKeyRing}
     *
     * @param secretKeyRing The secret key ring to verify the password against.
     * @param password The password
     * @param salt The salt
     * @return True if able to decode a secret key from the key ring using the given password and salt, false otherwise
     */
    public static boolean verifyPassword(PGPSecretKeyRing secretKeyring, String password, String salt) {
        try {
            return decodePrivate(secretKeyring, password, salt) != null;
        } catch (PGPException e) {
            return false;
        }
    }

    public static boolean verifyPassword(GuardKeys key, String password) {
        key = Objects.requireNonNull(key, "key must not be null");
        return verifyPassword(key.getPGPSecretKeyRing(), password, key.getSalt());
    }

    /**
     * Returns the signature type of the specified signature identifier
     *
     * @param signature The signature identifier
     * @return The signature type as string
     */
    public static String getSignatureType(int signature) {
        switch (signature) {
            case -1:
                return ("Fail");
            case 0:
                return ("Missing Public key");
            case PGPSignature.POSITIVE_CERTIFICATION:
                return ("Positive");
            case PGPSignature.CASUAL_CERTIFICATION:
                return ("Casual");
            case PGPSignature.CERTIFICATION_REVOCATION:
                return ("Revoke Cert");
            case PGPSignature.DEFAULT_CERTIFICATION:
                return ("Default Cert");
            case PGPSignature.DIRECT_KEY:
                return ("Direct");
            case PGPSignature.KEY_REVOCATION:
                return ("Revoke");
            case PGPSignature.SUBKEY_REVOCATION:
                return ("Sub revoke");
            case PGPSignature.SUBKEY_BINDING:
                return ("Subkey Binding");
            case PGPSignature.PRIMARYKEY_BINDING:
                return ("Primary Binding");
        }
        return ("");
    }
}
