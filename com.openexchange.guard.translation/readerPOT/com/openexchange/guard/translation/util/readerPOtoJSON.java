
package com.openexchange.guard.translation.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class readerPOtoJSON {

    private static ArrayList<File> getFileList(String directory) {
        ArrayList<File> files = new ArrayList<File>();
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                if (file.getName().endsWith("po"))
                    files.add(file);
            }
        }
        return (files);
    }

    private static String getDomain(String filename) {
        int i = filename.indexOf("-");
        if (i < 0)
            return ("");
        int j = filename.lastIndexOf(".");
        String domain = filename.substring(i + 1, j);
        return (domain);
    }

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        // TODO Auto-generated method stub
    	String readerLocation = readerPOtoJSON.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath() + "../../guest-reader/";
        ArrayList<File> files = getFileList(readerLocation + "/l10n");
        for (int i = 0; i < files.size(); i++) {
            String domain = getDomain(files.get(i).getName());
            String repl = files.get(i).getAbsolutePath().replace(".po", ".json");
            if (!domain.equals("")) {
                String command = "i18next-conv -l " + domain + " -s " + files.get(i).getAbsolutePath() + " -t " + repl;
                System.out.println("running " + command);
                Process p = Runtime.getRuntime().exec(command);
                p.waitFor();
            }

        }
    }

}
