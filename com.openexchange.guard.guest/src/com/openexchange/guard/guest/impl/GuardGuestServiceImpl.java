
package com.openexchange.guard.guest.impl;
/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import static com.openexchange.guard.common.util.FunctionalExceptionUtil.toExceptionThrowingConsumer;
import static com.openexchange.guard.common.util.FunctionalExceptionUtil.toExceptionThrowingFunction;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.GuardGuestEmail;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.MailQuery;
import com.openexchange.guard.guest.MailSortField;
import com.openexchange.guard.guest.MailUpdate;
import com.openexchange.guard.guest.OrderDirection;
import com.openexchange.guard.guest.impl.comparators.ComparatorRegistry;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheItem;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuardGuestServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestServiceImpl implements GuardGuestService {

    private static final Logger                  LOGGER                = LoggerFactory.getLogger(GuardGuestServiceImpl.class);
    private static final int                     DEFAULT_PAGING_SIZE   = Integer.MAX_VALUE;
    private static final int                     DEFAULT_PAGING_OFFSET = 0;
    private static final int                     DEFAULT_MESSAGE_FLAGS = 1 << 4; /** The recent flag. See {@link MailMessage} **/
    private final Storage                        guestFileStorage;
    private final FileCacheStorage               fileCacheStorage;
    private final PGPMimeCryptoService           pgpMimeCryptoService;
    private final GuardGuestEMailMetadataService guardGuestEMailMetadataService;
    private final ActivityTrackingService        activityTracker;
    private final GuestUpgradeStorageService     guestStorageService;

    /**
     * Initializes a new {@link GuardGuestServiceImpl}.
     *
     * @param pgpCryptoService The service used for en/de-cryption
     * @param guestFileStorage The underlying storage to use
     * @param fileCacheStorage The DB cache service
     * @param convertes A set of converters used for converting guest items on the fly.
     */
    public GuardGuestServiceImpl(PGPMimeCryptoService pgpMimeCryptoService,
        Storage guestFileStorage,
        FileCacheStorage fileCacheStorage,
        GuardConfigurationService guardConfigurationService,
        GuardGuestEMailMetadataService guardGuestEMailMetadataService,
        ActivityTrackingService activityTracker,
        GuestUpgradeStorageService guestStorageService) {

        this.pgpMimeCryptoService = Objects.requireNonNull(pgpMimeCryptoService, "pgpMimeCryptoService must not be null");
        this.guestFileStorage = Objects.requireNonNull(guestFileStorage, "guestFileStorage must not be null");
        this.fileCacheStorage = Objects.requireNonNull(fileCacheStorage, "fileCacheStorage must not be null");
        this.guardGuestEMailMetadataService = Objects.requireNonNull(guardGuestEMailMetadataService, "guardGuestEMailMetadataService must not be null");
        this.activityTracker = activityTracker;
        this.guestStorageService = guestStorageService;
    }

    private GuardGuestEMailMetadataService getMetaDataServiceForUser(int contextId, int userId) {
        return this.guardGuestEMailMetadataService;
    }

    /**
     * Gets an {@link InputStream} to the message with the given id
     *
     * @param itemId The ID of the messge to get and InputStream to
     * @return The InputStream to the message with the given ID, or null if no such message was found for the given ID.
     * @throws OXException
     */
    private InputStream getMimeMessageInputStream(String itemId) throws OXException {
        return this.guestFileStorage.readObj(itemId);
    }

    /**
     * Updates the message flags of a given message
     *
     * @param guestIdentity The guest user owning the message to change
     * @param emailMetaData The meta data of the message containing the current message flags
     * @param messageFlags The message flags to apply as bit mask
     * @param set True in order to add the bits, false to remove them
     * @throws OXException
     */
    private void updateMailMessageFlags(UserIdentity guestIdentity, GuardGuestEmailMetadata emailMetaData, Integer messageFlags, boolean set) throws OXException {
        int currentFlags = emailMetaData.getMessageFlags();

        //Calculate new flags
        int updatedFlags = set ?
            currentFlags | messageFlags :
            currentFlags & ~messageFlags;

        //ensure that the "Allow user defined flags"-Flag is NOT set
        updatedFlags = updatedFlags & ~64;

        if (updatedFlags != currentFlags) {
            final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(guestIdentity.getOXGuardUser().getContextId(), guestIdentity.getOXGuardUser().getId());
            metaDataService.updateMessageFlags(
                guestIdentity.getOXGuardUser().getContextId(),
                guestIdentity.getOXGuardUser().getId(),
                emailMetaData.getId(), updatedFlags);
            emailMetaData.setMessageFlags(updatedFlags);
        }
    }

    /**
     * Internal message to get a {@link GuardGuestEmail} item for a given ID
     *
     * @param guestIdentity The user to the fetch the item for.
     * @param itemId The ID of the item to fetch.
     * @param fetchFull true to fetch the whole message body, false to only fetch meta data
     * @param decrypt true to decrypt the item, false to retrieve the plaintext item.
     * @return The fetched guest email item, or null if no such item was found,
     * or an existing old guest item could not be converted to a proper PGP/MIME object.
     * @throws OXException
     */
    private GuardGuestEmail getMailItem (UserIdentity guestIdentity, String itemId, boolean fetchFull, boolean decrypt) throws OXException {
        return getAndUpdateMailItem(guestIdentity, itemId, fetchFull, decrypt, null, false);
    }

    /**
     * Internal method to fetch the an Email item from the underlaying storage.
     *
     * @param guestIdentity The user to the fetch the item for.
     * @param itemId The ID of the item to fetch.
     * @param fetchFull true to fetch the whole message body, false to only fetch meta data
     * @param decrypt true to decrypt the item, false to retrieve the plaintext item.
     * @param newMailFlags The new mail flags to apply to the item before returned or null if no flags should be applied
     * @param set True, to set the new mail flags, false to unset
     * @return The fetched guest email item, or null if no such item was found,
     * or an existing old guest item could not be converted to a proper PGP/MIME object.
     * @throws OXException
     */
    private GuardGuestEmail getAndUpdateMailItem (UserIdentity guestIdentity, String itemId, boolean fetchFull, boolean decrypt, Integer newMailFlags, boolean set) throws OXException {
        guestIdentity = Objects.requireNonNull(guestIdentity, "guestUser must not be null");

        final OXGuardUser guestUser = guestIdentity.getOXGuardUser();

        //Check for existing meta data
        GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(guestUser.getContextId(), guestUser.getId());
        GuardGuestEmailMetadata metaData = metaDataService.get(guestUser.getContextId(), guestUser.getId(), itemId);

        //Fetching the full message if specified by the caller or if we do not have meta data yet
        //MimeMessage fullBody = fetchFull || metaData == null ? getMimeMessage(guestUser.getContextId(), itemId) : null;
        InputStream fullBodyStream = null;
        try {
            fullBodyStream = fetchFull || metaData == null ? getMimeMessageInputStream(itemId) : null;
        } catch (OXException ex) {  // Not found or other retrieval issue
            LOGGER.error("Error fetching email body", ex);
        }

        if(metaData != null) {
            if (fullBodyStream != null && decrypt) {
                //Decrypt the guest MIME message
                //Not implemented
            }
            if(newMailFlags != null) {
                updateMailMessageFlags(guestIdentity, metaData, newMailFlags, set);
            }
            return new GuardGuestEmail(guestIdentity, itemId)
                .setMetaData(metaData)
                .setMessageStream(fetchFull ? fullBodyStream : null);
        }
        else {
            //Item not found
            if(fullBodyStream != null) {
                try {
                    fullBodyStream.close();
                } catch (IOException e) {
                    throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
                }
            }
            return null;
        }
    }

    /**
     * Internal method for setting the message flags for an email item
     *
     * @param userIdentity The guest user to fetch the item for
     * @param itemId  The ID of the item to update
     * @param messageFlags The flags to update
     * @param true to set the specified flags, false to remove them
     * @throws OXException
     */
    private void setMailMessageFlags(UserIdentity userIdentity, String itemId, int messageFlags, boolean set) throws OXException {
        final boolean fetchFull = false;
        final boolean decrypt = false;
        GuardGuestEmail mail = null;
        try {
            //Get and update the flags
            mail = getAndUpdateMailItem(userIdentity, itemId, fetchFull, decrypt, messageFlags, set);
        }
        finally {
            try {
                if (mail != null) {
                    mail.close();
                }
            } catch (IOException e) {
                throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
            }
        }
    }

    /**
     * Internal method to update a message's color label
     *
     * @param guestIdentity The owner of the message
     * @param itemId The ID of the message
     * @param colorLabel The new color label
     * @throws OXException
     */
    private void updateColorLabel(UserIdentity guestIdentity, String itemId, int colorLabel) throws OXException {
        final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId());
        metaDataService.updateColorLabel(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId(),
            itemId,
            colorLabel);
    }

    /**
     * Internal method to update a message's folder
     *
     * @param guestIdentity The owner of the message
     * @param itemId The item to update
     * @param folderId The new folder to set
     * @throws OXException
     */
    private void updateFolderId(UserIdentity guestIdentity, String itemId, String folderId) throws OXException {
        final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId());
        metaDataService.updateFolderId(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId(),
            itemId,
            folderId);
    }

    /**
     * Internal method to update all messages to a new folder id for a specified guest.
     * @param guestIdentity The guest
     * @param folderId  The folder ID to update
     * @param newFolderId The new folder ID to set
     * @throws OXException
     */
    private void updateAllFolderIds(UserIdentity guestIdentity, String folderId, String newFolderId) throws OXException{
        final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId());
        metaDataService.updateAllFolderIdsInFolder(
            guestIdentity.getOXGuardUser().getContextId(),
            guestIdentity.getOXGuardUser().getId(),
            folderId,
            newFolderId);
    }

    /**
     * Gets a list of guest mail items belonging to the given user
     *
     * @param guestUser The guest user
     * @return A list of item IDs for the given guest user
     * @throws OXException
     */
    private List<String> getItemIdsFor(UserIdentity guestUser) throws OXException{
        OXGuardUser user = null;
        if (guestUser.getOXGuardUser().getContextId() > 0) {
            Email guest = guestStorageService.getGuestRecord(null, guestUser.getOXGuardUser().getId(), guestUser.getOXGuardUser().getContextId());
            if (guest != null) {
                user = new OXGuardUser(guest.getUserId(), guest.getContextId());
            }
        } else {
            user = guestUser.getOXGuardUser();
        }
        if (user == null) {
            return Collections.emptyList();
        }
        List<FileCacheItem> itemsForUser =
            fileCacheStorage.getForUser(user.getId(), user.getContextId(), DEFAULT_PAGING_OFFSET, DEFAULT_PAGING_SIZE);
        return itemsForUser.stream().map(item -> item.getItemId()).collect(Collectors.toList());
    }

    /**
     * Checks if a mail item has the specified message flags set
     *
     * @param email The mail item
     * @param flags The flags to check
     * @return True if the specified flags are set for the given mail item, false otherwise
     */
    private boolean hasFlags(GuardGuestEmail email, int flags) {
        return email.getMetaData().hasMessageFlags(flags);
    }

    /**
     * Internal message to full(not only metadata, but also binary content from storage) delete a guest mail
     *
     * @param itemId
     * @param contextId
     * @param userId
     * @throws OXException
     */
    private void deleteMessage(String itemId, int contextId, int userId) throws OXException {
        FileCacheItem fileCacheItem = this.fileCacheStorage.getById(itemId);
        if (fileCacheItem != null) {

            //Remove from the DB
            this.fileCacheStorage.delete(itemId);

            //Remove from the storage
            try {
                this.guestFileStorage.deleteEncrObj(fileCacheItem.getLocation());
            } catch (OXException ex) {
                if (ex.getCode() == 17) {
                    LOGGER.info("File item already removed " + itemId);
                } else {
                    throw ex;
                }
            }

            //Delete meta data
            if(fileCacheItem.getLocation().contains(Storage.DELIMITER)) {

                String directoryPrefix =
                    fileCacheItem.getLocation().substring(0, fileCacheItem.getLocation().indexOf(Storage.DELIMITER));

                GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(contextId, userId);

                Integer shardId = guestFileStorage.getGuestShardIdFromPrefix(directoryPrefix);
                if (shardId != null) {
                    metaDataService.delete(shardId,
                        new GuardGuestEmailMetadata(fileCacheItem.getItemId()));
                }
            }
        }
    }

    /**
     * Internal method to copy a guest email
     *
     * @param guestUser The user/owner of the guest email to copy
     * @param messageId The ID of the email to copy
     * @param newFolderId The ID of the folder to copy the meail to
     * @return The ID of the copied message
     * @throws OXException
     */
    private GuardGuestEmail copyMessage(UserIdentity guestUser, String messageId, String newFolderId) throws OXException {

        FileCacheItem fileCacheItem = this.fileCacheStorage.getById(messageId);
        if (fileCacheItem != null) {
            //Copy the item
            String newId = "pgp-" + CipherUtil.getUUID();
            this.guestFileStorage.copy(guestUser.getOXGuardUser().getId(),
                guestUser.getOXGuardUser().getContextId(),
                messageId,
                newId);

            //Copy the meta data
            final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(guestUser.getOXGuardUser().getContextId(), guestUser.getOXGuardUser().getId());
            GuardGuestEmailMetadata metaData = metaDataService.get(guestUser.getOXGuardUser().getContextId(),
                guestUser.getOXGuardUser().getId(),
                messageId);
            if (metaData != null) {
                GuardGuestEmailMetadata newMetaData = new GuardGuestEmailMetadata(metaData);
                newMetaData.setId(newId);
                newMetaData.setFolderId(newFolderId);

                metaDataService.insert(guestUser.getOXGuardUser().getContextId(),
                    guestUser.getOXGuardUser().getId(),
                    newMetaData);
                return new GuardGuestEmail(guestUser, newId).setMetaData(newMetaData);
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GuardGuestService#getItemIds(UserIdentity)
     */
    @Override
    public List<String> getEmailItemIds(UserIdentity guestUser) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        Objects.requireNonNull(guestUser.getOXGuardUser(), "OXGuardUser must not ne null");

        List<FileCacheItem> itemsForUser =
            fileCacheStorage.getForUser(guestUser.getOXGuardUser().getId(), guestUser.getOXGuardUser().getContextId(), DEFAULT_PAGING_OFFSET, DEFAULT_PAGING_SIZE);
        return itemsForUser.stream().map(fileCacheItem -> fileCacheItem.getItemId()).collect(Collectors.toList());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#getEmailItems(com.openexchange.guard.user.UserIdentity)
     */
    @Override
    public Collection<GuardGuestEmail> getEmailItems(UserIdentity guestUser, MailQuery query) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        query = Objects.requireNonNull(query, "query must not be null");

        //Getting a list of the user's item IDs
        List<String> itemIds = getItemIdsFor(guestUser);
        //Filter email IDs as described in the given query
        final MailQuery closureQuery = query;
        if(query.getMailIds() != MailQuery.ALL_IDS) {
            //Filter only queried Ids and ensure that the IDs are in the order requests by the query
           final List<String> userMailIds = itemIds;
           itemIds = Arrays.stream(query.getMailIds()).filter( id -> userMailIds.contains(id)).collect(Collectors.toList());
        }

        //Get a comparator for sorting the results
        final MailSortField orderBy = query.getOrderBy();
        Comparator<GuardGuestEmail> comparator  = null;
        if(orderBy != null) {
            comparator = ComparatorRegistry.getComparatorFor(orderBy);
            if(comparator != null && query.getOrderDirection() == OrderDirection.DESC) {
                comparator = comparator.reversed();
            }
        }

        //For each obtained ID: fetch the related mail item
        final boolean decrypt = false;
        final boolean fetchBody = query.getFetchMessageBody();
        final boolean set = true;
        final UserIdentity closureIdentity = guestUser;
        Stream<GuardGuestEmail> result = itemIds.stream()
            .map(toExceptionThrowingFunction(itemId -> getAndUpdateMailItem(closureIdentity, itemId, fetchBody, decrypt, closureQuery.getNewMailFlags(), set))) //get the item for each ID
            .filter(item -> item != null);//filter possible null values

        //Sort the result if a comparator is present
        if(comparator != null) {
            result = result.sorted(comparator::compare);
        }

        //Filter items which matches the given folder
        if(query.getFolderId() != null) {
            result = result.filter(item -> item.getMetaData().getFolderId().equals(closureQuery.getFolderId()));
        }

        //apply the range filter
        if(query.getIndexRangeStart() != null && query.getIndexRangeEnd() != null) {
           final long amount = query.getIndexRangeEnd() - query.getIndexRangeStart();
           result = result.skip(query.getIndexRangeStart()).limit(amount);
        }

        //limit the result as specified by the query
        result = result.limit(query.getLimit()); //limit the result

        //Filter the items which have the specified mail flags set
        if(query.getMailFlags() != null){
           result = result.filter(item -> hasFlags(item, closureQuery.getMailFlags()));
        }

        //Filter the items which do NOT have the specified "NO"-mail flags set
        if(query.getNotMailFlags() != null) {
           result = result.filter(item -> !hasFlags(item, closureQuery.getNotMailFlags()));
        }

        return result.collect(Collectors.toList());

    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GuardGuestService#getGuestItem(UserIdentity, java.lang.String)
     */
    @Override
    public GuardGuestEmail getEmailItem(UserIdentity guestUser, String itemId) throws OXException {
        final boolean doDecryption = false;
        final boolean doFetchFull = true;
        return getMailItem(guestUser, itemId, doFetchFull, doDecryption);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#decryptEmailItem(com.openexchange.guard.user.UserIdentity, java.lang.String)
     */
    @Override
    public GuardGuestEmail getDecryptedEmailItem(UserIdentity guestUser, String itemId) throws OXException {
        final boolean doDecryption = true;
        final boolean doFetchFull = true;
        return getMailItem(guestUser, itemId, doFetchFull, doDecryption);
    }

    private void updateMessage(UserIdentity guestUser, String itemId, MailUpdate mailUpdate) throws OXException {

        //Update the message flags for each provided item
        if(mailUpdate.getMailFlags() != null) {
           final boolean set = mailUpdate.getMailFlagsMode() == MailUpdate.MailFlagsMode.SET;
           setMailMessageFlags(guestUser, itemId, mailUpdate.getMailFlags(), set);
        }

        //Update the message color labels
        if(mailUpdate.getColorLabel() != null) {
           updateColorLabel(guestUser, itemId, mailUpdate.getColorLabel());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#updateMessage(com.openexchange.guard.user.UserIdentity, com.openexchange.guard.guest.MailUpdate)
     */
    @Override
    public void updateMessages(UserIdentity guestUser, MailUpdate mailUpdate) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        mailUpdate = Objects.requireNonNull(mailUpdate, "mailUpdate must not be null");


        final UserIdentity closureGuestUser = guestUser;

        //Getting a list of the user's item IDs
        List<String> itemIds = getItemIdsFor(guestUser);
        //Filter email IDs as described in the given update
        final MailUpdate closureUpdate = mailUpdate;
        if(mailUpdate.getMailIds() != MailUpdate.ALL_IDS) {
           itemIds = itemIds.stream().filter(id -> Arrays.asList(closureUpdate.getMailIds()).contains(id)).collect(Collectors.toList());
        }

        //Update the message
        final MailUpdate closureMailUpdate = mailUpdate;
        itemIds.stream().forEach(toExceptionThrowingConsumer(
                itemId -> updateMessage(closureGuestUser, itemId, closureMailUpdate)
            ));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#moveMessages(com.openexchange.guard.user.UserIdentity, java.lang.String[], java.lang.String)
     */
    @Override
    public void moveMessages(UserIdentity guestUser, String[] messageIds, String newFolderId) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        newFolderId = Objects.requireNonNull(newFolderId, "newFolderId must not be null");
        if(messageIds != null) {

            //Getting a list of the user's item IDs
            List<String> itemIds = getItemIdsFor(guestUser);
            //Filter email IDs as described in the given update
            itemIds = itemIds.stream().filter(id -> Arrays.asList(messageIds).contains(id)).collect(Collectors.toList());

            final UserIdentity closureGuestUser =  guestUser;
            final String closureNewFolderId = newFolderId;
            itemIds.stream().forEach(toExceptionThrowingConsumer(
               itemId ->  updateFolderId(closureGuestUser, itemId, closureNewFolderId)));
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#moveAllMessages(com.openexchange.guard.user.UserIdentity, java.lang.String, java.lang.String)
     */
    @Override
    public void moveAllMessages(UserIdentity guestUser, String folderId, String newFolderId) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        this.updateAllFolderIds(guestUser, folderId, newFolderId);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#copyMessages(com.openexchange.guard.user.UserIdentity, java.lang.String[], java.lang.String)
     */
    @Override
    public Collection<GuardGuestEmail> copyMessages(UserIdentity guestUser, String[] messageIds, String newFolderId) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        messageIds = Objects.requireNonNull(messageIds,  "messageIds  must not be null");
        final String[] closureMessageIds = messageIds;
        final UserIdentity closureGuestUser = guestUser;

        //Getting a list of the user's item IDs
        List<String> itemIds = getItemIdsFor(guestUser);
        //Filter email IDs as described in the given update
        itemIds = itemIds.stream().filter(id -> Arrays.asList(closureMessageIds).contains(id)).collect(Collectors.toList());

        //Copy all messages
        return itemIds.stream().map(
            toExceptionThrowingFunction(itemId -> copyMessage(closureGuestUser, itemId, newFolderId))).collect(Collectors.toList());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#appendMessage(com.openexchange.guard.user.UserIdentity, java.io.InputStream, java.lang.String, java.lang.String)
     */
    @Override
    public GuardGuestEmail appendMessage(UserIdentity guestUser, InputStream msg, String itemId, String folderId) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        msg = Objects.requireNonNull(msg, "msg must not be null");
        folderId = Objects.requireNonNull(folderId, "folderId must not be null");

        try {
            //TODO Check if the msg is a PGP message ?!
            final byte[] msgData = IOUtils.toByteArray(msg);

            //Save object data to storage
            this.guestFileStorage.saveEncrObj(guestUser.getOXGuardUser().getId(),
                                              guestUser.getOXGuardUser().getContextId(),
                                              itemId,
                                              msgData);

            // Save meta data
            MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(msgData));
            final GuardGuestEMailMetadataService metaDataService = getMetaDataServiceForUser(guestUser.getOXGuardUser().getContextId(), guestUser.getOXGuardUser().getId());
            GuardGuestEmailMetadata metaData = metaDataService.createMetaDataFrom(itemId, folderId, mimeMessage, DEFAULT_MESSAGE_FLAGS);
            metaDataService.insert(
                guestUser.getOXGuardUser().getContextId(),
                guestUser.getOXGuardUser().getId(),
                metaData);

            if (activityTracker != null) {
                activityTracker.updateActivity(guestUser.getOXGuardUser().getId(), guestUser.getOXGuardUser().getContextId());
            }
            return new GuardGuestEmail(guestUser, itemId).setMetaData(metaData);
        }
        catch(IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#appendMessage(com.openexchange.guard.guest.MimeMessage, java.lang.String)
     */
    @Override
    public GuardGuestEmail appendMessage(UserIdentity guestUser, InputStream msg, String folderId) throws OXException {
        final String itemId = "pgp-" + CipherUtil.getUUID();
        return appendMessage(guestUser, msg, itemId, folderId);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#deleteMessages(com.openexchange.guard.user.UserIdentity, java.lang.String[])
     */
    @Override
    public void deleteMessages(UserIdentity guestUser, String[] messageIds) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        if(messageIds != null) {

            //Getting a list of the user's item IDs
            List<String> itemIds = getItemIdsFor(guestUser);
            //Filter email IDs as described in the given update
            itemIds = itemIds.stream().filter(id -> Arrays.asList(messageIds).contains(id)).collect(Collectors.toList());

            final OXGuardUser closureUser = guestUser.getOXGuardUser();
            itemIds.stream().forEach(toExceptionThrowingConsumer(itemId -> deleteMessage(itemId, closureUser.getContextId(), closureUser.getId())));
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuardGuestService#deleteAllMessages(com.openexchange.guard.user.UserIdentity, java.lang.String)
     */
    @Override
    public void deleteAllMessages(UserIdentity guestUser, String folderId) throws OXException {
        guestUser = Objects.requireNonNull(guestUser, "guestUser must not be null");
        folderId = Objects.requireNonNull(folderId, "folderId must not be null");

        //Getting all mails related to the given folder
        GuardGuestEMailMetadataService metaDataService =
            getMetaDataServiceForUser(guestUser.getOXGuardUser().getContextId(),
                                      guestUser.getOXGuardUser().getId());
        Collection<GuardGuestEmailMetadata> mailsForFolder = metaDataService.getForFolder(
            guestUser.getOXGuardUser().getContextId(),
            guestUser.getOXGuardUser().getId(),
            folderId);

        //Delete all these mails
        OXGuardUser closureUser = guestUser.getOXGuardUser();
        mailsForFolder.forEach(toExceptionThrowingConsumer(mail ->
            deleteMessage(mail.getId(),
                          closureUser.getContextId(),
                          closureUser.getId())));
    }
}
