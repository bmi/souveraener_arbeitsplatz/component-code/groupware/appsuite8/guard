/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.updater;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;;

/**
 * {@link SetDBEngineUpdateTask}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since 2.10.0
 */
public class SetDBEngineUpdateTask extends UpdateTaskAdapter {

    private static final String[] AFFECTED_TABLES = { "og_mailvelope" };

    /**
     * Initialises a new {@link SetDBEngineUpdateTask}.
     */
    public SetDBEngineUpdateTask() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.groupware.update.UpdateTaskV2#perform(com.openexchange.groupware.update.PerformParameters)
     */
    @Override
    public void perform(PerformParameters params) throws OXException {
        String schemaName = params.getSchema().getSchema();
        Connection connection = params.getConnection();
        try {
            for (String tableName : AFFECTED_TABLES) {
                if (isEngineMyISAM(schemaName, tableName, connection)) {
                    changeEngine(tableName, connection);
                }
            }
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        }
    }

    /**
     * Checks whether the DB engine is MyISAM
     * 
     * @param schemaName The schema name
     * @param tableName The table name
     * @param connection The {@link Connection}
     * @return <code>true</code> if the engine of the specified table
     *         in the specified schema is of 'MyISAM'; <code>false</code> otherwise
     * @throws SQLException if an SQL error is occurred
     */
    private boolean isEngineMyISAM(String schemaName, String tableName, Connection connection) throws SQLException {
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("SELECT ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME=?");
            statement.setString(1, schemaName);
            statement.setString(2, tableName);
            rs = statement.executeQuery();
            if (rs.next()) {
                String engine = rs.getString(1);
                return engine != null && engine.equalsIgnoreCase("myisam");
            }
            return false;
        } finally {
            Databases.closeSQLStuff(statement, rs);
        }
    }

    /**
     * Changes the database engine of the specified table to 'InnoDB'
     * 
     * @param tableName The table's name
     * @param connection The {@link Connection} to use
     * @throws SQLException if an SQL error is occurred
     */
    private void changeEngine(String tableName, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("ALTER TABLE " + tableName + " ENGINE=InnoDB");
            statement.execute();
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.groupware.update.UpdateTaskV2#getDependencies()
     */
    @Override
    public String[] getDependencies() {
        return new String[0];
    }
}
