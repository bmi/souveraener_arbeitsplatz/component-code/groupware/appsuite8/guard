/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.helpfiles.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.translation.GuardTranslationService;

/**
 * {@link HelpFileServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.8.0
 */
public class HelpFileServiceImpl implements HelpFileService {

    private static final Logger LOG = LoggerFactory.getLogger(HelpFileServiceImpl.class);
    private static final  String HELP_FILE_NAME = "help";
    private final GuardTranslationService translationService;
    private final GuardConfigurationService guardConfigurationService;

    /**
     * Initializes a new {@link HelpFileServiceImpl}.
     *
     * @param translationService The translation to use
     */
    public HelpFileServiceImpl(GuardTranslationService translationService, GuardConfigurationService guardConfigurationService) {
        this.translationService = Objects.requireNonNull(translationService, "translationService must not be null");
        this.guardConfigurationService = Objects.requireNonNull(guardConfigurationService, "guardConfigurationService must not be null");
    }

    private List<HelpAttach> getHelp(int userid, int cid, final String...languages) {
        final List<HelpAttach> helpfiles = new ArrayList<HelpAttach>();
        try {
            String externalReaderPath =
                guardConfigurationService.getProperty(GuardProperty.externalReaderPath, userid, cid);
            String oxURL = guardConfigurationService.getProperty(GuardProperty.externalEmailURL, userid, cid);

            for (int i = 0; i < languages.length; i++) {
                final String lang = translationService.getAvailableCode(languages[i]);
                String data = translationService.getHelpFile(lang);
                if (data != null) {
                    data = data.replace("%guest%", "https://" + externalReaderPath);
                    data = data.replace("%webmail%", "https://" + oxURL);
                    if (!contains(helpfiles, lang)) {
                        final HelpAttach att = new HelpAttach(lang, data);
                        helpfiles.add(att);
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("Problem loading help file ", e);
        }
        return (helpfiles);
    }

    private boolean contains(final List<HelpAttach> list, final String lang) {
        for (final HelpAttach att : list) {
            if (att.getLanguage().equals(lang)) {
                return (true);
            }
        }
        return (false);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.mime.helpfiles.HelpFileService#getHelpFiles(java.lang.String[])
     */
    @Override
    public List<BodyPart> getHelpFiles(int userId, int cid, String...languages) throws OXException {
        final List<BodyPart> ret = new ArrayList<BodyPart>();
        final List<HelpAttach> attachments = getHelp(userId, cid, languages);
        for (int i = 0; i < attachments.size(); i++) {
            final BodyPart help = new MimeBodyPart();
            try {
                help.setFileName(translationService.getTranslation(HELP_FILE_NAME, attachments.get(i).getLanguage()) + ".txt");
                help.setContent(attachments.get(i).getTranslated(), "text/plain; charset=utf-8");
                help.setDisposition("inline");
                ret.add(help);
            }
            catch(MessagingException e) {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.mime.helpfiles.HelpFileService#getHelpFiles(java.util.Locale[])
     */
    @Override
    public List<BodyPart> getHelpFiles(int userId, int cid, Locale... locales) throws OXException {
        return getHelpFiles(userId, cid, Arrays.stream(locales).map(locale -> locale.toString()).toArray(String[]::new));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.mime.helpfiles.HelpFileService#attachHelpFile()
     */
    @Override
    public boolean attachHelpFile(int userId, int cid) {
        return guardConfigurationService.getBooleanProperty(GuardProperty.attachHelpFile, userId, cid);
    }
}
