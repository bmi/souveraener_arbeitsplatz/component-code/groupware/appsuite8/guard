/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.notification.internal;

import static com.openexchange.java.Autoboxing.I;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.notification.GuardNotificationMessages;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.notification.exceptions.GuardNotificationExceptionCodes;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.mail.mime.MimeDefaultSession;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import net.htmlparser.jericho.Source;

/**
 * {@link GuardNotificationServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardNotificationServiceImpl implements GuardNotificationService {

    private static final int MAX_MIME_LINE_LENGTH = 998;
    private static final int MAX_IMAGE_LINE_LENGTH = 76;
    private static final String X_ORIG_IP = "X-Originating-IP";

    boolean attachSenderIp;
    GuardRatifierService guardRatifierService;
    GuardConfigurationService guardConfigService;

    public GuardNotificationServiceImpl(GuardRatifierService guardRatifierService, GuardConfigurationService guardConfigService, boolean attachSenderIp) {
        this.guardRatifierService = guardRatifierService;
        this.guardConfigService = guardConfigService;
        this.attachSenderIp = attachSenderIp;
    }

    private static Logger LOG = LoggerFactory.getLogger(GuardNotificationServiceImpl.class);

    @Override
    public void send(final JsonObject email, int senderUserId, int senderCid, final String senderIp) throws OXException {
        send(email, null, "", senderUserId, senderCid, guardConfigService.getProperty(GuardProperty.defaultLanguage, senderUserId, senderCid), getMessageID(senderUserId, senderCid), senderIp);
    }

    @Override
    public void send(final JsonObject email, int senderUserId, int senderCid, String lang, final String senderIp) throws OXException {
        send(email, null, "", senderUserId, senderCid, lang, getMessageID(senderUserId, senderCid), senderIp);
    }

    @Override
    public void send(final JsonObject email, final List<BodyPart> attachments, int senderUserId, int senderCid, final String senderIp) throws OXException {
        send(email, attachments, "", senderUserId, senderCid, guardConfigService.getProperty(GuardProperty.defaultLanguage, senderUserId, senderCid), getMessageID(senderUserId, senderCid), senderIp);
    }

    private boolean isLocal(int cid) {
        return (cid > 0);
    }

    @Override
    public void send(JsonObject email, final List<BodyPart> attachments, final String sender, int senderUserId, int senderCid, final String lang, final String messageId, String senderIp) throws OXException {
        try {
            final Session session = getSession(senderUserId, senderCid, isLocal(senderCid));
            final MimeMessage message = new MimeMessage(session);
            email = email.get("data").getAsJsonObject();
            // Parse Json for email headers
            // To
            if (email.get("to").isJsonArray()) {
                final JsonArray toAddrArray = email.get("to").getAsJsonArray();
                final Address[] addrTo = new Address[toAddrArray.size()];
                for (int i = 0; i < toAddrArray.size(); i++) {
                    final JsonArray addr = toAddrArray.get(i).getAsJsonArray();
                    final InternetAddress toaddr = new InternetAddress(IDNUtil.aceEmail(addr.get(1).getAsString()), addr.get(0).getAsString(), "UTF-8");
                    addrTo[i] = toaddr;
                }
                message.setRecipients(Message.RecipientType.TO, addrTo);
            } else {
                final String to = email.get("to").getAsString();
                if (to != null && to.contains("@")) {
                    final Address[] addrTo = new Address[1];
                    addrTo[0] = EmailParser.parseEmailAddress(to);  // right now, only one recipient per email if just string
                    message.setRecipients(Message.RecipientType.TO, addrTo);
                }
            }
            // CC
            if (email.get("cc").isJsonArray()) {
                final JsonArray ccAddrArray = email.get("cc").getAsJsonArray();
                final Address[] addrCc = new Address[ccAddrArray.size()];
                for (int i = 0; i < ccAddrArray.size(); i++) {
                    final JsonArray addr = ccAddrArray.get(i).getAsJsonArray();
                    final InternetAddress ccaddr = new InternetAddress(IDNUtil.aceEmail(addr.get(1).getAsString()), addr.get(0).getAsString(), "UTF-8");
                    addrCc[i] = ccaddr;
                }
                message.setRecipients(Message.RecipientType.CC, addrCc);
            } else {
                final String cc = email.get("cc").getAsString();
                if (cc != null && cc.contains("@")) {
                    final Address[] addrCc = new Address[1];
                    addrCc[0] = EmailParser.parseEmailAddress(cc);  // right now, only one recipient per email if just string
                    message.setRecipients(Message.RecipientType.CC, addrCc);
                }
            }
            // Bcc
            if (email.get("bcc").isJsonArray()) {
                final JsonArray bccAddrArray = email.get("bcc").getAsJsonArray();
                final Address[] addrBcc = new Address[bccAddrArray.size()];
                for (int i = 0; i < bccAddrArray.size(); i++) {
                    final JsonArray addr = bccAddrArray.get(i).getAsJsonArray();
                    final InternetAddress bccaddr = new InternetAddress(IDNUtil.aceEmail(addr.get(1).getAsString()), addr.get(0).getAsString(), "UTF-8");
                    addrBcc[i] = bccaddr;
                }
                message.setRecipients(Message.RecipientType.BCC, addrBcc);
            } else {
                final String bcc = email.get("bcc").getAsString();
                if (bcc != null && bcc.contains("@")) {
                    final Address[] addrBcc = new Address[1];
                    addrBcc[0] = EmailParser.parseEmailAddress(bcc);  // right now, only one recipient per email if just string
                    message.setRecipients(Message.RecipientType.BCC, addrBcc);
                }

            }

            addOrigIp(message, senderIp);

            // From
            InternetAddress fromaddr = null;
            InternetAddress replyaddr = null;
            if (email.get("from").isJsonArray()) {
                final JsonArray fromArray = email.get("from").getAsJsonArray();
                fromaddr = new InternetAddress(IDNUtil.aceEmail(fromArray.get(0).getAsJsonArray().get(1).getAsString()), fromArray.get(0).getAsJsonArray().get(0).getAsString(), "UTF-8");
                replyaddr = new InternetAddress(IDNUtil.aceEmail(fromaddr.getAddress()), fromaddr.getPersonal(), "UTF-8");
            } else {
                final String from = email.get("from").getAsString();
                fromaddr = EmailParser.parseEmailAddress(from);
                replyaddr = EmailParser.parseEmailAddress(from);
            }

            if (!sender.equals("")) {
                GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
                fromaddr.setPersonal(translationService.getTranslation(GuardNotificationMessages.SENT_ON_BEHALF_OF, lang,
                    ((fromaddr.getPersonal() == null) ? "" : (fromaddr.getPersonal().replace("\"", "") + " - ")) + fromaddr.getAddress().replace("<", "").replace(">", "")));
                fromaddr.setAddress(sender);
            }
            final String subject = email.get("subject").getAsString();
            String htmlcontent = null;
            String plaincontent = null;
            final JsonArray at = email.getAsJsonArray("attachments").getAsJsonArray();
            for (int i = 0; i < at.size(); i++) {
                final JsonObject item = at.get(i).getAsJsonObject();
                final String type = item.get("content_type").getAsString();
                if (type.contains("html") || type.contains("ALTERN")) {
                    htmlcontent = item.get("content").getAsString();
                }
                if (type.contains("text/plain")) {
                    plaincontent = item.get("content").getAsString();
                }
            }

            if (email.get("headers") != null) {
                final JsonObject headers = email.get("headers").getAsJsonObject();
                for (final Map.Entry<String, JsonElement> h : headers.entrySet()) {
                    message.setHeader(h.getKey(), Strings.removeCarriageReturn(h.getValue().getAsString()));
                }
            }

            message.setHeader("Reply-To", replyaddr.toString());

            message.setSubject(subject, "UTF-8");
            final MimeBodyPart multipart = new MimeBodyPart();  // Main message wrapper
            // Create alternative wrapped message
            final Multipart alternate = new MimeMultipart("alternative");
            final BodyPart pt = new MimeBodyPart();
            if (plaincontent != null) { // If plaintext exists, use
                pt.setContent(plaincontent, "text/plain; charset=utf-8");
                alternate.addBodyPart(pt);
            } else {  // else create from html
                if (htmlcontent != null) {
                    final StringReader sr = new StringReader(htmlcontent);
                    final Source src = new Source(sr);
                    pt.setContent(src.getRenderer().toString(), "text/plain; charset=utf-8");
                    alternate.addBodyPart(pt);
                    sr.close();
                }
            }
            final MimeMultipart content = new MimeMultipart("mixed");
            final ArrayList<MimeBodyPart> cids = new ArrayList<MimeBodyPart>();
            if (htmlcontent != null) {
                try {
                    htmlcontent = checkInlineImages(htmlcontent, cids);
                    htmlcontent = WrapUtils.wrapText(htmlcontent, MAX_MIME_LINE_LENGTH);
                } catch (final Exception ex) {
                    LOG.error("Problem checking for inline mail images ", ex);
                }
                if (cids.size() > 0) {
                    final MimeMultipart htmlpart = new MimeMultipart("related");
                    final BodyPart main = new MimeBodyPart();
                    main.setContent(htmlcontent, "text/html; charset=utf-8");
                    htmlpart.addBodyPart(main);
                    for (int i = 0; i < cids.size(); i++) {
                        htmlpart.addBodyPart(cids.get(i));
                    }
                    final MimeBodyPart htmlbody = new MimeBodyPart();
                    htmlbody.setContent(htmlpart);
                    alternate.addBodyPart(htmlbody);
                } else {
                    final BodyPart main = new MimeBodyPart();
                    main.setContent(htmlcontent, "text/html; charset=utf-8");
                    alternate.addBodyPart(main);
                }
            }
            // add the alternate to multipart
            multipart.setContent(alternate);
            // email multipart

            // add the wrapped alternate email
            content.addBodyPart(multipart);

            // add attachments
            if (attachments != null) {
                for (int i = 0; i < attachments.size(); i++) {
                    BodyPart attachment = attachments.get(i);
                    if (attachment != null) {
                        content.addBodyPart(attachment);
                    }
                }
            }
            message.setContent(content);
            message.setFrom(fromaddr);
            message.setSentDate(new Date());
            message.saveChanges();
            message.setHeader("Message-ID", messageId);

            // Connect and send
            {
                final Transport transport = session.getTransport("smtp");
                transport.connect();
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
            }
            LOG.debug("Success SMTP send");
            // Transport.send(message);;

        } catch (final Exception ex) {
            LOG.error("Error sending email to " + email.get("to").getAsString(), ex);
            throw GuardNotificationExceptionCodes.MAIL_SEND_ERROR.create(ex, ex.getMessage());
        }
    }

    @Override
    public void sendMessage(final Message msg, final String toAddr, final String sender, int guardSenderUserId, int guardSenderCid, int senderUserId, int senderCid, final String lang, final String senderIp) throws OXException {
        final Session session = getSession(senderUserId, senderCid, isLocal(guardSenderCid));
        Transport transport;
        try {
            if (!sender.equals("")) {
                msg.removeHeader("Reply-To");
                msg.addHeader("Reply-To", msg.getFrom()[0].toString());
                final InternetAddress fromaddr = (InternetAddress) msg.getFrom()[0];
                GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
                fromaddr.setPersonal(translationService.getTranslation(GuardNotificationMessages.SENT_ON_BEHALF_OF, lang,
                    ((fromaddr.getPersonal() == null) ? "" : (fromaddr.getPersonal().replace("\"", "") + " - ")) + fromaddr.getAddress().replace("<", "").replace(">", "")));
                fromaddr.setAddress(sender);
                msg.setFrom(fromaddr);
            }
            addOrigIp(msg, senderIp);
            transport = session.getTransport("smtp");
            transport.connect();
            transport.sendMessage(msg, toAddr == null ? msg.getAllRecipients() : InternetAddress.parse(toAddr));
            transport.close();
        } catch (final MessagingException | UnsupportedEncodingException e) {
            LOG.error("Error sending email", e);
            throw GuardNotificationExceptionCodes.MAIL_SEND_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Add X-Originating-IP header if configured
     *
     * @param msg
     * @param senderIp
     * @throws MessagingException
     */
    private void addOrigIp(Message msg, String senderIp) throws MessagingException {
        if (attachSenderIp) {  // If attach Originating IP header requested, add now
            senderIp = guardRatifierService.verifyIPAddress(senderIp);  // Verify
            if (senderIp != null) {
                msg.addHeader(X_ORIG_IP, senderIp);
            }
        }
    }

    /**
     * Returns a javax.mail.Session
     *
     * @return a javax.mail.Session
     * @throws OXException if the GuardConfigurationService is absent
     */
    private Session getSession(int userId, int cid, boolean local) {
        final String server = guardConfigService.getProperty(GuardProperty.guestSMTPServer, userId, cid);
        final int port = guardConfigService.getIntProperty(GuardProperty.guestSMTPPort, userId, cid);
        final String username = guardConfigService.getProperty(GuardProperty.guestSMTPUsername, userId, cid);

        final Properties prop = MimeDefaultSession.getDefaultMailProperties();
        boolean authenticate = true;
        if (username.length() < 2) {
            authenticate = false;
            prop.put("mail.smtp.auth", "false");
        } else {
            prop.put("mail.smtp.auth", "true");
        }
        prop.put("mail.smtp.host", server);
        prop.put("mail.smtp.port", I(port));
        if (guardConfigService.getBooleanProperty(GuardProperty.useStartTLS, userId, cid)) {
            SSLSocketFactoryProvider factoryProvider = Services.getService(SSLSocketFactoryProvider.class);
            if (factoryProvider != null) {
                String socketFactoryClass = factoryProvider.getDefault().getClass().getName();
                prop.put("mail.smtp.socketFactory.class", socketFactoryClass);
            }
            prop.put("mail.smtp.starttls.enable", "true");
        }
        // Bug 55211, add guestSMTPMailFrom only for replies/system emails.
        if (!local) {
            String mailFrom = guardConfigService.getProperty(GuardProperty.guestSMTPMailFrom, userId, cid);
            if (mailFrom != null && !mailFrom.isEmpty()) {
                prop.put("mail.smtp.from", mailFrom);
            }
        }
        LOG.debug("Using SMTP server " + server + ":" + port + ", TLS=" + guardConfigService.getBooleanProperty(GuardProperty.useStartTLS, userId, cid));
        Session session = null;
        if (authenticate) {
            session = Session.getInstance(prop, new Authenticator() {

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, guardConfigService.getProperty(GuardProperty.guestSMTPPassword, userId, cid));
                }
            });
        } else {
            session = Session.getInstance(prop);
        }
        return session;
    }

    /**
     * Check for inline data images and convert them to cid images
     *
     * @param html
     * @param content
     * @return
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    private String checkInlineImages(String html, final ArrayList<MimeBodyPart> cids) throws MessagingException {
        final Pattern p = Pattern.compile("<img[^>]*");
        final Matcher m = p.matcher(html);
        int count = 0;
        while (m.find()) {
            final String image = m.group();
            if (image.contains("data:image")) {
                final Pattern p2 = Pattern.compile("src[ ]*=[ ]*\"[^\"]*");
                final Matcher m2 = p2.matcher(image);
                if (m2.find()) {
                    final String data = m2.group();
                    final int i = data.indexOf("image");
                    final int j = data.indexOf(",");
                    final String header = data.substring(i, j);
                    final String imagedata = data.substring(j + 1).replace("%3D", "=");
                    final String[] headers = header.split(";");
                    if (headers.length > 1) {
                        if (headers[1].toLowerCase().trim().equals("base64")) {
                            final String cid = CipherUtil.getUUID() + "@guard";
                            final String replacement = "src=\"cid:" + cid;
                            html = html.replace(data, replacement);
                            String suffix = "";
                            switch (headers[0]) {
                                case "image/jpeg":
                                    suffix = ".jpg";
                                    break;
                                case "image/jpg":
                                    suffix = ".jpg";
                                    break;
                                case "image/png":
                                    suffix = ".png";
                                    break;
                                case "images/gif":
                                    suffix = ".gif";
                                    break;
                            }
                            final InternetHeaders h = new InternetHeaders();
                            h.addHeader("Content-Type", Strings.removeCarriageReturn(headers[0]));
                            h.addHeader("Content-Transfer-Encoding", "base64");
                            final MimeBodyPart att = new MimeBodyPart(h, WrapUtils.wrapLine(imagedata, MAX_IMAGE_LINE_LENGTH, true).getBytes(StandardCharsets.UTF_8));
                            att.setDisposition(MimeBodyPart.INLINE);
                            att.setContentID("<" + cid + ">");
                            att.setFileName("att" + (++count) + suffix);
                            cids.add(att);
                        }
                    }
                }
            }
        }
        return html;
    }

    /**
     * Get the message identifier
     *
     * @return the message identifier
     */
    protected String getMessageID(int userId, int cid) {
        String mailIdDomain = guardConfigService.getProperty(GuardProperty.mailIdDomain, userId, cid);
        final String oxURL = guardConfigService.getProperty(GuardProperty.externalEmailURL, userId, cid);
        if (mailIdDomain.equals("")) {  // If maildomain not defined, try to extract domain from URL
            if (oxURL.contains("/")) {
                mailIdDomain = oxURL.substring(0, oxURL.indexOf("/"));
            }
            if (mailIdDomain.contains(":")) {
                mailIdDomain = mailIdDomain.substring(0, mailIdDomain.indexOf(":"));
            }
        }
        if (mailIdDomain.equals("")) {
            mailIdDomain = "GUARD";
        }  // Otherwise, just define it as Guard
        return "<" + CipherUtil.getUUID() + "@" + mailIdDomain + ">";
    }

}
