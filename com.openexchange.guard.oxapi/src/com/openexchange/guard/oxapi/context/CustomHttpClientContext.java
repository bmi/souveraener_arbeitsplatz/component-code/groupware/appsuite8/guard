/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package com.openexchange.guard.oxapi.context;

import java.net.URI;
import java.util.List;
import javax.annotation.concurrent.NotThreadSafe;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpConnection;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthSchemeRegistry;
import org.apache.http.auth.AuthState;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.RouteInfo;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.cookie.CookieSpecRegistry;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/**
 * Custom implementation of the in {@link HttpClient} 4.2.1 not available HttpClientContext.create()
 * <br><br>
 * Implementation of {@link HttpContext} that provides convenience
 * setters for user assignable attributes and getter for readable attributes.
 *
 * @since v2.4.0
 */
@NotThreadSafe
public class CustomHttpClientContext implements HttpContext, ClientContext {

    /**
     * Attribute name of a {@link org.apache.http.HttpConnection} object that
     * represents the actual HTTP connection.
     */
    public static final String HTTP_CONNECTION = "http.connection";

    /**
     * Attribute name of a {@link org.apache.http.HttpRequest} object that
     * represents the actual HTTP request.
     */
    public static final String HTTP_REQUEST = "http.request";

    /**
     * Attribute name of a {@link org.apache.http.HttpResponse} object that
     * represents the actual HTTP response.
     */
    public static final String HTTP_RESPONSE = "http.response";

    /**
     * Attribute name of a {@link org.apache.http.HttpHost} object that
     * represents the connection target.
     */
    public static final String HTTP_TARGET_HOST = "http.target_host";

    /**
     * Attribute name of a {@link Boolean} object that represents the
     * the flag indicating whether the actual request has been fully transmitted
     * to the target host.
     */
    public static final String HTTP_REQ_SENT = "http.request_sent";

    public static CustomHttpClientContext create() {
        return new CustomHttpClientContext(new BasicHttpContext());
    }

    public static CustomHttpClientContext adapt(final HttpContext context) {
        Validate.notNull(context, "HTTP context");
        if (context instanceof CustomHttpClientContext) {
            return (CustomHttpClientContext) context;
        } else {
            return new CustomHttpClientContext(context);
        }
    }

    private final HttpContext context;

    public CustomHttpClientContext(final HttpContext context) {
        super();
        this.context = context;
    }

    public CustomHttpClientContext() {
        super();
        this.context = new BasicHttpContext();
    }

    @Override
    public Object getAttribute(final String id) {
        return context.getAttribute(id);
    }

    @Override
    public void setAttribute(final String id, final Object obj) {
        context.setAttribute(id, obj);
    }

    @Override
    public Object removeAttribute(final String id) {
        return context.removeAttribute(id);
    }

    public <T> T getAttribute(final String attribname, final Class<T> clazz) {
        Validate.notNull(clazz, "Attribute class");
        final Object obj = getAttribute(attribname);
        if (obj == null) {
            return null;
        }
        return clazz.cast(obj);
    }

    public <T extends HttpConnection> T getConnection(final Class<T> clazz) {
        return getAttribute(HTTP_CONNECTION, clazz);
    }

    public HttpConnection getConnection() {
        return getAttribute(HTTP_CONNECTION, HttpConnection.class);
    }

    public HttpRequest getRequest() {
        return getAttribute(HTTP_REQUEST, HttpRequest.class);
    }

    public boolean isRequestSent() {
        final Boolean b = getAttribute(HTTP_REQ_SENT, Boolean.class);
        return b != null && b.booleanValue();
    }

    public HttpResponse getResponse() {
        return getAttribute(HTTP_RESPONSE, HttpResponse.class);
    }

    public void setTargetHost(final HttpHost host) {
        setAttribute(HTTP_TARGET_HOST, host);
    }

    public HttpHost getTargetHost() {
        return getAttribute(HTTP_TARGET_HOST, HttpHost.class);
    }

    /**
     * Attribute name of a {@link org.apache.http.conn.routing.RouteInfo}
     * object that represents the actual connection route.
     */
    public static final String HTTP_ROUTE = "http.route";

    /**
     * Attribute name of a {@link List} object that represents a collection of all
     * redirect locations received in the process of request execution.
     */
    public static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";

    /**
     * Attribute name of a {@link org.apache.http.config.Lookup} object that represents
     * the actual {@link CookieSpecProvider} registry.
     */
    public static final String COOKIESPEC_REGISTRY = "http.cookiespec-registry";

    /**
     * Attribute name of a {@link org.apache.http.cookie.CookieSpec}
     * object that represents the actual cookie specification.
     */
    public static final String COOKIE_SPEC = "http.cookie-spec";

    /**
     * Attribute name of a {@link org.apache.http.cookie.CookieOrigin}
     * object that represents the actual details of the origin server.
     */
    public static final String COOKIE_ORIGIN = "http.cookie-origin";

    /**
     * Attribute name of a {@link org.apache.http.client.CookieStore}
     * object that represents the actual cookie store.
     */
    public static final String COOKIE_STORE = "http.cookie-store";

    /**
     * Attribute name of a {@link org.apache.http.client.CredentialsProvider}
     * object that represents the actual credentials provider.
     */
    public static final String CREDS_PROVIDER = "http.auth.credentials-provider";

    /**
     * Attribute name of a {@link org.apache.http.client.AuthCache} object
     * that represents the auth scheme cache.
     */
    public static final String AUTH_CACHE = "http.auth.auth-cache";

    /**
     * Attribute name of a {@link org.apache.http.auth.AuthState}
     * object that represents the actual target authentication state.
     */
    public static final String TARGET_AUTH_STATE = "http.auth.target-scope";

    /**
     * Attribute name of a {@link org.apache.http.auth.AuthState}
     * object that represents the actual proxy authentication state.
     */
    public static final String PROXY_AUTH_STATE = "http.auth.proxy-scope";

    /**
     * Attribute name of a {@link java.lang.Object} object that represents
     * the actual user identity such as user {@link java.security.Principal}.
     */
    public static final String USER_TOKEN = "http.user-token";

    /**
     * Attribute name of a {@link org.apache.http.config.Lookup} object that represents
     * the actual {@link AuthSchemeProvider} registry.
     */
    public static final String AUTHSCHEME_REGISTRY = "http.authscheme-registry";

    /**
     * Attribute name of a {@link org.apache.http.client.config.RequestConfig} object that
     * represents the actual request configuration.
     */
    public static final String REQUEST_CONFIG = "http.request-config";

    public RouteInfo getHttpRoute() {
        return getAttribute(HTTP_ROUTE, HttpRoute.class);
    }

    @SuppressWarnings("unchecked")
    public List<URI> getRedirectLocations() {
        return getAttribute(REDIRECT_LOCATIONS, List.class);
    }

    public CookieStore getCookieStore() {
        return getAttribute(COOKIE_STORE, CookieStore.class);
    }

    public void setCookieStore(final CookieStore cookieStore) {
        setAttribute(COOKIE_STORE, cookieStore);
    }

    public CookieSpec getCookieSpec() {
        return getAttribute(COOKIE_SPEC, CookieSpec.class);
    }

    public CookieOrigin getCookieOrigin() {
        return getAttribute(COOKIE_ORIGIN, CookieOrigin.class);
    }

    public CredentialsProvider getCredentialsProvider() {
        return getAttribute(CREDS_PROVIDER, CredentialsProvider.class);
    }

    public void setCredentialsProvider(final CredentialsProvider credentialsProvider) {
        setAttribute(CREDS_PROVIDER, credentialsProvider);
    }

    public AuthCache getAuthCache() {
        return getAttribute(AUTH_CACHE, AuthCache.class);
    }

    public void setAuthCache(final AuthCache authCache) {
        setAttribute(AUTH_CACHE, authCache);
    }

    public AuthState getTargetAuthState() {
        return getAttribute(TARGET_AUTH_STATE, AuthState.class);
    }

    public AuthState getProxyAuthState() {
        return getAttribute(PROXY_AUTH_STATE, AuthState.class);
    }

    public <T> T getUserToken(final Class<T> clazz) {
        return getAttribute(USER_TOKEN, clazz);
    }

    public Object getUserToken() {
        return getAttribute(USER_TOKEN);
    }

    public void setUserToken(final Object obj) {
        setAttribute(USER_TOKEN, obj);
    }

    public void setCookieSpecRegistry(final CookieSpecRegistry registry) {
        setAttribute(COOKIESPEC_REGISTRY, registry);
    }

    public CookieSpecRegistry getCookieSpecRegistry() {
        return getAttribute(COOKIESPEC_REGISTRY, CookieSpecRegistry.class);
    }

    public void setAuthSchemeRegistry(final AuthSchemeRegistry registry) {
        setAttribute(AUTHSCHEME_REGISTRY, registry);
    }

    public AuthSchemeRegistry getAuthSchemeRegistry() {
        return getAttribute(AUTHSCHEME_REGISTRY, AuthSchemeRegistry.class);
    }
}
