/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.certificatemanagement.services.impl;

import java.time.Duration;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.RecipCertificateLookupStrategy;
import com.openexchange.guard.certificatemanagement.RecipCertificateService;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.java.Strings;
import com.openexchange.osgi.ServiceListing;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RecipientCertificateService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class RecipCertificateServiceImpl implements RecipCertificateService {

    private static final Logger logger = LoggerFactory.getLogger(RecipCertificateServiceImpl.class);
    static final String PARAM_EMAIL = "email";

    private final ServiceLookup services;
    private final ServiceListing<RecipCertificateLookupStrategy> lookupStategies;

    /**
     * Initializes a new {@link RecipCertificateServiceImpl}.
     *
     * @param services The {@link ServiceLookup}
     * @param lookupStrategies A service listing containing the different {@link RecipCertificateLookupStrategy}s to use.
     */
    public RecipCertificateServiceImpl(ServiceLookup services, ServiceListing<RecipCertificateLookupStrategy> lookupStategies) {
        this.services = services;
        this.lookupStategies = lookupStategies;
    }

    /**
     * Internal method to validate the given email
     *
     * @param email The email to validate
     * @throws OXException if the given email does not seem to be a valid email address
     */
    private void validateEmail(String email) throws OXException {
        if (Strings.isNotEmpty(email)) {
            GuardRatifierService ratifier = services.getServiceSafe(GuardRatifierService.class);
            ratifier.validate(email);
        } else {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(PARAM_EMAIL);
        }
    }

    /**
     * Internal method to get the timeout when searching for {@link RecipCertificate}
     *
     * @param userId The ID of the user to get the timeout for
     * @param cid The context ID of the user to get the timeout for
     * @return The timeout in milliseconds when searching for remote certificates / S/MIME keys
     * @throws OXException
     */
    private int getTimeout(int userId, int cid) throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class).getIntProperty(GuardProperty.remoteKeyLookupTimeout, userId, cid);
    }

    @Override
    public RecipCertificate getRecipCertificate(int userId, int contextId, String email) throws OXException {
        //check if the given String is a valid email
        validateEmail(email);

        final int timeout = getTimeout(userId, contextId);
        Instant start = Instant.now();
        for (RecipCertificateLookupStrategy strategy : lookupStategies) {
            Instant current = Instant.now();
            Duration elapsed = Duration.between(start, current);  // Keep track of elapsed time and pass
            RecipCertificate recipCertificate = strategy.lookup(userId, contextId, email, timeout - Math.toIntExact(elapsed.toMillis()));
            if (recipCertificate != null) {
                logger.debug("Found recipient certificate (S/MIME key) for email {} using {}", email, strategy.getClass().getCanonicalName());
                return recipCertificate;
            }
            logger.debug("Could not find recipient certificate (S/MIME key) for email {} using {}", email, strategy.getClass().getCanonicalName());
        }
        logger.debug("Could not find any certificate (S/MIME key) for email", email);
        return null;
    }
}
