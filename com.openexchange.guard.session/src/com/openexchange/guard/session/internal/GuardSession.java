/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session.internal;

import java.util.Date;

/**
 * {@link GuardSession} represents a OX Guard session
 */
public class GuardSession {

    private final String sessionId;
    private final String token;
    private final Date date;
    private final int userId;
    private final int contextId;

    /**
     * Initializes a new {@link GuardSession}.
     * 
     * @param sessionId the Appsuite session id
     * @param token the ox guard authentication token
     * @param date the creation date of the session
     * @param userId the related user id
     * @param contextId the related context id
     */
    public GuardSession(String sessionId, String token, Date date, int userId, int contextId) {
        super();
        this.sessionId = sessionId;
        this.token = token;
        this.date = date;
        this.userId = userId;
        this.contextId = contextId;
    }

    /**
     * @return the related user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @return the Appsuite session id
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @return the OX Guard authentication token
     */
    public String getToken() {
        return token;
    }

    /**
     * @return The creation date of the session
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return the related context id
     */
    public int getContextId() {
        return contextId;
    }
}
