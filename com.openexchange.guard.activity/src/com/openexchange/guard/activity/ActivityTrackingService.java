/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.activity;

import java.util.List;
import com.openexchange.guard.user.OXGuardUser;

/**
 * {@link ActivityTrackingService}
 *
 * Service to keep track of user activity.
 * Will also pull guests that haven't had activity greater than certain timeframe
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public interface ActivityTrackingService {

    /**
     * Mark activity for a user
     * @param userId
     * @param contextId
     */
    public void updateActivity(int userId, int contextId);

    /**
     * Remove record of activity for a user.  Possibly during a user delete event
     * @param userId
     * @param contextId
     */
    public void removeActivityRecord(int userId, int contextId);

    /**
     * Get a list of Guest users that haven't had any activity for > defined number days
     * @param days
     * @return  A list of Guard Guest users
     */
    public List<OXGuardUser> getExpiredGuests(int days);

}
