/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link RecipKeyService} defines default functionality for resolving recipient keys.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public interface RecipKeyService {

    /**
     * Searches for a recipient key ({@link RecipKey}) by email using the given {@link RecipKeyLookupStrategy}.
     *
     * @param lookupStrategy The strategy defining where/how to search
     * @param email The email address of the recipient
     * @param userId The id of the sender
     * @param cid The cid of the sender
     * @return The {@link RecipKey} for the given email, or null if no such {@link RecipKey} was found
     * @throws OXException
     */
    RecipKey getRecipKey(RecipKeyLookupStrategy lookupStrategy, String email, int userId, int cid) throws OXException;

    /**
     * Searches for a recipient key ({@link RecipKey}) by email using a default {@link RecipKeyLookupStrategy} set by the implementing class.
     *
     * @param senderUserId The ID of the user who wants to lookup for a recipient key
     * @param senderContextId The context ID of the user who wants to lookup for a recipient key
     * @param email The email address of the recipient
     * @return The {@link RecipKey} for the given email, or null if no such {@link RecipKey} was found
     * @throws OXException
     */
    RecipKey getRecipKey(int senderUserId, int senderContextId, String email) throws OXException;

    /**
     * Searches for a recipient key ({@link RecipKey}) by email using a default {@link RecipKeyLookupStrategy} set by the implementing class, but will only return "internal" recipient (i.e. only regular OX Users)
     *
     * @param senderUserId The ID of the user who wants to lookup for a recipient key
     * @param senderContextId The context ID of the user who wants to lookup for a recipient key
     * @param email The email address of the recipient
     * @return The {@link RecipKey} for the given email, or null if no such {@link RecipKey} was found
     * @throws OXException
     */
    RecipKey getInternalRecipKey(int senderUserId, int senderContextId, String email) throws OXException;

}
