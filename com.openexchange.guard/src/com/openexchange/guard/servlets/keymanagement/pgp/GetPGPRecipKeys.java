/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.servlets.keymanagement.pgp;

import static com.openexchange.java.Autoboxing.l;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.keymanagement.responses.RecipKeyInfoResponse;

/**
 * {@link GetPGPRecipKeys}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class GetPGPRecipKeys {

    /**
     * Gets a {@link RecipKey} by ID
     *
     * @param keyId The ID
     * @return The {@link RecipKey} for the given ID, or null if no such key was found
     * @throws OXException
     */
    private static RecipKey getKey(long keyId) throws OXException {
        GuardKeys key = Services.getService(GuardKeyService.class).getKeys(keyId);
        if (key != null) {
            return new RecipKey(key);
        }
        return null;
    }

    /**
     * Gets a {@link RecipKey} by email
     *
     * @param userSession The user who searches for the key
     * @param email The email
     * @return The {@link RecipKey} for the given email, or null if no such key was found
     * @throws OXException
     */
    private static RecipKey getKey(int userId, int cid, String email) throws OXException {
        RecipKeyService recipKeyService = Services.getService(RecipKeyService.class);
        return recipKeyService.getRecipKey(userId, cid, email);
    }

    /**
     * Creates a new key
     *
     * @param userSession The user who requested a new key
     * @param recipKey The {@link RecipKey} to create
     * @return The update {@link RecipKey} object
     * @throws OXException
     */
    private static RecipKey createKey(RecipKey recipKey) throws OXException {
        AccountCreationService accountCreationService = Services.getService(AccountCreationService.class);
        return accountCreationService.createUserFor(recipKey);
    }

    public static RecipKeyInfoResponse getKeys(String email, Long keyId, boolean createIfMissing, int userId, int cid) throws OXException {


        RecipKey recipKey = keyId != null ? getKey(l(keyId)) : getKey(userId, cid, email);
        if (recipKey != null && recipKey.isNewKey() && createIfMissing) {
            //create missing key if desired by the client
            recipKey = createKey(recipKey);
        }

        if (recipKey != null) {
            return new RecipKeyInfoResponse(recipKey, userId, cid);
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }

}
