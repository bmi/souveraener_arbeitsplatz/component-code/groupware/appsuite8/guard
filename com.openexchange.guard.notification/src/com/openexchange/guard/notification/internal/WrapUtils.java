/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.notification.internal;

/**
 * {@link WrapUtils} - Helper class for wrapping long text
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4
 */
public class WrapUtils {

    /**
     * Wraps a long line of text
     * <br>
     * System.getProperty("line.separator") will be used for line break characters.
     * <br>
     * @param line the line to wrap
     * @param wrapLength the length of new lines
     * @param wrapLongWords Whether to wrap long words (a word which is longer than one line (wrapLength); i.e. the word does not contain a white space before wrapLength) at the position of wrapLength or not.
     * @return the wrapped line
     */
    public static String wrapLine(String line, int wrapLength, boolean wrapLongWords) {
        return wrapLine(line, wrapLength, wrapLongWords, System.getProperty("line.separator"));
    }
    
    /**
     * Wraps a long line of text
     * @param line the line to wrap
     * @param wrapLength the length of new lines
     * @param lineBreak the line break to use
     * @param wrapLongWords Whether to wrap long words (a word which is longer than one line (wrapLength); i.e. the word does not contain a white space before wrapLength) at the position of wrapLength or not.
     * @return the wrapped line
     */
    public static String wrapLine(String line, int wrapLength, boolean wrapLongWords, String lineBreak) {
        StringBuilder ret = new StringBuilder();
        if (line.length() > wrapLength) {
            int positionToWrap = line.lastIndexOf(' ', wrapLength);
            if (positionToWrap > 0) {
                //Found a space to wrap the line
                String beforeSplit = line.substring(0, positionToWrap);
                String afterSplit = line.substring(positionToWrap);
                ret.append(beforeSplit);
                ret.append(lineBreak);
                ret.append(wrapLine(afterSplit, wrapLength, wrapLongWords, lineBreak));
            }
            else {
                //No space to wrap the line - This is a really long word over one or more lines
                if (wrapLongWords) {
                    //Just Wrapping the line
                    String beforeSplit = line.substring(0, wrapLength);
                    String afterSplit = line.substring(wrapLength);
                    ret.append(beforeSplit);
                    ret.append(lineBreak);
                    ret.append(wrapLine(afterSplit, wrapLength, wrapLongWords, lineBreak));
                }
                else {
                    //Just use the line as it is
                    ret.append(line);
                }
            }
        }
        else {
            ret.append(line);
            ret.append(lineBreak);
        }
        return ret.toString();
    }
    
    /**
     * Wraps a text of multiple lines into a text with a maximum line length of wrapLength.
     * <br>
     * Text will be wrapped at the position of a suitable whitespace if found, or at wrapLength if no suitable whitespace was found.
     * <br>
     * System.getProperty("line.separator") will be used for line break characters.
     * <br>
     * @param text the text to wrap
     * @param wrapLength the maximum line of 
     * @return the wrapped text
     */
    public static String wrapText(String text, int wrapLength) {
        return wrapText(text, wrapLength, System.getProperty("line.separator"));
    }
    
    /**
     * Wraps a text of multiple lines into a text with a maximum line length of wrapLength.
     * <br>
     * Text will be wrapped at the position of a suitable whitespace if found, or at wrapLength if no suitable whitespace was found.
     * @param text the text to wrap
     * @param wrapLength the maximum line of
     * @param lineBreak String to use for line breaks 
     * @return the wrapped text
     */
    public static String wrapText(String text, int wrapLength, String lineBreak) {
        return wrapText(text,wrapLength,true,lineBreak);
    }

    /**
     * Wraps a text of multiple lines into a text with a maximum line length of wrapLength.
     * <br>
     * Text will be wrapped at the position of a suitable whitespace if found. 
     * If no suitable whitespace was found the line will only be wrapped if wrapLongWords is true.
     * @param text the text to wrap
     * @param wrapLength the maximum line of
     * @param wrapLongWords Whether to wrap long words (a word which is longer than one line (wrapLength); i.e. the word does not contain a white space before wrapLength) at the position of wrapLength or not.
     * @param lineBreak String to use for line breaks 
     * @return the wrapped text
     */
    public static String wrapText(String text, int wrapLength, boolean wrapLongWords, String lineBreak) {
        StringBuilder ret = new StringBuilder();
        String lines[] = text.split(lineBreak);
        for (String line : lines) {
            ret.append(wrapLine(line, wrapLength, true, lineBreak));
        }
        return ret.toString();
    }
}
