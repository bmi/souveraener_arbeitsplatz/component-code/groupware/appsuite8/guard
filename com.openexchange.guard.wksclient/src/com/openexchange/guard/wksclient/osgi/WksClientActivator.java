/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.wksclient.WKSClientService;
import com.openexchange.guard.wksclient.impl.CachingWKSClientImpl;
import com.openexchange.guard.wksclient.impl.WKSClientImpl;
import com.openexchange.guard.wksclient.impl.WKSKeySources;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link WksClientActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class WksClientActivator extends HousekeepingActivator {

    private static final Logger logger = LoggerFactory.getLogger(WksClientActivator.class);

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {GuardConfigurationService.class, KeySourceFactory.class, RemoteKeyCacheStorage.class};
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        logger.info("Starting bundle: {}", context.getBundle().getSymbolicName());
        WKSKeySources.initialize(getService(KeySourceFactory.class));
        WKSClientService wksClient = new CachingWKSClientImpl(new WKSClientImpl(this),getService(RemoteKeyCacheStorage.class));
        registerService(WKSClientService.class,wksClient);
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        logger.info("Stopping bundle: {}", context.getBundle().getSymbolicName());
        unregisterService(WKSClientService.class);
        super.stopBundle();
    }
}
