/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.autocrypt;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link BackendAutoCryptServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class BackendAutoCryptServiceImpl implements BackendAutoCryptService {

    private static final Logger LOG = LoggerFactory.getLogger(BackendAutoCryptServiceImpl.class);

    private ServiceLookup services;
    private final String GUARD_MAIL_CAPABILITY = "guard-mail";
    private final GuardApi guardApi;

    public BackendAutoCryptServiceImpl(ServiceLookup services, GuardApi guardApi) {
        this.services = services;
        this.guardApi = guardApi;
    }

    /**
     * Verify the user has guard-mail capability
     * @param session
     * @return true if capability exists
     * @throws OXException
     */
    private boolean userHasCapability (Session session) throws OXException {
        if (session == null) {
            return false;
        }
        final CapabilitySet capabilities = services.getServiceSafe(CapabilityService.class).getCapabilities(session);
        return capabilities.contains(GUARD_MAIL_CAPABILITY);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.mail.autocrypt.AutoCryptService#updateOutgoing(javax.mail.internet.MimeMessage, com.openexchange.session.Session)
     */
    @Override
    public MimeMessage updateOutgoing(MimeMessage message, Session session) throws OXException {
        if (userHasCapability(session)) {
            try {
                final Address[] fromArray = message.getFrom();
                if (fromArray != null && fromArray.length > 0) {  // requires from address
                    final InternetAddress addr = new InternetAddress(fromArray[0].toString());
                    final String fromAddress = addr.getAddress();
                    final JSONObject response = guardApi.doCallGet(
                        GuardApis.mapFor("action", "getautocrypt",
                        "id", Integer.toString(session.getUserId()),
                        "cid", Integer.toString(session.getContextId()),
                        "email", fromAddress
                        ), JSONObject.class, session);

                    if (response.has("data")) {
                        try {
                            final String header = response.getString("data");
                            if (header != null && !header.isEmpty() && !header.equals("null")) {
                                message.addHeader("Autocrypt", header);
                                message.saveChanges();
                            }
                        } catch (JSONException | MessagingException ex) {
                            LOG.error("Error processing autocrypt key", ex);
                        }
                    }
                }
                // Catch all errors for adding autocrypt and proceed.  We don't want to
                // block outgoing emails due to autocrypt failure.  But log it.  Issues have existed
                // with versioning between Guard and MW causing all outgoing emails to fail.
            } catch (final Throwable ex) {
                LOG.error("Error adding autocrypt header ", ex);
            }

        }
        return message;

    }

}
