/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link MailGuestEmailAddressHandler}
 * Retrieves email addresses associated with a user account
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class MailGuestEmailAddressHandler {

    private final UserService userService;

    public MailGuestEmailAddressHandler(UserService userService) {
        this.userService = userService;
    }


    private String[] getAddresses (int userId, int cid) throws OXException {
        User user = userService.getUser(userId, cid);
        if (user != null) {
            return user.getAliases();
        }
        return null;
    }

    public JSONObject getEmails (int userId, int cid) throws OXException {
        try {
            User user = userService.getUser(userId, cid);
            if (user == null) {
                return new JSONObject().put("data", "");
            }
            String[] aliases = getAddresses(userId, cid);
            JSONObject result = new JSONObject();
            result.put("primary", user.getMail());
            JSONArray aliasesArray = new JSONArray();
            if (aliases != null) {
                for (String alias: aliases) {
                    aliasesArray.put(alias);
                }
            }
            result.put("aliases", aliasesArray);
            return result;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
