/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.user.UserIdentity;

public class UserData {

    private static final Logger LOG = LoggerFactory.getLogger(UserData.class);

    private int userid;

    private int cid;


    private String encr_password;

    public UserData() {

    }

    public UserData(String auth, GuardUserSession userSession) {
        long start = System.currentTimeMillis();
        userid = -1;
        encr_password = null;
        try {
            GuardSessionService sessionService = Services.getService(GuardSessionService.class);
            String token = sessionService.getToken(userSession.getGuardSession());
            if (token == null) {
                return;
            }
            TokenAuthenticationService tokenService = Services.getService(TokenAuthenticationService.class);
            UserIdentity userIdent = tokenService.decryptUserIdentity(auth, token);
            userid = userIdent.getOXUser().getId();
            cid = userIdent.getOXUser().getContextId();
            encr_password = new String(userIdent.getPassword());
        } catch (Exception e) {
            LOG.error("Error creating user data for " + userid, e);
        }
        LOG.debug("Decode user data at " + start);
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getEncr_password() {
        return encr_password;
    }

    public void setEncr_password(String encr_password) {
        this.encr_password = encr_password;
    }
}
