/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.masterKey;

import static com.openexchange.java.Autoboxing.I;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.keysources.RealtimeKeyPairSource;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardMasterKeyExceptionCodes;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.server.ServiceLookup;

/**
 * {@link IndexMasterKeyService}
 * Retrieve the master keys from file system and database
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.6
 */
public class IndexMasterKeyService extends AbstractMasterKeyService {

    private static final Logger LOG = LoggerFactory.getLogger(IndexMasterKeyService.class);

    private final ServiceLookup services;

    public IndexMasterKeyService(ServiceLookup services, GuardCipherService cipherService) {
        super(cipherService);
        this.services = services;
    }

    /**
     * Return the guardConfigService
     * getConfigService
     *
     * @return
     * @throws OXException
     */
    private GuardConfigurationService getConfigService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }

    /**
     * Gets the location and filename for the oxguardpass file for a specific index
     * getOxGuardPassFile
     *
     * @param index
     * @return
     * @throws OXException
     */
    protected File getOxGuardPassFile(int index) throws OXException {
        if (index == 0) {
            File oldPass = getConfigService().getPropertyFile("oxguardpass");
            if (oldPass != null && oldPass.exists()) {
                return oldPass;
            }
        }
        String directory = getConfigService().getProperty(GuardProperty.masterKeyPath);
        File f = new File(directory);
        if (!f.exists()) {
            throw GuardMasterKeyExceptionCodes.BAD_CONFIGURATION.create("Master key file path", "Missing folder");
        }
        final String filename = index == 0 ? "/oxguardpass" : ("/oxguardpass." + index);
        return new File(directory + filename);
    }

    /**
     * Reads the master key file from the specified file and returns the contents in a List where every entry represents a line
     *
     * @param file The file to read the master key from
     * @return A list with the contents of the file, where every entry represents a line
     * @throws OXException
     */
    private List<String> readMasterKeyFile(File file) throws OXException {
        try {
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            if (lines.size() != 2) {
                throw GuardMasterKeyExceptionCodes.MALFORMED_MASTER_KEY_FILE.create(file.getAbsolutePath());
            }
            return lines;
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Reads the master password file 'oxguardpass'
     *
     * @return
     * @throws OXException
     */
    protected Map<String, String> readMasterKey(File file) throws OXException {
        // Read lines from the 'oxguardpass'
        List<String> lines = readMasterKeyFile(file);

        // Get lines
        Map<String, String> tmp = new HashMap<String, String>(2);
        for (String line : lines) {
            String k = line.substring(0, 2);
            String v = line.substring(2);
            tmp.put(k, v);
        }

        if (tmp.isEmpty()) {
            throw GuardMasterKeyExceptionCodes.MALFORMED_MASTER_KEY_FILE.create(file.getAbsolutePath());
        }

        return tmp;
    }

    /**
     * Retrieve the key with specified index.
     * getKey
     *
     * @param index Index of the key
     * @param recovery If the master private key should be loaded for recovery
     * @return
     * @throws OXException
     */
    private MasterKey getKey(int index, boolean recovery) throws OXException {
        File oxguardPass = getOxGuardPassFile(index);
        if (!oxguardPass.exists()) {
            throw GuardMasterKeyExceptionCodes.INDEXED_MASTER_PASSWORD_FILE_MISSING.create(I(index));
        }
        // Read file
        Map<String, String> codes = readMasterKey(oxguardPass);
        final String mc = codes.get("MC");
        final String rc = codes.get("RC");
        GuardKeyService guardKeyService = services.getServiceSafe(GuardKeyService.class);
        final int base = (index + 1) * -2;
        GuardKeys mKey = null;
        if (recovery) {
            mKey = guardKeyService.getKeys(base + 1, 0);
            if (mKey == null) {
                throw GuardMasterKeyExceptionCodes.UNABLE_TO_RETIREVE_MASTER_PASSWORD.create();
            }
        }
        GuardKeys cKey = guardKeyService.getKeys(base, 0);
        if (cKey != null) {
            return new MasterKey(rc, mc, cKey, mKey, index);
        }
        throw GuardMasterKeyExceptionCodes.UNABLE_TO_RETIREVE_MASTER_PASSWORD.create();
    }

    /**
     * Generates the master keys in database with the specified mc and rc salts
     *
     * @param mc The MC salt
     * @param rc The RC salt
     * @throws OXException
     */
    private MasterKey generateMasterKey(String rc, String mc, int index) throws OXException {
        try {
            final int base = (index + 1) * -2;
            KeyCreationService keyCreationService = services.getServiceSafe(KeyCreationService.class);
            GuardKeyService guardKeyService = services.getServiceSafe(GuardKeyService.class);
            GuardKeys mkeys = keyCreationService.create(new RealtimeKeyPairSource(getConfigService()), base + 1, 0, "master", "dont@remove.com", mc, null, true, false, 0, 0);

            if (mkeys == null) {
                LOG.error("Unable to create master keys");
            } else {
                guardKeyService.storeKeys(mkeys, false);
            }
            GuardKeys ckeys = keyCreationService.create(new RealtimeKeyPairSource(getConfigService()), base, 0, "master", "dont@remove.com", rc, null, true, false, 0, 0);
            if (ckeys == null) {
                LOG.error("Unable to create master keys for client encryption");
                throw GuardMasterKeyExceptionCodes.UNABLE_TO_CREATE.create();
            }

            guardKeyService.storeKeys(ckeys, false);
            return new MasterKey(rc, mc, ckeys, mkeys, index);
        } catch (Exception ex) {
            LOG.error("Unable to create master keys", ex);
            throw ex;
        }

    }

    /**
     * Gets the next index to be used
     * getNextIndex
     *
     * @return
     * @throws OXException
     */
    private int getNextIndex() throws OXException {
        KeyTableStorage keyStorage = services.getServiceSafe(KeyTableStorage.class);
        final int lowest = keyStorage.getMinFromSystemTable();
        int index = lowest / -2;
        index -= lowest % 2;  // May be odd number if system public key imported.
        return index;
    }

    @Override
    public MasterKey getMasterKey(int index, boolean recovery) throws OXException {
        return getKey(index, recovery);
    }

    @Override
    public int getIndexForUser(int userId, int cid) throws OXException {
        if (cid <= 0) {
            return 0;
        }
        return getConfigService().getIntProperty(GuardProperty.masterKeyIndex, userId, cid);
    }

    @Override
    public MasterKey getMasterKey(int userId, int cid, boolean recovery) throws OXException {
        return getMasterKey(getIndexForUser(userId, cid), recovery);
    }

    @Override
    public Key getDecryptedClientKey(MasterKey key) throws OXException {
        if (key.getClientPrivateKey() != null) {
            return key.getClientPrivateKey();
        }
        GuardKeyService guardKeyService = services.getServiceSafe(GuardKeyService.class);
        Key privateKey = guardKeyService.decryptPrivateKey(key.getClientEncodedPrivate(), key.getRC(), key.getClientSalt());
        key.setClientPrivateKey(privateKey);
        return privateKey;
    }

    @Override
    public String getRcEncryted(String data, String salt, int index) throws OXException {
        return encrypt(getMasterKey(index, false), data, salt);
    }

    @Override
    public String getRcDecrypted(String data, String salt, int index) throws OXException {
        return decrypt(getMasterKey(index, false), data, salt);
    }

    @Override
    public MasterKey createKey() throws OXException {
        return createKey(getNextIndex());
    }

    @Override
    public MasterKey createKey(int index) throws OXException {
        File newOxguardpass = getOxGuardPassFile(index);
        String mc, rc;
        if (!newOxguardpass.exists()) {
            mc = CipherUtil.generateSalt();
            rc = CipherUtil.generateSalt();
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newOxguardpass), StandardCharsets.UTF_8))) {
                writer.write("MC" + mc + "\n");
                writer.write("RC" + rc + "\n");
            } catch (UnsupportedEncodingException | FileNotFoundException e) {
                LOG.error("Error writing oxguardpass file", e);
                throw OXException.general("Unable to save oxguardPass file", e);
            } catch (IOException e) {
                LOG.error("Error writing oxguardpass file", e);
                throw GuardMasterKeyExceptionCodes.UNABLE_TO_CREATE.create();
            }
        } else {
            Map<String, String> pass = readMasterKey(newOxguardpass);
            mc = pass.get("MC");
            rc = pass.get("RC");
            if (rc == null || mc == null) {
                throw GuardMasterKeyExceptionCodes.MALFORMED_MASTER_KEY_FILE.create();
            }
        }

        return generateMasterKey(rc, mc, index);
    }
}
