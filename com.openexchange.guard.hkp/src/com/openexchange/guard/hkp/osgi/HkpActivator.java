/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.osgi;

import org.osgi.service.http.HttpService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.hkp.servlets.HkpServlet;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link HkpActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class HkpActivator extends HousekeepingActivator {

    private static final String GUARD_PGPHKP_SERVLET_PATH = "/pgp";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { HttpService.class, GuardConfigurationService.class, RecipKeyService.class,
                                GuardKeyService.class, GuardRatifierService.class, AccountCreationService.class, PGPKeysStorage.class, OGPGPKeysStorage.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(HkpActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        getService(HttpService.class).registerServlet(GUARD_PGPHKP_SERVLET_PATH, new HkpServlet(), null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(HkpActivator.class).info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());

        HttpService httpService = getService(HttpService.class);
        if (httpService != null) {
            httpService.unregister(GUARD_PGPHKP_SERVLET_PATH);
        }

        Services.setServiceLookup(null);

        super.stopBundle();
    }
}
