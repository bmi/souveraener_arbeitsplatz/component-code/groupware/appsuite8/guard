/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration;

import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.osgi.Services;

/**
 * {@link GuardProductName}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardProductName {

    /**
     * Gets the configured product name
     *
     * @param host the host to the get the host specific product name for
     * @return the configured product name for the given host
     * @throws OXException
     */
    public static String getProductName(String host) throws OXException {
        return getProductName(host, 0, 0);
    }

    /**
     * Gets the configured product name
     *
     * @param host the host to the get the host specific product name for
     * @param cid the context id of the user to get the product name for
     * @param userid the id of the user to get the product name for
     * @return the configured product name for the given host
     * @throws OXException
     */
    public static String getProductName(String host, int cid, int userid) throws OXException {
        GuardConfigurationService configuartionService = Services.getService(GuardConfigurationService.class);
        String name = null;
        if (host != null && !host.isEmpty()) {
            if (cid < 0) {  // Guest account, no cascade settings here.
                userid = -1;
                cid = -1;
            }
            name = configuartionService.getServerConfiguration(host, userid, cid, "guard.productName");
        }
        if (name == null) {
            if (cid > 0) {
                return (configuartionService.getProperty(GuardProperty.productName, userid, cid));
            } else {
                return (configuartionService.getProperty(GuardProperty.productName, -1, cid));
            }
        }
        return name;
    }
}
