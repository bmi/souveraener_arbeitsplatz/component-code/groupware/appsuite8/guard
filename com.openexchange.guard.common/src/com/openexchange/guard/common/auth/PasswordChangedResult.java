/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.common.auth;

/**
 * {@link PasswordChangedResult}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.6
 */
public class PasswordChangedResult {

    private final String auth;
    private boolean hasRecovery;

    /**
     * Initializes a new {@link PasswordChangedResult}.
     *
     * @param auth The new authentication token which can be used after a password change
     */
    public PasswordChangedResult(String auth) {
        this.auth = auth;
    }

    /**
     * Gets the new authentication token which can be used after a password change
     *
     * @return The new authentication token
     */
    public String getAuth() {
        return auth;
    }

    /**
     * Gets whether or not the password recovery is activated
     *
     * @return <code>True</code> if the password recovery is activated, <code>False</code> otherwise
     */
    public boolean hasRecovery() {
        return hasRecovery;
    }

    /**
     * Sets whether the password recovery is available
     *
     * @param hasRecoverys <code>True</code> if the password recovery is available, <code>False</code> otherwise
     * @return <code>this</code> for convenience reason
     */
    public PasswordChangedResult withRecovery(boolean hasRecoverys) {
        this.hasRecovery = hasRecoverys;
        return this;
    }
}
