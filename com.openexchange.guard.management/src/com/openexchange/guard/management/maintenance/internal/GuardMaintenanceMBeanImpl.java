/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.maintenance.internal;

import javax.management.MBeanException;
import javax.management.NotCompliantMBeanException;
import javax.management.StandardMBean;
import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.GuardMaintenanceService;
import com.openexchange.guard.management.maintenance.GuardMaintenanceMBean;

/**
 * {@link GuardMaintenanceMBeanImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
// FIXME: Just copied the logic from {@link HandleArgs} class. Needs cleaning up, exception handling.
public class GuardMaintenanceMBeanImpl extends StandardMBean implements GuardMaintenanceMBean {

    private final GuardMaintenanceService maintenanceService;

    /**
     * Initialises a new {@link GuardMaintenanceMBeanImpl}.
     *
     * @param guardMaintenanceService
     *
     * @throws NotCompliantMBeanException
     */
    public GuardMaintenanceMBeanImpl(GuardMaintenanceService guardMaintenanceService) throws NotCompliantMBeanException {
        super(GuardMaintenanceMBean.class);
        this.maintenanceService = guardMaintenanceService;
    }

    @Override
    public void reset(String email, String type) throws MBeanException {
        try {
            maintenanceService.reset(email, CryptoType.getTypeFromString(type));
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void importKey(String type, byte[] file, int userId, int cid, String oldPass, String newPass) throws MBeanException {
        try {
            maintenanceService.importKey(type, file, userId, cid, oldPass, newPass);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void removePIN(String email) throws MBeanException {
        try {
            maintenanceService.removePIN(email);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void upgrade(String email, String userid, String cid) throws MBeanException {
        try {
            maintenanceService.upgrade(email, userid, cid);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public String test(String email) throws MBeanException {
        try {
            return maintenanceService.test(email).toString();
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public TestUserResultImpl testUser(String email) throws MBeanException {
        try {
            JsonObject test = maintenanceService.test(email).get(email).getAsJsonObject();
            return new TestUserResultImpl(test.get("uid").getAsInt(),
                test.get("cid").getAsInt(),
                test.get("user").getAsJsonObject().has("displayName") ? test.get("user").getAsJsonObject().get("displayName").getAsString() : "",
                test.get("user").getAsJsonObject().get("language").getAsString());
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void deleteUser(String email) throws MBeanException {
        try {
            maintenanceService.delete(email);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void resetGuest(String email, String fromEmail, int fromId, int fromCid) throws MBeanException {
        try {
            maintenanceService.resetGuest(email, fromEmail, fromId, fromCid);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    @Override
    public void importPublic (String file) throws MBeanException {
        try {
            maintenanceService.importPublic(file);
        } catch (OXException e) {
            throw new MBeanException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.management.maintenance.GuardMaintenanceMBean#repair()
     */
    @Override
    public StringBuilder repair(boolean dryRun) throws MBeanException {
        try {
            return maintenanceService.repair(dryRun);
        } catch (OXException e) {
            throw new MBeanException(e);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.management.maintenance.GuardMaintenanceMBean#changeUser(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public void changeUser(String email, String newEmail) throws MBeanException {
        try {
            maintenanceService.changeUser(email, newEmail);
        } catch (OXException e) {
            throw new MBeanException(e);
        }

    }

    @Override
    public int createMaster() throws MBeanException {
        try {
            return maintenanceService.createMasterKeys();
        } catch (OXException e) {
            throw new MBeanException(e);
        }

    }

    @Override

    public void clean() throws MBeanException {
        try {
            maintenanceService.clean();
        } catch (OXException e) {
            throw new MBeanException(e);
        }

    }

    public void importCA(byte[] file, int grpId) throws MBeanException {
        try {
            maintenanceService.importCa(file, grpId);
        } catch (OXException e) {
            throw new MBeanException(e);
        }

    }
}
