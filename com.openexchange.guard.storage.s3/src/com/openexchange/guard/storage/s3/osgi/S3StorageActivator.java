/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.s3.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.filestore.FileStorageService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.s3.internal.S3StorageImpl;
import com.openexchange.osgi.HousekeepingActivator;

/**
 *
 * {@link S3StorageActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class S3StorageActivator extends HousekeepingActivator {

    /**
     * Initialises a new {@link S3StorageActivator}.
     */
    public S3StorageActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, FileStorageService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(S3StorageActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        GuardConfigurationService guardConfigService = getService(GuardConfigurationService.class);
        if (guardConfigService.getProperty(GuardProperty.fileStorageType).equals("s3")) {
            String fileStore = guardConfigService.getProperty(GuardProperty.s3FileStore);
            registerService(Storage.class, new S3StorageImpl(this.getService(FileStorageService.class), fileStore));
            logger.info("Guard S3Storage Service registered.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(S3StorageActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        unregisterService(Storage.class);
        logger.info("Guard S3Storage Service unregistered.");

        super.stopBundle();
    }
}
