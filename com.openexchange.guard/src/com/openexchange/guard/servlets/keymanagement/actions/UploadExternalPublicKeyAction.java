/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.keymanagement.responses.ExternalPublicKeyRingCollectionResponse;
import com.openexchange.pgp.keys.parsing.KeyRingParserResult;
import com.openexchange.pgp.keys.parsing.PGPKeyRingParser;

/**
 * {@link UploadExternalPublicKeyAction} uploads a public keyring related an external recipient for later retrieval
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UploadExternalPublicKeyAction extends GuardServletAction {

    public static final int MAX_KEY_FILE_UPLOAD_SIZE = 1000000; /* 1 MB */

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        //Parsing incoming key rings
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        List<PGPPublicKeyRing> parsedPublicKeyRings = new ArrayList<PGPPublicKeyRing>();
        Collection<FileItem> items = fileUploadHandler.parseItems(request, MAX_KEY_FILE_UPLOAD_SIZE);
        PGPKeyRingParser pgpKeyParser= Services.getService(PGPKeyRingParser.class);
        if (items.size() > 0) {
            for (FileItem item : items) {
                KeyRingParserResult parseResult = pgpKeyParser.parse(item.getInputStream());
                parsedPublicKeyRings.addAll(parseResult.toPublicKeyRings());
            }
        } else {
            throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_MISSING.create();
        }

        //Adding all parsed key rings
        PublicExternalKeyService publicExternalKeyService = Services.getService(PublicExternalKeyService.class);
        List<OGPGPKeyRing> imported = publicExternalKeyService.importPublicKeyRing(userSession.getUserId(),
                                                                                   userSession.getContextId(),
                                                                                   parsedPublicKeyRings.toArray(new PGPPublicKeyRing[parsedPublicKeyRings.size()]));
        ServletUtils.sendObject(response, new ExternalPublicKeyRingCollectionResponse(imported));
    }
}
