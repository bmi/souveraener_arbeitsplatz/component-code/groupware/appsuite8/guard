/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPSecretKey;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link SecretKeyInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class SecretKeyInfoResponse {

    private final PGPSecretKey key;

    /**
     * Initializes a new {@link SecretKeyInfoResponse}.
     *
     * @param key The secret key
     */
    public SecretKeyInfoResponse(PGPSecretKey key) {
        this.key = Objects.requireNonNull(key, "key must not be null");
    }

    public String getId() {
        return Long.toString(this.key.getKeyID());
    }

    public String getFingerPrint() {
        if (this.key.getPublicKey() == null) {
            return null;
        }
        return PGPKeysUtil.getFingerPrint(this.key.getPublicKey().getFingerprint());
    }

    public boolean isMasterKey() {
        return this.key.isMasterKey();
    }

    public String[] getUserIds() {
        ArrayList<String> userIds = new ArrayList<String>();
        Iterator iter = key.getUserIDs();
        while (iter.hasNext()) {
            userIds.add((String) iter.next());
        }
        return userIds.toArray(new String[userIds.size()]);
    }
}
