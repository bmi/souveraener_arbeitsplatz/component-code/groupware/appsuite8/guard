/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage;

import java.util.Date;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * {@link SmimePublicKeyStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface SmimePublicKeyStorage {

    /**
     * Get a list of all known keys
     *
     * @return A list of known {@link SmimeKeys}}
     * @throws OXException
     */
    List<SmimeKeys> getKeys() throws OXException;

    /**
     * Get a list of keys associated with the given email address
     *
     * @param email The email
     * @return A list of known {@link SmimeKeys} for the given email
     * @throws OXException
     */
    List<SmimeKeys> getKeys(String email) throws OXException;

    /**
     * Checks if a key exists
     *
     * @param serial The serial number of the key to check
     * @param email The email of the owner
     * @param The date that the key was used (from date on email, or current date if actively added)
     * @return <code>True</code> if a key exists with the given serial exists for the given user, <code>false</code> otherwise.
     * @throws OXException
     */
    boolean keyExists(String serial, String email, Date fromDate) throws OXException;

    /**
     * Store the public components of {@link SmimeKeys}}
     *
     * @param userId The ID of the user to store the key for
     * @param cid The context ID of the user to store the key for
     * @param keys The {@link SmimeKeys} to store
     * @param The date that the key was used (from date on email, or current date if actively added)
     * @throws OXException
     */
    void storePublicKeys(int userId, int cid, SmimeKeys keys, Date fromDate) throws OXException;

    /**
     * Get the {@link SmimeKeys} for the user with the specified serial number
     *
     * @param serial The serial number to get the {@link SmimeKeys} for
     * @param userId The ID of the user to get the key for
     * @param cid The ID of the context to get the key for
     * @return The {@link SmimeKeys} for the given serial and ids, or <code>null</code> if no such key was found
     * @throws OXException
     */
    SmimeKeys getKey(String serial, int userId, int cid) throws OXException;

    /**
     * Deletes a specified key
     *
     * @param serial The serial id of the key to delete
     * @param userId The ID of the owner
     * @param cid The context ID of the owner
     * @return <code>True</code>, if the given key was present and deleted, </code>False</code> if the key was not found and therefore not deleted.
     * @throws OXException
     */
    boolean deleteKeyById(String serial, int userId, int cid) throws OXException;

    /**
     * Deletes all public keys for a given user
     *
     * @param userId The ID of the user to delete the keys for
     * @param cid The context ID of the user to delete the key for
     * @return <code>True</code>, if at least one key was present and deleted, </code>False</code> if no key were found and therefore nothing was deleted.
     * @throws OXException
     */
    boolean deleteForUser(int userId, int cid) throws OXException;

    /**
     * Delete all {@link SmimeKeys} associated with the given context
     *
     * @param cid The ID of the context to delete all keys for
     * @return <code>True</code> if at least one key was deleted, <code>False</code> otherwise
     * @throws OXException
     */
    boolean deleteForContext(int cid) throws OXException;
}
