/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.rest;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.impl.ContextImpl;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link GuestToOxLookup}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.5
 */
public class GuestToOxLookup {

    public static JSONObject lookup(UserService userService, String email, int context) throws OXException {
        JSONObject json = new JSONObject();
        if (email == null || email.isEmpty()) {
            return json;
        }
        if (context < 0) {
            context = context * -1;
        }
        User user = userService.searchUser(email, new ContextImpl(context), true, true, false);
        if (user != null) {
            try {
                json.put("id", user.getId());
            } catch (JSONException e) {
                throw OXException.general("JSON Error", e);
            }
        }
        return json;
    }

}
