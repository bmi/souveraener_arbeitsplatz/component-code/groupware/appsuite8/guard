package com.openexchange.guard.guestupgrade.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.guestupgrade.GuestUpgradeService;
import com.openexchange.guard.guestupgrade.internal.GuestUpgradeServiceImpl;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageServiceImpl;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuestUpgradeServiceActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuestUpgradeServiceActivator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[]{GuardKeyService.class, KeyTableStorage.class,
                              EmailStorage.class, PGPKeysStorage.class,
                              GuardDatabaseService.class, GuardShardingService.class, OXUserService.class};
    }

    @Override
    protected void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(GuestUpgradeServiceActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);
        registerService(GuestUpgradeService.class, new GuestUpgradeServiceImpl());
        registerService(GuestUpgradeStorageService.class, new GuestUpgradeStorageServiceImpl(getService(GuardDatabaseService.class)));
        trackService(GuestUpgradeStorageService.class);
        openTrackers();
        logger.info("GuardGuestUpgrade registered.");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuestUpgradeServiceActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(null);
        unregisterService(GuestUpgradeService.class);
        logger.info("GuardGuestUpgrade unregistered.");
        super.stopBundle();
    }
}
