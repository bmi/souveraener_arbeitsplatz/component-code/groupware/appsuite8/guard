/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link PGPSigningService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public interface PGPSigningService {

    /**
     * Creates a signature
     *
     * @param input The data to create a signature for
     * @param output The signature
     * @param armored True, if the signature should be written ASCII-Armored, false if binary
     * @param signer The identity used for signing
     * @throws OXException
     */
    void sign(InputStream input, OutputStream output, boolean armored, UserIdentity signer) throws OXException;

    /**
     * Creates a signature
     *
     * @param input The data to create a signature for
     * @param output The signature
     * @param armored True, if the signature should be written ASCII-Armored, false if binary
     * @param signerKey The GuardKeys to create the signature with
     * @param password The password
     * @throws OXException
     */
    void sign(InputStream input, OutputStream output, boolean armored, GuardKeys signerKey, char[] password) throws OXException;

    /**
     * Verifies signatures
     *
     * @param signedData The data which are signed
     * @param signatureData The data containing one or more signatures
     * @return A list of verification results
     * @throws OXException
     */
    List<SignatureVerificationResult> verify(InputStream signedData, InputStream signatureData) throws OXException;
}
