/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.trust;

import java.util.HashMap;
import java.util.Map;
import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link KeySourceFactoryImpl} default implementation for {@link KeySourceFactory}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class KeySourceFactoryImpl implements KeySourceFactory {

    private static final Map<String, GuardProperty> trustLevelCache = new HashMap<String, GuardProperty>();

    /**
     * Internal method to cache a trust level for a given keySource
     *
     * @param keySource The {@link KeySource} to add to the internal cache.
     */
    private static void cacheTrustLevel(KeySource keySource, GuardProperty trustLevelConfigurationProperty) {
        trustLevelCache.put(keySource.getName(), trustLevelConfigurationProperty);
    }

    @Override
    public synchronized KeySource create(String name, GuardProperty trustLevelConfigurationProperty) {
        KeySource keySource = new KeySource(name, trustLevelConfigurationProperty);
        cacheTrustLevel(keySource, trustLevelConfigurationProperty);
        return keySource;
    }

    @Override
    public synchronized KeySource create(String name) {
        GuardProperty existingTrustLevel = trustLevelCache.get(name);
        if (existingTrustLevel != null) {
            return new KeySource(name, existingTrustLevel);
        }
        throw new IllegalArgumentException("Cannot create KeySource for unknown name \"" + name + "\"");
    }

}
