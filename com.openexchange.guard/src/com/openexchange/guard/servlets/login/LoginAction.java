/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.login;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.UpdateBehavior;
import com.openexchange.groupware.contexts.impl.ContextExceptionCodes;
import com.openexchange.guard.auth.AuthenticationService;
import com.openexchange.guard.auth.AuthenticationService.AuthenticationResult;
import com.openexchange.guard.auth.AuthenticationServiceRegistry;
import com.openexchange.guard.auth.GuardAuthenticationParameters;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.crypto.Exceptions.CryptoServletExceptionCodes;
import com.openexchange.server.ServiceLookup;

/**
 * OX Guard login
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class LoginAction extends GuardServletAction {

    private static final Logger LOG = LoggerFactory.getLogger(LoginAction.class);
    private static final String AUTH_RET_ERROR = "Error: Problem with login";
    private static String AUTH_RET_UPDATING = "Updating";

    private final ServiceLookup services;

    /**
     * Initializes a new {@link LoginAction}.
     *
     * @param services The service lookup
     */
    public LoginAction(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Parses required authentication parameters from the given {@link HttpServletRequest}
     *
     * @param request The request to parse the parameters from
     * @param userSession The {@link GuardUserSession} to parse the parameters for
     * @return The parsed {@link GuardAuthenticationParameters} ready to be used for a further login process
     * @throws OXException
     */
    private GuardAuthenticationParameters parseAuthenticationParameters(HttpServletRequest request, GuardUserSession userSession) throws OXException {
        final Map<String, Object> requestParameter = new HashMap<String, Object>();

        //Parse the json request body as flat parameter map
        ResponseHandler responseHandler = Services.getService(ResponseHandler.class);
        JsonObject jsonBody = responseHandler.getJsonAndDecodeEncryptedPasswords(request, userSession.getUserId(), userSession.getContextId());
        if (jsonBody != null) {
            requestParameter.putAll(new Gson().fromJson(jsonBody.toString(), HashMap.class));
        }

        //Parse the request parameters
        requestParameter.putAll(ServletUtils.getMapParameters(request));

        //@formatter:off
        return  new GuardAuthenticationParameters(
            ServletUtils.getClientIP(request),
            request.getHeader("User-Agent"),
            request.getScheme(), new OxCookie(request.getCookies()),
            requestParameter);
        //@formatter:on
    }

    // Check any update tasks needed for user
    private static boolean updateTasksNeeded(int cid, Locale locale, HttpServletResponse response) {
        try {
            ContextService contextService = Services.getService(ContextService.class);
            contextService.getContext(cid, UpdateBehavior.TRIGGER_UPDATE);
        } catch (OXException ex) {
            if (ContextExceptionCodes.UPDATE.equals(ex)) {
                LOG.info("Updates triggered for cid " + cid);
                JsonObject returnJson = new JsonObject();
                returnJson.addProperty("auth", AUTH_RET_UPDATING);
                returnJson.addProperty("error", ex.getDisplayMessage(locale));
                ServletUtils.sendJsonOK(response, returnJson);
                return true;
            }
        }
        return false;
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        if (updateTasksNeeded(userSession.getContextId(), userSession.getLocale(), response)) {
            return;
        }
        AuthenticationServiceRegistry authenticationRegistry = services.getServiceSafe(AuthenticationServiceRegistry.class);
        AuthenticationService authService = authenticationRegistry.getAuthenticationService(getCryptoType(request));
        if (authService != null) {
            GuardAuthenticationParameters authenticationParameters = parseAuthenticationParameters(request, userSession);
            try {
                AuthenticationResult authenticationResult = authService.authenticate(userSession, authenticationParameters);
                response.setContentType("application/json");
                response.addHeader("Connection", "close");
                ServletUtils.sendJsonOK(response, authenticationResult.getJson());
            } catch (Exception e) {
                JsonObject ret = new JsonObject();
                ret.addProperty("auth", AUTH_RET_ERROR);
                ServletUtils.sendJsonOK(response, ret);
            }
        } else {
            throw CryptoServletExceptionCodes.UNKNOWN_CRYPTO_TYPE.create();
        }
    }
}
