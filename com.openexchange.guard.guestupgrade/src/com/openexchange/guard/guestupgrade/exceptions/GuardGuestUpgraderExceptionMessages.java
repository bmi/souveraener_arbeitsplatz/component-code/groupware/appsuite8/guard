/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guestupgrade.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardGuestUpgraderExceptionMessages}
 *;
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestUpgraderExceptionMessages implements LocalizableStrings {

    // Unable to find guest account with specified email
    public static final String UNKNOWN_GUEST_ACCOUNT_MSG = "Unable to find guest account for email '%1$s'.";
    // The user specified by the email address is already upgraded
    public static final String ALREADY_UPGRADED_MSG = "Already upgraded user with email '%1$s'.";
    // No OX account exists with the specified email address
    public static final String UNKNOWN_OX_ACCOUNT_MSG = "No OX account with email '%1$s'.";
    // Error upgrading account specified by email %1, error message %2
    public static final String UPGRADE_ERROR_MSG = "Error upgrading account for email '%1$s': '%2$s'.";
    // Unable to upgrade the account specified by email %1 as this email resolves to a guest account
    public static final String GUEST_ACCOUNT_MSG = "Unable to upgrade by email address, email '%1$s' resolves to a guest account";

    private GuardGuestUpgraderExceptionMessages() { }
}
