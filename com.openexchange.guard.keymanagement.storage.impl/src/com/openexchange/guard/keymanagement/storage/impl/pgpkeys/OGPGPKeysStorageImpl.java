/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.pgpkeys;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.ListUtil;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;

public class OGPGPKeysStorageImpl implements OGPGPKeysStorage {

    private final static String VALUES_LIST = "{@VALUES}";

    /**
     * Internal method to convert a string of whitespace separated key ids into a list of key ids
     *
     * @param s the string of whitespace separated key ids
     * @return a list of key ids
     */
    private List<String> stringToList(String s) {
        String[] idList = s.trim().split("\\s+");
        return Arrays.asList(idList);
    }

    @Override
    public List<OGPGPKey> getForUser(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.SELECT_BY_USER_ID_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.setInt(3, userId);

            ArrayList<OGPGPKey> ret = new ArrayList<OGPGPKey>();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                OGPGPKey pgpKey = new OGPGPKey(resultSet.getInt("userid"), contextId, resultSet.getString("Email"), stringToList(resultSet.getString("ids")), resultSet.getString("PGPPublic"), resultSet.getInt("share_level"), resultSet.getBoolean("inline"));
                ret.add(pgpKey);
            }
            return ret;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public List<OGPGPKey> getForUserByEmail(int userId, int contextId, List<String> emails) throws OXException {
        if(emails.isEmpty()) {
            return Collections.emptyList();
        }

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            String sql = OGPGPKeysSql.SELECT_BY_USER_ID_AND_MAILS_STMT;
            stmt = connectionWrapper.getConnection().prepareStatement(sql.replace(VALUES_LIST, ListUtil.listToDbValuesList(emails.size())));
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.setInt(3, userId);
            int i = 4;
            for (String email : emails) {
                stmt.setString(i++, email);
            }

            ArrayList<OGPGPKey> ret = new ArrayList<OGPGPKey>();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                OGPGPKey pgpKey = new OGPGPKey(resultSet.getInt("userid"), contextId, resultSet.getString("Email"), stringToList(resultSet.getString("ids")), resultSet.getString("PGPPublic"), resultSet.getInt("share_level"), resultSet.getBoolean("inline"));
                ret.add(pgpKey);
            }
            return ret;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public OGPGPKey getForUserByEmailAndIds(int userId, int contextId, String email, List<String> keyIds) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.SELECT_BY_USER_ID_AND_MAIL_STMT);
            stmt.setString(1, email);
            stmt.setString(2, ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP));
            stmt.setInt(3, contextId);
            stmt.setInt(4, userId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new OGPGPKey(resultSet.getInt("userid"), contextId, resultSet.getString("Email"), stringToList(resultSet.getString("ids")), resultSet.getString("PGPPublic"), resultSet.getInt("share_level"), resultSet.getBoolean("inline"));

            }
            return null;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public List<OGPGPKey> getForUserByIds(int userId, int contextId, List<String> keyIds) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.SELECT_BY_IDS_STMT);
            stmt.setInt(1, contextId);
            stmt.setInt(2, userId);
            stmt.setString(3, "%" + ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP) + "%");

            resultSet = stmt.executeQuery();

            ArrayList<OGPGPKey> ret = new ArrayList<OGPGPKey>();
            while (resultSet.next()) {
                ret.add(new OGPGPKey(resultSet.getInt("userid"), contextId, resultSet.getString("Email"), stringToList(resultSet.getString("ids")), resultSet.getString("PGPPublic"), resultSet.getInt("share_level"), resultSet.getBoolean("inline")));
            }
            return ret;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public void insertOrUpdate(int userId, int contextId, String email, List<String> keyIds, String ascKeyData) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.INSERT_OR_UPDATE_STMT);

            String keyList = ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP);
            stmt.setString(1, keyList);
            stmt.setString(2, email);
            stmt.setString(3, ascKeyData);
            stmt.setInt(4, userId);
            stmt.setInt(5, 0); //Default share-level of 0
            stmt.setInt(6, contextId);
            stmt.setString(7, ascKeyData);
            stmt.setString(8, keyList);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public int delete(int userId, int contextId, List<String> keyIds) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.DELETE_STMT);
            stmt.setInt(1, contextId);
            stmt.setInt(2, userId);
            stmt.setString(3, ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP));

            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateShareLevel(int userId, int contextId, List<String> keyIds, int shareLevel) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.UPDATE_SHARE_STMT);
            stmt.setInt(1, shareLevel);
            stmt.setInt(2, contextId);
            stmt.setInt(3, userId);
            stmt.setString(4, ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP));

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateInlineMode(int userId, int contextId, List<String> keyIds, boolean useInline) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(OGPGPKeysSql.UPDATE_INLINE_STMT);
            stmt.setBoolean(1, useInline);
            stmt.setInt(2, contextId);
            stmt.setInt(3, userId);
            stmt.setString(4, ListUtil.listToString(keyIds, ListUtil.WHITE_SPACE_SEP));

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }
}
