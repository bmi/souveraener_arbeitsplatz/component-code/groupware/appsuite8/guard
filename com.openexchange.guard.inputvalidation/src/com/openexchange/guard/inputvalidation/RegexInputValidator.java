/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import java.util.regex.Pattern;

/**
 * {@link RegexInputValidator} validates input for a given regular expression
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class RegexInputValidator extends AbstractInputValidator<String> {

    private final Pattern pattern;

    /**
     * Initializes a new {@link RegexInputValidator}.
     *
     * @param pattern The pattern used for input validation
     */
    public RegexInputValidator(Pattern pattern) {
        this.pattern = pattern;
    }

    /**
     * Initializes a new {@link RegexInputValidator}.
     *
     * @param pattern The string representation of the pattern used for input validation
     */
    public RegexInputValidator(String pattern) {
        this.pattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Initializes a new {@link RegexInputValidator}.
     *
     * @param pattern The string representation of the pattern used for input validation
     * @param flags Regex options
     */
    public RegexInputValidator(String pattern, int flags) {
        this.pattern = Pattern.compile(pattern, flags);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.inputvalidation.InputValidator#isValid(java.lang.Object)
     */
    @Override
    public boolean isValid(String input) {
        return pattern.matcher(input).matches();
    }
}
