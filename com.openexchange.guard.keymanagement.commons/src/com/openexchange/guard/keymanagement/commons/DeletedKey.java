/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * Class for a key that was deleted and saved in the deletedKeyTable
 *
 * @author greg
 *
 */
public class DeletedKey {

    private final int userid;
    private final int cid;
    private PGPSecretKeyRing secretKeyRing;
    private String pgpSecret;
    private String recovery;
    private String salt;
    private int version;
    private String email;
    private boolean exposed;

    /**
     * Creating a deleted key object based on a existing key
     *
     * @param key The key to create the deleted key from
     * @throws OXException
     */
    public DeletedKey(GuardKeys key) throws OXException {
        this.userid = key.getUserid();
        this.cid = key.getContextid();
        this.recovery = key.getRecovery();
        this.salt = key.getSalt();
        this.version = key.getVersion();
        this.email = key.getEmail();
        try {
            if (key.getEncodedPGPSecret() != null) {
                this.secretKeyRing = new PGPSecretKeyRing(Base64.decodeBase64(key.getEncodedPGPSecret()), new BcKeyFingerprintCalculator());
            } else {
                this.secretKeyRing = null;
            }
            this.pgpSecret = key.getEncodedPGPSecret();
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (PGPException e) {
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        }
    }

    public DeletedKey(int userId, int cid, String pgpSecret, String recovery, String salt, int version, String email, boolean exposed) throws OXException {
        this.userid = userId;
        this.cid = cid;
        try {
            if (pgpSecret != null) {
                this.secretKeyRing = new PGPSecretKeyRing(Base64.decodeBase64(pgpSecret), new BcKeyFingerprintCalculator());
            } else {
                this.secretKeyRing = null;
            }
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (PGPException e) {
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        }

        this.pgpSecret = pgpSecret;
        this.recovery = recovery;
        this.salt = salt;
        this.version = version;
        this.email = email;
        this.exposed = exposed;
    }

    public PGPSecretKeyRing getSecretKeyRing() {
        return secretKeyRing;
    }

    public void setSecretKeyRing(PGPSecretKeyRing secretKeyRing) {
        this.secretKeyRing = secretKeyRing;
    }

    public String getRecovery() {
        return recovery;
    }

    public void setRecovery(String recovery) {
        this.recovery = recovery;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPGPSecret() {
        return pgpSecret;
    }

    public void setPgpSecret(String pgpSecret) {
        this.pgpSecret = pgpSecret;
    }

    public boolean isExposed() {
        return exposed;
    }

    public void setExposed(boolean exposed) {
        this.exposed = exposed;
    }

    public int getUserId() {
        return userid;
    }

    public int getCid() {
        return cid;
    }

    /**
     * Verify the password is correct for this key
     *
     * @param password
     * @return
     */
    public boolean verifyPassword(String password) {
        char[] pass = CipherUtil.getSHA(password, salt).toCharArray();
        PGPSecretKey sec_key = secretKeyRing.getSecretKey();
        try {
            PGPPrivateKey decryptor = sec_key.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build(pass));
            if (decryptor != null) {
                return (true);
            }
        } catch (PGPException ex) {
            return (false);
        }
        return (false);
    }

    /**
     * Get arraylist of userids for this keyring
     *
     * @return
     */
    public ArrayList<String> getUserIds() {
        ArrayList<String> ids = new ArrayList<String>();
        @SuppressWarnings("unchecked") Iterator<String> it = secretKeyRing.getSecretKey().getUserIDs();
        while (it.hasNext()) {
            ids.add(it.next());
        }
        return (ids);
    }

    /**
     * Get arraylist of fingerprints for this keyring
     *
     * @return
     */
    public ArrayList<String> getKeyFingerprints() {
        ArrayList<String> fps = new ArrayList<String>();
        Iterator<PGPSecretKey> keys = secretKeyRing.getSecretKeys();
        while (keys.hasNext()) {
            PGPSecretKey skey = keys.next();
            fps.add(PGPKeysUtil.getFingerPrintInBlocks(skey.getPublicKey().getFingerprint()));
        }
        return (fps);
    }
}
