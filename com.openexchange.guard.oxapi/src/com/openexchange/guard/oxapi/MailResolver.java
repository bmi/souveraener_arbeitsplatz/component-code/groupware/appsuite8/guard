/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.context.CustomHttpClientContext;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionCodes;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * {@link MailResolver} resolves email addresses to internal OX Guard users
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class MailResolver extends AbstractOXCalls {

    private static final Logger LOG = LoggerFactory.getLogger(MailResolver.class);

    /**
     * Internal method to call the remote mail resolver
     *
     * @param url The complete URL pointing to the mail resolver
     * @param userAgent The user agent to send
     * @return A JSON structure as described in {@link https://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver#Custom_Mail_Resolver}
     * @throws OXException
     */
    private JsonObject resolveInternal(String url, String userAgent) throws OXException {
        JsonObject result = null;
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();

        HttpGet getRequest = new HttpGet(url);
        getRequest.addHeader("accept", "application/json");
        getRequest.addHeader(getAuthenticationHeader());
        getRequest.setHeader("User-Agent", userAgent);

        HttpResponse response = null;
        try {
            HttpContext context = CustomHttpClientContext.create();
            response = httpClient.execute(getRequest, context);
            if (response.getStatusLine().getStatusCode() != 200) {
                EntityUtils.consume(response.getEntity());
                getRequest.releaseConnection();

                String resp = response.getStatusLine().getReasonPhrase();
                if (resp != null) {
                    if (resp.contains("Cannot find user")) {
                        result = new JsonObject();
                        result.addProperty("user", "not found");
                        return (result);
                    }
                }
                LOG.error("Error getting user data from backend " + response.getStatusLine().getReasonPhrase());
                throw OXApiExceptionCodes.WRONG_ERROR_CODE.create(response.getStatusLine().getStatusCode());
            }
            result = parseResponse(response);
            EntityUtils.consume(response.getEntity());
            getRequest.releaseConnection();
        } catch (Exception e2) {
            LOG.error("unable to close http stream after error", e2);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return result;
    }

    /**
     * Format response into a JSON object
     *
     * @param response
     * @return The JsonObject from the given response body
     * @throws UnsupportedEncodingException
     * @throws IllegalStateException
     * @throws IOException
     */
    private JsonObject parseResponse(HttpResponse response) throws OXException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
            JsonParser parser = new JsonParser();
            JsonObject result = (JsonObject) parser.parse(reader);
            return result;
        } catch (IllegalStateException | IOException e) {
            throw OXApiExceptionCodes.HANDLE_SERVER_RESPONSE_ERROR.create(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw OXApiExceptionCodes.HANDLE_SERVER_RESPONSE_ERROR.create(e);
                }
            }
        }
    }

    /**
     * Internal method to obtain the default resolver's REST end point
     *
     * @return The REST location for the default resolver
     * @throws OXException
     */
    private String getDefaultURL() throws OXException {
        return getMainURI() + "/preliminary/utilities/mailResolver/v1/resolve/";
    }

    /**
     * Internal method for URL encoding a given value
     *
     * @param value The value to encode
     * @return The URL encoded value
     * @throws OXException
     */
    private String urlEncode(String value) throws OXException {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e,e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.oxapi.AbstractOXCalls#getBasicAuthenticationEncoding()
     */
    @Override
    protected Header getAuthenticationHeader() throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        String basicAuthUserName = guardConfigurationService.getProperty(GuardProperty.basicAuthUsername);
        String basicAuthPassword  = guardConfigurationService.getProperty(GuardProperty.basicAuthPassword);
        if(basicAuthUserName != null && !basicAuthUserName.isEmpty()) {
            //Using the configured credentials for accessing the MailResolver
            return getBasicAuthenticationForCredentials(basicAuthUserName, basicAuthPassword);
        }
        //Using the REST-API credentials as fallback/default
        return super.getAuthenticationHeader();
    }

    public MailResolverResult resolveUserObject(String email) throws OXException {
        JsonObject jsonResult = resolveUser(email);
        if (jsonResult == null) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("No response when resolving user");
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonResult.toString(), MailResolverResult.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Resolves an internal OX user by a given email
     *
     * @param email The email
     * @return A JSON structure as described in {@link https://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver#Custom_Mail_Resolver}
     * @throws OXException
     */
    public JsonObject resolveUser(String email) throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        String apiUserAgent = guardConfigurationService.getProperty(GuardProperty.apiUserAgent);
        String customMailResolverUrl = guardConfigurationService.getProperty(GuardProperty.mailResolverUrl);
        email = urlEncode(email);

        //If mailReolverUrl is not defined in the configuration file, use default location. Otherwise, use the assigned URL
        String resolverUrl = Strings.isEmpty(customMailResolverUrl) ? getDefaultURL() : customMailResolverUrl;
        return resolveInternal(resolverUrl + email, apiUserAgent);
    }
}
