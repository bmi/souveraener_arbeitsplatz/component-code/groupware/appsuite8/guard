/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.servlets.keymanagement.pgp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.KeyImportService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.keymanagement.responses.GuardKeyInfoCollectionResponse;
import com.openexchange.pgp.keys.parsing.KeyRingParserResult;
import com.openexchange.pgp.keys.parsing.PGPKeyRingParser;

/**
 * {@link UploadPGPKeys}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class UploadPGPKeys {

    private static final String KEY_PARAMETER_NAME = "key";
    private static final String KEY_PASSWORD_PARAMETER_NAME = "keyPassword";
    private static final String NEW_PASSWORD_PARAMETER_NAME = "newPassword";

    /**
     * Internal method to remove duplicated key rings
     *
     * @param importedKeyRings The list of keys rings
     * @return The list where duplicates have been removed
     */
    private static Collection<GuardKeys> removeDuplicatedImportedKeyResults(Collection<GuardKeys> importedKeyRings) {

        Collection<GuardKeys> ret = new ArrayList<GuardKeys>(importedKeyRings.size());
        Collection<Long> recognizedIds = new ArrayList<Long>(importedKeyRings.size());

        importedKeyRings.forEach(key -> {
            if (!recognizedIds.contains(new Long(key.getPGPPublicKey().getKeyID()))) {
                recognizedIds.add(new Long(key.getPGPPublicKey().getKeyID()));
                ret.add(key);
            }
        });

        return ret;
    }

    public static GuardKeyInfoCollectionResponse doUpload(Collection<FileItem> items, String email, GuardUserSession userSession) throws OXException {
        //Parsing
        List<PGPSecretKeyRing> parsedSecretKeyRings = null;
        List<PGPPublicKeyRing> parsedPublicKeyRings = null;
        String keyPassword = null;
        String newPassword = null;
        PGPKeyRingParser keyParser = Services.getService(PGPKeyRingParser.class);
        try {
            if (items.size() > 0) {
                //Get actual Key data
                for (FileItem item : items) {
                    if (item.getFieldName().equals(KEY_PASSWORD_PARAMETER_NAME)) {
                        keyPassword = item.getString(StandardCharsets.UTF_8.displayName());
                    } else if (item.getFieldName().equals(NEW_PASSWORD_PARAMETER_NAME)) {
                        newPassword = item.getString(StandardCharsets.UTF_8.displayName());
                    } else {
                        KeyRingParserResult parseResult = keyParser.parse(item.getInputStream());
                        if (parseResult != null) {
                            parsedSecretKeyRings = new ArrayList<PGPSecretKeyRing>();
                            parsedSecretKeyRings.addAll(parseResult.toSecretKeyRings());
                            parsedPublicKeyRings = new ArrayList<PGPPublicKeyRing>();
                            parsedPublicKeyRings.addAll(parseResult.toPublicKeyRings());
                        }
                    }
                }
            } else {
                throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_MISSING.create();
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_ERROR.create(e.getMessage());
        }
        if ((parsedPublicKeyRings == null || parsedSecretKeyRings == null) || // Rings should be initialized
            (parsedPublicKeyRings.isEmpty() && parsedSecretKeyRings.isEmpty())) {  // Should have found something...
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create(KEY_PARAMETER_NAME);
        }

        //Check if password parameters are present when importing secret keys
        if (parsedSecretKeyRings.size() > 0) {
            if (keyPassword == null) {
                parsedSecretKeyRings.clear();
            }
            if (!parsedSecretKeyRings.isEmpty() && (newPassword == null || newPassword.isEmpty())) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(NEW_PASSWORD_PARAMETER_NAME);
            }
        }

        //Import parsed keys
        KeyImportService keyImportService = Services.getService(KeyImportService.class);
        Collection<GuardKeys> importedKeyRings = keyImportService.importPrivateKeyRing(userSession.getGuardUserId(), userSession.getGuardContextId(), email, userSession.getLocale(), keyPassword, newPassword, parsedSecretKeyRings.toArray(new PGPSecretKeyRing[parsedSecretKeyRings.size()]));
        importedKeyRings.addAll(keyImportService.importPublicKeyRing(userSession.getGuardUserId(), userSession.getGuardContextId(), email, userSession.getLocale(), parsedPublicKeyRings.toArray(new PGPPublicKeyRing[parsedPublicKeyRings.size()])));

        //the result list can contain duplicates because if public + private keys are imported / merged keys are also returned.
        //Cleaning up for a client.
        importedKeyRings = removeDuplicatedImportedKeyResults(importedKeyRings);
        return new GuardKeyInfoCollectionResponse(newPassword, importedKeyRings);
    }

}
