/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.IOException;
import java.io.InputStream;
import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;

/**
 * {@link GuardSmimeUtil}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class GuardSmimeUtil {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(GuardSmimeUtil.class);
    }

    /**
     * Rewrite the content of a MimeMessage with the contents of an inputStream
     *
     * @param msg Origianl message
     * @param in InputStream used to replace the msg contents
     * @param type Mime type of the inputStream
     * @return MimeMessage updated with the contents of the new inputstream
     * @throws OXException
     */
    public static MimeMessage updateFromInputStream(MimeMessage msg, InputStream in, String type) throws OXException {
        try {
            msg.setDataHandler(new DataHandler(new ByteArrayDataSource(in, type)));
            msg.saveChanges();
        } catch (MessagingException | IOException e) {
            LoggerHolder.LOGGER.debug("Unable to replace content", e);
            throw SmimeExceptionCodes.SMIME_ERROR.create(e.getMessage());
        }
        return msg;
    }

}
