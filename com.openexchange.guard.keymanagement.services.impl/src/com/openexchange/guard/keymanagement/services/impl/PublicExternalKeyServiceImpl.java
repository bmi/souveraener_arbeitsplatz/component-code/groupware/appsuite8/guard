/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardKeyImportExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PublicExternalKeyServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class PublicExternalKeyServiceImpl implements PublicExternalKeyService {

    public ServiceLookup services;

    /**
     * Initializes a new {@link PublicExternalKeyServiceImpl}.
     *
     * @param guardRatifierService
     * @param pgpKeysStorage
     */
    public PublicExternalKeyServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    private OGPGPKeysStorage getKeyStorage() throws OXException {
        return services.getServiceSafe(OGPGPKeysStorage.class);
    }

    private String parseEmailAdress(String identity) throws AddressException, UnsupportedEncodingException {
        if (identity.contains("<")) {
            String name = identity.substring(0, identity.lastIndexOf("<"));
            if (!name.contains("\"")) {
                identity = "\"" + name.trim() + "\" " + identity.substring(identity.lastIndexOf("<"));
            }
        }

        InternetAddress[] recips = InternetAddress.parse(IDNUtil.aceEmail(identity), false);
        if (recips.length > 0) {
            return new InternetAddress(recips[0].getAddress(), recips[0].getPersonal(), "UTF-8").getAddress();
        }
        return null;
    }

    private OGPGPKeyRing importPublicKeyRing(int userId, int contextId, PGPPublicKeyRing publicKeyRing) throws OXException {

        ArrayList<String> keyEmailIdentities = new ArrayList<String>();
        ArrayList<String> keyHexIds = new ArrayList<String>();
        for (PGPPublicKey publicKey : publicKeyRing) {
            Iterator iter = publicKey.getUserIDs();
            while (iter.hasNext()) {
                try {
                    String adress = parseEmailAdress((String) iter.next());
                    if (adress != null) {
                        keyEmailIdentities.add(IDNUtil.decodeEmail(adress));
                    }
                } catch (AddressException | UnsupportedEncodingException e) {
                    continue;
                }
            }
            keyHexIds.add(LongUtil.longToHexStringTruncated(publicKey.getKeyID()));
        }

        if (!keyHexIds.isEmpty()) {
            if (keyEmailIdentities.isEmpty()) {
                throw GuardKeyImportExceptionCodes.NO_SUCH_USERID_IN_KEY_ERROR.create();
            }
            for (String keyEmailIdentity : keyEmailIdentities) {
                try {
                    services.getServiceSafe(GuardRatifierService.class).validate(keyEmailIdentity);
                } catch (OXException e) {
                    // Invalid email address.
                    keyEmailIdentity = "";
                }

                //Adding the new uploaded key to the storage
                String asciiData = KeyExportUtil.export(publicKeyRing);
                OGPGPKeysStorage ogpgpKeysStorage = Services.getService(OGPGPKeysStorage.class);
                ogpgpKeysStorage.insertOrUpdate(userId, contextId, keyEmailIdentity, keyHexIds, asciiData);

            }
            final boolean isInline = false;
            final int shareLevel = 0;
            final boolean owned = true;
            return new OGPGPKeyRing(OGPGPKey.keyIdsAsString(keyHexIds), publicKeyRing, isInline, shareLevel, owned);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#importPublicKeyRing(org.bouncycastle.openpgp.PGPPublicKeyRing)
     */
    @Override
    public List<OGPGPKeyRing> importPublicKeyRing(int userId, int contextId, PGPPublicKeyRing... publicKeyRings) throws OXException {
        publicKeyRings = Objects.requireNonNull(publicKeyRings, "publicKeyRing must not be null");
        List<OGPGPKeyRing> importedKeysRings = new ArrayList<OGPGPKeyRing>();
        for (PGPPublicKeyRing publicKeyRing : publicKeyRings) {
            OGPGPKeyRing imported = importPublicKeyRing(userId, contextId, publicKeyRing);
            if(imported != null) {
                importedKeysRings.add(imported);
            }
        }
        return importedKeysRings;
    }

    /**
     * Summarize the list of keys by ids
     *
     * @param keyEntries, full list of keys
     * @param userId of the user currently pulling the keys
     * @param contextId of the user
     * @return
     * @throws OXException
     */
    private List<OGPGPKeyRing> normalize(List<OGPGPKey> keyEntries, int userId, int contextId) throws OXException {
        List<OGPGPKeyRing> ret = new ArrayList<OGPGPKeyRing>();
        try {
            ArrayList<String> processedIds = new ArrayList<String>();
            for (OGPGPKey ogpgpKey : keyEntries) {
                //summarize by ids so that each key-ring is treated as unique
                String ids = ogpgpKey.getKeyIdsAsString();
                if (!processedIds.contains(ids)) {
                    boolean owned = (ogpgpKey.getUserId() == userId) && (ogpgpKey.getContextId() == contextId);  // Establish if this key is owned
                    ret.add(new OGPGPKeyRing(ids, PGPPublicKeyRingFactory.create(ogpgpKey.getPublicPGPAscData()), ogpgpKey.isInline(),
                        ogpgpKey.getShareLevel(), owned));
                    processedIds.add(ids);
                }
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#delete(java.lang.String)
     */
    @Override
    public void delete(int userId, int contextId, String ids) throws OXException {
        OGPGPKeysStorage pgpKeysStorage = getKeyStorage();
        String[] idList = ids.trim().split("\\s+");
        List<OGPGPKey> entries = pgpKeysStorage.getForUserByIds(userId, contextId,  Arrays.asList(idList));
        if(entries.size() > 0) {
            pgpKeysStorage.delete(userId, contextId, Arrays.asList(idList));
        }
        else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#get(int, int)
     */
    @Override
    public List<OGPGPKeyRing> get(int userId, int contextId) throws OXException {
        //The storage returns a key entry for each email of the key-ring
        List<OGPGPKey> keyEntries = getKeyStorage().getForUser(userId, contextId);
        return normalize(keyEntries, userId, contextId);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#get(int, int, java.util.List)
     */
    @Override
    public List<OGPGPKeyRing> get(int userId, int contextId, List<String> emails) throws OXException {
        List<OGPGPKey> keyEntries = getKeyStorage().getForUserByEmail(userId, contextId, emails);
        return normalize(keyEntries, userId, contextId);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#get(int, int, java.lang.String)
     */
    @Override
    public OGPGPKeyRing get(int userId, int contextId, String hexid) throws OXException {
        List<OGPGPKeyRing> ret = normalize(
            getKeyStorage().getForUserByIds(userId, contextId, Arrays.asList(hexid)),
            userId,
            contextId);
        return ret.isEmpty() ? null : ret.get(0);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#share(int, int, java.lang.String)
     */
    @Override
    public void share(int userId, int contextId, String ids, boolean share) throws OXException {
        OGPGPKeysStorage pgpKeysStorage = getKeyStorage();
        String[] idList = ids.trim().split("\\s+");
        List<OGPGPKey> entries = pgpKeysStorage.getForUserByIds(userId, contextId,  Arrays.asList(idList));
        if(entries.size() > 0) {
            pgpKeysStorage.updateShareLevel(userId, contextId, Arrays.asList(idList), share ? 1 : 0);
        }
        else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.PublicExternalKeyService#setInline(int, int, java.lang.String, boolean)
     */
    @Override
    public void setInline(int userId, int contextId, String ids, boolean inline) throws OXException {
        OGPGPKeysStorage pgpKeysStorage = getKeyStorage();
        String[] idList = ids.trim().split("\\s+");
        List<OGPGPKey> entries = pgpKeysStorage.getForUserByIds(userId, contextId,  Arrays.asList(idList));
        if(entries.size() > 0) {
            pgpKeysStorage.updateInlineMode(userId, contextId, Arrays.asList(idList), inline);
        }
        else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }
}
