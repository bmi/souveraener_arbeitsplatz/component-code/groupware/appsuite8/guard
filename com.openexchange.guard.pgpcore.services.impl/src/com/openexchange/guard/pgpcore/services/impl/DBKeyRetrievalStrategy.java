/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.util.Iterator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.core.PGPSecretKeyDecoder;

/**
 * {@link DBKeyRetrievalStrategy}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class DBKeyRetrievalStrategy implements PGPKeyRetrievalStrategy {

    private static final Logger logger = LoggerFactory.getLogger(DBKeyRetrievalStrategy.class);
    private final GuardKeyService guardKeyService;

    /**
     * Initializes a new {@link DBKeyRetrievalStrategy}.
     *
     * @param pgpKeysStorage The PGPKeysStorage service
     * @param emailStorage The EmailStorage service
     * @param keyTableStorage The KeyTableStorage service
     */
    public DBKeyRetrievalStrategy(GuardKeyService guardKeyService) {
        this.guardKeyService = guardKeyService;
    }

    private boolean hasUserId(Iterator<String> userIDs, String userIdentity) {
        while(userIDs.hasNext()) {
            String id = userIDs.next();
            if(id != null && id.toLowerCase().contains(userIdentity.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private boolean hasUserId(PGPSecretKey key, String userIdentity) {
        return hasUserId(key.getUserIDs(), userIdentity);
    }

    /**
     * Retrieves Guard Key information for a given key id
     *
     * @param keyId The id of the key
     * @return The GuardKey for the given key id, or null if not key was found
     * @throws OXException
     */
    private GuardKeys getGuardKeyById(long keyId) throws OXException {
        return guardKeyService.getKeys(keyId);
    }

    /**
     * Retrieves Guard key information for a given key id belonging to a user
     *
     * @param keyId The ID of the key
     * @param userIdentity The user's identity
     * @return The GuardKey for the given key id and userIdentity, or null if not key was found
     * @throws OXException
     */
    private GuardKeys getGuardKeyByIdForUserIdentity(long keyId, String userIdentity) throws OXException {
        return guardKeyService.getKeysBySubKeyId(userIdentity, keyId);
    }

    /**
     * Extracts a private key from a PGPSecretKey object
     *
     * @param secretKey The PGPSecretKeyObject
     * @param password The password
     * @return The decoded private key
     * @throws PGPException
     */
    private PGPPrivateKey decodePrivateKey(PGPSecretKey secretKey, char[] password) throws Exception {
        try {
            return PGPSecretKeyDecoder.decodePrivateKey(secretKey, password);
        } catch (PGPException e) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
    }

    /**
     * @param password
     * @param salt
     * @return
     */
    private char[] hashPassword(char[] password, String salt) {
        if (password == null) {
            return null;
        }
        return CipherUtil.getSHA(new String(password), salt).toCharArray();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.PGPKeyRetrievalStrategy#getSecretKey(long)
     */
    @Override
    public PGPPrivateKey getSecretKey(long keyId, String userIdentity, char[] password) throws Exception {
        GuardKeys guardKeys = userIdentity != null ? getGuardKeyByIdForUserIdentity(keyId, userIdentity) : getGuardKeyById(keyId);
        if (guardKeys != null) {
            PGPSecretKeyRing pgpSecretKeyRing = guardKeys.getPGPSecretKeyRing();
            if (pgpSecretKeyRing == null) {
                return null;
            }
            PGPSecretKey secretKey = pgpSecretKeyRing.getSecretKey(keyId);
            if (secretKey != null) {
                if(userIdentity == null) {
                    //Do not check for userIdentity
                    return decodePrivateKey(secretKey, hashPassword(password,guardKeys.getSalt()));
                }
                else {
                    //Check if we find the userIdentity within the key, or master-key
                    if(userIdentity.contains(guardKeys.getEmail()) || hasUserId(secretKey,userIdentity) || hasUserId(pgpSecretKeyRing.getSecretKey()/*masterKey*/,userIdentity)) {
                        return decodePrivateKey(secretKey, hashPassword(password,guardKeys.getSalt()));
                    }
                    else {
                        logger.info("Unable to find Userid {} in keyid {}.", userIdentity, Long.toString(keyId));
                    }
                }
            }
            else {
                logger.error("Unable to extract PGPSecretKey with key id {} from PGPSecretKeyRing.", Long.toString(keyId));
            }
        }
        else {
            logger.info("Unable to find PGPSecretKey for key id {}.", Long.toString(keyId));
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.PGPKeyRetrievalStrategy#getPublicKey(long)
     */
    @Override
    public PGPPublicKey getPublicKey(long keyId) throws Exception {
        GuardKeys guardKeys = getGuardKeyById(keyId);
        if (guardKeys != null) {
            PGPPublicKeyRing publicKeyRing = guardKeys.getPGPPublicKeyRing();
            PGPPublicKey publicKey = publicKeyRing.getPublicKey(keyId);
            if (publicKey != null) {
                return publicKey;
            }
            else {
                logger.debug("Unable to extract PGPPublicKey with key id %s from PGPPublicKeyRing.", Long.toString(keyId));
            }
        }
        logger.debug("Unable to find PGPPublicKey for keyId {}", Long.toString(keyId));
        return null;
    }
}
