/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.services;

import java.io.InputStream;
import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.crypto.CryptoType;

/**
 * {@link GuardParsedMimeMessage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class GuardParsedMimeMessage {

    private MimeMessage      msg;
    private JSONArray        recipients;
    private JSONObject       settings;
    private final MimeParser mime;
    private final int        senderUserId;
    private final int        senderContextId;
    private String           senderSessionId;
    private final String     fromName;
    private final boolean    draft;
    private final String     host;
    private final String     senderEmail;
    private final boolean    senderIsGuest;
    private String           senderIp;

    //@formatter:off
    public GuardParsedMimeMessage(InputStream msg,
                                  JSONObject json,
                                  int userid,
                                  int cid,
                                  String fromName,
                                  boolean draft,
                                  String host,
                                  String senderEmail,
                                  boolean senderIsGuest) throws MessagingException, JSONException {
        //@formatter:on
        this.mime = new MimeParser(msg);
        this.msg = this.mime.getMessage();
        if (json != null) {
            if (json.has("security")) {
                this.settings = json.getJSONObject("security");
            } else {
                this.settings = new JSONObject();
            }
            if (json.has("recipients")) {
                this.recipients = json.getJSONArray("recipients");
            } else {
                this.recipients = new JSONArray();
            }
            if (json.has("senderSessionId")) {
                this.senderSessionId = json.getString("senderSessionId");
            }
            if (json.has("senderIp")) {
                this.senderIp = json.getString("senderIp");
            } else {
                this.senderIp = null;
            }
        }
        else {
            this.settings = new JSONObject();
        }
        this.senderUserId = userid;
        this.senderContextId = cid;
        this.fromName = fromName;
        this.draft = draft;
        this.host = host;
        this.senderEmail = senderEmail;
        this.senderIsGuest = senderIsGuest;
    }

    public void setMessage(MimeMessage msg) {
        this.msg = msg;
    }

    public MimeMessage getMessage() {
        return msg;
    }

    public MimeParser getParser() {
        return mime;
    }

    public JSONArray getRecipients() {
        return recipients;
    }

    public boolean isSign() throws JSONException {
        if (!settings.has("sign")) {
            return false;
        }
        return settings.getBoolean("sign");
    }

    public boolean isInline() throws JSONException {
        if (!settings.has("inline")) {
            return false;
        }
        return settings.getBoolean("inline");
    }

    public boolean isEncrypt() throws JSONException {
        if (!settings.has("encrypt")) {
            return false;
        }
        return settings.getBoolean("encrypt");
    }

    /**
     * Pulls Guest greeting message from sent JSON
     *
     * @return
     */
    public String getGuestMessage() {
        if (!settings.has("guest_message")) {
            return null;
        }
        try {
            return settings.getString("guest_message");
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Pulls selected Guest language from JSON
     *
     * @return
     * @throws JSONException
     */
    public String getGuestLanguage() throws JSONException {
        if (!settings.has("guest_language")) {
            return null;
        }
        return settings.getString("guest_language");
    }

    /**
     * Pull guest pin from JSON
     *
     * @return
     * @throws JSONException
     */
    public String getPIN() throws JSONException {
        if (!settings.has("pin")) {
            return null;
        }
        return settings.getString("pin");
    }

    public CryptoType.PROTOCOL getType() {
        if (!settings.has("type")) {
            return null;
        }
        try {
            return CryptoType.getTypeFromString(settings.getString("type").toLowerCase());
        } catch (JSONException e) {
            return null;
        }
    }

    public int getSenderUserId() {
        return this.senderUserId;
    }

    public int getSenderContextId() {
        return this.senderContextId;
    }

    public String getSenderSessionId() {
        return this.senderSessionId;
    }

    /**
     * Parse the from address from message header
     *
     * @return
     */
    public InternetAddress getFromAddress() {
        try {
            Address[] from = this.msg.getFrom();
            if (from.length > 0) {
                return ((InternetAddress) from[0]);
            }
            return null;
        } catch (MessagingException e) {
            return null;
        }
    }

    /**
     * Gets the senderName
     *
     * @return The senderName
     */
    public String getSenderName() {
        return fromName;
    }

    /**
     * Parse the subject from message header
     *
     * @return
     */
    public String getSubject() {
        try {
            return this.msg.getHeader("Subject", null);
        } catch (MessagingException e) {
            return null;
        }
    }

    /**
     * Returns if message is a draft message
     *
     * @return
     */
    public boolean isDraft() {
        return draft;
    }

    /**
     * Returns the domain that the client was using
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     * TO find the type of recipient, parse the mime header for the email address
     * If not in Mime, then returns bcc
     *
     * @param email
     * @return
     */
    public RecipientType getRecipType(String email) {
        try {
            String[] toHeaders = this.msg.getHeader("To");
            if (toHeaders != null) {
                for (String to : toHeaders) {
                    if (to.toLowerCase().contains(email.toLowerCase())) {
                        return RecipientType.TO;
                    }
                }
            }

            String[] ccHeaders = this.msg.getHeader("Cc");
            if (ccHeaders != null) {
                for (String cc : ccHeaders) {
                    if (cc.toLowerCase().contains(email.toLowerCase())) {
                        return RecipientType.CC;
                    }
                }
            }

            String[] bcHeaders = this.msg.getHeader("Bcc");
            if (bcHeaders != null) {
                for (String bc : bcHeaders) {
                    if (bc.toLowerCase().contains(email.toLowerCase())) {
                        return RecipientType.BCC;
                    }
                }
            }
            return null;
        } catch (MessagingException e) {
            return null;
        }
    }

    /**
     * Gets the sender's email
     *
     * @return The sender's email
     */
    public String getSenderEmail() {
        return this.senderEmail;
    }

    /**
     * Parse the headers for the message reference of original Guest email
     *
     * @return
     * @throws JSONException
     */
    public String getGuardMessagRef() throws JSONException {
        String refs = (this.settings.has("msgRef") ? this.settings.getString("msgRef") : null);
        return refs;
    }

    /**
     * Gets the guest status of the sender
     *
     * @return The guest status of the sender
     */
    public boolean senderIsGuest() {
        return senderIsGuest;
    }

    public String getSenderIP() {
        return senderIp;
    }
}
