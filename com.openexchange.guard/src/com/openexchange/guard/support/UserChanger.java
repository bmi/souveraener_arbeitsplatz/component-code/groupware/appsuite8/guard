/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.support;

import com.openexchange.exception.OXException;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;

/**
 * {@link UserChanger}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class UserChanger {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UserChanger.class);

    public void changeUser (String email, String newEmail) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys keys = keyService.getKeys(email);
        if (keys == null) {
            throw OXException.general("Unable to find user specified by " + email);
        }
        if (newEmail != null) {
            // Change primary email address
            updateEmailAddress (keys, newEmail);
        }
    }

    public void changeUser (int userId, int contextId, String newEmail) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys keys = keyService.getKeys(userId, contextId);
        if (keys == null) {
            logger.info("Unable to find any Guard user by Id = " + userId + " in context " + contextId);
            return;
        }
        if (newEmail != null) {
            // Change primary email address
            updateEmailAddress (keys, newEmail);
        }
    }

    public void updateEmailAddress (GuardKeys key, String newEmail) throws OXException {
        GuardRatifierService ratifier = Services.getService(GuardRatifierService.class);
        ratifier.validate(newEmail);
        if (newEmail.contains("%")) { // One last sanity check
            throw OXException.general("Invalid email address " + newEmail);
        }
        // Update user og_KeyTable
        KeyTableStorage keyStorage = Services.getService(KeyTableStorage.class);
        keyStorage.updateEmailAddress(key, newEmail);
        // Update the og_email lookup table
        EmailStorage emailStorage = Services.getService(EmailStorage.class);
        emailStorage.updateEmail(key.getEmail(), newEmail);
        // Update PGP Key lookup table
        PGPKeysStorage pgpKeyStorage = Services.getService(PGPKeysStorage.class);
        pgpKeyStorage.changeEmailAddress(key.getEmail(), newEmail, key.getContextid());

    }
}
