Name:          open-xchange-guard-file-storage
BuildArch:     noarch
BuildRequires: ant
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: open-xchange-guard >= @OXVERSION@
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GPL-2.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       OX Guard File Storage
Autoreqprov:   no
Requires:      open-xchange-guard >= @OXVERSION@
Provides:      open-xchange-guard-storage

%description
This package adds the bundles for file storage support to the OX Guard product.

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
. /opt/open-xchange/lib/oxfunctions.sh

# prevent bash from expanding, see bug 13316
GLOBIGNORE='*'

if [ ${1:-0} -eq 1 ]; then # only during first install

    # SoftwareChange_Request 3105
    # Installing open-xchange-guard created a backup so we are able to take
    # over the previous config params to the new location.
    old_core_file_back=/opt/open-xchange/etc/guard.properties.move
    file_properties=/opt/open-xchange/etc/guard-file.properties
    key="com.openexchange.guard.storage.file.uploadDirectory"

    if [ -e $old_core_file_back ] && [ -e $file_properties ]; then
        #if property is uncommented
        if $(ox_exists_property ${key} ${old_core_file_back}); then
            value=$(ox_read_property ${key} ${old_core_file_back})
            #and property has a value
            if [ -n "${value}" ]; then
                ox_comment ${key} remove ${file_properties}
                ox_set_property ${key} "${value}" ${file_properties}
            fi
        fi
        # after open-xchange-guard and open-xchange-storage-* are installed we
        # can clean up as properties have been moved
        rm ${old_core_file_back}
    fi

elif [ ${1:-0} -eq 2 ]; then # only when updating
    : #remove noop when adding SCRs for updates
fi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*
%config(noreplace) /opt/open-xchange/etc/guard-file.properties

%changelog
* Thu Aug 11 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.7 release
* Thu Aug 11 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.7 release
* Mon Jul 04 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-07-11 (6145)
* Thu Jun 02 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-05-30 (6138)
* Wed Mar 16 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-03-21 (6116)
* Mon Feb 21 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-02-22 (6107)
* Mon Feb 14 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-02-15 (6099)
* Mon Jan 17 2022 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2022-01-24
* Mon Nov 29 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.10.6 release
* Fri Oct 22 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.6 release
* Wed Jun 09 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.6 release
* Wed Apr 21 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2021-04-27 (5984)
* Tue Mar 23 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2021-03-29 (5979)
* Thu Feb 11 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Build for patch 2021-02-22 (5954)
* Mon Mar 30 2021 Greg Hill <greg.hill@open-xchange.com>
Remove conflict with open-xchange-guard-s3-storage
* Mon Feb 01 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate for 2.10.5 release
* Fri Jan 15 2021 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.10.5 release
* Thu Dec 17 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.10.5 release
* Mon Nov 30 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.5 release
* Tue Oct 06 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.5 release
* Tue Jul 28 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.10.4 release
* Tue Jun 30 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.10.4 release
* Wed May 20 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.4 release
* Mon Feb 03 2020 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.4
* Thu Nov 28 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate of 2.10.3 release
* Thu Nov 21 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate of 2.10.3 release
* Thu Oct 17 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.3 release
* Wed Jun 19 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.3 release
* Fri May 10 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate of 2.10.2 release
* Wed May 01 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.10.2 release
* Thu Mar 28 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.2 release
* Mon Mar 11 2019 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.2
* Fri Nov 23 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
RC 1 for 2.10.1 release
* Mon Nov 05 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview for 2.10.1 release
* Fri Oct 12 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview for 2.10.1 release
* Mon Sep 10 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.1
* Mon Jun 25 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate for 2.10.0 release
* Mon Jun 11 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.10.0 release
* Fri May 18 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Fifth preview of 2.10.0 release
* Thu Apr 19 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Fourth preview of 2.10.0 release
* Tue Apr 03 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Third preview of 2.10.0 release
* Tue Feb 20 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.10.0 release
* Fri Feb 02 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.10.0 release
* Wed Jan 10 2018 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.10.0 release
* Tue May 16 2017 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.8.0 release
* Thu May 04 2017 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.8.0 release
* Mon Apr 03 2017 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.8.0 release
* Fri Dec 02 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.8.0 release
* Fri Nov 25 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second release candidate for 2.6.0 release
* Thu Nov 24 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First release candidate for 2.6.0 release
* Tue Nov 15 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Third preview of 2.6.0 release
* Sat Oct 29 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview of 2.6.0 release
* Fri Oct 14 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First preview of 2.6.0 release
* Wed Oct 12 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.6.0 release
* Tue Jul 12 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate for 2.4.2 release
* Wed Jul 06 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.4.2 release
* Wed Jun 29 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second preview for 2.4.2 release
* Thu Jun 16 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.4.2 release
* Thu Jun 16 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
prepare for 2.4.2
* Wed Mar 30 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate for 2.4.0 release
* Thu Mar 24 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.4.0 release
* Wed Mar 16 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Fifth preview of 2.4.0 release
* Fri Mar 04 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Fourth preview of 2.4.0 release
* Sat Feb 20 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Third candidate for 2.4.0 release
* Fri Feb 05 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Second candidate for 2.4.0 release
* Fri Feb 05 2016 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
First candidate for 2.4.0 release
* Mon Nov 16 2015 Ioannis Chouklis <ioannis.chouklis@open-xchange.com>
Initial release
