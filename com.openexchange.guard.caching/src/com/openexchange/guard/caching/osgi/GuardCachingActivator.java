/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.caching.osgi;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.caching.CacheService;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.caching.impl.GenericCacheFactoryImpl;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardCachingActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardCachingActivator extends HousekeepingActivator {

    private final static Logger logger = LoggerFactory.getLogger(GuardCachingActivator.class);

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] {CacheService.class};
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        {
            ServiceTrackerCustomizer<CacheService, CacheService> customizer = new ServiceTrackerCustomizer<CacheService, CacheService>() {

                private final String[] regionNames = { "GuestFileItem", "GuestMetaData", "GuestLookup" };

                @Override
                public CacheService addingService(ServiceReference<CacheService> reference) {
                    CacheService cacheService = context.getService(reference);

                    int idleSeconds = 3600; // 1 hours
                    int shrinkInterval = 600; // Every 10 minutes

                    for (String regionName : regionNames) {
                        try {
                            byte[] ccf = ("jcs.region."+regionName+"=LTCP\n" +
                            "jcs.region."+regionName+".cacheattributes=org.apache.jcs.engine.CompositeCacheAttributes\n" +
                            "jcs.region."+regionName+".cacheattributes.MaxObjects=10000\n" +
                            "jcs.region."+regionName+".cacheattributes.MemoryCacheName=org.apache.jcs.engine.memory.lru.LRUMemoryCache\n" +
                            "jcs.region."+regionName+".cacheattributes.UseMemoryShrinker=true\n" +
                            "jcs.region."+regionName+".cacheattributes.MaxMemoryIdleTimeSeconds="+idleSeconds+"\n" +
                            "jcs.region."+regionName+".cacheattributes.ShrinkerIntervalSeconds="+shrinkInterval+"\n" +
                            "jcs.region."+regionName+".elementattributes=org.apache.jcs.engine.ElementAttributes\n" +
                            "jcs.region."+regionName+".elementattributes.IsEternal=false\n" +
                            "jcs.region."+regionName+".elementattributes.MaxLifeSeconds=-1\n" +
                            "jcs.region."+regionName+".elementattributes.IdleTime="+idleSeconds+"\n" +
                            "jcs.region."+regionName+".elementattributes.IsSpool=false\n" +
                            "jcs.region."+regionName+".elementattributes.IsRemote=false\n" +
                            "jcs.region."+regionName+".elementattributes.IsLateral=false\n").getBytes(StandardCharsets.UTF_8);
                            cacheService.loadConfiguration(new ByteArrayInputStream(ccf), true);
                        } catch (Exception e) {
                            // Failed to initialize cache region
                            logger.warn("Failed to initialize cache region {}", regionName, e);
                        }
                    }
                    addService(CacheService.class, cacheService);
                    return cacheService;
                }

                @Override
                public void modifiedService(ServiceReference<CacheService> reference, CacheService service) {
                    // Ignore
                }

                @Override
                public void removedService(ServiceReference<CacheService> reference, CacheService service) {
                    for (String regionName : regionNames) {
                        try {
                            service.freeCache(regionName);
                        } catch (Exception e) {
                            // Ignore
                        }
                    }
                    removeService(CacheService.class);
                    context.ungetService(reference);
                }
            };

            track(CacheService.class, customizer);
        }
        openTrackers();

        registerService(GenericCacheFactory.class, new GenericCacheFactoryImpl(getService(CacheService.class)));
    }

     /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(GenericCacheFactory.class);
    }
}
