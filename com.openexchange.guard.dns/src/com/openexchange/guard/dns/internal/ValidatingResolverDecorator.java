/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns.internal;

import java.io.IOException;
import java.util.List;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Message;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.TXTRecord;
import com.openexchange.exception.OXException;
import com.openexchange.guard.dns.ValidationResult;

/**
 * {@link ValidatingResolverDecorator} is a decorator which performs validation of DNS responses using a given {@link DNSValidator} after a query was sent.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class ValidatingResolverDecorator extends AbstractResolverDecorator {

    private static final String GUARD_DNS_VALIDATED_RECORD_VALUE = "GUARD-DNS-VALIDATED: YES";
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ValidatingResolverDecorator.class);
    private final DNSValidator validator;
    private final boolean validateFailures;

    /**
     * Initializes a new {@link ValidatingResolverDecorator}.
     *
     * @param decoratedResolver The resolver to decorate
     * @param validator The validator to use
     * @param validateFailures True, if the validator should retry the DNS lookup without validation after the first attempt was not validated in order to notice potential MITM-Attacks.
     */
    public ValidatingResolverDecorator(Resolver decoratedResolver, DNSValidator validator, boolean validateFailures) {
        super(decoratedResolver);
        this.validator = validator;
        this.validateFailures = validateFailures;
    }

    /**
     * Initializes a new {@link ValidatingResolverDecorator} which validates failures (i.e with validateFailures=true)
     *
     * @param decoratedResolver The resolver to decorate
     * @param validator The validator to use
     */
    public ValidatingResolverDecorator(Resolver decoratedResolver, DNSValidator validator) {
        this(decoratedResolver, validator, true);
    }

    /**
     * Internal method to remove the validation record from a given message
     *
     * @param m The message to remove the validation record from
     */
    private void clearValidationRecord(Message m) {
        List<RRset> answers = m.getSectionRRsets(Section.ANSWER);
        for (RRset rrset : answers) {
            List<Record> records = rrset.rrs();
            for (Record record : records) {
                if (record.toString().contains(GUARD_DNS_VALIDATED_RECORD_VALUE)) {
                    m.removeRecord(record, Section.ANSWER);
                }
            }
        }
    }

    /**
     * Gets the maximum defined TTL for a message
     *
     * @param answer The message to get the maximum TTL for.
     * @return The maximum TTL set for the given message or 0
     */
    private long getMaxTTL(Message message) {
        long maxTTL = 0;
        if (message != null) {
            List<RRset> rrsets = message.getSectionRRsets(Section.ANSWER);
            for (RRset rrset : rrsets) {
                if (rrset.getTTL() > maxTTL) {
                    maxTTL = rrset.getTTL();
                }
            }
        }
        return maxTTL;
    }

    /**
     * Returns whether or not the validator considered the given records as validated
     *
     * @param records The records to check
     * @return True if the records are considered as validated, false otherwise.
     */
    public ValidationResult getValidationResultFor(Record... records) {
        for (Record r : records) {
            if (r.toString().contains(GUARD_DNS_VALIDATED_RECORD_VALUE)) {
                return new ValidationResult(true);
            }
        }
        return new ValidationResult(false);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.xbill.DNS.Resolver#send(org.xbill.DNS.Message)
     */
    @Override
    public synchronized Message send(Message query) throws IOException {

        //Send the message
        Message answer = getDecoratedResolver().send(query);
        clearValidationRecord(answer);

        //Validate the response
        ValidationResult validationResult = null;
        try {
            validationResult = this.validator.isValid(answer);
        } catch (OXException e) {
            logger.error("Error while validating DNS response. Response will be treated as not validated.", e);
        }

        //If validation fails we retry the DNS query without validation:
        //Retrying the DNS query with the CD (Check Disabled) Flag set. If this works we probably have a MITM-A and not a DNS/Network error
        if (validationResult != null && !validationResult.isValidated() && validateFailures) {
            Message cdQuery = (Message) query.clone();
            cdQuery.getHeader().setFlag(Flags.CD);
            Message cdAnswer = getDecoratedResolver().send(cdQuery);
            if (cdAnswer.getRcode() != Rcode.SERVFAIL) {
                logger.warn("! WARNING ! DNSSEC validation failed but another DNS lookup was successful with validation turned off. This could be a potential Man-In-The-Middle-Attack. The DNS record will be discarded.");
            }
        }
        if (validationResult != null && !validationResult.isValidated()) {
            //Validation failed; Discarding the whole answer
            answer.getHeader().setRcode(Rcode.SERVFAIL);
        } else if (validationResult != null && validationResult.isValidated()) {
            //Adding a new Text record to indicate that validation has been done successfully by Guard.
            TXTRecord test = new TXTRecord(
                query.getQuestion().getName(),
                query.getQuestion().getDClass(),
                getMaxTTL(answer),
                GUARD_DNS_VALIDATED_RECORD_VALUE);
            answer.addRecord(test, Section.ANSWER);
        }

        return answer;
    }
}
