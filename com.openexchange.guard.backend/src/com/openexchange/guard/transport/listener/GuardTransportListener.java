/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.transport.listener;

import javax.mail.Address;
import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.GuardResult;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.mail.autocrypt.BackendAutoCryptService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.mail.dataobjects.SecuritySettings;
import com.openexchange.mail.transport.listener.MailTransportListener;
import com.openexchange.mail.transport.listener.Reply;
import com.openexchange.mail.transport.listener.Result;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link GuardTransportListener}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class GuardTransportListener extends AbstractGuardAccess implements MailTransportListener {

    private static ServiceLookup serviceLookup;

    /**
     * Initializes a new {@link GuardTransportListener}.
     */
    public GuardTransportListener(ServiceLookup serviceLookup) {
        super();
        this.serviceLookup = serviceLookup;
    }

    private User getUserFrom(Session session) throws OXException {
        UserService userService = Services.getService(UserService.class);
        if(userService == null) {
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(UserService.class);
        }
        return userService.getUser(session.getUserId(), session.getContextId());
    }

    @Override
    public boolean checkSettings(SecuritySettings securitySettings, Session session) throws OXException {
        return true;
    }

    @Override
    public Result onBeforeMessageTransport(MimeMessage message, Address[] recipients, SecuritySettings securitySettings, Session session) throws OXException {

        if (securitySettings == null ||
            (!securitySettings.isEncrypt() && !securitySettings.isSign())) {
            BackendAutoCryptService autoCrypt = serviceLookup.getService(BackendAutoCryptService.class);
            if (autoCrypt != null) {
                return new GuardResult(autoCrypt.updateOutgoing(message, session), recipients, Reply.NEUTRAL);

            }
            // Nothing set.  Nothing to do
            return new GuardResult(message, recipients, Reply.NEUTRAL);
        }

        GuardApiImpl guardApi = getGuardApi(CRYPTO_ENDPOINT);
        if (null == guardApi) {
            // Guard end point not available
            if (securitySettings.isEncrypt()) { // If set to encrypt, need to fail here
                throw GuardExceptionCodes.MISSING_ENDPOINT.create();
            }
            return new GuardResult(message, recipients, Reply.NEUTRAL);
        }

        GuardAuthenticationToken authToken = new AuthenticationTokenHandler().getForSession(session, securitySettings.getType());
        if(securitySettings.getAuthentication() != null && !securitySettings.getAuthentication().isEmpty()) {
            authToken = GuardAuthenticationToken.fromString(securitySettings.getAuthentication());
        }

        if (securitySettings.isSign()) {  // If signing, Auth code is required
            if (authToken == null) {
                throw GuardExceptionCodes.MISSING_AUTH.create();
            }
        }

        final User user = getUserFrom(session);
        GuardResult processed = guardApi.processMimeMessage(message, recipients, securitySettings,
                 GuardApis.mapFor("action", "process_mime",
                     "user", Integer.toString(session.getUserId()),
                     "context", Integer.toString(session.getContextId()),
                     "fromName", user.getDisplayName(),
                     "isGuest", Boolean.toString(user.isGuest()),
                     "email", user.getMail()
                     ),
                 session,
                 authToken);

        return processed;

    }

    @Override
    public void onAfterMessageTransport(MimeMessage message, Exception exception, Session session) throws OXException {
        // Nothing to do
    }

    // -----------------------------------------------------------------------------------------------------------------------------

}
