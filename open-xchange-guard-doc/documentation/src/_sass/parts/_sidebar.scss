@import '../colors';
@import '../gradiants';

.sidebar > .sidebar-nav {
    padding-right: 0;
    padding-left: 0;
    border: none;

    // show sidebar
    @media(min-width:768px) {
        z-index: 1;
        position: absolute;
        width: 250px;
        //margin-top: 51px;
    }

    // search
    .sidebar-search {
        padding: 15px;
        // disable search field
        display: none
    }

    // tree
    .navigation {

        // yep, minus one for proper baseline
        margin-top: 45px - 1;

        // width
        width: 100%;
        @media(min-width:768px) { width: 250px; }

        // background color
        background-color: $ox-grey;
        @media(min-width:768px) { background-color: transparent; }

        // colors
        .target {
            .link {
                .link-title { color: $ox-black; }
                .link-icon { color: lighten($ox-black, 20%) }
            }
            &.active {
                background-color: #eee;
                .link-title { color: $ox-accent; }
                .link-icon { color: $ox-black; }
            }
         }

        // padding
        .target {
            // link height (touch vs. mouse)
            .link { padding: 10px 15px };
            @media(min-width:768px) { padding: 0; }
            // space between icon/title
            .link-title { padding-left: 4px; }
            // visual hierarchy
            &.level-1>a { padding-left: 15px; }
            &.level-2>a { padding-left: 30px; }
            &.level-3>a { padding-left: 45px; }
        }

        // font weight/style
        .target {
            &.level-1 { font-weight: 500; }
            &.level-2 { font-weight: 300; }
            &.level-3 {
                font-weight: 300;
                font-style: italic;
            }
            &.level-1.active, &.level-2.active, &.level-3.active { font-weight: 700; }
        }

        // search within tree
        .target {
            &.force-show { display: block!important; }
            &.force-hide { display: none!important; }
        }

        // overflow handling
        .target .link {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        &.nav>li>a:focus {
            outline: 0;
            background: darken(#eee, 5%);
        }
    }
}
