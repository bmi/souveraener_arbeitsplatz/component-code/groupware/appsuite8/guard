---
title: Installation
description: Installation guide for OX Guard
---

If upgrading from 2.6 or 2.8, please see

- [Upgrading to 2.10](./Upgrade2_10.html)

#Overview

This article will guide you through the installation of Guard and describes the basic configuration and software requirements. As it is intended as a quick walk-through it assumes an existing installation of the operating system including a single server App Suite setup as well as average system administration skills. This guide will also show you how to setup a basic installation with none of the typically used distributed environment settings. The objective of this guide is:

- To setup a single server installation
- To setup a single Guard instance on an existing Open-Xchange installation, no cluster
- To use the database service on the existing Open-Xchange installation for Guard, no replication
- To provide a basic configuration setup, no mail server configuration

##Requirements

Please review [OX Guard Requirements](https://oxpedia.org/wiki/index.php?title=AppSuite:OX_System_Requirements#OX_Guard) for a full list of requirements.

Since OX Guard is a Microservice it can either be added to an existing Open-Xchange installation or it can be deployed on a dedicated environment. The version of Guard installed is dependent on the Appsuite version installed. Please refer to the version matrix below.

### Prerequisites

- Open-Xchange REST API
- Grizzly HTTP connector (open-xchange-grizzly)
- A supported Java Virtual Machine (Java 8)
- An Open-Xchange App Suite installation (see version Matrix)
- Please Note: To get access to the latest minor features and bug fixes, you need to have a valid license. The article [Updating OX-Packages](https://oxpedia.org/wiki/index.php?title=AppSuite:UpdatingOXPackages) explains how that can be done.

### Version Matrix

| Core Version | Guard Version |
| --- | --- |
| 7.8.1 | 2.4.0 or 2.4.2 |
| 7.8.2 | 2.4.2 |
| 7.8.3 | 2.6.0 |
| 7.8.4 | 2.8.0 |
| 7.10.0 | 2.10.0 |
| 7.10.1 | 2.10.1 |
| 7.10.2 | 2.10.2 |
| 7.10.3 | 2.10.3 |
| 7.10.4 | 2.10.4 |
| 7.10.5 | 2.10.5 |
| 7.10.6 | 2.10.6 |
| 8.0.0 | 3.0.0 |

##Important Notes

###Customization

OX Guard version supports branding / theming using the configuration cascade, defining a templateID for a user or context. Check the OX Guard Customization documentation for more details.

###Mail Resolver

READ THIS VERY CAREFULLY; BEFORE PROCEEDING WITH GUARD INSTALLATION!

The Guard installation must be able to determine if an email recipient is a local OX user or if it should be a guest account. The default MailResolver uses the context domain name to do this. On many installations, domains may extend across multiple context and multiple database shards. In these cases, the default MailResolver won&#39;t work. In addition, if a custom authentication package is used, the Mail Resolver will likely not work.

Once Guard is installed, please be sure to test the mail resolver using:

```
/opt/open-xchange/sbin/guard test email@domain
```

to see if the mail Resolver works.

If the test does not work, you will likely need a custom Mail Resolver. Please see MailResolver documentation.

This resolver software depends heavily on your local deployment.

# Download and Installation

## General

The installation of the `open-xchange-guard-backend-plugin` package which is required for Guard and the main `open-xchange-guard` package in version 2.4.0 or higher will eventually execute database update tasks if installed and activated. Please take this into account.

There are several components to the Guard service. They can be all installed on the same server as the OX middleware or on a separate server.

The components required for the OX middleware are: `open-xchange-rest`, `open-xchange-guard-backend-plugin` and `open-xchange-guard-ui`.

The components required for the OX frontend are: `open-xchange-guard-ui-static` and optionally `open-xchange-guard-help-en-us` (or preferred language for help files).

The components required for the Guard server `open-xchange-guard` and either `open-xchange-guard-file-storage` or `open-xchange-guard-s3-storage` depending on what storage you want to use. The examples below make use of the `open-xchange-guard-file-storage`. Adjust the commands accordingly to fit your needs. In addition `open-xchange` and `open-xchange-core` are required to run OX Guard.

## Debian Linux 8.0 (Jessie)

Support of Debian Jessie is deprecated. Read More: https://forum.open-xchange.com/showthread.php?11205

## Debian Linux 9.0 (Stretch)

If not already done, add the following repositories to your Open-Xchange apt configuration:

```
deb https://software.open-xchange.com/products/guard/stable/guard/DebianStretch /
deb https://software.open-xchange.com/products/appsuite/stable/backend/DebianStretch /
```

and then run for a single node installation:

```
$ apt-get update
$ apt-get install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin 
```

or the following for a distributed installation:

```
$ apt-get update
$ apt-get install open-xchange-guard open-xchange-guard-file-storage
```

The packages `open-xchange-guard-ui`, `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware. The package `open-xchange-guard-ui-static` must be installed in the frontend (apache node).

### Debian Linux 10.0 (Buster) *Version 2.10.3+ only*

If not already done, add the following repositories to your Open-Xchange apt configuration:

```
deb https://software.open-xchange.com/products/guard/stable/guard/DebianBuster /
deb https://software.open-xchange.com/products/appsuite/stable/backend/DebianBuster /
```

and then run for a single node installation:

```
apt-get update 
apt-get install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
```

or the following for a distributed installation:

```
apt-get update
apt-get install open-xchange-guard open-xchange-guard-file-storage
```

The packages `open-xchange-guard-ui` `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware. The package `open-xchange-guard-ui-static` must be installed in the frontend (apache node).

## RedHat Enterprise Linux 6 or CentOS 6

If not already done, add the following repositories to your Open-Xchange yum configuration:

```
[open-xchange-guard-stable-guard]
name=Open-Xchange-guard-stable-guard
baseurl=https://software.open-xchange.com/products/guard/stable/guard/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://software.open-xchange.com/products/appsuite/stable/backend/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
```

and then run for a single node installation:

```
$ yum update
$ yum install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
```

or the following for a distributed installation:

```
$ yum update
$ yum install open-xchange-guard open-xchange-guard-file-storage
```

The packages _open-xchange-guard-ui_, _open-xchange-rest_ and _open-xchange-guard-backend-plugin_ missing in the distributed installation have to be installed on the node running the middleware. The package open-xchange-guard-ui-static must be installed in the frontend (apache node).

## Redhat Enterprise Linux 7 or CentOS 7

If not already done, add the following repositories to your Open-Xchange yum configuration:

```
[open-xchange-guard-stable-guard]
name=Open-Xchange-guard-stable-guard
baseurl=https://software.open-xchange.com/products/guard/stable/guard/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://software.open-xchange.com/products/appsuite/stable/backend/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
```

and then run for a single node installation:

```
$ yum update
$ yum install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
```

or the following for a distributed installation:

```
$ yum update
$ yum install open-xchange-guard open-xchange-guard-file-storage
```

The packages _open-xchange-guard-ui_, _open-xchange-rest_ and _open-xchange-guard-backend-plugin_ missing in the distributed installation have to be installed on the node running the middleware. The package open-xchange-guard-ui-static must be installed in the frontend (apache node).

## SUSE Linux Enterprise Server 12

Add the package repository using zypper if not already present:

```
$ zypper ar https://software.open-xchange.com/products/guard/stable/guard/SLE_12 guard-stable-guard
$ zypper ar https://software.open-xchange.com/products/appsuite/stable/backend/SLE_12 ox-backend
```

and then run for a single node installation:

```
$ zypper ref
$ zypper in open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static
```

or the following for a distributed installation:

```
$ zypper ref
$ zypper in open-xchange-guard open-xchange-guard-file-storage
```

The packages _open-xchange-guard-ui_, _open-xchange-rest_ and _open-xchange-guard-backend-plugin_ missing in the distributed installation have to be installed on the node running the middleware. The package open-xchange-guard-ui-static must be installed in the frontend (apache node).

## Univention Corporate Server

If you have purchased the OX App Suite for UCS, the OX Guard is part of the offering. OX Guard is available in the Univention App Center. Please check the UMC module App Center for the installation of the OX Guard at your already available environment.

Please note: By default, OX Guard generates the link to the secure content for external recipients on the basis of the local fully qualified domain name (FQDN). If the local FQDN is not reachable from the Internet, it has to be specified manually. This can be done by setting a UCR variable, e.g. via the UMC module &quot;Univention Configuration Registry&quot;. The variable has to contain the external FQDN of the OX Guard system:

```
oxguard/cfg/guard.properties/com.openexchange.guard.externalEmailURL=HOSTNAME.DOMAINNAME
```

# Update OX Guard

This section contains information about updating a 2.10.0 version (e.g. for patch fixes). Upgrading from prior versions is discussed in different articles.

## Debian Linux 8.0 (Jessie)

Support of Debian Jessie is deprecated. Read More: https://forum.open-xchange.com/showthread.php?11205

## Debian Linux 9.0 (Stretch)

If not already done, add the following repositories to your Open-Xchange apt configuration:

```
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/DebianStretch /
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/DebianStretch /
```

Then run:

```
$ apt-get update
$ apt-get dist-upgrade
```

If you want to see, what apt-get is going to do without actually doing it, you can run:

```
$ apt-get dist-upgrade -s
```

## Debian Linux 10.0 (Buster)

If not already done, add the following repositories to your Open-Xchange apt configuration:

```
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/DebianBuster /
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/DebianBuster /
```

Then run:

```
$ apt-get update
$ apt-get dist-upgrade
```

If you want to see, what apt-get is going to do without actually doing it, you can run:

```
$ apt-get dist-upgrade -s
```

## Redhat Enterprise Linux 6 or CentOS 6

If not already done, add the following repositories to your Open-Xchange yum configuration:

```
[open-xchange-guard-stable-guard-updates]
name=Open-Xchange-guard-stable-guard-updates
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
```

and then run:

```
$ yum update
$ yum upgrade
```

## Redhat Enterprise Linux 7 or CentOS 7

If not already done, add the following repositories to your Open-Xchange yum configuration:

```
[open-xchange-guard-stable-guard-updates]
name=Open-Xchange-guard-stable-guard-updates
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
```

and then run:

```
$ yum update
$ yum upgrade
```

## SUSE Linux Enterprise Server 12

Add the package repository using zypper if not already present:

```
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/SLE_12 guard-stable-guard-updates
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/SLE_12 ox-backend
```

and then run:

```
$ zypper dup -r guard-stable-guard-backend-updates
$ zypper dup -r guard-stable-guard-ui-updates
```

You might need to run:

```
$ zypper ref
```

to update the repository metadata before running zypper up.

## Univention Corporate Server

If you have purchased the OX App Suite for UCS, the OX Guard is part of the offering. OX Guard is available in the Univention App Center. Please check the UMC module App Center for the update of the OX Guard.

# Configuration

The following gives an overview of the most important settings to enable Guard for users on the Open-Xchange installation. 
Some of those settings have to be modified in order to establish the database and _REST API_ access from the Guard service. 
All settings relating to the Guard backend component are located in the configuration file _guard-core.properties_ located in _/opt/open-xchange/etc_. 
The default configuration should be sufficient for a basic &quot;up-and-running&quot; setup (with the exception of defining the database username and password). 
Please refer to the inline documentation of the configuration file for more advanced options. Additional information can be found in the Guard Configuration article.

## Basic Configuration

```
$ vim /opt/open-xchange/etc/guard-core.properties
```

Guard database for storing Guard user information, main lookup tables:

```
com.openexchange.guard.oxguardDatabaseHostname=localhost
```

Guard database that stores keys for guest users. May be the same as above. New guest shards will be created on this database as needed. If not supplied, will use the oxguardDatabaseHostname:

```
com.openexchange.guard.oxguardShardDatabase=localhost
```

Username and Password for the databases above:

```
com.openexchange.guard.databaseUsername=openexchange
com.openexchange.guard.databasePassword=db_password
```

Open-Xchange REST API host:

```
com.openexchange.guard.restApiHostname=localhost
```

Open-Xchange REST API username and password (need to be defined in the OX backend in the &quot;Configure services&quot; below):

```
com.openexchange.guard.restApiUsername=apiusername
com.openexchange.guard.restApiPassword=apipassword
```

External URL for this Open-Xchange installation. This setting will be used to generate the link to the secure content for external recipients:

```
com.openexchange.guard.externalEmailURL=URL_TO_OX
```

## Middleware Configuration on OX Guard node

If you are installing OX Guard on a node that until yet did not host an Open-Xchange middleware you have to additionally configure some parts of the following properties files:

- *configdb.properties*: information about the existing configuration database.
- *server.properties*: information about the connections have to be set.
- *system.properties*: at least SERVER_NAME should be set.

## Services Configuration

### Apache

#### Redhat Enterprise Linux 6 or CentOS 6

```
$ vim /etc/httpd/conf.d/proxy_http.conf
```

#### Debian GNU/Linux 9.0

```
$ vim /etc/apache2/conf-enabled/proxy_http.conf
```

```
<Proxy balancer://oxguard>
       Order deny,allow
       Allow from all

       BalancerMember http://localhost:8009/ timeout=1800 smax=0 ttl=60 retry=60 loadfactor=100 route=OX1
       ProxySet stickysession=JSESSIONID|jsessionid scolonpathdelim=ON
      SetEnv proxy-initial-not-pooled
       SetEnv proxy-sendchunked
</Proxy>

ProxyPass /appsuite/api/oxguard balancer://oxguard/oxguard
ProxyPass /pks balancer://oxguard/pgp
ProxyPass /.well-known/openpgpkey balancer://oxguard/openpgpkey
```

__Please Note__: The Guard API settings must be inserted __before__ the existing *ProxyPass /appsuite/api* parameter.

__Also Note__: If you already have a Proxy balancer for the OX backend with the same URL (say http://localhost:8080) then you don&#39;t need the second BalacnerMember entry, and you can just have the ProxyPass address that balancer instead.

After the configuration is done, restart the Apache webserver

```
$ apachectl restart
```

## Open-Xchange Middleware Configuration

Edit the *guard-api.properties* configuration file for the OX backend where the guard-backend-plugin was installed. 
Please remove comments in front of the following settings to the configuration file *guard-api.properties* on the Open-Xchange backend servers:

```
$ vim /opt/open-xchange/etc/guard-api.properties
```

```
# OX Guard general permission, required to activate Guard in the AppSuite UI.
com.openexchange.capability.guard=true

# Default theme template id for all users that have no custom template id configured.
com.openexchange.guard.templateID=0
```

Configure the API username and password that you assigned to Guard in the *server.properties file*:

```
$ vim /opt/open-xchange/etc/server.properties
```

```
# Specify the user name used for HTTP basic auth by internal REST servlet
com.openexchange.rest.services.basic-auth.login=apiusername

# Specify the password used for HTTP basic auth by internal REST servlet
com.openexchange.rest.services.basic-auth.password=apipassword
```

Finally, the OX backend needs to know where the Guard server is located. This is used to notify the Guard server of changes in users, and to send emails marked for signature. The URL for the Guard server should include the URL suffix /guardadmin. In the event of a cluster setup, any Guard server can be referenced here, as it is not session specific, though ideally would have a HTTP load balancer/failover URL:

```
$ vim /opt/open-xchange/etc/guard-api.properties
```

```
# Specifies the URI to the OX Guard end-point; e.g. http://guard.host.invalid:8081/guardadmin
# Default is empty
com.openexchange.guard.endpoint=http://guardserver:8009/guardadmin
```

Restart the OX backend

```
$ /etc/init.d/open-xchange restart
```

### SELinux

Running SELinux prohibits your local Open-Xchange backend service to connect to localhost:8009, which is where the Guard backend service listens to. In order to allow localhost connections to 8009 execute the following:

```
$ setsebool -P httpd_can_network_connect 1
```

## Generating the oxguardpass

Once the Guard configuration (database and backend configuration) and the service configuration has been applied, 
the Guard administration script needs to be executed in order to create the master password file in */opt/open-xchange/etc/oxguardpass*. 
The initiation only needs to be done __once__ for a multi server setup, for details please see the sections __Optional__ and/or __Clustering__.

Please Note: If you run a cluster of OX / Guard nodes, only execute this command on __ONE__ node. Not on all nodes! See OX Guard Clustering for details.

```
$ /opt/open-xchange/sbin/guard --init
```

__Please Note__: It is important to understand that the master password file located at */opt/open-xchange/etc/oxguardpass* is required to reset user passwords; 
without them the administrator will not be able to reset user passwords anymore in the future. 
The file contains the passwords used to encrypt the master database key, as well as passwords used to encrypt protected data in the users table. It must be the same on all Guard servers.

## Test Setup

Not required, but it is a good idea to test the Guard setup before enabling for any users. The test function will verify that Guard has a good connection to the OX backend, and that it can resolve email addresses to users.

To test, use an email address that exists on the OX backend (john@example.com for this example)

```
/opt/open-xchange/sbin/guard --test john@example.com
```

Guard should return information from the OX backend regarding the user associated with &quot;john@example.com&quot;. Problems resolving information for the user should be resolved before using Guard. Check Rest API passwords and settings if errors returned.

## Enabling Guard for Users

Guard provides two capabilities for users in the environment as well as a basic &quot;core&quot; level:

- Guard: *com.openexchange.capability.guard*
- Guard Mail: *com.openexchange.capability.guard-mail*
- Guard Drive: *com.openexchange.capability.guard-drive*

The &quot;core&quot; Guard enabled a basic read functionality for Guard encrypted emails. We recommend enabling this for all users, as this allows all recipients to read Guard emails sent to them. Great opportunity for upsell. Recipients with only Guard enabled can then do a secure reply to the sender, but they can&#39;t start a new email or add recipients.

__Guard Mail__ and __Guard Drive__ are additional options for users. &quot;Guard Mail&quot; allows users the full functionality of Guard emails. &quot;Guard Drive&quot; allows for encryption and decryption of drive files.

Each of those two Guard components is enabled for all users that have the according capability configured. Please note that users need to have the Drive permission set to use Guard Drive. So the users that have Guard Drive enabled must be a subset of those users with OX Drive permission. Since v7.6.0 we enforce this via the default configuration. Those capabilities can be activated for specific user by using the Open-Xchange provisioning scripts:

Guard Mail:

```
$ /opt/open-xchange/sbin/changeuser -c 1 -A oxadmin -P admin_password -u testuser --config/com.openexchange.capability.guard-mail=true
```

Guard Drive:

```
$ /opt/open-xchange/sbin/changeuser -c 1 -A oxadmin -P admin_password -u testuser --config/com.openexchange.capability.guard-drive=true
```

__Please Note__: Guard Drive requires Guard Mail to be configured for the user as well. 
In addition, these capabilities may be configured globally by editing the *guard-api.properties* file on the OX backend.

## External Guest recipients:

Starting in Guard 2.10.0, when an encrypted email is sent to a user that does not have Guard, a guest account is created for them in appsuite. 
The recipient uses the Guest account to read the encrypted email. These guest users MUST have guard capabilities. 
To do this, guard capability must be added to guest accounts. */opt/open-xchange/etc/share.properties*

```
com.openexchange.share.guestCapabilityMode=static
com.openexchange.share.staticGuestCapabilities=guard
```

In a distributed system, the Guest accounts should not be considered transient. Guard servers must be able to verify the guest account exists in the session storage services.

```
com.openexchange.share.transientSessions=false
```

### Guest Storage

When an encrypted email is sent to an external Guest, a copy of the fully encrypted email is stored on the server. This is used to create an inbox of encrypted emails for the guest. By entering in a password, the emails can be decrypted and displayed.

How these files are stored depend on which package, open-xchange-guard-file-storage or open-xchange-guard-s3-storage, was installed.

The file retention policy is configured in the guard-core.properties file.

## Recipient key detection

### Local

Guard needs to determine if an email recipients email address is an internal or external (non-ox) user.

To detect if the recipient is an account on the same OX Guard system there is a mechanism needed to map a recipient mail address to the correct local OX context. 
The default implementation delivered in the product achieves that by looking up the mail domain (@example.com) within the list of context mappings. 
That is at least not possible in case of ISPs where different users/contexts use the same mail domain. 
In case your OX system does not use mail domains in context mappings it is required to deploy an OX OSGi bundle implementing the *com.openexchange.mailmapping.MailResolver* class or by interfacing Guard with your mail resolver system. 
Please see MailResolver documentation for details.

### External

Starting with Guard 2.0, Guard will use public PGP Key servers if configured to find PGP Public keys. In addition, Guard will also look up SRV records for PGP Key servers for a recipients domain. This follows the standards [OpenPGP HKP Draft](https://tools.ietf.org/html/draft-shaw-openpgp-hkp-00).

External PGP servers to use can be configured in the *guard.properties* file on the Guard servers.

```
com.openexchange.guard.publicPGPDirectory = hkp://keys.gnupg.net:11371, hkp://pgp.mit.edu:11371
```

If you would like this Guard installation discoverable by other Guard servers, then create an SRV record for each domain (&quot;example.com&quot; in this illustration):

```
_hkp._tcp.open-xchange.com. 28800 IN    SRV     10 1 80 appsuite.example.com.
```

__Please Note__: PGP Public key servers by default append use the URL server/pks when the record is obtained from an SRV record. The proxy above routes anything with the Apache domain/pks to the OX Guard PGP server.

Guard keys are also discoverable using the webkey service as specified here: https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-12 This is enabled if you include the

```
ProxyPass /.well-known/openpgpkey/hu balancer://oxguard/hu
```

in the proxy_http.conf as above.

## Clustering

You can run multiple OX Guard servers in your environment to ensure high availability or enhance scalability. 
OX Guard integrates seamlessly into the existing Open-Xchange infrastructure by using the existing interface standards and is therefor transparent to the environment. 
A couple of things have to be prepared in order to loosely couple OX Guard servers with Open-Xchange servers in a cluster.

### MySQL

The MySQL servers need to be configured in order to allow access to the configdb of Open-Xchange. To do so you need to set the following configuration in the MySQL *my.cnf*:

```sql
bind = 0.0.0.0
```

This allows the Guard backend to bind to the MySQL host which is configured in the *guard-core.properties* file with *com.openexchange.guard.configdbHostname*. 
After the bind for the MySQL instance is configured and the OX Guard backend would be able to connect to the configured host, you have to grant access for the OX Guard service on the MySQL instance to manage the databases. Do so by connecting to the MySQL server via the MySQL client. Authenticate if necessary and execute the following, please note that you have to modify the hostname / IP address of the client who should be able to connect to this database, it should include all possible OX Guard servers:

```sql
GRANT ALL PRIVILEGES ON *.* TO 'openexchange'@'oxguard.example.com' IDENTIFIED BY ‘secret’;
```

### Apache

OX Guard uses the Open-Xchange REST API to store and fetch data from the Open-Xchange databases. 
The REST API is a servlet running in the Grizzly container. By default it is not exposed as a servlet through Apache and is only accessibly via port 8009. 
In order to use Apache&#39;s load balancing via mod_proxy we need to add a servlet called &quot;preliminary&quot; to *proxy_http.conf*, 
example based on a clustered mod_proxyconfiguration:

```
<Location /preliminary>
     Order Deny,Allow
     Deny from all
     # Only allow access from Guard servers within the network. Do not expose this
     # location outside of your network. In case you use a load balancing service in front
     # of your Apache infrastructure you should make sure that access to /preliminary will
     # be blocked from the Internet / outside clients. Examples:
     # Allow from 192.168.0.1
     # Allow from 192.168.1.1 192.168.1.2
     # Allow from 192.168.0.
</Location>

ProxyPass /preliminary balancer://oxcluster/preliminary
```

Make sure that the balancer is properly configured in the mod_proxy configuration. 
Examples on how to do so can be found in our clustering configuration for Open-Xchange AppSuite. 
Like explained in the example above, please make sure that this location is only available in your internal network, 
there is no need to expose */preliminary* to the public, it is only used by Guard servers to connect to the OX backend. 
If you have a load balancer in front of the Apache cluster you should consider blocking access to */preliminary* from WAN to restrict access to the servlet to internal network services only.

Now add the OX Guard BalancerMembers to the oxguard balancer configuration (also in *proxy_http.conf*) to address all your OX Guard nodes in the cluster in this balancer configuration. 
The configuration has to be applied to all Apache nodes within the cluster.

If the Apache server is a dedicated server / instance you also have to install the OX Guard UI-Static package on all Apache nodes in the cluster in order to provide static files like images or CSS to the OX Guard client. 
Example for Debian (the OX Guard repository has to be configured in the package management prior):

```
$ apt-get install open-xchange-guard-ui-static
```

### Open-Xchange

Disable the Open-Xchange IPCheck for session verification. 
This is required because OX Guard will use the users session cookie to connect to the Open-Xchange REST API, 
but as a different IP address than the OX Guard server has been used during authentication the request would fail if you don&#39;t disable the IPCheck:

```
$ vim /opt/open-xchange/etc/server.properties
```

and set:

```
com.openexchange.IPCheck=false
```

The OX Guard UI package has to be installed on all Open-Xchange backend nodes as well, example for Debian (the OX Guard repository has to be configured in the package management prior):

```
$ apt-get install open-xchange-guard-ui
```

Restart the Open-Xchange service afterwards.

### OX Guard

For details in clustering Guard servers, please see OX Guard [Clustering](./Clustering.html) article. 
It is __critical__ that all Guard servers have the same *oxguardpass* file. 
Please see the clustering link for details. Do not run */opt/open-xchange/sbin/guard --init* on more than one server.

After all the services like MySQL, Apache and Open-Xchange have been configured you need to update the OX Guard backend configuration to point to the correct API endpoints. 
Set the REST API endpoint to an Apache server by setting the following value in */opt/open-xchange/etc/guard-core.properties*:

```
com.openexchange.guard.restApiHostname=apache.example.com
```

Per default Guard will try to connect to port 8009 to this host, but as we configured the REST API to be proxies thorugh the servlet */preliminary* on every Apache 
we now also need to change the target port for the REST API. You can do so by adding the following line into */opt/open-xchange/etc/guard-core.properties*:

```
com.openexchange.guard.oxBackendPort=80
```

Please also change all settings in regards to MySQL like *com.openexchange.guard.configdbHostname*, *com.openexchange.guard.oxguardDatabaseHostname*,
*com.openexchange.guard.databaseUsername* or *com.openexchange.guard.databasePassword*.

Afterwards restart the OX Guard service and check the log file if the OX Guard backend is able to connect to the configured REST API.

## Multi Node

If you have multiple OX and Guard installations, please see the following documentation: [OX Guard Multi Node Setup](./MultiNodeSetup.html).

# Support API

The OX Guard Support API enables administrative access to various functions for maintaining OX Guard from a client in a role as a support employee. 
A client has to do a BASIC AUTH authentication in order to access the API. Username and password can be configured in the guard-core.properties file using the following settings:

```
# Specify the username and password for accessing the Support API of Guard
com.openexchange.guard.supportApiUsername=
com.openexchange.guard.supportApiPassword=
```

In contrast to the rest of the OX Guard requests, the OX Guard support API requests are accessible using: */guardsupport*.
This distinction allows more flexible configuration since the support API should not always be accessible from everywhere.

__Warning__: Exposing the support API to the internet could be huge security risk. Only add to Apache if you know what you are doing.

Please refer to the API documention for more details.

# Customization

Guard&#39;s templates are customizable at the user and context level. Please see customization documentation for details.
