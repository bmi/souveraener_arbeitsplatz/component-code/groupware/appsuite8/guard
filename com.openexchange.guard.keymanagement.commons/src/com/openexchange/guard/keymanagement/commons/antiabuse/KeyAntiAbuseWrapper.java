/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.antiabuse;

import java.util.Objects;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.ReportValue;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;

/**
 * {@link KeyAntiAbuseWrapper} is a helper around {@link GuardAntiAbuseService} which allows easier anti abuse handling for PGP keys.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyAntiAbuseWrapper implements AntiAbuseWrapper {

    private final GuardAntiAbuseService antiAbuseService;
    private final PGPSecretKeyRing secretKeyRing;
    private final String salt;
    private AllowParameters allowParams;

    /**
     * Initializes a new {@link KeyAntiAbuseWrapper}.
     *
     * @param antiAbuseService The {@link GuardAntiAbuseService} to wrap.
     * @param key The user's {@link GuardKeys} to check the password against.
     * @param contextId The constext ID of the user
     * @param userId The user ID
     * @param password The password to check
     * @param remoteIp The remote IP of the request
     */
    public KeyAntiAbuseWrapper(GuardAntiAbuseService antiAbuseService,
                            GuardKeys key,
                            AllowParameters allowParams) {
        this(antiAbuseService,
             key.getPGPSecretKeyRing(),
             key.getSalt(),
             allowParams);
    }

    /**
     * Initializes a new {@link KeyAntiAbuseWrapper}.
     *
     * @param antiAbuseService The {@link GuardAntiAbuseService} to wrap.
     * @param secretKeyRing The {@link PGPSecretKeyRing} to check the password against.
     * @param contextId The constext ID of the user
     * @param userId The user ID
     * @param password The password to check
     * @param salt The salt of the key
     * @param remoteIp The remote IP of the request
     */
    public KeyAntiAbuseWrapper(GuardAntiAbuseService antiAbuseService,
                            PGPSecretKeyRing secretKeyRing,
                            String salt,
                            AllowParameters allowParams) {
        this.antiAbuseService = Objects.requireNonNull(antiAbuseService, "antiAbuseService must not be null");
        this.secretKeyRing = Objects.requireNonNull(secretKeyRing, "secretKeyRing must not be null");
        this.salt = salt;
        this.allowParams = allowParams;
    }

    @Override
    public <T> T doAction(AntiAbuseAction<T> action) throws Exception {
        if(antiAbuseService.allowLogin(allowParams)){
            if(PGPUtil.verifyPassword(secretKeyRing, allowParams.getPassword(),salt)) {
                antiAbuseService.report(AntiAbuseUtils.getReportParameter(ReportValue.SUCCESS, allowParams));
                return action.doAction();
            }
            else {
                antiAbuseService.report(AntiAbuseUtils.getReportParameter(ReportValue.FAILURE, allowParams));
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
        }
        else {
            throw GuardAuthExceptionCodes.LOCKOUT.create();
        }
    }
}
