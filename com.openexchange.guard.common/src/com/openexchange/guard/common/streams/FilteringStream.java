/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.streams;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

/**
 * {@link FilteringStream}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class FilteringStream extends InputStream {

    private InputStream in;
    private FilterStrategy filter;

    /**
     * {@link FilterStrategy} defines a filter applied to the stream content
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v7.10.0
     */
    public interface FilterStrategy {
       public boolean filter(byte b);
    }

    /**
     * {@link Filter} a concrete {@link FilterStrategy} which will remove all bytes from the stream which are part of a given set.
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v7.10.0
     */
    public static class Filter implements FilterStrategy {

        private List<Byte> filterSet;

        /**
         * Initializes a new {@link Filter}.
         *
         * @param filterSet The set of characters which will be removed from the stream
         */
        public Filter(List<Byte> filterSet) {
            this.filterSet = Objects.requireNonNull(filterSet, "filterSet must not be null");
        }

        /* (non-Javadoc)
         * @see com.openexchange.guard.common.util.FilteringStream.FilterStrategy#filter(byte[])
         */
        @Override
        public boolean filter(byte b) {
            return filterSet.contains(b);
        }
    }

    /**
     * {@link InverseFilter} a concrete {@link FilterStrategy} which remove all bytes from the stream which are NOT part of a given set.
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v7.10.0
     */
    public static class InverseFilter implements FilterStrategy {

        private Filter innerFilter;

        /**
         * Initializes a new {@link InverseFilter}.
         *
         * @param filterSet
         */
        public InverseFilter(List<Byte> filterSet) {
            this.innerFilter = new Filter(filterSet);
        }

        /* (non-Javadoc)
         * @see com.openexchange.guard.common.streams.FilteringStream.FilterStrategy#filter(byte)
         */
        @Override
        public boolean filter(byte b) {
            return ! this.innerFilter.filter(b);
        }
    }

    /**
     * A concrete {@link FilterStrategy} which remove all bytes from the stream which are NOT normal printable characters or standard whitespace
     * {@link PrintableFilter}
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v2.10.0
     */
    public static class PrintableFilter implements FilterStrategy {

        @Override
        public boolean filter(byte b) {
            int value = b;
            if (value >= 9 && value <= 13) return false;  // cr, tab
            if (value >= 32 && value <= 126) return false;  // regular text
            return true;
        }
    }

    /**
     * Initializes a new {@link FilteringStream}.
     *
     * @param in The stream to filter
     * @param filter The filter defining what bytes should be filtered
     */
    public FilteringStream(InputStream in, FilterStrategy filter)  {
        this.in = Objects.requireNonNull(in, "in must not be null");
        this.filter= Objects.requireNonNull(filter, "filter must not be null");
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#read()
     */
    @Override
    public int read() throws IOException {
        int b = in.read();
        if(b != -1 && this.filter.filter((byte)b)) {
           //Skip this byte, because it is "filtered out"
           return read();
        }
        else {
           return b;
        }
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#available()
     */
    @Override
    public int available() throws IOException {
        return in.available();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#close()
     */
    @Override
    public void close() throws IOException {
        in.close();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#mark(int)
     */
    @Override
    public synchronized void mark(int readlimit) {
        in.mark(readlimit);
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#reset()
     */
    @Override
    public synchronized void reset() throws IOException {
        in.reset();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#markSupported()
     */
    @Override
    public boolean markSupported() {
        return in.markSupported();
    }
}
