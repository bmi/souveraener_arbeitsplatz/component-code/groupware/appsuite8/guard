/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.crypto.Exceptions.CryptoServletExceptionCodes;

/**
 * Gets the user's secondary email address
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GetSecondaryEmailAction extends GuardServletAction {

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        CryptoManager cryptoManager = Services.getService(CryptoManager.class);
        PasswordManagementService keymanagementService = cryptoManager.getPasswordManagementService(getCryptoType(request));
        if (keymanagementService != null) {
            String secondaryEmail = keymanagementService.getSecondaryEmail(userSession);
            if (secondaryEmail != null) {
                ServletUtils.sendHtmlOK(response, secondaryEmail);
            } else {
                ServletUtils.sendHtmlOK(response, "");
            }
            return;
        }
        throw CryptoServletExceptionCodes.UNKNOWN_CRYPTO_TYPE.create();
    }
}
