/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.pgpkeys;

class OGPGPKeysSql {

    static final String INSERT_OR_UPDATE_STMT = "INSERT INTO og_pgp_keys (ids, Email, PGPPublic, userid, share_level, cid) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE PGPPublic = ?, ids = ?";
    static final String DELETE_STMT = "DELETE FROM og_pgp_keys WHERE CID = ? AND userid = ? AND ids = ?";
    static final String UPDATE_SHARE_STMT = "UPDATE og_pgp_keys SET share_level = ? WHERE cid = ? AND userid = ? AND ids = ?";
    static final String UPDATE_INLINE_STMT = "UPDATE og_pgp_keys SET inline = ? WHERE cid = ? AND userid = ? AND ids = ?";
    static final String SELECT_BY_IDS_STMT = "SELECT ids, Email, PGPPublic, userid, cid, share_level, inline FROM og_pgp_keys WHERE cid = ? and (userid = ? OR share_level > 0) AND ids LIKE ?";
    static final String SELECT_BY_USER_ID_STMT = "SELECT ids,PGPPublic, Email, share_level, userid, inline, (userid = ?) AS owned FROM og_pgp_keys WHERE cid = ? and (userid = ? OR share_level > 0) ORDER BY owned DESC, Email ASC";
    static final String SELECT_BY_USER_ID_AND_MAIL_STMT = "SELECT ids, Email, PGPPublic, userid, cid, share_level, inline FROM og_pgp_keys WHERE Email = ? AND ids LIKE ? AND cid = ? AND (userid = ? OR share_level > 0)";
    static final String SELECT_BY_USER_ID_AND_MAILS_STMT = "SELECT ids,PGPPublic, Email, share_level, userid, inline, (userid = ?) AS owned FROM og_pgp_keys WHERE cid = ? and (userid = ? OR share_level > 0)  AND Email IN ({@VALUES}) ORDER BY owned DESC, Email ASC";
}
