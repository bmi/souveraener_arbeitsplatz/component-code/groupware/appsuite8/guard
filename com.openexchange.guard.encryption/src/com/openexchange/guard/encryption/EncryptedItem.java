
package com.openexchange.guard.encryption;

/**
 * {@link EncryptedItem} holds information about Mails which has been sent to a guest user
 */
public class EncryptedItem {

    private final String itemId;
    private final int ownerId;
    private final int ownerCid;
    private final long expiration;
    private final int type;
    private final String xml;
    private final String salt;

    /**
     * Initializes a new {@link EncryptedItem}.
     * 
     * @param itemId the item it
     * @param ownerId the owner's (sender's) user id
     * @param ownerCid the owner's (sender's) context id
     * @param expiration the expiration
     * @param type the type
     * @param xml XML structure
     */
    public EncryptedItem(String itemId, int ownerId, int ownerCid, long expiration, int type, String xml, String salt) {
        this.itemId = itemId;
        this.ownerId = ownerId;
        this.ownerCid = ownerCid;
        this.expiration = expiration;
        this.type = type;
        this.xml = xml;
        this.salt = salt;
    }

    public String getItemId() {
        return itemId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int getOwnerCid() {
        return ownerCid;
    }

    public long getExpiration() {
        return expiration;
    }

    public int getType() {
        return type;
    }

    public String getXml() {
        return xml;
    }

    public String getSalt() {
        return salt;
    }
}
