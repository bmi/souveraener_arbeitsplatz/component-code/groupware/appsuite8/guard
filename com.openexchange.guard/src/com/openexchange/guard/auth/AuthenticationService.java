/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.auth;

import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;

/**
 * {@link AuthenticationService} Service for authenticating a user, such as authenticating against crypto keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface AuthenticationService {

    /**
     * Returns the {@link CryptoType} associated with the implementation of this interface
     *
     * @return The related {@link CryptoType}
     */
    public CryptoType.PROTOCOL getCryptoType();

    /**
     * {@link AuthenticationResult} - An authentication result
     */
    public static class AuthenticationResult {

        private final JsonObject json;

        /**
         * Initializes a new {@link AuthenticationResult}.
         *
         * @param json The result data as pure JSON
         */
        public AuthenticationResult(JsonObject json) {
            this.json = json;
        }

        /**
         * Gets the result data
         *
         * @return The result data as pure JSON
         */
        public JsonObject getJson() {
            return json;
        }
    }

    /**
     * Performs authentication
     *
     * @param usersession The {@link GuardUserSession} to perform the authentication for
     * @param parameters The {@link GuardAuthenticationParameters} provided by the client and required for the authentication process
     * @return The result of the authentication as {@link AuthenticationResult}
     */
    public AuthenticationResult authenticate(GuardUserSession usersession, GuardAuthenticationParameters parameters) throws OXException;
}
