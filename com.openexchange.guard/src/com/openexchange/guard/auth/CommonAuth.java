/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.auth;

import static com.openexchange.java.Autoboxing.I;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.GuardVersion;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.osgi.Services;

/**
 * {@link CommonAuth} generates authentication data that is common between different
 * authentication {@link AuthenticationService} implementations
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CommonAuth {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CommonAuth.class);
    }

    public static JsonObject initJson(int userId, int contextId) {
        JsonObject returnJson = new JsonObject();
        returnJson.addProperty("server", GuardVersion.getGuardVersion());
        JsonObject pubkey = getPublicKey(userId, contextId);
        if (pubkey != null) {
            returnJson.add("pubKey", pubkey);
        }
        return returnJson;
    }

    /**
     * Get the public key. If not loaded, load from database
     *
     * @return
     * @throws OXException
     */
    private static JsonObject getPublicKey(int userId, int cid) {
        try {
            MasterKeyService masterKeys = Services.getService(MasterKeyService.class);
            MasterKey mKey = masterKeys.getMasterKey(userId, cid);
            JsonObject pubKey = new JsonObject();
            pubKey.addProperty("index", I(mKey.getIndex()));
            pubKey.addProperty("key", Base64.encodeBase64String(mKey.getClientPublic().getEncoded()));
            return pubKey;
        } catch (OXException ex) {
            LoggerHolder.LOGGER.error("Unable to retrieve public key for user", ex);
        }
        return null;
    }
}
