/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

/**
 * {@link MailQuery} defines an E-Mail query.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class MailQuery {

    private static final String ALL_IDS_STR      = "ALL_MAIL_IDS";
    private String              folderId;
    private String[]            mailIds          = ALL_IDS;
    private MailSortField       orderBy          = null;
    private OrderDirection      orderDirection   = OrderDirection.DESC;
    private boolean             fetchMessageBody = false;
    private int                 limit            = Integer.MAX_VALUE;
    private Integer             mailFlags        = null;
    private Integer             notMailFlags     = null;
    private Integer             newMailFlags     = null;
    private Integer             indexRangeStart  = null;
    private Integer             indexRangeEnd    = null;

    /**
     * Constant for querying all known E-Mails
     */
    public static final String[] ALL_IDS = new String[] { ALL_IDS_STR };

    /**
     * An empty query
     */
    public static final MailQuery EMPTY_QUERY = new MailQuery().setMailIds(ALL_IDS);

    /**
     * Gets the folder Id to query
     * @return The folder Id
     */
    public String getFolderId() {
        return this.folderId;
    }

    /**
     * Sets the folder Id
     * @param folderId The folder Id to query
     * @return The folder Id
     */
    public MailQuery setFolderId(String folderId) {
        this.folderId = folderId;
        return this;
    }

    /**
     * Gets the IDs of the E-Mails to query
     *
     * @return A set of E-Mail IDs
     */
    public String[] getMailIds() {
        return mailIds;
    }

    /**
     * Sets the IDs of the E-Mails to query
     *
     * @param mailIds The set of E-Mail IDs to query, or {@link MailQuery#ALL_IDS} to query all known E-Mails
     * @return this
     */
    public MailQuery setMailIds(String[] mailIds) {

        if (mailIds == null ||
            (mailIds.length == 1 && mailIds[0].equals(ALL_IDS_STR))) {
            this.mailIds = ALL_IDS;
        } else {
            this.mailIds = mailIds;
        }
        return this;
    }

    /**
     * Gets the {@link MailSortField} for ordering the result
     *
     * @return The order field
     */
    public MailSortField getOrderBy() {
        return this.orderBy;
    }

    /**
     * Sets the {@link MailSortField} for ordering the result
     *
     * @param orderBy The order field
     * @return this
     */
    public MailQuery setOrderBy(MailSortField orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    /**
     * Gets the {@link OrderDirection} for ordering the result
     *
     * @return The order direction
     */
    public OrderDirection getOrderDirection() {
        return this.orderDirection;
    }

    /**
     * Sets the {@link OrderDirection} for ordering the result
     *
     * @param orderDirection The order direction
     * @return this
     */
    public MailQuery setOrderDirection(OrderDirection orderDirection) {
        this.orderDirection = orderDirection;
        return this;
    }

    /**
     * Sets whether this query should fetch the whole message body or not
     *
     * @param fetchFull True, to fetch the whole message body, false to not fetch the message body
     * @return this
     */
    public MailQuery setFetchMessageaBody(boolean fetchMessageBody) {
        this.fetchMessageBody = fetchMessageBody;
        return this;
    }

    /**
     * Gets whether this query should fetch the whole message body or not
     * @param True, to fetch the whole message body, false to not fetch the message body
     */
    public boolean getFetchMessageBody() {
        return this.fetchMessageBody;
    }

    /**
     * Gets the maximum number of returned results
     *
     * @return The maximum amount of returned results
     */
    public int getLimit() {
        return this.limit;
    }

    /**
     * Sets the maximum amount of returned results.
     *
     * @param limit The maximum amount of returned results.
     */
    public MailQuery setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    /**
     * Gets the mail flags which should be considered for the query.
     *
     * @return The mail flags
     */
    public Integer getMailFlags() {
        return this.mailFlags;
    }

    /**
     * Sets the mail flags for the query. The query will only return items having the given flags set
     * @param mailFlags The flags
     * @return this
     */
    public MailQuery setMailFlags(int mailFlags) {
       this.mailFlags = mailFlags;
       return this;
    }

    /**
     * Gets the "NOT"-mail flags which should be considered for the query. The query will only return items NOT having the given flags set
     *
     * @return The "NOT"-mail flags
     */
    public Integer getNotMailFlags() {
        return this.notMailFlags;
    }

    /**
     * Sets the "NOT" mail flags for the query. The query will only return items NOT having the given flags set
     *
     * @param mailFlags The "NOT" flags
     * @return this
     */
    public MailQuery setNotMailFlags(int notMailFlags) {
        this.notMailFlags = notMailFlags;
        return this;
    }

    /**
     * Gets the mail flags which should be applied to the queried items before the result is returned
     *
     * @return The mail flags which should be applied to the queried items before the result is returned
     **/
    public Integer getNewMailFlags() {
        return this.newMailFlags;
    }

    /**
     * Sets the mail flags which should be applied to the queried items before the result is returned
     *
     * @param markSeen the mail flags which should be applied to the queried items before the result is returned
     * @return this
     */
    public MailQuery setNewMailFlags(int newMailFlags) {
        this.newMailFlags = newMailFlags;
        return this;
    }


    /**
     * Gets the start of the index range
     *
     * @return The start of the index range
     */
    public Integer getIndexRangeStart() {
        return indexRangeStart;
    }

    /**
     * Sets the start of the index range
     *
     * @param indexRangeStart The start of the index range
     */
    public MailQuery setIndexRangeStart(int indexRangeStart) {
        this.indexRangeStart = indexRangeStart;
        return this;
    }

    /**
     * Gets the end of the index range
     *
     * @return The end of the index range
     */
    public Integer getIndexRangeEnd() {
        return indexRangeEnd;
    }

    /**
     * Sets the end of the index range
     *
     * @param indexRangeEnd The end of the index range
     */
    public MailQuery setIndexRangeEnd(Integer indexRangeEnd) {
        this.indexRangeEnd = indexRangeEnd;
        return this;
    }
}
