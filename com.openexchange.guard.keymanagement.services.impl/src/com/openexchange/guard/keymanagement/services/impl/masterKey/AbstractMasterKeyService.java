/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.masterKey;

import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.services.MasterKeyService;

/**
 * {@link AbstractMasterKeyService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.5
 */
public abstract class AbstractMasterKeyService implements MasterKeyService {

    private final GuardCipherService cipher;

    /**
     * Initializes a new {@link AbstractMasterKeyService}.
     *
     * @param cipher The {@link GuardCipherService} to use
     */
    public AbstractMasterKeyService(GuardCipherService cipher) {
        this.cipher = cipher;
    }

    /**
     * Use the cipher service to decrypt the data
     * decrypt
     *
     * @param key Key to use
     * @param data Data to decrypt
     * @param salt Salt
     * @return
     */
    protected String decrypt(MasterKey key, String data, String salt) {
        if (data == null) {
            return null;
        }
        return cipher.decrypt(data, key.getRC(), salt);
    }

    /**
     * Use the cipher service to encrypt the data
     * encrypt
     *
     * @param key to use
     * @param data to encrypt
     * @param salt to use
     * @return
     * @throws OXException
     */
    protected String encrypt(MasterKey key, String data, String salt) throws OXException {
        if (data == null) {
            return null;
        }
        return cipher.encrypt(data, key.getRC(), salt);
    }

}
