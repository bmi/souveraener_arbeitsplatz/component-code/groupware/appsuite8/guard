/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.update.internal;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.update.KeyUpgradeTask;

/**
 * {@link GuardKeysUpdater}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardKeysUpdater {

    private static final int currentVersion = 1;
    private static final Logger LOG = LoggerFactory.getLogger(GuardKeysUpdater.class);

    ArrayList<KeyUpgradeTask> upgradeServices;
    private KeyTableStorage ogKeyTableStorage;

    public GuardKeysUpdater (KeyTableStorage ogKeyTableStorage, ArrayList<KeyUpgradeTask> upgradeServices) {
        this.ogKeyTableStorage = ogKeyTableStorage;
        this.upgradeServices = upgradeServices;
    }

    public GuardKeysUpdater (KeyTableStorage ogKeyTableStorage) {
        this.ogKeyTableStorage = ogKeyTableStorage;
        this.upgradeServices = new ArrayList<KeyUpgradeTask>();
    }

    public GuardKeysUpdater addUpgradeService (KeyUpgradeTask service) {
        this.upgradeServices.add(service);
        return this;
    }

    private JsonObject getJson (String misc) {
        if (misc == null || misc.isEmpty()) return new JsonObject();
        JsonObject metaData = new JsonParser().parse(misc).getAsJsonObject();
        return metaData;
    }

    /**
     * Parse the misc data from the key to get the current upgrade version
     * @param key
     * @return
     */
    private int getCurrentVersion (GuardKeys key) {
        String misc = key.getMisc();
        JsonObject metaData = getJson (misc);
        if (!metaData.has("version")) return 0;
        return (metaData.get("version").getAsInt());
    }

    /**
     * Update the key table to reflect done
     * @param key
     */
    private void updateKeyToCurrentVersion (GuardKeys key) {
        String misc = key.getMisc();
        JsonObject metaData = getJson (misc);
        metaData.addProperty("version", currentVersion);
        try {
            ogKeyTableStorage.updateMisc(key, metaData.toString());
        } catch (OXException e) {
            LOG.error("Problem updating key version in updater ", e);
        }
    }

    /**
     * Returns the current upgrade version
     * @return
     */
    public static int getCurrentVersion () {
        return currentVersion;
    }

    /**
     * Check the key for any needed upgrades/fixes
     * Mark as fixed once done
     * @param key
     * @param password
     */
    public void checkUpgrades (GuardKeys key, String password) {
        int current = getCurrentVersion (key);
        if (current == currentVersion) return;
        for (int i = current + 1; i <= currentVersion; i++) {
            for (KeyUpgradeTask service : upgradeServices) {
                if (service.handles(i)) {
                    try {
                        service.doUpgrade(key, password);
                    } catch (Exception ex) {
                        LOG.error("Problem during update of key ", ex);
                        return; // Terminate.  Possibly can't proceed if prior updates failed
                    }
                }
            }
        }
        updateKeyToCurrentVersion (key);
    }

}
