/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.login;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.OXGuardSessionAuthenticationHandler;
import com.openexchange.server.ServiceLookup;

/**
 * Provides login and authentication functionality for OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class LoginServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = -1451153388216107948L;
    private static final String LOGOUT_ACTION = "logout";
    private static final String RESET_PASSWORD_ACTION = "reset";
    private static final String GET_SECONDARY_EMAIL_ACTION = "secondary";
    private static final String LOGIN_ACTION = "login";
    private static final String CHANGE_PASSWORD_ACTION = "changepass";
    private static final String CHANGE_SECONDARY_EMAIL_ACTION = "changesecondary";
    private static final String DELETE_RECOVERY_ACTION = "deleterecovery";
    private static final String DELETE_SESSION = "delete_session";

    private transient final HashMap<String, GuardServletAction> servletGetActions;
    private transient final HashMap<String, GuardServletAction> servletPostActions;
    private transient final HashMap<String, GuardServletAction> servletPutActions;

    public LoginServlet(ServiceLookup services, GuardAuthenticationHandler basicAuthServletAuthentication) {

        OXGuardSessionAuthenticationHandler uiSessionAuthentication = new OXGuardSessionAuthenticationHandler();

        //GET
        servletGetActions = new HashMap<String, GuardServletAction>();
        servletGetActions.put(LOGOUT_ACTION, new LogoutAction().setAuthenticationHandler(uiSessionAuthentication));
        servletGetActions.put(RESET_PASSWORD_ACTION, new ResetPasswordAction().setAuthenticationHandler(uiSessionAuthentication));
        servletGetActions.put(GET_SECONDARY_EMAIL_ACTION, new GetSecondaryEmailAction().setAuthenticationHandler(uiSessionAuthentication));

        //POST
        servletPostActions = new HashMap<String, GuardServletAction>();
        servletPostActions.put(LOGIN_ACTION, new LoginAction(services).setAuthenticationHandler(uiSessionAuthentication));
        servletPostActions.put(CHANGE_PASSWORD_ACTION, new ChangePasswordAction().setAuthenticationHandler(uiSessionAuthentication));
        servletPostActions.put(CHANGE_SECONDARY_EMAIL_ACTION, new ChangeSecondaryEmailAction().setAuthenticationHandler(uiSessionAuthentication));
        servletPostActions.put(DELETE_RECOVERY_ACTION, new DeleteRecoveryAction(services).setAuthenticationHandler(uiSessionAuthentication));

        //PUT
        servletPutActions = new HashMap<String, GuardServletAction>();
        if (basicAuthServletAuthentication != null) {
            servletPutActions.put(DELETE_SESSION, new DeleteSesssionAction().setAuthenticationHandler(basicAuthServletAuthentication));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, servletGetActions);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, servletPostActions);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, servletPutActions);
    }
}
