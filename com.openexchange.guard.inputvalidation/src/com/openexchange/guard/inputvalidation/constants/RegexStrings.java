package com.openexchange.guard.inputvalidation.constants;

public class RegexStrings {

	public static final String Email = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[^\\s\\.]*)?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

	public static final String Encoded_Domain = "\\b((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}\\b";  // Strict domain check.  Must be ascii encoded

	public static final String Domain = "\\b(([^\\\\s@'`\"!=+|#/\\\\.])+\\.)+[a-z]{2,}\\b";  //Loose domain check

	public static final String Filename = "((?!\\.\\.)([^\\\\/:*\"<>|]))*";  // Excludes path / and .. as well as pipes

	public static final String Folder = "(?:((?!\\.\\.)[^:*\"<>|]))+[\\\\/]";  // Folder.  Must end with / or \.  Exclude .. , piping

	public static final String Folder_Filename = "((?!\\.\\.)([^\\:*\"<>|]))*[^/\\\\]";  // Folder plus filename.  Must not end with /.  Exclude .. and piping

	public static final String UIID_UPPER_CASE = "^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$"; // a UUID
}
