/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.servlets;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.omg.CORBA.portable.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.wks.internal.GetKeysByHu;

/**
 *
 * {@link WksGetAction} Handles incoming WebKey Srvice get actions
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class WksGetAction {

    private static final Logger logger = LoggerFactory.getLogger(WksGetAction.class);
    private final HttpServletResponse response;
    private final String search;
    private final String domain;

    /**
     * Initializes a new {@link WksGetAction}.
     *
     * @param response
     * @param search
     */
    public WksGetAction(HttpServletResponse response, String search, String domain) {
        this.response = response;
        this.search = search;
        this.domain = domain;
    }


    /**
     * Performs the get action
     *
     * @throws OXException
     * @throws IOException
     */
    public void doAction() throws OXException, IOException {
        PGPPublicKeyRing ring = GetKeysByHu.getKeys(search, domain);
        if (ring != null) {
            logger.debug("WKS: Key found, exporting");
            response.setContentType("application/octet-stream");
            ServletOutputStream out = response.getOutputStream();
            out.write(ring.getEncoded(true));
            out.close();
        } else {
            logger.debug("WKS: No key found");
            ServletUtils.sendResponse(response, 404, "Not Found");
        }

    }
}
