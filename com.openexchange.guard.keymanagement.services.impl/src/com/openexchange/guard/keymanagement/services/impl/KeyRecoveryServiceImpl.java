/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardKeyTableExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardMasterKeyExceptionCodes;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link KeyRecoveryServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class KeyRecoveryServiceImpl implements KeyRecoveryService {

    ServiceLookup services;
    private static final Logger LOG = LoggerFactory.getLogger(KeyRecoveryServiceImpl.class);

    /**
     * Initializes a new {@link KeyRecoveryServiceImpl}.
     *
     * @param configurationService The configuration service
     * @param userService The user service
     */
    public KeyRecoveryServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    private GuardConfigurationService getConfigService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }


    @Override
    public String createRecovery(int masterKeyIndex, String password, String salt, String extra, int userId, int cid, String email) throws OXException {
        MasterKeyService masterKeyService = services.getServiceSafe(MasterKeyService.class);
        MasterKey mkey = masterKeyService.getMasterKey(masterKeyIndex, true);
        String recovery = "";
        GuardCipherService aesCipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        String encrPass = aesCipherService.encrypt(password, mkey.getRC() + extra, salt);
        GuardCipherService rsaCipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.RSA);
        recovery = rsaCipherService.encrypt(encrPass, mkey.getMasterPublic());


        if (Strings.isEmpty(recovery) && userId > 0) {  // make sure not for system email
            throw GuardKeyTableExceptionCodes.UNABLE_TO_CREATE_RECOVERY.create(userId, cid, email);
        }
        return recovery;
    }

    PrivateKey decryptPrivateKey(String privateKey, String unhashed_password, String salt) throws OXException {
        String password = CipherUtil.getSHA(unhashed_password, salt);
        GuardCipherService cipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        byte[] decoded;
        try {
            decoded = cipherService.decrypt(privateKey, password, salt).getBytes("UTF-8");
            if (decoded.length == 0) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
        } catch (UnsupportedEncodingException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
        // for private keys use PKCS8EncodedKeySpec; for public keys use X509EncodedKeySpec
        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(Base64.decode(new String(decoded, StandardCharsets.UTF_8)));

        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey priv = kf.generatePrivate(ks);
            return priv;
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            LOG.error("Unable decode private", e);
            return null;
        }
    }

    @Override
    public String getRecoveryHash(String recoveryString, int masterKeyIndex, String salt, String extra, int userId, int cid) throws OXException {
        GuardCipherService rsaCipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.RSA);
        MasterKeyService masterKeyService = services.getService(MasterKeyService.class);
        MasterKey masterKeys = masterKeyService.getMasterKey(masterKeyIndex, true);
        if (masterKeys == null) {
            throw GuardMasterKeyExceptionCodes.UNABLE_TO_RETIREVE_MASTER_PASSWORD.create();
        }
        PrivateKey privateKey = decryptPrivateKey(masterKeys.getMasterEncodedPrivate(), masterKeys.getMC(), masterKeys.getMasterSalt());
        if (privateKey == null) {
            throw OXException.general("Bad master key");
        }
        String encrhash = rsaCipherService.decrypt(recoveryString, privateKey);
        if (Strings.isEmpty(encrhash)) {
            LOG.error("Unable to decode master key.  Possible corrupted data");
            throw OXException.general("Bad master key");
        }
        GuardCipherService aesCipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        return aesCipherService.decrypt(encrhash, masterKeys.getRC() + extra, salt);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyRecoveryService#checkCreateRecovery(int, int)
     */
    @Override
    public boolean checkCreateRecovery(int contextId, int userId) throws OXException {
        if (getConfigService().getBooleanProperty(GuardProperty.noRecovery, userId, contextId)) {
            // If global no Recovery then return false.
            return false;
        }
        if (contextId <= 0) {
            return true;
        }

        // get individual settings
        OXUserService userService = services.getServiceSafe(OXUserService.class);
        GuardCapabilities userCapabilities = userService.getGuardCapabilieties(contextId, userId);
        return !userCapabilities.isNorecovery();
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyRecoveryService#createRecovery(com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String)
     */
    @Override
    public String createRecovery(GuardKeys keys, String password) throws OXException {
        if (Strings.isEmpty(password)) {
            return "-1";
        }
        password = CipherUtil.getSHA(password, keys.getSalt());
        String extra = keys.getPublicKey() == null ? "" : Integer.toString(keys.getEncodedPublic().hashCode());
        return createRecovery(keys.getMasterKeyIndex(), password, keys.getSalt(), extra, keys.getUserid(), keys.getContextid(), keys.getEmail());
    }
}
