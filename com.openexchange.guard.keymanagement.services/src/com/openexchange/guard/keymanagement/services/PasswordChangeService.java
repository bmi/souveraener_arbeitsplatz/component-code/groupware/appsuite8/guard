package com.openexchange.guard.keymanagement.services;

import com.openexchange.exception.OXException;
import com.openexchange.guard.common.auth.PasswordChangedResult;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

public interface PasswordChangeService {

    /**
     * Changes the password of the "current" key
     *
     * @param userid The ID of the user to change current key's password for
     * @param cid The CID of the user
     * @param userEmail The email of the guestuser, or <code>null</code> if the password should be changed for a regular user
     * @param oldpass The current password required in order to change the password
     * @param newpass The new password to set
     * @param question Prior Guard 2.10: The question set for a guest account, or <code>null</code> otherwise
     * @param answer Prior Guard 2.10: The answer set for a guest accoun, or <code>null</code> otherwise
     * @param pin Prior Guard 2.10: The guest's second factor PIN if enabled and required, <code>null</code> otherwise
     * @param sessionId The session ID
     * @return The result of the change operation
     * @throws OXException
     */
    public PasswordChangedResult changePassword(int userid, int cid, String userEmail, String oldpass, String newpass, String question, String answer, String pin, String sessionId) throws OXException;

    /**
     * Changes the key password and generates new {@link GuardKeys}
     *
     * @param oldPass The old password
     * @param newPass The new password
     * @param oldKey The old key
     * @return The new {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys changePassword(String oldPass, String newPass, GuardKeys oldKey) throws OXException;

    /**
     * Changes the key password and generates a new {@link GuardKeys}
     *
     * @param oldPassHash The old hashed password
     * @param newPass The new password (unhashed)
     * @param oldKey The old key
     * @return The new {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys changePasswordWithRecovery(String oldPassHash, String newPass, GuardKeys oldKey) throws OXException;

    /**
     * Changes the temporary password of a key.  Key must never have been updated.
     * Will return new keys, but will not update database
     * @param userid
     * @param cid
     * @param password
     * @return
     * @throws OXException
     */
    GuardKeys changeTemporaryPassword (int userid, int cid, String password) throws OXException;

    /**
     * Check newpassword against password requirements
     *
     * @param newpass
     * @param userid
     * @param cid
     * @throws OXException
     */
    void checkPasswordRequirements(String newpass, int userid, int cid) throws OXException;
}
