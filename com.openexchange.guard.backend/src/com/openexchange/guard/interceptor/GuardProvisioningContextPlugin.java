/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.interceptor;

import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.admin.plugins.OXContextPluginInterface;
import com.openexchange.admin.plugins.PluginException;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.dataobjects.UserModuleAccess;
import com.openexchange.admin.rmi.extensions.OXCommonExtension;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.tools.pipesnfilters.Filter;

/**
 * {@link GuardProvisioningContextPlugin}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public final class GuardProvisioningContextPlugin extends AbstractGuardAccess implements OXContextPluginInterface {

    /**
     * Initializes a new {@link GuardProvisioningContextPlugin}.
     */
    public GuardProvisioningContextPlugin() {
        super();
    }

    @Override
    public void change(final Context ctx, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void changeCapabilities(final Context ctx, final Set<String> capsToAdd, final Set<String> capsToRemove, final Set<String> capsToDrop, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void changeQuota(final Context ctx, final String module, final long quotaValue, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public Context preCreate(final Context ctx, final User admin_user, final Credentials auth) throws PluginException {
        // Nothing to do
        return ctx;
    }

    @Override
    public Context postCreate(final Context ctx, final User admin_user, final UserModuleAccess access, final Credentials auth) throws PluginException {
        // Nothing to do
        return ctx;
    }

    @Override
    public void delete(final Context ctx, final Credentials auth) throws PluginException {
        try {
            GuardApiImpl guardApi = getGuardApi(GUARDADMIN_ENDPOINT);
            if (null == guardApi) {
                // Guard end point not available
                return;
            }
            guardApi.doCallPut(GuardApis.mapFor("action", "context_delete"), new JSONObject(2).put("context_id", ctx.getId().intValue()), Void.class, null);
        } catch (Exception e) {
            Logger logger = org.slf4j.LoggerFactory.getLogger(GuardProvisioningContextPlugin.class);
            logger.error("Failed to delete context {} at guard end point.", ctx.getId(), e);
        }
    }

    @Override
    public void disable(final Context ctx, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void disableAll(final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void enable(final Context ctx, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void enableAll(final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public List<OXCommonExtension> getData(final List<Context> ctx, final Credentials auth) throws PluginException {
        return null;
    }

    @Override
    public void changeModuleAccess(final Context ctx, final UserModuleAccess access, final Credentials auth) throws PluginException {
        // TODO: Check for dropped "messenger" capability
    }

    @Override
    public void changeModuleAccess(final Context ctx, final String access_combination_name, final Credentials auth) throws PluginException {
        // TODO: Check for dropped "messenger" capability
    }

    @Override
    public void downgrade(final Context ctx, final Credentials auth) throws PluginException {
        // TODO: Check for dropped "messenger" capability
    }

    @Override
    public String getAccessCombinationName(final Context ctx, final Credentials auth) throws PluginException {
        return null;
    }

    @Override
    public UserModuleAccess getModuleAccess(final Context ctx, final Credentials auth) throws PluginException {
        return null;
    }

    @Override
    public Boolean checkMandatoryMembersContextCreate(final Context ctx) throws PluginException {
        return Boolean.TRUE;
    }

    @Override
    public void exists(final Context ctx, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public void getAdminId(final Context ctx, final Credentials auth) throws PluginException {
        // Nothing to do
    }

    @Override
    public Filter<Context, Context> list(final String search_pattern, final Credentials auth) throws PluginException {
        return null;
    }

    @Override
    public Filter<Integer, Integer> filter(final Credentials auth) throws PluginException {
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.admin.plugins.OXContextPluginInterface#existsInServer(com.openexchange.admin.rmi.dataobjects.Context, com.openexchange.admin.rmi.dataobjects.Credentials)
     */
    @Override
    public void existsInServer(Context ctx, Credentials auth) throws PluginException {
        // Nothing to do

    }

}
