/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link ColorLabelComparator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class ColorLabelComparator extends GuardGuestEmailComparator {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7596590022233072711L;

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.impl.comparators.GuardGuestEmailComparator#compareEmails(com.openexchange.guard.guest.GuardGuestEmail, com.openexchange.guard.guest.GuardGuestEmail)
     */
    @Override
    public int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        String colorLabel0 = arg0.getMetaData() != null ? arg0.getMetaData().getColorLabel() : null;
        String colorLabel1 = arg1.getMetaData() != null ? arg1.getMetaData().getColorLabel() : null;

        if(checkForNull(colorLabel0, colorLabel1)) {
           return compareNull(colorLabel0, colorLabel1);
        }

        return colorLabel0.compareTo(colorLabel1);

    }

}
