/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import com.openexchange.guard.keymanagement.commons.RecipKey;

/**
 * {@link RecipKeyInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class RecipKeyInfoResponse {

    private final RecipKey recipKey;
    private int userId, cid;

    /**
     * Initializes a new {@link RecipKeyInfoResponse}.
     *
     * @param recipKey
     * @param userId ID of sender to adjust trust levels
     * @param cid Context of sender
     */
    public RecipKeyInfoResponse(RecipKey recipKey, int userId, int cid) {
        this.recipKey = recipKey;
        this.userId = userId;
        this.cid = cid;
    }

    public PublicKeyRingInfoResponse getPublicRing() {
        return recipKey.getPGPPublicKeyRing() != null ?
               new PublicKeyRingInfoResponse(recipKey.getPGPPublicKeyRing(), recipKey.getPGPSecretKeyRing()) :
               null;
    }

    public boolean isGuest() {
        return recipKey.isGuest();
    }

    public KeySourceResponse getKeySource() {
        return new KeySourceResponse(recipKey.getKeySource(), userId, cid);
    }

    public boolean isNewKey() {
        return recipKey.isNewCreated() || recipKey.isNewKey();
    }

    public boolean isNewCreated() {
        return recipKey.isNewCreated();
    }

    public String getPreferredFormat() {
       return recipKey.isInline() ? "PGP/INLINE" : "PGP/MIME";
    }
}
