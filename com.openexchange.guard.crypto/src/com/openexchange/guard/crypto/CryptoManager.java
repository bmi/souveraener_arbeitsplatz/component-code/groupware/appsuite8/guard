/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.crypto;

import com.openexchange.crypto.CryptoType;
import com.openexchange.guard.mime.services.MimeEncryptionService;

/**
 * {@link CryptoManager}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface CryptoManager {

    /**
     * Returns the {@link MimeSignatureVerificationService} for the given type
     *
     * @param type The type to get the {@link MimeSignatureVerificationService} for
     * @return The {@link MimeSignatureVerificationService} for the given type, or <code>null</code> if no service was associated with the given type
     */
    public MimeSignatureVerificationService getSignatureVerificationService(CryptoType.PROTOCOL type);

    /**
     * Gets the {@link PasswordManagementService} for the given type
     *
     * @param type The type to get the {@link PasswordManagementService} for
     * @return The {@link PasswordManagementService} for the given type, or <code>null</code> if no service was associated with the given type
     */
    public PasswordManagementService getPasswordManagementService(CryptoType.PROTOCOL type);

    /**
     * Gets the {@link MimeEncryptionService} for the given type
     *
     * @param type The type to get the {@link MimeEncryptionService} for
     * @return The {@link MimeEncryptionService} for the given type, or <code>null</code> if no service was associated with the given type
     */
    public MimeEncryptionService getMimeEncryptionService(CryptoType.PROTOCOL type);
}
