/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.interceptor;

import java.util.Map;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.Contact;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.User;
import com.openexchange.user.interceptor.UserServiceInterceptor;

/**
 * {@link GuardUserServiceInterceptor}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 */
public class GuardUserServiceInterceptor extends AbstractGuardAccess implements UserServiceInterceptor {

    private final ServiceLookup services;

    /**
     * Initializes a new {@link GuardUserServiceInterceptor}.
     */
    public GuardUserServiceInterceptor(final ServiceLookup services) {
        super();
        this.services = services;
    }

    @Override
    public int getRanking() {
        return 0;
    }

    @Override
    public void beforeCreate(final Context context, final User user, final Contact contactData) throws OXException {
        // Nothing
    }

    @Override
    public void afterCreate(final Context context, final User user, final Contact contactData) {
        // Nothing
    }

    @Override
    public void beforeUpdate(final Context context, final User user, final Contact contactData, final Map<String, Object> properties) throws OXException {
        // nothing
    }

    @Override
    public void afterUpdate(final Context context, final User user, final Contact contactData, final Map<String, Object> properties) throws OXException {
        try {
            if (user == null || user.getMail() == null)
             {
                return;  // If not updating primary email, no Guard update needed
            }
            GuardApiImpl guardApi = getGuardApi(GUARDADMIN_ENDPOINT);
            if (null == guardApi) {
                // Guard end point not available
                return;
            }
            guardApi.doCallPut(GuardApis.mapFor("action", "user_update"), new JSONObject(4).put("context_id", context.getContextId()).put("user_id", user.getId()).put("email",  user.getMail()), Void.class, null);
        } catch (Exception e) {
            Logger logger = org.slf4j.LoggerFactory.getLogger(GuardUserServiceInterceptor.class);
            logger.error("Failed to update user {} in context {} at guard end point.", user.getId(), context.getContextId(), e);
        }
    }

    @Override
    public void beforeDelete(final Context context, final User user, final Contact contactData) throws OXException {
        try {
            GuardApiImpl guardApi = getGuardApi(GUARDADMIN_ENDPOINT);
            if (null == guardApi) {
                // Guard end point not available
                return;
            }
            guardApi.doCallPut(GuardApis.mapFor("action", "user_delete"), new JSONObject(4).put("context_id", context.getContextId()).put("user_id", user.getId()), Void.class, null);
        } catch (Exception e) {
            Logger logger = org.slf4j.LoggerFactory.getLogger(GuardUserServiceInterceptor.class);
            logger.error("Failed to delete user {} in context {} at guard end point.", user.getId(), context.getContextId(), e);
        }
    }

    @Override
    public void afterDelete(Context context, User user, Contact contactData) throws OXException {
        // NTD

    }


}
