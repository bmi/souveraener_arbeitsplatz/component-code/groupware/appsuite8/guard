/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.passwordManagementServices.responses;

import java.util.Date;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * {@link SmimePublicKeysResponse} API response containing SmimePublicKeys data
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimePublicKeysResponse {

    private SmimeKeys key;

    public SmimePublicKeysResponse(SmimeKeys key) {
        this.key = key;
    }

    /**
     * Returns the type of crypto
     *
     * @return
     */
    public String getType() {
        return "smime";
    }

    /**
     * Returns the date of expiration
     *
     * @return
     */
    public Date getExpires() {
        return key.getExpires();
    }

    /**
     * Returns the serial number of the certificate
     *
     * @return
     */
    public String getSerial() {
        return key.getSerial().toString();
    }

    /**
     * Returns the certifier for the certificate
     *
     * @return
     */
    public String getCertifier() {
        return key.getCertifier();
    }

}
