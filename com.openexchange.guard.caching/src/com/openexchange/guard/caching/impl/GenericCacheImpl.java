/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.caching.impl;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.caching.Cache;
import com.openexchange.caching.CacheService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.GenericCache;

/**
 * {@link GenericCacheImpl} - a {@link GenericCache} which utilizes {@link CacheService} for doing the actual caching
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 * @param <T>
 */
public class GenericCacheImpl<T extends Serializable> implements GenericCache<T> {

    private final static Logger logger = LoggerFactory.getLogger(GenericCacheImpl.class);
    private final CacheService  cacheService;
    private String              cacheName;

    /**
     * Initializes a new {@link GenericCacheImpl}.
     *
     * @param cacheService The {@link CacheService} to use
     * @param cacheName The name of the cache
     */
    public GenericCacheImpl(CacheService cacheService, String cacheName) {
        this.cacheService = cacheService;
        this.cacheName = cacheName;
    }

    private Object getInternal(Serializable key) throws OXException {
        final Cache cache = this.cacheService.getCache(cacheName);
        return cache.get(key);
    }

    private void putInternal(Serializable key, Serializable object) throws OXException {
        final Cache cache = cacheService.getCache(cacheName);
        final boolean invalidate = true;
        cache.put(key, object, invalidate);
    }

    private void removeInternal(Serializable key) throws OXException {
        final Cache cache = cacheService.getCache(cacheName);
        cache.remove(key);
    }

    private void clearInternal() throws OXException {
        final Cache cache = cacheService.getCache(cacheName);
        cache.clear();
    }

    private void logGet(Object obj, Serializable key) {
        if (obj != null) {
            logger.debug("Got cached object of type {} for key {} from cache {}",
                obj.getClass().getName(),
                key.toString(),
                this.cacheName);
        } else {
            logger.debug("Could not find object in cache for key {} in cache {}",
                key.toString(),
                this.cacheName);
        }
    }

    private void logPut(Object obj, Serializable key) {
        logger.debug("Put object of type {} for key {} into cache {}",
            obj.getClass().getName(),
            key.toString(),
            this.cacheName);
    }

    private void logRemove(Serializable key) {
        logger.debug("Removed object for key {} from the cache {}",
            key.toString(),
            this.cacheName);
    }

    private void logClear() {
        logger.debug("Cleared cached {}", this.cacheName);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get(Serializable key) throws OXException {
        final Object cachedObject = getInternal(key);
        logGet(cachedObject, key);
        return cachedObject != null ? (T) cachedObject : null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GenericCache#getArray(int, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public T[] getArray(Serializable key) throws OXException {
        final Object cachedObject = getInternal(key);
        logGet(cachedObject, key);
        return cachedObject != null ? (T[]) cachedObject : null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GenericCache#put(int, java.lang.String, java.io.Serializable)
     */
    @Override
    public void put(Serializable key, T object) throws OXException {
        putInternal(key, object);
        logPut(object, key);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GenericCache#putArray(int, java.lang.String, java.io.Serializable[])
     */
    @Override
    public void putArray(Serializable key, T[] array) throws OXException {
        putInternal(key, array);
        logPut(array, key);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GenericCache#remove(int, java.lang.String)
     */
    @Override
    public void remove(Serializable key) throws OXException {
        removeInternal(key);
        logRemove(key);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.GenericCache#clear()
     */
    @Override
    public void clear() throws OXException {
        clearInternal();
        logClear();
    }
}
