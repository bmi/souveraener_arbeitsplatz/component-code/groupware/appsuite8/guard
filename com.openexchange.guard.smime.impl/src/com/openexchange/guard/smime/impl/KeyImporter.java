/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.pkcs.Attribute;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.bc.BcDefaultDigestProvider;
import org.bouncycastle.pkcs.PKCS12PfxPdu;
import org.bouncycastle.pkcs.PKCS12SafeBag;
import org.bouncycastle.pkcs.PKCS12SafeBagFactory;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.bc.BcPKCS12MacCalculatorBuilderProvider;
import org.bouncycastle.pkcs.jcajce.JcePKCSPBEInputDecryptorProviderBuilder;
import org.bouncycastle.util.io.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.server.ServiceLookup;

/**
 * {@link KeyImporter}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class KeyImporter {

    private final static Logger LOG = LoggerFactory.getLogger(KeyImporter.class);

    Map<String, X509Certificate> certMap;
    Map<ASN1Encodable, X509Certificate> certKeyIds;
    Map<String, PrivateKey> privKeyMap;
    Map<PrivateKey, ASN1Encodable> privKeyIds;
    ServiceLookup services;

    /**
     * Initializes a new {@link KeyImporter}.
     *
     * @param services The service lookup
     */
    public KeyImporter(ServiceLookup services) {
        this.services = services;
        certMap = new HashMap<String, X509Certificate>();
        certKeyIds = new HashMap<ASN1Encodable, X509Certificate>();
        privKeyMap = new HashMap<String, PrivateKey>();
        privKeyIds = new HashMap<PrivateKey, ASN1Encodable>();
    }

    public Map<String, PrivateKey> getPrivKeyMap() {
        return privKeyMap;
    }

    public Map<String, X509Certificate> getCertMap() {
        return certMap;
    }

    /**
     * Removes the userCert from the list of certs. Used to
     * get a clean list of chain certificates
     *
     * @param userCert User certificate to remove from chain
     * @param certs List of certs found
     * @return List with the userCert removed
     */
    private List<X509Certificate> chainOnly(X509Certificate userCert, ArrayList<X509Certificate> certs) {
        // If certs contains the usercert, just go ahead and remove
        if (certs.contains(userCert)) {
            certs.remove(userCert);
            return certs;
        }
        // If not found as exact match, try to match via serial number and subject
        X509Certificate certToRemove = null;
        for (X509Certificate cert : certs) {
            if (cert.getSerialNumber().equals(userCert.getSerialNumber()) && cert.getSubjectDN().equals(userCert.getSubjectDN())) {
                certToRemove = cert;
                break;
            }
        }
        if (certToRemove != null) {
            certs.remove(certToRemove);
        }
        // Not found, just remove
        return certs;
    }


    public SmimeKeys readPKCS12File(InputStream pfxIn, String password, int userId, int cid)
        throws Exception
    {
        ArrayList<X509Certificate> certs = new ArrayList<X509Certificate>();
        PrivateKey privKey = null;

        PKCS12PfxPdu pfx = new PKCS12PfxPdu(Streams.readAll(pfxIn));

        // MAC check is really just a password verification.  Fails for some keys exported with windows
        // So we won't hard fail here.  Should fail with bad passwords further down
        if (pfx.hasMac()) {
            if (!pfx.isMacValid(new BcPKCS12MacCalculatorBuilderProvider(BcDefaultDigestProvider.INSTANCE), password.toCharArray())) {
                LOG.debug("Invalid mac check importing key for user");
            }
        }

        ContentInfo[] infos = pfx.getContentInfos();

        InputDecryptorProvider inputDecryptorProvider = new JcePKCSPBEInputDecryptorProviderBuilder()
            .setProvider("BC").build(password.toCharArray());
        JcaX509CertificateConverter  jcaConverter = new JcaX509CertificateConverter().setProvider("BC");


        for (int i = 0; i != infos.length; i++)
        {
            PKCS12SafeBagFactory dataFact;
            if (infos[i].getContentType().equals(PKCSObjectIdentifiers.encryptedData)) {
                dataFact = new PKCS12SafeBagFactory(infos[i], inputDecryptorProvider);
            } else {
                dataFact = new PKCS12SafeBagFactory(infos[i]);
            }

            PKCS12SafeBag[] bags = dataFact.getSafeBags();
            for (int b = 0; b != bags.length; b++) {
                PKCS12SafeBag bag = bags[b];

                if (bag.getBagValue() instanceof PKCS8EncryptedPrivateKeyInfo) {
                    PKCS8EncryptedPrivateKeyInfo encInfo = (PKCS8EncryptedPrivateKeyInfo) bags[0].getBagValue();
                    PrivateKeyInfo info = encInfo.decryptPrivateKeyInfo(inputDecryptorProvider);

                    KeyFactory keyFact = KeyFactory.getInstance(info.getPrivateKeyAlgorithm().getAlgorithm().getId(), "BC");
                    privKey = keyFact.generatePrivate(new PKCS8EncodedKeySpec(info.getEncoded()));

                    Attribute[] attributes = bags[0].getAttributes();
                    for (int a = 0; a != attributes.length; a++) {
                        Attribute attr = attributes[a];

                        if (attr.getAttrType().equals(PKCS12SafeBag.friendlyNameAttribute)) {
                            privKeyMap.put(((DERBMPString) attr.getAttributeValues()[0]).getString(), privKey);
                        } else if (attr.getAttrType().equals(PKCS12SafeBag.localKeyIdAttribute)) {
                            privKeyIds.put(privKey, attr.getAttributeValues()[0]);
                        }
                    }
                }
                if (bag.getBagValue() instanceof X509CertificateHolder) {
                    X509CertificateHolder certHldr = (X509CertificateHolder) bag.getBagValue();
                    X509Certificate cert = jcaConverter.getCertificate(certHldr);
                    if (certs.contains(cert)) {
                        LOG.debug("Duplicate certificate found in import");
                        continue;
                    }
                    certs.add(cert);

                    Attribute[] attributes = bag.getAttributes();
                    if (attributes != null) {
                        for (int a = 0; a != attributes.length; a++) {
                            Attribute attr = attributes[a];

                            if (attr.getAttrType().equals(PKCS12SafeBag.friendlyNameAttribute)) {
                                certMap.put(((DERBMPString) attr.getAttributeValues()[0]).getString().trim(), cert);
                            } else if (attr.getAttrType().equals(PKCS12SafeBag.localKeyIdAttribute)) {
                                certKeyIds.put(attr.getAttributeValues()[0], cert);
                            }
                        }
                    }
                }
            }
        }

        for (Iterator<String> it = privKeyMap.keySet().iterator(); it.hasNext();)
        {
            String alias = it.next();

            X509Certificate cert = certMap.get(alias);
            if (cert != null) {
                SmimeKeyService smimeKeyService = services.getServiceSafe(SmimeKeyService.class);
                return smimeKeyService.createKey(cert, chainOnly(cert, certs), privKeyMap.get(alias), password, userId, cid, true);
            }
        }

        for (Iterator<Entry<PrivateKey, ASN1Encodable>> it = privKeyIds.entrySet().iterator(); it.hasNext();) {
            Map.Entry<PrivateKey, ASN1Encodable> entr = it.next();
            X509Certificate cert = certKeyIds.get(entr.getValue());
            if (cert != null) {
                SmimeKeyService smimeKeyService = services.getServiceSafe(SmimeKeyService.class);
                return smimeKeyService.createKey(cert, chainOnly(cert, certs), entr.getKey(), password, userId, cid, true);
            }
        }
        // No private keys associated, shouldn't really happen

        if (certKeyIds.size() == 1) {
            return new SmimeKeys(certKeyIds.get(0), chainOnly(certKeyIds.get(0), certs), null, userId, cid, new Date().getTime());
        }

        // No certificates marked as local.  No private keys.  Throw error
        throw SmimeExceptionCodes.KEY_NOT_FOUND.create();

    }

}
