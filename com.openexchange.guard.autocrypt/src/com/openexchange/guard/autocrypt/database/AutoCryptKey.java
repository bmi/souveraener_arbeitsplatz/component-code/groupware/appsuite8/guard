/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.database;

import java.io.IOException;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.impl.AutoCryptUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link AutoCryptKey}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptKey {

    public enum Preference {
        mutual ("mutual"),
        none ("none");

        private String name;

        private Preference(String name) {
            this.name = name;
        }

        public boolean equalsName(String toCheck) {
            return this.name.equals(toCheck);
        }

        public String toString() {
            return this.name();
        }

    }

    private PGPPublicKeyRing keyRing;
    private int userId;
    private int cid;
    private String email;
    private long lastUpdate;
    private String hash;
    private Preference preference;
    private boolean verified;

    public AutoCryptKey (int userId, int cid, String email, PGPPublicKeyRing ring, long lastUpdate, Preference preference) {
        this.userId = userId;
        this.cid = cid;
        this.email = email;
        this.keyRing = ring;
        this.lastUpdate = lastUpdate;
        this.preference = preference;
        this.hash = AutoCryptUtils.getHash(this);
    }

    public AutoCryptKey (int userId, int cid, String email, byte[] ringBytes, long lastUpdate, String preference, String hash, boolean verified) throws OXException {
        this.userId = userId;
        this.cid = cid;
        this.email = email;
        try {
            this.keyRing = new PGPPublicKeyRing(ringBytes, new BcKeyFingerprintCalculator());
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Unable to create Autocrypt Key");
        }
        this.lastUpdate = lastUpdate;
        this.preference = preference.equals("mutual") ? Preference.mutual : Preference.none;
        this.hash = hash;
        this.verified = verified;
    }

    public boolean equals(AutoCryptKey otherKey) {
        if (this.hash == null) return false;
        return this.hash.equals(otherKey.getHash());
    }

    public int getUserId() {
        return userId;
    }

    public int getCid() {
        return cid;
    }

    public String getEmail () {
        return email;
    }

    public PGPPublicKeyRing getPGPPublicKeyRing () {
        return keyRing;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public Preference getPreference() {
        return preference;
    }

    public String getPreferenceString () {
        return preference.toString();
    }

    public String getHash() {
        return this.hash;
    }

    public boolean getVerified() {
        return this.verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

}
