/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xbill.DNS.SRVRecord;

/**
 * {@link DNSResult} A result for a DNS query
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class DNSResult {

    private List<SRVRecord> resultRecords = new ArrayList<SRVRecord>();
    private ValidationResult validationResult = new ValidationResult(false);

    /**
     * Initializes a new {@link DNSResult}.
     *
     * @param resultRecords The result records
     * @param validationResult The DNSSEC validation result
     */
    public DNSResult(List<SRVRecord> resultRecords, ValidationResult validationResult) {
        this.resultRecords = resultRecords;
        this.validationResult = validationResult;
    }

    /**
     * Gets the resultRecords
     *
     * @return The resultRecords
     */
    public List<SRVRecord> getResultRecords() {
        return Collections.unmodifiableList(resultRecords);
    }

    /**
     * Gets the DNSSEC validation result
     *
     * @return The DNSSEC validation result
     */
    public ValidationResult getValidationResult() {
        return this.validationResult;
    }
}
