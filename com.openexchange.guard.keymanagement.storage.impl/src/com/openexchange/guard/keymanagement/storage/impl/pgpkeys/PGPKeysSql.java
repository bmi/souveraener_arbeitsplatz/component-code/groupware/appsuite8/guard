/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.pgpkeys;

class PGPKeysSql {

    static final String SELECT_BY_EMAIL_STMT = "SELECT id,email,hexid, cid, local,keyid FROM PGPKeys WHERE email = ? AND cid >= 0";
    static final String SELECT_BY_HEXID_STMT = "SELECT id,email,hexid, cid, local,keyid FROM PGPKeys WHERE hexid = ?";
    static final String SELECT_BY_ID_STMT = "SELECT id,email,hexid, cid, local,keyid FROM PGPKeys WHERE id = ?";
    static final String SELECT_BY_IDS_STMT = "SELECT id, email, hexid, cid, local, keyid FROM PGPKeys WHERE id IN ({@VALUES});";
    static final String SELECT_BY_ID_IN_CONTEXT_STMT = "SELECT id,email,hexid,cid,local,keyid from PGPKeys WHERE cid = ? and id = ?";
    static final String DELETE_BY_KEYID_STMT = "DELETE FROM PGPKeys WHERE keyid = ?";
    static final String DELETE_BY_EMAIL_STMT = "DELETE FROM PGPKeys WHERE email = ?";
    static final String INSERT_OR_UPDATE_STMT = "INSERT INTO PGPKeys VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE cid = ?";
    static final String DELETE_BY_KEYID_IN_CONTEXT_STMT = "DELETE FROM PGPKeys WHERE cid = ? and keyid = ?";
    static final String UPDATE_CONTEXT_ID_BY_KEY_ID_STMT = "UPDATE PGPKeys set cid = ? where keyid = ?";
    static final String UPDATE_EMAIL_ADDRESS = "UPDATE PGPKeys set email = ? where email = ? and cid = ?";
}
