/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.callback;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.support.UserChanger;

/**
 * {@link UserUpdateCallbackAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class UserUpdateCallbackAction extends GuardServletAction {

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws OXException {
        JsonObject json = Services.getService(ResponseHandler.class).getJson(request);
        if (json != null) {
            String newEmail = JsonUtil.getStringFromJson(json, "email", true);
            GuardRatifierService ratifier = Services.getService(GuardRatifierService.class);
            ratifier.validate(newEmail);
            RangeInputValidator<Integer> positiveValidator = new RangeInputValidator<Integer>(1, Integer.MAX_VALUE);
            int userid = positiveValidator.assertInput(JsonUtil.getIntFromJson(json, "user_id", true /* mandatory */), "user_id");
            int context = positiveValidator.assertInput(JsonUtil.getIntFromJson(json, "context_id", true /* mandatory */), "context_id");
            new UserChanger().changeUser(userid, context, newEmail);
        } else {
            throw GuardCoreExceptionCodes.JSON_ERROR.create("JSON object missing in request.");
        }
    }

}
