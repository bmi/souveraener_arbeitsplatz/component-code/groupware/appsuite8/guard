/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailFields;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageExt;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.mail.search.SearchTerm;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;


/**
 * {@link GuardMessageStorageExt} - The Guard-aware message storage delegating to {@link IMailMessageStorageExt}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorageExt extends GuardMessageStorage implements IMailMessageStorageExt {

    private final IMailMessageStorageExt messageStorageExt;

    /**
     * Initializes a new {@link GuardMessageStorageExt}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     */
    public GuardMessageStorageExt(Session session, IMailMessageStorageExt delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(session, delegate, services, authToken, type);
        this.messageStorageExt = delegate;
    }

    @Override
    public void clearCache() throws OXException {
        messageStorageExt.clearCache();
    }

    @Override
    public MailMessage[] getMessages(String fullName, String[] mailIds, MailField[] fields, String[] headerNames) throws OXException {
        MailMessage[] messages = messageStorageExt.getMessages(fullName, mailIds, fields, headerNames);
        if (null == messages || messages.length <= 0) {
            return messages;
        }

        //Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
            for (int i = messages.length; i-- > 0;) {
                messages[i] = processMessage(messages[i]);
            }
        }

        return messages;
    }

    @Override
    public MailMessage[] getMessagesByMessageID(String... messageIDs) throws OXException {
        return messageStorageExt.getMessagesByMessageID(messageIDs);
    }

    @Override
    public MailMessage[] getMessagesByMessageIDByFolder(String fullName, String... messageIDs) throws OXException {
        return messageStorageExt.getMessagesByMessageIDByFolder(fullName, messageIDs);
    }

    @Override
    public MailMessage[] searchMessages(String fullName, IndexRange indexRange, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields, String[] headerNames) throws OXException {
        MailMessage[] messages = messageStorageExt.searchMessages(fullName, indexRange, sortField, order, searchTerm, fields, headerNames);
            if (null == messages || messages.length <= 0) {
                return messages;
            }

            // Only decrypt if body should be fetched
            MailFields mailFields = new MailFields(fields);
            if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
                for (int i = messages.length; i-- > 0;) {
                    messages[i] = processMessage(messages[i]);
                }
            }

            return messages;
    }

}
