/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.deletedkeys;

public class DeletedKeysSql {

    public static final String INSERT_KEY_STMT = "INSERT INTO deletedKeys (id, cid, email, PGPSecret, salt, recovery, version, deleted_date) VALUES ( ?, ?, ?, ?, ?, ?, ?, NOW());";
    public static final String UPDATE_KEY_SET_EXPOSED_STMT = "UPDATE deletedKeys SET exposed=b'1', exposed_date= ? WHERE email= ? and cid = ?";
    public static final String UPDATE_KEY_SET_UNEXPOSED_BY_DATE_STMT = "UPDATE deletedKeys SET exposed=b'0', exposed_date=NULL WHERE exposed_date <= ?";
    public static final String SELECT_KEY_BY_EMAIL_CID_STMT = "SELECT id, cid, PGPSecret, recovery, salt, version, email, exposed FROM deletedKeys WHERE email = ? AND cid = ? ORDER BY version DESC";
    public static final String SELECT_KEY_EXPOSED_BY_EMAIL_CID_STMT = "SELECT id, cid, PGPSecret, recovery, salt, version, email, exposed FROM deletedKeys WHERE cid = ? AND email = ? AND id = ? AND exposed = 1 ORDER BY version DESC";
}
