/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.servlets;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.inputvalidation.RegexInputValidator;

/**
 *
 * Provides basic WKS (WebKey Service) servlet
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v3.0.0
 */
public class WksServletAdvanced extends HttpServlet {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1376745436525L;
    private static final Logger logger = LoggerFactory.getLogger(WksServletAdvanced.class);
    private static final String ERROR_MSG = "Problem handling WebKey request";
    private Pattern checkAdvanced;
    private boolean oldFormat;

    public WksServletAdvanced() {
        oldFormat = false;
        checkAdvanced = Pattern.compile("/(((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,})/hu");
    }

    public WksServletAdvanced(boolean oldFormat) {
        super();
        this.oldFormat = oldFormat;
    }

    /**
     * Get the hash from the request url
     * getHash
     *
     * @param request The HTTP request
     * @return String of the hash if valid
     */
    private String getHash(HttpServletRequest request) {
        String path = request.getPathInfo();
        if (oldFormat) {
            return new RegexInputValidator("[a-z0-9]{32}").getInput(path.replace("/", ""));
        }
        String hu = path.substring(path.lastIndexOf("/") + 1);
        return new RegexInputValidator("[a-z0-9]{32}").getInput(hu);
    }

    /**
     * Parse the domain from the URL. If direct, it is simply the domain of the URL call. If advanced
     * it parses the domain from within the URL
     * getDomain
     *
     * @param request
     * @return
     */
    private String getDomain(HttpServletRequest request) {
        if (oldFormat || !isAdvanced(request)) {
            return request.getServerName();
        }
        Matcher match = checkAdvanced.matcher(request.getRequestURI());
        if (match.find()) {
            String domain = match.group(0);
            return domain.substring(1, domain.indexOf("/hu"));
        }
        return request.getServerName();
    }

    /**
     * Check if the incoming URL is wks advanced type
     * isAdvanced
     *
     * @param request
     * @return
     */
    private boolean isAdvanced(HttpServletRequest request) {
        return checkAdvanced.matcher(request.getRequestURI()).find();
    }

    /**
     * Check if this is a policy file quesry
     * isPolicyFileRequest
     *
     * @param request
     * @return True if policy file
     */
    private boolean isPolicyFileRequest(HttpServletRequest request) {
        return request.getRequestURI().toLowerCase().endsWith("policy");
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doHead(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Webkey Service request " + request.getPathInfo());
        try {
            final String hash = getHash(request);
            if(hash != null) {
                new WksHeadAction(response, hash, getDomain(request)).doAction();
            }
            else {
                logger.error("Bad search string for webkey service");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        catch(Exception e) {
            logger.error(ERROR_MSG,e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ERROR_MSG);
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        logger.debug("Webkey Service request " + request.getPathInfo());
        // Return blank policy file
        if (isPolicyFileRequest(request)) {
            response.setStatus(HttpServletResponse.SC_OK);
            return;
        }

        final String hash = getHash(request);
        try {
            if(hash != null) {
                new WksGetAction(response, hash, getDomain(request)).doAction();
                ;
            }
            else {
                logger.error("Bad search string for webkey service");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (OXException | IOException e) {
            logger.error(ERROR_MSG,e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ERROR_MSG);
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            //OX Guard does not support/allow key uploads via HKP
            //To be RFC compliant we send a 501-not implemented
            logger.debug("Hu post not implemented");
            ServletUtils.sendAnswer(response, HttpServletResponse.SC_NOT_IMPLEMENTED, "WKS post not supported");
        } catch (IOException e) {
            logger.error("Unexpected error while handling WKS request", e);
            ServletUtils.sendError(response, e);
        }
    }
}
