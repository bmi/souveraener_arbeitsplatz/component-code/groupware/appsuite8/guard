
package com.openexchange.guard.support;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.common.session.FakeSession;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.keymanagement.commons.DeletedKey;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheItem;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.jslob.JSlobService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;

/**
 * {@link UserDeleter} can be used for user or context based key deletion
 */
//TODO: maybe solve with a com.openexchange.groupware.delete.DeleteListener?
public class UserDeleter {

    private static final Logger LOG = LoggerFactory.getLogger(UserDeleter.class);

    private <S extends Object> S getOptService(final Class<? extends S> clazz) throws OXException {
        final S service = Services.getService(clazz);
        if (service == null) {
            LOG.warn("Unable to get service {} .  User data from that service will not be removed.", clazz.getSimpleName());
        }
        return service;
    }

    private void backupKeys(List<GuardKeys> keys) throws OXException {
        //Backup keys to deleted key table / backup table
        ArrayList<DeletedKey> deletedKeys = new ArrayList<DeletedKey>();
        for (GuardKeys key : keys) {
            deletedKeys.add(new DeletedKey(key));
        }
        if (deletedKeys.size() > 0) {
            DeletedKeysStorage deletedKeysStorage = Services.getService(DeletedKeysStorage.class);
            deletedKeysStorage.insert(deletedKeys.toArray(new DeletedKey[deletedKeys.size()]));
        }
    }

    private void removeEmailMappings(int contextId) throws OXException {
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        ogEmailStorage.deleteAllForContext(contextId);
    }

    private void removeEmailMappings(int userId, int contextId) throws OXException {
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        ogEmailStorage.deleteAllForUser(contextId, userId);
    }

    private void removeKeys(int contextId) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.deleteAllForContext(contextId);
    }

    private void removeKeys(int userId, int contextId) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.deleteAllForUser(userId, contextId);
    }

    private void removeKeyMappings(List<GuardKeys> keys) throws OXException {
        for (GuardKeys key : keys) {
            //TODO: Use a service locator for resolving this dependencies
            PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
            pgpKeysStorage.deleteByKeyId(key.getKeyid());
        }
    }

    private void removeAutocrypt(int userId, int contextId) throws OXException {
        if (contextId < 0) {
            return;
        }
        AutoCryptStorageService autocryptStorage = Services.getService(AutoCryptStorageService.class);
        autocryptStorage.deleteAllForUser(userId, contextId);
    }

    private void deleteSmime(int userId, int contextId) throws OXException {
        if (contextId < 0) {
            return;
        }
        getOptService(SmimeKeyService.class).deleteKeys(userId, contextId);
    }

    private void deleteSmime(int contextId) throws OXException {
        if (contextId < 0) {
            return;
        }
        getOptService(SmimeKeyService.class).deleteKeys(contextId);
    }

    private void deleteGuestData(int userId, int contextId) throws OXException {
        // Cleanup metadata
        GuardGuestEMailMetadataService guestMetaService = Services.getService(GuardGuestEMailMetadataService.class);
        guestMetaService.deleteUserMetaData(userId, contextId);
        // Cleanup file cache
        FileCacheStorage fileStorage = Services.getService(FileCacheStorage.class);
        Storage storage = Services.getService(Storage.class);
        List<FileCacheItem> items = fileStorage.findAllForUser(userId, contextId);
        for (FileCacheItem item: items) {
            try {
                storage.deleteEncrObj(item.getLocation());
                fileStorage.delete(item.getItemId());
            } catch (OXException ex) {
                LOG.error("Unable to delete Guest file ", item.getLocation());
            }
        }
    }

    /**
     * If JSLob sevice available, try removing guard configurations assuming
     * default "com.openexchange.jslob.config" serviceId
     * tryDeleteConfig
     *
     * @param userId
     * @param contextId
     * @throws OXException
     */
    private static void tryDeleteConfig(int userId, int contextId) throws OXException {
        if (contextId < 0) {  // Guest delete
            return;
        }
        final JSlobServiceRegistry jslobServiceReg = Services.getServiceLookup().getOptionalService(JSlobServiceRegistry.class);
        if (jslobServiceReg == null)
            return;
        JSlobService service = jslobServiceReg.getJSlobService("com.openexchange.jslob.config");
        if (service == null)
            return;
        try {
            service.set("oxguard", null, new FakeSession(userId, contextId));
        } catch (OXException ex) {
            // If nothing found, no error
            if (ex.getExceptionCode() != null && ex.getExceptionCode().getPrefix().equals("JSNCON") && ex.getExceptionCode().getNumber() == 4) {
                return;
            }
            LOG.error("Cleanup of user configuration error during delete: ", ex);
        }
    }


    /**
     * Deletes all Guard PGP keys within a given context.
     * Keys are backed up in {@link DeletedKeysStorage} before deletion
     *
     * @param contextId the id of the context
     * @throws OXException due an error
     */
    public void deleteWithBackup(int contextId) throws OXException {

        KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);

        //Backup and remove all keys within the deleted context
        List<GuardKeys> keys = keyTableStorage.getKeysForContext(contextId);
        backupKeys(keys);

        removeKeyMappings(keys);
        removeKeys(contextId);
        //The order matters: removing the email mapping at the end, because it contains the shard id for the other operations
        removeEmailMappings(contextId);
        deleteSmime(contextId);
    }

    /**
     * Deletes all Guard PGP keys related to a given user.
     * Keys are backed up in {@link DeletedKeysStorage} before deletion
     *
     * @param userId the user's id
     * @param contextId the context id
     * @throws OXException due an error
     */
    public void deleteWithBackup(int userId, int contextId) throws OXException {

        KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);
        if (keyTableStorage.exists(userId, contextId, 0 /* not relevant */)) {
            List<GuardKeys> keys = keyTableStorage.getKeysForUser(userId, contextId);
            backupKeys(keys);
            removeKeyMappings(keys);
            removeKeys(userId, contextId);
        }
        performDelete(userId, contextId);
    }

    /**
     * Deletes all Guard PGP keys related to a given user without creating a backup.
     * For backing up keys before deletion see {@link #deleteWithBackup(int,int)}
     *
     * @param userId
     * @param contextId
     * @throws OXException
     */
    public void delete(int userId, int contextId) throws OXException {

        KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);
        if (keyTableStorage.exists(userId, contextId, 0 /* not relevant */)) {
            List<GuardKeys> keys = keyTableStorage.getKeysForUser(userId, contextId);
            removeKeyMappings(keys);
            removeKeys(userId, contextId);
        }
        performDelete(userId, contextId);
    }

    /**
     * Common cleanup deleting a user
     * @param userId
     * @param contextId
     * @throws OXException
     */
    private void performDelete(int userId, int contextId) throws OXException {
        removeAutocrypt(userId, contextId);
        if (contextId < 0) {
            deleteGuestData(userId, contextId);
        }
        //The order matters: removing the email mapping at the end, because it contains the shard id for the other operations
        removeEmailMappings(userId, contextId);
        // Cleanup any activity tracking
        ActivityTrackingService activityTracker = Services.getServiceLookup().getOptionalService(ActivityTrackingService.class);
        if (activityTracker != null) {
            activityTracker.removeActivityRecord(userId, contextId);
        }
        // Cleanup UI configuration if able
        tryDeleteConfig(userId, contextId);
        deleteSmime(userId, contextId);

    }
}
