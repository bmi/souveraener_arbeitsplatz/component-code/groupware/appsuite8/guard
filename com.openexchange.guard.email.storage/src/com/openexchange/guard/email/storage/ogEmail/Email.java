/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.email.storage.ogEmail;

import java.io.Serializable;

/**
 * Represents a mapping between a user's ID and a user's email
 */
public class Email implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5621725946814838901L;

    private final String email;
    private final int userId;
    private final int contextId;
    private final int shardingId;

    /**
     * Constructor
     *
     * @param email The user's email
     * @param userId The user's ID
     * @param contextId The user's context id
     * @param shardingId the shard/database id pointing to the database which
     *            contains the key data related to this email
     */
    public Email(String email, int userId, int contextId, int shardingId) {
        this.email = email;
        this.userId = userId;
        this.contextId = contextId;
        this.shardingId = shardingId;
    }

    /**
     * Gets the user's email
     *
     * @return the user's email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Gets the user's ID
     *
     * @return the user's ID
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * Gets the user's context ID
     *
     * @return the user's context ID
     */
    public int getContextId() {
        return this.contextId;
    }

    /**
     * The shard ID pointing to the database which contains the key information related to this email
     *
     * @return the shard id
     */
    public int getShardingId() {
        return shardingId;
    }

}
