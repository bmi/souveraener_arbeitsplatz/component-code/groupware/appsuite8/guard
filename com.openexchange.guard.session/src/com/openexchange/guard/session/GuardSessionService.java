/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session;

import com.openexchange.exception.OXException;

/**
 * {@link GuardSessionService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardSessionService {

    /**
     * Insert new token to oxGuard_sessions for encrypting passwords
     *
     * @param sessionId the session ID to create a new token for
     * @param userId the user's identifier
     * @param userid the user's identifier
     * @return the new created token
     * @throws Exception due an error
     */
    String newToken(String sessionId, int userId, int contextId) throws OXException;

    /**
     * Delete token from sessions for logout
     *
     * @param sessionID
     * @throws Exception
     */
    void deleteToken(String sessionId) throws OXException;
    
    /**
     * Deletes tokens which are older than a given amount of milliseconds
     * 
     * @param seconds The amount of milliseconds
     * @return The amount of tokens which have been removed 
     * @throws OXException
     */
    int deleteTokens(long milliSeconds) throws OXException;

    /**
     * Get the token associated with the sessionID
     *
     * @param sessionID the session to get the token for
     * @return the token related to the given session or null if no token was found for the sessions
     */
    String getToken(String sessionId) throws OXException;
}
