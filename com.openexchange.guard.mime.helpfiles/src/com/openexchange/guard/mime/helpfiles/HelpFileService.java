/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.helpfiles;

import java.util.List;
import java.util.Locale;
import javax.mail.BodyPart;
import com.openexchange.exception.OXException;

/**
 * {@link HelpFileService} offers functionality to create plain text help attachments.
 * <br>
 * <br>
 * An additional help file is a plain text attachment which contains useful information for the end-user about how to decrypt an E-Mail.
 * This is can be displayed by clients if no functionality is available/installed to decrypt the pgp part of the message.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface HelpFileService {

    /**
     * Gets the help attachments as instances of {@link BodyPart} for the given languages.
     *
     * @param userId  The userId of the sender
     * @param cid      The contextId of the sender
     * @param languages The languages to get the help attachments for.
     * @return A collection of help attachments
     * @throws OXException
     */
    public List<BodyPart> getHelpFiles(int userId, int cid, String... languages) throws OXException;

    /**
     * Gets the help attachments as instances of {@link BodyPart} for the given locales.
     *
     * @param userId  The userId of the sender
     * @param cid      The contextId of the sender
     * @param locales The locales to get the help attachments for.
     * @return A collection of help attachments for the given locales
     * @throws OXException
     */
    public List<BodyPart> getHelpFiles(int userId, int cid, Locale... locales) throws OXException;

    /**
     * Determines if help files should be attached.
     * Convenience method for calling GuardConfigurationService::getBooleanProperty(GuardProperty.attachHelpFile);
     *
     * @param userId  The userId of the sender
     * @param cid      The contextId of the sender
     * @return True if help files should be attached, false otherwise
     */
    boolean attachHelpFile(int userId, int cid);
}
