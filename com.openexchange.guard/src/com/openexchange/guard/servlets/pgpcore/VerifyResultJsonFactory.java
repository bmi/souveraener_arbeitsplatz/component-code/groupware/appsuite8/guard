/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;


/**
 * {@link VerifyResultJsonFactory}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class VerifyResultJsonFactory {

    private static final String RESULTS_PROPERTY = "results";
    private static final String VERIFIED_PROPERTY = "verified";
    private static final String DATE_PROPERTY = "signatureCreatedOn";
    private static final String MISSING_PROPERTY = "missing";
    private static final String ERROR_PROPERTY = "error";
    private static final String ISSUER_USERIDS_PROPERTY = "issuerUserIds";
    private static final String ISSUER_KEY_FINGERPRINT_PROPERTY = "issuerKeyFingerprint";
    private static final String ISSUER_KEY_ID_PROPERTY = "issuerKeyId";

    public static JsonElement toJson(List<SignatureVerificationResult> results) throws OXException {
        JsonObject json = new JsonObject();
        JsonArray array = new JsonArray();
        for (SignatureVerificationResult result : results) {
            if (result instanceof PGPSignatureVerificationResult) {
                array.add(toJson((PGPSignatureVerificationResult) result));
            } else {
                throw OXException.general("Unexpected signature result type");
            }
        }
        json.add(RESULTS_PROPERTY, array);
        return json;
    }

    public static JsonElement toJson(PGPSignatureVerificationResult result) {
        JsonObject json = new JsonObject();
        json.addProperty(VERIFIED_PROPERTY, result.isVerified());
        json.addProperty(DATE_PROPERTY, result.getSignature().getCreationTime().getTime());
        json.addProperty(MISSING_PROPERTY, result.isMissing());
        if(result.getIssuerKey() != null) {
            json.addProperty(ISSUER_KEY_ID_PROPERTY, result.getIssuerKey().getKeyID());
            json.addProperty(ISSUER_KEY_FINGERPRINT_PROPERTY, PGPKeysUtil.getFingerPrint(result.getIssuerKey().getFingerprint()));
        }
        List<String> userIds = result.getIssuerUserIds();
        JsonArray userIdsArray = new JsonArray(userIds.size());
        for(String userId : userIds) {
           userIdsArray.add(userId);
        }
        json.add(ISSUER_USERIDS_PROPERTY, userIdsArray);
        json.addProperty(ERROR_PROPERTY, result.getError());
        return json;
    }
}
