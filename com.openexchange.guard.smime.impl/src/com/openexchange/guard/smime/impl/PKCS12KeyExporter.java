/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.crypto.util.PBKDF2Config;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.pkcs.PKCS12PfxPdu;
import org.bouncycastle.pkcs.PKCS12PfxPduBuilder;
import org.bouncycastle.pkcs.PKCS12SafeBag;
import org.bouncycastle.pkcs.PKCS12SafeBagBuilder;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.jcajce.JcaPKCS12SafeBagBuilder;
import org.bouncycastle.pkcs.jcajce.JcePKCS12MacCalculatorBuilder;
import org.bouncycastle.pkcs.jcajce.JcePKCSPBEOutputEncryptorBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;

/**
 * {@link PKCS12KeyExporter} exports SmimeKeys to PKCS12 file
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class PKCS12KeyExporter {

    /**
     * Exports the given {@link SmimeKeys} as encrypted and PKCS12 certificate data
     *
     * @param key {@link SmimeKey} to write
     * @param password Password currently used protecting the SmimeKey
     * @param newPassword New password to use to protect the export
     * @throws OXException
     */
    public static InputStream exportPKCS12(SmimeKeys key, String password, String newPassword) throws OXException {

        try {
            OutputEncryptor encOut = new JcePKCSPBEOutputEncryptorBuilder(NISTObjectIdentifiers.id_aes256_CBC).setPRF(PBKDF2Config.PRF_SHA256).setProvider("BC").setIterationCount(100000).build(newPassword.toCharArray());
            ArrayList<PKCS12SafeBag> bags = new ArrayList<PKCS12SafeBag>();
            for (int i = key.getChain().size(); i > 0; i--) {
                X509Certificate cert = key.getChain().get(i - 1);
                PKCS12SafeBagBuilder certBagBuilder = new JcaPKCS12SafeBagBuilder(cert);

                certBagBuilder.addBagAttribute(PKCS12SafeBag.friendlyNameAttribute, new DERBMPString(cert.getSubjectDN().getName()));
                bags.add(certBagBuilder.build());
            }

            JcaX509ExtensionUtils extUtils = new JcaX509ExtensionUtils();
            PKCS12SafeBagBuilder localCertBagBuilder = new JcaPKCS12SafeBagBuilder(key.getCertificate());

            localCertBagBuilder.addBagAttribute(PKCS12SafeBag.friendlyNameAttribute, new DERBMPString(key.getCertificate().getSubjectDN().getName()));
            SubjectKeyIdentifier pubKeyId = extUtils.createSubjectKeyIdentifier(key.getCertificate().getPublicKey());
            localCertBagBuilder.addBagAttribute(PKCS12SafeBag.localKeyIdAttribute, pubKeyId);
            bags.add(localCertBagBuilder.build());

            PrivateKey privKey = SmimeCryptoKeyUtil.decryptPrivateKey(key.getPrivateKey().getEncryptedKeyData(), password, key.getPrivateKey().getSalt());
            PKCS12SafeBagBuilder keyBagBuilder = new JcaPKCS12SafeBagBuilder(privKey, encOut);

            keyBagBuilder.addBagAttribute(PKCS12SafeBag.friendlyNameAttribute, new DERBMPString(key.getCertificate().getSubjectDN().getName()));
            keyBagBuilder.addBagAttribute(PKCS12SafeBag.localKeyIdAttribute, pubKeyId);

            PKCS12PfxPduBuilder builder = new PKCS12PfxPduBuilder();

            builder.addData(keyBagBuilder.build());

            builder.addEncryptedData(new JcePKCSPBEOutputEncryptorBuilder(NISTObjectIdentifiers.id_aes256_CBC).setPRF(PBKDF2Config.PRF_SHA256).setIterationCount(100000).setProvider("BC").build(newPassword.toCharArray()), bags.toArray(new PKCS12SafeBag[bags.size()]));

            PKCS12PfxPdu pfx = builder.build(new JcePKCS12MacCalculatorBuilder(NISTObjectIdentifiers.id_sha256), newPassword.toCharArray());

            return new ByteArrayInputStream(pfx.getEncoded(ASN1Encoding.DL));
        } catch (OperatorCreationException | PKCSException | IOException | NoSuchAlgorithmException e) {
            throw OXException.general("Unable to export S/Mime key", e);
        }
    }

}
