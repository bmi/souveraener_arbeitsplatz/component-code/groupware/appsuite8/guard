/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;
import com.openexchange.guard.keymanagement.services.impl.GuardMasterKeyExceptionMessages;

/**
 * {@link GuardMasterKeyExceptionCodes}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public enum GuardMasterKeyExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * Malformed 'oxguardpass' file in '%1$s'.
     */
    MALFORMED_MASTER_KEY_FILE("Malformed 'oxguardpass' file in '%1$s'.", GuardMasterKeyExceptionMessages.MALFORMED_MASTER_KEY_FILE_MSG, CATEGORY_ERROR, 1),

    /**
     * The OX Guard properties directory does not exist.
     */
    PROPS_DIR_NOT_EXIST("The OX Guard properties directory does not exist.", GuardMasterKeyExceptionMessages.PROPS_DIR_NOT_EXIST_MSG, CATEGORY_ERROR, 2),
    /**
     * The 'oxguardpass' file does not exist. If this is a new installation please generate one with the 'guard' command line tool and restart the server. If this is the initial installation of an n-th node in the cluster, please copy the
     * 'oxguardpass' from your initial installation of your first node.
     */
    MASTER_KEY_FILE_NOT_EXIST("The 'oxguardpass' file does not exist. If this is a new installation please generate one with the 'guard' command line tool, place it in the configuration directory of the installation and restart the server. If this is the initial installation of an n-th node in the cluster, please copy the 'oxguardpass' from your initial installation of your first node.", CATEGORY_ERROR, 3),

    MASTER_PASSWORD_FILE_CORRUPT("THe 'oxguardpass' file seems to be incorrect.  Unable to decrypt main keys", CATEGORY_ERROR, 4),

    UNABLE_TO_RETIREVE_MASTER_PASSWORD("Unable to retrieve master password from database", CATEGORY_ERROR, 5),

    BAD_CONFIGURATION("The '%1$s' configuration seems incorrect.  '%2$s'", CATEGORY_ERROR, 6),

    INDEXED_MASTER_PASSWORD_FILE_MISSING("The oxguardpass file for index '%1$s' missing", CATEGORY_ERROR, 7),

    UNABLE_TO_CREATE("Unable to create new master keys", CATEGORY_ERROR, 8),
    ;

    private String message;
    private String displayMessage;
    private Category category;
    private int number;

    private GuardMasterKeyExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private GuardMasterKeyExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-MK-SRV";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
