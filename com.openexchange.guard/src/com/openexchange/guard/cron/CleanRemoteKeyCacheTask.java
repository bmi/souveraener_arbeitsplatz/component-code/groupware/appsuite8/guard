/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cron;

import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.osgi.Services;

/**
 * {@link CleanRemoteKeyCacheTask}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class CleanRemoteKeyCacheTask implements Runnable {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CleanRemoteKeyCacheTask.class);
    private final int days;

    /**
     * Initializes a new {@link CleanRemoteKeyCacheTask}.
     *
     * @param days The amount of days a remote PGP key should be cached
     */
    public CleanRemoteKeyCacheTask(int days) {
        this.days = days;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {

        try {
            //Cleanup remote key cache (cached HKP Keys)
            logger.info("Running Scheduled cleanup of PGP Cache");
            RemoteKeyCacheStorage remoteKeyCacheStorage = Services.getService(RemoteKeyCacheStorage.class);

            int deletedKeys = remoteKeyCacheStorage.deleteOld(days);
            if (deletedKeys > 0) {
                logger.info("Deleted " + deletedKeys + " old PGP key rings from remote cache");
            }
        } catch (Exception e) {
            logger.error("Error removing older PGP keys from remote cache", e);
        }
    }

}
