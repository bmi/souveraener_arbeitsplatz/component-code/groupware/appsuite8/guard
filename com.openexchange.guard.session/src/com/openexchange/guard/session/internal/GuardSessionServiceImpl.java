/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.session.osgi.Services;

/**
 * {@link GuardSessionServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardSessionServiceImpl implements GuardSessionService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardSessionServiceImpl.class);

    private final GuardSessionStorage guardSessionStorage;

    /**
     * Initialises a new {@link GuardSessionServiceImpl}.
     */
    public GuardSessionServiceImpl() {
        super();
        guardSessionStorage = new GuardSessionStorageImpl();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.session.GuardSessionService#newToken(java.lang.String, int, int)
     */
    @Override
    public String newToken(String sessionId, int userId, int contextId) throws OXException {
        //Create a new token
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        String token = cipherService.getRandomKey();

        //Insert or update the token
        guardSessionStorage.InsertOrUpdate(sessionId, token, userId, contextId);

        return token;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.session.GuardSessionService#deleteToken(java.lang.String)
     */
    @Override
    public void deleteToken(String sessionId) throws OXException {
        if (sessionId != null) {
            guardSessionStorage.deleteById(sessionId);
        }
    }
    
    /* (non-Javadoc)
     * @see com.openexchange.guard.session.GuardSessionService#deleteTokens(long)
     */
    @Override
    public int deleteTokens(long milliSeconds) throws OXException {
        return guardSessionStorage.deleteOldSessions(milliSeconds);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.session.GuardSessionService#getToken(java.lang.String)
     */
    @Override
    public String getToken(String sessionId) throws OXException {
        try {
            GuardSession session = guardSessionStorage.getById(sessionId);
            if (session != null) {
                return session.getToken();
            }
        } catch (Exception e) {
            LOG.error("Error while retrieving token from session " + sessionId, e);
        }
        //TODO: maybe throw exception instead of returning null
        return null;
    }

}
