/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.update.internal;

import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.settings.GlobalSettingsStorage;
import com.openexchange.guard.update.GuardUpdateService;
import com.openexchange.guard.update.osgi.Services;

/**
 * {@link GuardUpdateServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardUpdateServiceImpl implements GuardUpdateService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardUpdateServiceImpl.class);

    @Override
    public void createMasterKeysIfNecessary() throws OXException {
        if (checkUpgrade()) {
            doUpgrade();
        }
    }

    private boolean checkUpgrade() {
        try {
            GlobalSettingsStorage globalSettingsStorage = Objects.requireNonNull(Services.getService(GlobalSettingsStorage.class), "Missing GlobalSettingsStorage service");
            int previousVersion = Integer.parseInt(globalSettingsStorage.getByKey("version"));

            GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
            int configuredVersion = configService.getIntProperty(GuardProperty.version);
            return previousVersion < configuredVersion;
        } catch (OXException e) {
            LOG.error("Error while comparing versions. Stop update", e);
        }
        return false;
    }

    /**
     * For future database upgrade action.
     */
    private void doUpgrade() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        int version = configService.getIntProperty(GuardProperty.version);
        setCurrent(version);
    }

    private void setCurrent(int version) {
        try {
            GlobalSettingsStorage globalSettingsStorage = Services.getService(GlobalSettingsStorage.class);
            globalSettingsStorage.update("version", Integer.toString(version));
        } catch (OXException e) {
            LOG.error("Error while setting new version.", e);
        }
    }
}
