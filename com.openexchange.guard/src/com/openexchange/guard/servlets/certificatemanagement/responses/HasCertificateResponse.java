/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.servlets.certificatemanagement.responses;

import java.util.Optional;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * {@link HasCertificateResponse} 
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class HasCertificateResponse {

    private final boolean hasPrivateKey;
    private final boolean hasPublicKey;

    /**
     * Initializes a new {@link HasCertificateResponse}.
     *
     * @param keys The optional {@link SmimeKeys} to wrap into a response
     */
    public HasCertificateResponse(Optional<SmimeKeys> keys) {
        this.hasPublicKey = keys.isPresent();
        this.hasPrivateKey = keys.isPresent() && keys.get().hasPrivateKey();
    }

    /**
     * Gets whether a private key is available
     *
     * @return <code>True</code> if a private key is available, <code>false</code> otherwise
     */
    public boolean getHasPrivateKey() {
        return this.hasPrivateKey;
    }
    
    /**
     * Gets whether a public key is available
     *
     * @return <code>True</code> if a public key is available, <code>false</code> otherwise
     */
    public boolean getHasPublicKey() {
        return this.hasPublicKey;
    }
}
