
package com.openexchange.guard.servlets.authentication;

import static com.openexchange.guard.servlets.authentication.ConnectionInformationFactory.createConnectionInformationFrom;
import static com.openexchange.guard.servlets.util.ServletLocaleUtil.getLocaleFor;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.SessionInformation;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.OxCookie;

/**
 * {@link RestSessionValidator} Validates the sessionID using the OX Rest API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4
 */
public class RestSessionValidator implements SessionValidator {

    private static final String JSON_CONTEXT = "context";
    private static final String JSON_USER = "user";
    private static final String[] SESSION_PARAMETER_NAMES = { "session" };
    private static final String JSON_CRYPTO_SESSION = "cryptoSessionId";

    private static final Logger logger  = LoggerFactory.getLogger(RestSessionValidator.class);

    private String getSession(HttpServletRequest request) {

        //Getting session from parameter
        for (String sessionIdentifier : SESSION_PARAMETER_NAMES) {
            String session = request.getParameter(sessionIdentifier);
            if (session != null && !session.isEmpty()) {
                return session;
            }
        }
        return null;
    }

    @Override
    public
    GuardUserSession validateSession(HttpServletRequest request) throws OXException {
        //Getting the session id from the request
        String sessionId = getSession(request);
        String userAgent = request.getHeader("User-Agent");
        if (sessionId != null && !sessionId.isEmpty()) {
            logger.debug("Validating session for session {} ...", sessionId);
            //Checking if session is valid
            if(new Api(new OxCookie(request.getCookies()),sessionId,userAgent).verifyLogin()) {
                //Getting user-id and context-id from session - instead of trusting client input
                JsonObject sessionObject = new SessionInformation().getSessionInformation(sessionId, request.getCookies());
                if (sessionObject != null && sessionObject.has(JSON_CONTEXT) && sessionObject.has(JSON_USER)) {
                    boolean isGuest = sessionObject.has("guest") ? sessionObject.get("guest").getAsBoolean() : false;
                    if (isGuest) {
                        logger.debug("Verified session {} as guest-session", sessionId);
                        GuestLookupService lookup = Services.getService(GuestLookupService.class);
                        Email result = lookup.lookupGuardGuest(sessionObject.get(JSON_USER).getAsInt(), sessionObject.get(JSON_CONTEXT).getAsInt());
                        if (result != null) {
                            // Return UserSession with specified Guard userId and context
                            return new GuardUserSession(sessionId,
                                sessionObject.has(JSON_CRYPTO_SESSION) ? sessionObject.get(JSON_CRYPTO_SESSION).getAsString() : "",
                                sessionObject.get(JSON_CONTEXT).getAsInt(),
                                sessionObject.get(JSON_USER).getAsInt(),
                                result.getContextId(),
                                result.getUserId(),
                                true,
                                getLocaleFor(request),
                                createConnectionInformationFrom(request));
                        }
                        else {
                            logger.debug("Unable to lookup guard guest for session {}", sessionId);
                        }
                    }
                    logger.debug("Verified session {} as regular user-session", sessionId);
                    return new GuardUserSession(sessionId,
                        sessionObject.has(JSON_CRYPTO_SESSION) ? sessionObject.get(JSON_CRYPTO_SESSION).getAsString() : "",
                        sessionObject.get(JSON_CONTEXT).getAsInt(),
                        sessionObject.get(JSON_USER).getAsInt(),
                        isGuest,
                        getLocaleFor(request),
                        createConnectionInformationFrom(request));
                }
                else {
                    logger.debug("Unable to obtain session information for session {}", sessionId);
                }
            } else {
                logger.error("Unable to verify session {0}", sessionId);
            }
        }
        return null;
    }
}
