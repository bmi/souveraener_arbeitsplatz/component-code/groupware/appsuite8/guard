/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import com.openexchange.exception.OXException;
import com.openexchange.mail.Protocol;
import com.openexchange.mail.api.AbstractProtocolProperties;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailProvider;
import com.openexchange.mailaccount.Constants;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailProvider} - A provider for handling OX Guard encrypted guest emails
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailProvider extends MailProvider {

    private final static Protocol GUARD_GUEST_PROTOCOL = new Protocol(Constants.MAIL_PROTOCOL_GUARD_GUEST);
    private final static AbstractProtocolProperties EMPTY_PROPERTIES = new AbstractProtocolProperties() {

        @Override
        protected void resetFields() { }

        @Override
        protected void loadProperties0() throws OXException { }
    };

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#getProtocol()
     */
    @Override
    public Protocol getProtocol() {
        return GUARD_GUEST_PROTOCOL;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#createNewMailAccess(com.openexchange.session.Session)
     */
    @Override
    public MailAccess<?, ?> createNewMailAccess(Session session) throws OXException {
        return new GuardGuestMailAccess(session);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#createNewMailAccess(com.openexchange.session.Session, int)
     */
    @Override
    public MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> createNewMailAccess(Session session, int accountId) throws OXException {
        return new GuardGuestMailAccess(session, accountId);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#getProtocolProperties()
     */
    @Override
    protected AbstractProtocolProperties getProtocolProperties() {
        return EMPTY_PROPERTIES;
    }

}
