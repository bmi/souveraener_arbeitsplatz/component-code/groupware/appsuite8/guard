---
title: Customization
description: Template customization
---

#Overview

Guard uses several templates for emails and the Guest reader (< 2.10). 
These templates are fully customizable, and can be customized at the global level, 
but also at the context/user level.  Changing images, colors, and layout is easy. 
Changing the wording is also possible, though the translation tables will then need to be updated.

#Template ID

Guard uses a template ID for choosing the templates to use. The template ID can be chosen for a user or context using the configuration cascade.

```
com.openexchange.guard.templateID=x
```

For any template below, a customized template can be created with the name x-templatename where x is the integer value of the template ID for the user. 
For example, if you wanted a custom password template based on the template *"passwordtempl.html"* for a context of users, you could create a template 
*"2-passwordtempl.html"* and assign the value *com.openexchange.guard.templateID=2* to the context. 
Then, Guard will use any templates that start with "2-" for the context.

__NOTE__: If no template ID is specified, or if a file specified by the template ID is not found, then the default template is used. 
The default is the templates with no number prefix, i.e. *"passwordtempl.html"*


# Templates

## Emails

- *guesttempl.html* 

    Email template used when sending to a guest user (not an OX account)
- *passwordtempl.html*

    Template used to send a new password to a guest user. As of 2.8, only used if *newGuestsRequirePassword=true* is configured. __No longer used after 2.10__
- *oxpasswordtempl.html* 

    Email template used when sending a new password to an OX user. __No longer used after 2.6__
- *resettempl.html* 

    Template used when sending a password reset
- *guestresettempl.html* 
    Template used when resetting guest account and password recovery is disabled

## Guest Reader (OX Guard version <= 2.8 only)

- *reader.html*

    The main guest reader template. __THIS IS NOT CUSTOMIZABLE WITH TEMPLATE ID. GLOBAL CHANGES ONLY.__ We recommend not changing this file and using the header, footer, and style sheets for branding.
- *header.html*

    Top header bar of the Guest reader
- *footer.html*

    Footer of the Guest reader
- *style.css*

    Style sheet for the Guest reader


# Email Template GetText

In the HTML templates, wording is surrounded by a call to gettext, which will get the translation for the user. It is used in a HTML call *<$gettext("text here")>*. Example:

```
<$gettext("You have received this email because $from has sent you a secure email message with OX Guard. You will receive a link to the secure message in a separate email.")>
```

# Variables
Some email templates have space for variables depending on their function. The variable name will begin with *$* such as the above example *$from*. 

# Guest Reader Translations (OX Guard version <= 2.8 only)

The Guest reader webpage uses i18next for translations. If changing the header and footer such that you need translation, use a call such as

```html
<h2 data-i18n="PIN Required:">PIN Required:</h2>
```

The "data-i18n" property results in the inner HTML wording being replaced with the translation (if available for the specified language)
Guest Translations as of Version 2.4.2-rev8

Custom Guest reader translations can be managed by creating a file custom-Lang.json located in the /var/www/html/reader/l10n (on Debian 8) This custom file should contain only the translations you want to replace in the default translation-Lang.json files.

For example, if you wanted to change the French translation of "Welcome" from "Bienvenue" to "Bonjour", you would create a file custom-FR_fr.json with the contents

{
    "Welcome" : "Bonjour"
}

The reader will load the file translations-FR_fr.json first, then it will load custom and overwrite any values found. 
Some email templates have space for variables depending on their function. The variable name will begin with $ such as the above example $from.

# Guard Product Name Customization 

## Overview

The Guard product name can be configured by general setting for smaller deployments, by configuration cascade, or by URL

## Product name by configuration.

The configuration

*com.openexchange.guard.productName*

can be defined in the guard-core.properties. This product name will be passed to the UI.

This value can also be configured at the configuration cascade level

## Product name by URL

By editing the file yml located in */opt/open-xchange/etc/as-config.yml* the property

*guard.productName*

can be defined based on the browser URL/IP used to address the OX backend. This product name will the be passed to the UI to be displayed by the user. 
