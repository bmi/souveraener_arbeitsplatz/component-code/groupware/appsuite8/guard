/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher;

import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link GuardCipherAlgorithm}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public enum GuardCipherAlgorithm {

    RSA("RSA/NONE/PKCS1Padding", "BC", GuardProperty.rsaKeyLength, 0),
    AES_CBC("AES/CBC/PKCS5Padding", "BC", GuardProperty.aesKeyLength, 1),
    AES_GCM("AES/GCM/NoPadding", "BC", GuardProperty.aesKeyLength, 2);

    private final String transformation;
    private final String provider;
    private final GuardProperty keyLength;
    private final int value;

    /**
     * Initialises a new {@link GuardCipherAlgorithm}.
     */
    private GuardCipherAlgorithm(String transformation, String provider, GuardProperty keyLength, int value) {
        this.transformation = transformation;
        this.provider = provider;
        this.keyLength = keyLength;
        this.value = value;
    }

    /**
     * Gets the algorithm name
     * 
     * @return the algorithm name from the transformation, or null if no transformation was set
     * @see {@link javax.crypto.Cipher}
     */
    public String getAlgorithmName() {
        if (transformation != null && !transformation.isEmpty()) {
            if (transformation.contains("/")) {
                return transformation.substring(0, transformation.indexOf("/"));
            }
            else {
                return transformation;
            }
        }
        return null;
    }

    /**
     * Gets the transformation
     *
     * @return The transformation
     */
    public String getTransformation() {
        return transformation;
    }

    /**
     * Gets the provider
     *
     * @return The provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Gets the keyLength
     *
     * @return The keyLength
     */
    public GuardProperty getKeyLengthPropertyName() {
        return keyLength;
    }

    /**
     * Returns the value of the CipherAlg
     * getValue
     *
     * @return
     */
    public int getValue() {
        return value;
    }
}
