/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link GuestRecipKeyLookupStrategy} constructs a new {@link RecipKey} object for a new guest.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GuestRecipKeyLookupStrategy implements RecipKeyLookupStrategy {

    private final int senderContextId;
    private final int senderUserId;

    /**
     * Initializes a new {@link GuestRecipKeyLookupStrategy}.
     * @param senderContextId
     */
    public GuestRecipKeyLookupStrategy(int senderUserId, int senderContextId) {
        this.senderUserId = senderUserId;
        this.senderContextId = senderContextId;
    }

    private RecipKey newGuestRecipKey(String email) throws OXException {
        RecipKey recipKey = new RecipKey();
        recipKey.setGuest(true);
        recipKey.setEmail(email);
        recipKey.setCid(senderContextId * -1);
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        recipKey.setLang(configService.getProperty(GuardProperty.defaultLanguage, senderUserId, senderContextId));
        recipKey.setPgp(false); //The user is not a regular PGP user but an external guest
        return recipKey;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.impl.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        //Guest can't create another Guest account
        return senderContextId >= 0 ? newGuestRecipKey(email) : null;
    }

}
