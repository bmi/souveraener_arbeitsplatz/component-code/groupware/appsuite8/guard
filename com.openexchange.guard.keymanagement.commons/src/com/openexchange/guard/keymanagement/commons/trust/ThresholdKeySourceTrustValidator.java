/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.trust;

/**
 * {@link NumericalTrustValidator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
class ThresholdKeySourceTrustValidator implements KeySourceTrustValidator {

    private final int threshold;

    /**
     * Initializes a new {@link ThresholdTrustLevelValidator}.
     *
     * @param threshold Threshold of trust level at which a key source is considered as "trusted"
     */
    public ThresholdKeySourceTrustValidator(int threshold) {
        this.threshold = threshold;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.commons.trust.TrustValidator#isTrusted(com.openexchange.guard.keymanagement.commons.trust.TrustLevel)
     */
    @Override
    public boolean isTrusted(KeySource keySource, int userId, int cid) {
        return keySource.getTrustLevel(userId, cid) >= threshold;
    }
}
