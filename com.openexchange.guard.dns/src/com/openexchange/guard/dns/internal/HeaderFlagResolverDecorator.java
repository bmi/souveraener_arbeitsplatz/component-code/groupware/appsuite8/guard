/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns.internal;

import java.io.IOException;
import org.xbill.DNS.Message;
import org.xbill.DNS.Resolver;

/**
 * {@link HeaderFlagResolverDecorator} is a decorator which adds a given set of header flags to each DNS query
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class HeaderFlagResolverDecorator extends AbstractResolverDecorator {

    private final int[] flags;

    /**
     * Initializes a new {@link HeaderFlagResolverDecorator}.
     * 
     * @param decoratedResolver The resolver to decorate
     * @param flags The flags to add to the a message's header
     */
    public HeaderFlagResolverDecorator(Resolver decoratedResolver, int... flags) {
        super(decoratedResolver);
        this.flags = flags;
    }

    /**
     * Adds a bunch of flags to a query header
     * 
     * @param query The query
     * @param flags a set of flags to add to the query header
     */
    private void applyHeaderFlags(Message query, int[] flags) {
        if (query != null) {
            for (int flag : flags) {
                query.getHeader().setFlag(flag);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#send(org.xbill.DNS.Message)
     */
    @Override
    public Message send(Message query) throws IOException {
        //just applying the flags and send the message
        applyHeaderFlags(query, this.flags);
        return getDecoratedResolver().send(query);
    }
}
