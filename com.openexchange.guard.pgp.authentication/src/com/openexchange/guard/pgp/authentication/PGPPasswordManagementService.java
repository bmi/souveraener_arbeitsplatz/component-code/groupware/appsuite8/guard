/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the OX Software GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 OX Software GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.pgp.authentication;

import static com.openexchange.java.Autoboxing.I;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.auth.PasswordChangedResult;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PGPPasswordManagementService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class PGPPasswordManagementService implements PasswordManagementService {

    private static Logger LOG = LoggerFactory.getLogger(PGPPasswordManagementService.class);

    final ServiceLookup services;

    /**
     * Initializes a new {@link PGPPasswordManagementService}.
     *
     * @param services The {@link ServiceLookup} to use
     */
    public PGPPasswordManagementService(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to update saved PGP recovery email
     *
     * @param userid The ID of the user to update the email for
     * @param cid The ID of the user's context to update the email for
     * @param email Email address to update
     * @return True if success
     * @throws OXException
     */
    private boolean updateEmail(int userid, int cid, String email) throws OXException {
        GuardKeyService keyService = this.services.getServiceSafe(GuardKeyService.class);
        return keyService.storeQuestion(userid, cid, "e", email);
    }

    /**
     * Internal method to perform the password reset
     *
     * @param userId The ID of the user to perform the reser for
     * @param cid The CID of the user to perform the reser for
     * @param guardId The guest ID
     * @param guardCid The guest context ID
     * @param web The "web" flag
     * @param templid The ID of the template to use
     * @param lang The language identifier to use
     * @param userEmail The address to send the reset email to
     * @param secondEmail The secondary address
     * @param host The host, used to determine the host-based email template configuration
     * @param senderIp The IP of the sender
     * @throws OXException
     */
    private void reset(int userId, int cid, int guardId, int guardCid, boolean web, int templid, String lang, String userEmail, String secondEmail, String host, String senderIp) throws OXException {
        GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
        boolean recoveryEnabled = keyService.isRecoveryEnabled(guardId, guardCid);
        if (!recoveryEnabled) {
            if (web) {
                LOG.info("Unable to recover password due to no recovery");
                throw GuardAuthExceptionCodes.RECOVERY_MISSING.create();
            }
            System.out.println("No recovery available");
            throw GuardCoreExceptionCodes.DISABLED_ERROR.create("reset password");
        }
        String newpass = keyService.generatePassword(userId, cid);

        MailCreatorService mailCreatorService = services.getServiceSafe(MailCreatorService.class);
        GuardNotificationService guardNotificationService = services.getServiceSafe(GuardNotificationService.class);
        int senderId = userId == 0 ? -1 : userId;
        int senderCid = cid == 0 ? guardCid : cid;
        List<String> fromEmail = mailCreatorService.getFromAddress(userEmail, userEmail, senderId, senderCid);
        JsonObject mail = mailCreatorService.getResetEmail(secondEmail, fromEmail, newpass, lang, templid, host, senderId, senderCid);
        guardNotificationService.send(mail, senderId, senderCid, senderIp);
        keyService.resetPassword(userEmail, newpass);
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.PGP;
    }

    @Override
    public PasswordChangedResult changePassword(GuardUserSession userSession, String guestEmail, String oldpass, String newpass, String question, String answer, String pin) throws OXException {
        PasswordChangeService passService = services.getServiceSafe(PasswordChangeService.class);
        //@formatter:off
        return passService.changePassword(userSession.getUserId(), 
            userSession.getContextId(), 
            guestEmail, 
            oldpass, 
            newpass, 
            question, 
            answer, 
            pin, 
            userSession.getGuardSession());
        //@formatter:on
    }

    @Override
    public void changeSecondary(GuardUserSession userSession, String password, String email) throws OXException {
        final String newemail = new EmailValidator().assertInput(email, "email");
        GuardKeyService keyService = this.services.getServiceSafe(GuardKeyService.class);

        GuardKeys keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId());
        if (keys != null) {
            if (PGPUtil.verifyPassword(keys.getPGPSecretKeyRing(), password, keys.getSalt())) {
                if (updateEmail(userSession.getGuardUserId(), userSession.getGuardContextId(), newemail)) {
                    return;
                }
                LOG.info("Problem updating secondary email");
                throw OXException.general("Problem updating secondary email");
            }
            LOG.info("Bad password for changing secondary email");
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(I(userSession.getGuardUserId()), I(userSession.getGuardContextId()));
    }

    @Override
    public String getSecondaryEmail(GuardUserSession userSession) throws OXException {
        KeyTableStorage ogKeyTableStorage = this.services.getServiceSafe(KeyTableStorage.class);
        GuardKeys userKeys = ogKeyTableStorage.getCurrentKeyForUser(userSession.getGuardUserId(), userSession.getGuardContextId());
        if (userKeys != null) {
            String encr_answer = userKeys.getAnswer();
            if (encr_answer == null) {
                return null;
            }
            return this.services.getServiceSafe(MasterKeyService.class).getRcDecrypted(encr_answer, "e", userKeys.getMasterKeyIndex());
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(I(userSession.getGuardUserId()), I(userSession.getGuardContextId()));
    }

    @Override
    public void deleteRecover(GuardUserSession userSession, String password) throws OXException {
        GuardKeyService keyService = this.services.getServiceSafe(GuardKeyService.class);
        GuardKeys key = keyService.getKeys(userSession.getUserId(), userSession.getContextId());
        if (key == null) {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(I(userSession.getUserId()), I(userSession.getContextId()));
        }

        // verify password
        if (!PGPUtil.verifyPassword(key.getPGPSecretKeyRing(), password, key.getSalt())) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        KeyTableStorage ogKeyTableStorage = this.services.getServiceSafe(KeyTableStorage.class);
        ogKeyTableStorage.deleteRecovery(key);
    }

    @Override
    public ResetPasswordDestination resetPass(GuardUserSession userSession, String lang, String hostname, String senderIp, boolean web, String email) throws OXException {
        final int userId = userSession != null ? userSession.getUserId() : 0;
        final int cid = userSession != null ? userSession.getContextId() : 0;
        final int guardUserId = userSession != null ? userSession.getGuardUserId() : 0;
        final int guardCid = userSession != null ? userSession.getGuardContextId() : 0;

        int templid = 0;
        try {
            GuardConfigurationService guardConfigService = services.getServiceSafe(GuardConfigurationService.class);
            templid = guardConfigService.getIntProperty(GuardProperty.templateID, userId, cid);
        } catch (Exception e) {
            LOG.error("problem getting template id for reset password email");
        }
        KeyTableStorage ogKeyTableStorage = services.getServiceSafe(KeyTableStorage.class);
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        GuardKeys key = null;
        if (userId > 0) {
            if (lang == null) {
                lang = configService.getProperty(GuardProperty.defaultLanguage, userId, cid);
            }
            List<GuardKeys> keysForUser = ogKeyTableStorage.getKeysForUser(guardUserId, guardCid);
            if (keysForUser.size() > 0) {
                key = keysForUser.get(0);
            }
        } else {
            GuardKeyService keyservice = services.getServiceSafe(GuardKeyService.class);
            key = keyservice.getKeys(email);
        }
        // When would this happen?
        /*
         * if (key == null && request != null) {
         * GetUserService getUser = new GetUserServiceImpl(cookie, request);
         * OxUserResult result = getUser.getUser(userid);
         * if (result != null && result.isGuest()) {
         * GuardKeyService keyservice = Services.getService(GuardKeyService.class);
         * key = keyservice.getKeys(result.getEmail());
         * }
         * 
         * }
         */
        if (key != null) {
            String encr_answer = key.getAnswer();
            String userEmail = key.getEmail();
            boolean secondary = true;
            GuardCipherService cipherService = services.getServiceSafe(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
            // Mailing to secondary email address if web.  If from command line, then email to primary
            String rc = services.getServiceSafe(MasterKeyService.class).getMasterKey(key.getMasterKeyIndex(), false).getRC();
            String secondEmail;
            if (web) {
                secondEmail = (encr_answer == null) ? null : cipherService.decrypt(encr_answer, rc, "e");
            } else {
                secondEmail = userEmail;  // For reset from command line, use primary email address
            }
            if ((secondEmail == null) || secondEmail.equals("") || secondEmail.equals(userEmail)) {
                secondEmail = userEmail;
                secondary = false;
            }
            GuardRatifierService validatorService = services.getServiceSafe(GuardRatifierService.class);
            validatorService.validate(secondEmail);
            reset(userId, cid, key.getUserid(), key.getContextid(), web, templid, lang, userEmail, secondEmail, hostname, senderIp);
            if (web) {
                return secondary ? ResetPasswordDestination.SECONDARY : ResetPasswordDestination.PRIMARY;
            }
            System.out.println("OK");
            // For reset from command line, use primary email address
            return ResetPasswordDestination.PRIMARY;
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
    }
}
