/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.AbstractRecipKeyLookupStrategy;
import com.openexchange.guard.oxapi.capabilities.Capabilities;
import com.openexchange.guard.user.OXUser;
import com.openexchange.guard.user.OXUserService;

/**
 * {@link MailResolverRecipKeyLookupStrategy} searches for OX Users using the configured Mail Resolver.
 * <p>
 * See: com.openexchange.guard.mailResolverUrl property
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class MailResolverRecipKeyLookupStrategy extends AbstractRecipKeyLookupStrategy {

    /**
     * Internal method to convert a {@link OXUser} object into a {@link RecipKey} object
     *
     * @param user The user to conver
     * @return The {@link RecipKey} object for the given {@link OXUser}
     */
    private RecipKey toRecipKey(OXUser user) {
        RecipKey recipKey = new RecipKey();
        recipKey.setCid(user.getContextId());
        recipKey.setUserid(user.getId());
        recipKey.setLang(user.getLanguage());
        recipKey.setName(user.getName());
        recipKey.setEmail(user.getEmail());
        recipKey.setPgp(true);
        return recipKey;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.impl.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        OXUser user = Services.getService(OXUserService.class).getUser(email);
        if (user != null && !user.isGuest() /* don't handle guest here; use the guest lookup strategy */) {
            //First, let's check if this is an alias for a known user
            GuardKeys aliasKeys = Services.getService(GuardKeyService.class).getKeys(user.getId(), user.getContextId());
            if (aliasKeys != null && hasValidEncryptionKey(aliasKeys)) {
                return new RecipKey(aliasKeys);
            } else {
                //Check if the resolved OX user has mail and guard capabilities enabled
                Capabilities capabilities = new Capabilities();
                if (capabilities.hasUserCapability(Capabilities.WEBMAIL_CAPABILITY, user.getId(), user.getContextId()) && capabilities.hasUserCapability(Capabilities.GUARD_CAPABILITY, user.getId(), user.getContextId())) {
                    return toRecipKey(user);
                }
            }
        }
        return null;
    }

}
