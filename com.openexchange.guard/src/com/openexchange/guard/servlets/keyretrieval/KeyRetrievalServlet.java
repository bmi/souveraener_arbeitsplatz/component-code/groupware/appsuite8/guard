/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keyretrieval;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;


/**
 * Provides functionality for retrieving deleted but backed up keys which has been set to "exposed"
 * using the support API ({@link com.openexchange.guard.servlets.supportApi.ExposeKeyAction})
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class KeyRetrievalServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = 2005968044580975857L;
    private static final String POST_RETRIEVE_ALL_EXPOSED_KEYS_ACTION = "getExposedKeys";
    private transient final HashMap<String, GuardServletAction> getActions;

    /**
     * Initializes a new {@link KeyRetrievalServlet}.
     */
    public KeyRetrievalServlet() {
        //GET
        getActions = new HashMap<String, GuardServletAction>() ;
        getActions.put(POST_RETRIEVE_ALL_EXPOSED_KEYS_ACTION, new RetrievalAllExposedKeysAction());
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAction(request, response, getActions);
    }
}
