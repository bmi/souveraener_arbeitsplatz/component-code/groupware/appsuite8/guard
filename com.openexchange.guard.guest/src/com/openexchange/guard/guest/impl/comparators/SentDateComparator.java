/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import java.util.Date;
import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link SentDateComparator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class SentDateComparator extends GuardGuestEmailComparator {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2271235234261009944L;

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.impl.comparators.GuardGuestEmailComparator#compareInternal(com.openexchange.guard.guest.GuardGuestEmail, com.openexchange.guard.guest.GuardGuestEmail)
     */
    @Override
    public int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        Date date0 = arg0.getMetaData().getSentDate();
        Date date1 = arg1.getMetaData().getSentDate();

        if (checkForNull(date0, date1)) {
            return compareNull(date0, date1);
        }

        return date0.compareTo(date1);
    }
}
