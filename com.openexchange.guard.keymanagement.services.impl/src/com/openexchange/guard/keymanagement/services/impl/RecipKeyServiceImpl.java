/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.keymanagement.services.impl.lookup.DefaultRecipKeyLookupStrategy;
import com.openexchange.guard.keymanagement.services.impl.lookup.InternalRecipKeyLookupStrategy;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link RecipKeyServiceImpl} provides default functionality for resolving recipient keys.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class RecipKeyServiceImpl implements RecipKeyService {

    private final GuardKeyService guardKeyService;

    /**
     * Initializes a new {@link RecipKeyServiceImpl}.
     *
     * @param guardKeyService The {@link GuardKeyService} to use
     */
    public RecipKeyServiceImpl(GuardKeyService guardKeyService) {
        this.guardKeyService = guardKeyService;
    }

    /**
     * Internal method to create a token for HKP lookup
     *
     * @param userId The user's ID
     * @param contextId The user's context ID
     * @return A token created from the user's key email, or null if the user has no guard keys setup.
     * @throws OXException
     */
    private String createHKPClientToken(int userId, int contextId) throws OXException {
        try {
            GuardKeys keys = guardKeyService.getKeys(userId, contextId);
            if (keys != null) {
                return CipherUtil.getMD5(keys.getEmail());
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, "Error while creating HKP Client token");
        }
        return null;
    }

    private int getTimeout(int userId, int cid) throws OXException {
        return Services.getService(GuardConfigurationService.class).getIntProperty(GuardProperty.remoteKeyLookupTimeout, userId, cid);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.RecipKeyService#getRecipKey(java.lang.String, com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy)
     */
    @Override
    public RecipKey getRecipKey(RecipKeyLookupStrategy lookupStrategy, String email, int userId, int cid) throws OXException {
        lookupStrategy = Objects.requireNonNull(lookupStrategy, "lookupStrategy must not be null");
        return lookupStrategy.lookup(email, getTimeout(userId, cid));
    }

    @Override
    public RecipKey getRecipKey(int senderUserId, int senderContextId, String email) throws OXException {
        return new DefaultRecipKeyLookupStrategy(senderUserId, senderContextId, createHKPClientToken(senderUserId, senderContextId)).lookup(email, getTimeout(senderUserId, senderContextId));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.RecipKeyService#getInternalRecipKey(int, int, java.lang.String)
     */
    @Override
    public RecipKey getInternalRecipKey(int senderUserId, int senderContextId, String email) throws OXException {
        return new InternalRecipKeyLookupStrategy().lookup(email, getTimeout(senderUserId, senderContextId));
    }
}