/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.services;

public class Attachment {

    private String filename;

    private String type;

    private byte[] content;

    private String contentId = null;

    private byte[] encrContent;

    private boolean sizeMatch;

    private boolean pgp;

    public String getContentId() {
        return contentId;
    }

    public void setContent_id(String content_id) {
        this.contentId = content_id;
    }

    public byte[] getEncrContent() {
        return encrContent;
    }

    public void setEncrContent(byte[] encrContent) {
        this.encrContent = encrContent;
    }

    public boolean isSizeMatch() {
        return sizeMatch;
    }

    public void setSizeMatch(boolean sizeMatch) {
        this.sizeMatch = sizeMatch;
    }

    public boolean isPgp() {
        return pgp;
    }

    public void setPgp(boolean pgp) {
        this.pgp = pgp;
    }

    public String getFilename() {
        return filename;
    }

    public String getType() {
        return type;
    }

    public byte[] getContent() {
        return content;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    
    public void setType(String type) {
        this.type = type;
    }

    
    public void setContent(byte[] content) {
        this.content = content;
    }

}
