/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.autocrypt.database.AutoCryptKey;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.autocrypt.exceptions.AutocryptExceptionCodes;
import com.openexchange.guard.common.session.FakeSession;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.jslob.JSlob;
import com.openexchange.jslob.JSlobService;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.server.ServiceLookup;

/**
 * {@link AutoCryptServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */

public class AutoCryptServiceImpl implements AutoCryptService {

    private static final int MAX_AUTOCRYPT_HEADER_SIZE = 30000;

    private ServiceLookup services;

    private static Logger LOG = LoggerFactory.getLogger(AutoCryptServiceImpl.class);

    public AutoCryptServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    private GuardConfigurationService getConfigService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }

    private AutoCryptStorageService getAutocryptStorage() throws OXException {
        return services.getServiceSafe(AutoCryptStorageService.class);
    }

    /**
     * Check if autocrypt disabled for this user, either through config cascade or user settings
     *
     * @param userId
     * @param cid
     * @return true if disabled
     * @throws OXException
     */
    private boolean isDisabled(int userId, int cid) throws OXException {
        if (!getConfigService().getBooleanProperty(GuardProperty.autoCryptEnabled, userId, cid)) {
            return true;
        }
        JSlobService jsonLobService = services.getOptionalService(JSlobService.class);
        if (jsonLobService != null) {
            JSlob lob = jsonLobService.get("oxguard", new FakeSession(userId, cid));
            JSONObject guardJSON = lob.getJsonObject();
            if (guardJSON.has("autocryptHeader")) {
                try {
                    if (!guardJSON.getBoolean("autocryptHeader")) {
                        return true;
                    }
                } catch (JSONException e) {
                    LOG.error("Error parsing user JSlob for autocrypt headers", e);
                }
            }
        }
        return false;
    }

    /**
     * Create header with key data
     * @param keyData
     * @param fromAddr
     * @return
     */
    private String createHeader (String keyData, String fromAddr, boolean mutual) {
        if (keyData == null || keyData.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("addr=");
        sb.append(IDNUtil.aceEmail(fromAddr));
        sb.append(";");
        if (mutual) {
            sb.append(" prefer-encrypt=mutual;");
        }
        sb.append(" keydata=");
        sb.append(AutoCryptUtils.formatHeader(keyData));
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.AutoCryptService#getOutgoingHeaderString(int, int, java.lang.String)
     */
    @Override
    public String getOutgoingHeaderString(int userId, int cid, int oxUserId, int oxCid, String emailAddress) throws OXException {

        if (isDisabled(oxUserId, oxCid)) {
            return "";
        }

        String responseData = "";

        GuardKeys key;
        GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
        key = keyService.getKeys(userId, cid);  // First, grab default key based on userid/cid
        if (emailAddress != null) {  // If from email address passed, make sure same as for the key
            if (key == null || !key.getEmail().equalsIgnoreCase(IDNUtil.decodeEmail(emailAddress))) {
                key = keyService.getKeys(emailAddress);  // Could be different than their default key
            }
        }
        if (key != null && key.getPGPPublicKeyRing() != null && key.getContextid() > 0) {  // Key exists and not guest
            if (key.getContextid() == cid && key.getUserid() == userId) {  // Verify match with userId, cid if retrieved by email
                responseData = checkMaxLength(getHeaderValue(key, emailAddress));
            }
        }
        return responseData;
    }

    /**
     * Get string header value
     *
     * @param key GuardKeys that contain the public keys
     * @param emailAddress The email address to match. Should be the fromAddress
     * @return
     * @throws OXException
     */
    private String getHeaderValue (GuardKeys key, String emailAddress) throws OXException {
        return createHeader(
            AutoCryptUtils.createEncodedKeyString(key.getPGPPublicKeyRing(), emailAddress == null ? key.getEmail() : emailAddress),
            key.getEmail(),
            getConfigService().getBooleanProperty(GuardProperty.autoCryptMutual, key.getUserid(), key.getContextid())  // If header should be set to mutual
            );
    }

    /**
     * Check that the length of the autocrypt header string doesn't exceed configured max
     * checkMaxLength
     *
     * @param value
     * @return Unchanged header if length OK, otherwise returns ""
     */
    private static String checkMaxLength(String header) {
        if (header == null)
            return null;
        if (header.length() > MAX_AUTOCRYPT_HEADER_SIZE) {
            LOG.info("Autocrypt header size exceeds max allowed.  Not appended");
            return "";
        }
        return header;
    }

    @Override
    public void addOutgoingHeader(MimeMessage msg, GuardKeys key) throws OXException {
        if (key == null || isDisabled(key.getUserid(), key.getContextid())) {
            return;
        }

        String value = checkMaxLength(getHeaderValue(key, null));
        if (value == null || value.isEmpty()) {
            return;
        }
        try {
            msg.addHeader("Autocrypt", value);
            msg.saveChanges();
        } catch (MessagingException e) {
            LOG.error("Error adding autocrypt message", e);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.AutoCryptService#importKey(int, int, java.lang.String, java.lang.String, boolean)
     */
    @Override
    public AutoCryptKeysResponse importKey(int userId, int cid, String header, long date, boolean add, boolean confirmed) throws OXException {
        GuardRatifierService ratifier = services.getServiceSafe(GuardRatifierService.class);
        ratifier.validateReasonablePastDate(date);
        AutoCryptKey key = AutoCryptUtils.convertHeader(userId, cid, header, date);
        if (key == null) {
            return new AutoCryptKeysResponse("Incomplete or missing key data");
        }
        // Quick sanity check on email address in header
        ratifier.validate(key.getEmail());
        // Make sure header email matches the one in the key
        if (!AutoCryptUtils.confirmEmail(key)) {
            return new AutoCryptKeysResponse("Address mismatch");
        }
        GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
        GuardKeys gKey = keyService.getKeys(key.getEmail());
        if (gKey != null) {
            if (gKey.getContextid() > 0) {
                return new AutoCryptKeysResponse("Existing Guard Key");
            }
        }
        boolean newKey = false;
        boolean added = false;
        AutoCryptStorageService autoCryptStorageService = getAutocryptStorage();
        AutoCryptKey existing = autoCryptStorageService.getKey(key.getEmail(), userId, cid);
        if (existing != null) {
            if (existing.getLastUpdate() >= key.getLastUpdate()) {
                return new AutoCryptKeysResponse("Existing");
            }
            if (!existing.equals(key))
             {
                newKey = true;   // Different key
            }
        } else {
            newKey = true;
        }
        // Update
        if (add) {
            // If key was verified, must be confirmed to overwrite with update
            if (existing == null || !existing.getVerified() || (existing.getVerified() && confirmed)) {
                if (confirmed) {
                    key.setVerified(true);
                }
                added = autoCryptStorageService.storeAutoCryptKey(key);
            }
        }
        return new AutoCryptKeysResponse(key.getPGPPublicKeyRing(), newKey, added);
    }


    @Override
    public RecipKey getRecipKey(int userId, int cid, String email) throws OXException {
        AutoCryptKey key = getAutocryptStorage().getKey(email, userId, cid);
        return key != null ? AutoCryptUtils.convertToRecipKey(key) : null;
    }

    @Override
    public MimeMessage addGossipHeaders(MimeMessage msg, int userId, int cid, ArrayList<RecipKey> recips) throws OXException {
        for (RecipKey key : recips) {
            try {
                if (!key.isGuest()) {  // Don't add for Guests
                    String value = AutoCryptUtils.createEncodedKeyString(key.getPGPPublicKeyRing(), key.getEmail());
                    if (value != null && !value.isEmpty()) {
                        msg.addHeader("Autocrypt-Gossip",
                            createHeader(
                                value,
                                key.getEmail(),
                                false
                                ));
                    }
                }
            } catch (MessagingException e) {
                LOG.error("Error adding autocrypt-gossip header", e);
            }
        }
        return msg;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.AutoCryptService#getStartData(java.lang.String)
     */
    @Override
    public AutoCryptStartInfoResponse getStartData(String startAttachment) throws OXException {
        if (startAttachment.contains("----BEGIN PGP MESSAGE----")) {
            String format = "";
            String begin = "";
            String[] lines = normalize(startAttachment).split("\r");
            for (String line: lines) {
                line = line.trim();
                if (line.startsWith("Passphrase-Format")) {
                    format = line.substring(line.indexOf(":") + 1).trim();
                }
                if (line.startsWith("Passphrase-Begin")) {
                    begin = line.substring(line.indexOf(":") + 1).trim();
                }
            }
            return new AutoCryptStartInfoResponse(format, begin);
        }
        return null;
    }

    /**
     * Normalize a pgp message to make sure each line ends with \r\n as needed by BC
     * Returns message starting with the BEGIN PGP MESSAGE section
     * normalize
     *
     * @param attachment
     * @return message with each line ending in \r\n
     */
    private static String normalize(String attachment) {
        int start = attachment.indexOf("----BEGIN PGP MESSAGE----");
        if (start > -1) {
            attachment = attachment.substring(start);
            String[] lines = attachment.split("\n");
            StringBuilder sb = new StringBuilder();
            for (String line : lines) {
                sb.append(line.trim());
                sb.append("\r\n");
            }
            return sb.toString();

        }
        return attachment;
    }

    @Override
    public PGPSecretKeyRing getKeyFromAttachment(String attachment, String key) throws OXException {
        try {
            attachment = normalize(attachment);
            PGPSecretKeyRing secrKeyRing = AutoCryptCrypto.decryptSymmetricPGPObject(new ByteArrayInputStream(attachment.getBytes(StandardCharsets.UTF_8)), key.toCharArray());
            if (secrKeyRing == null) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
            return secrKeyRing;
        }
        catch(IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (PGPException e) {
            throw AutocryptExceptionCodes.UNABLE_DECRYPT.create(e);
        }
    }

    @Override
    public TransferResponse sendTransferEmail (GuardKeys key, String password, String host, String senderIp) throws OXException {
        String hashPass = CipherUtil.getSHA(password, key.getSalt());
        PGPSecretKeyRing newkeyring;
        try {
            newkeyring = PGPKeysUtil.duplicateSecretKeyRing(key.getPGPSecretKeyRing(), hashPass, password, PGPEncryptedData.AES_256);
        } catch (PGPException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
        if (newkeyring == null) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        return new SendTransferEmail(
            services.getServiceSafe(MailCreatorService.class),
            services.getServiceSafe(GuardTranslationService.class),
            getConfigService(),
            services.getServiceSafe(GuardNotificationService.class)
            ).sendEmail(newkeyring, key, host);
    }

}
