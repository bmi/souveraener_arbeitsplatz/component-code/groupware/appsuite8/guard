/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import org.junit.Assert;
import org.junit.Test;
import com.openexchange.exception.OXException;

/**
 * {@link IntegerRangeTest} tests package com.openexchange.guard.inputvalidation.WhiteList
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class IntegerRangeTest {

    @Test
    public void testCheckForValidValueShouldReturnTrue() {
        Integer[] allowedValues = new Integer[] { 115, 116, 117 };
        InputValidator<Integer> rangeValidator = new RangeInputValidator<Integer>(115, 117);
        for (int allowedValue : allowedValues) {
            Assert.assertTrue("The input value should be valid", rangeValidator.isValid(allowedValue));
        }
    }

    @Test
    public void testCheckForInvalidValueShouldReturnFalse() {
        Integer[] allowedValues = new Integer[] { 115, 116, 117 };
        Assert.assertFalse("The input should not be treated as valid", new RangeInputValidator<Integer>(115, 117).isValid(0));
    }

    @Test
    public void testAssertForValidValueShouldNotThrowException() throws OXException {
        Integer[] allowedValues = new Integer[] { 115, 116, 117 };
        InputValidator<Integer> rangeValidator = new RangeInputValidator<Integer>(115, 117);
        for (int allowedValue : allowedValues) {
            //This should not throw an exception
            rangeValidator.assertIsValid(allowedValue);
        }
    }

    @Test
    public void testAssertForInvalidValueShouldThrowException() {
        try {
            Integer[] allowedValues = new Integer[] { 115, 116, 117 };
            //This should throw an Exception
            new RangeInputValidator<Integer>(115, 117).assertIsValid(0);
            Assert.fail("An OX exception was expected but not thrown.");
        } catch (OXException e) {
            Assert.assertTrue("Catched exception should contain specific error code", e.getMessage().contains("GRD-INP-0001"));
        }
    }
}
