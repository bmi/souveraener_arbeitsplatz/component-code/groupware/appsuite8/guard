/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import static com.openexchange.guard.servlets.authentication.ConnectionInformationFactory.createConnectionInformationFrom;
import static com.openexchange.guard.servlets.util.ServletLocaleUtil.getLocaleFor;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.session.GuardSessionService;

/**
 * {@link GuestSessionValidator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class GuestSessionValidator implements SessionValidator {

    private static final String AUTH_PARAMETER_NAME = "auth";
    private static final String GUEST_READER_COOKIE_NAME = "OxReaderID";
    private static final Logger logger = LoggerFactory.getLogger(GuestSessionValidator.class);

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.authentication.SessionValidator#validateSession(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public GuardUserSession validateSession(HttpServletRequest request) throws OXException {
        String authToken = request.getParameter(AUTH_PARAMETER_NAME);
        try {
            if (authToken != null && !authToken.isEmpty() && !authToken.trim().toLowerCase().equals("null")) {
                String token = null;
                String guestSessionId = null;
                Cookie[] cookies = request.getCookies();
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(GUEST_READER_COOKIE_NAME)) {
                        guestSessionId = cookie.getValue();
                        GuardSessionService sessionService = Services.getService(GuardSessionService.class);
                        token = sessionService.getToken(guestSessionId);
                        if(token != null) {
                            break;
                        }
                    }
                }

                if (token != null) {
                    GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);
                    if (authToken.contains("!")) {
                        authToken = authToken.substring(authToken.indexOf("!") + 1);
                    }
                    String userdata = cipherService.decrypt(authToken.substring(1), token);
                    if (userdata != null && !userdata.isEmpty()) {
                        Gson json = new Gson();
                        JsonObject userDataJson = json.fromJson(userdata, JsonObject.class);
                        if (userDataJson.has("user_id") && userDataJson.has("cid")) {
                            int userId = userDataJson.get("user_id").getAsInt();
                            int contextId = userDataJson.get("cid").getAsInt();
                            if (contextId < 0) {
                                //Guest sessions do always have negative context-id
                                GuardUserSession guardUserSession = new GuardUserSession(guestSessionId, guestSessionId, contextId, userId, true, getLocaleFor(request),createConnectionInformationFrom(request));
                                return guardUserSession;
                            }
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            logger.error("Error decrypting guest auth-token: " + e.getMessage());
            return null;
        }
    }
}
