/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardExceptionMessages  implements LocalizableStrings {

    // The authentication is missing and is required
    public static final String MISSING_AUTH_MSG = "Missing authentication";
    // Purely technical error.  Unlikely to be displayed
    public static final String MISSING_ENDPOINT_MSG = "Guard endpoint \"%1$s\" not configured";
    // Authentication is invalid
    public static final String BAD_AUTH_MSG = "Invalid authentication";
    // Unknown error message
    public static final String UNKNOWN_ERROR_MSG = "Error: \"%1$s\"";
    // Error message returned when the user account is Locked out.  Usually due to excessive login attempts with bad passwords
    public static final String LOCKOUT_MSG = "Lockout";
    // Purely technical error.  Unlikely to be displayed
    public static final String URI_PARSE_FAILED_MSG =  "Unable to parse server URI \"%1$s\".";
    // The action requested is not allowed.  Variable will be reason or detail on the restriction
    public static final String ACTION_NOT_ALLOWED_MSG = "The action is not allowed: '%1$s'";
    // The action is not supported.  Variable will be the action that isn't supported
    public static final String ACTION_NOT_SUPPORTED_MSG = "The action is not supported: '%1$s'";
    // The account was assigned a pin, and can't do this function until the PIN code is removed by changing password.
    public static final String BLOCKED_BY_PIN = "Unable to perform action until PIN is removed.  Please do so by changing password.";
    // No keys were found
    public static final String MISSING_KEYS = "No keys found";

    private GuardExceptionMessages() { }
}
