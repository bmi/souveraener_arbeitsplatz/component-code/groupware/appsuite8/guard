/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.util.Objects;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.hkpclient.services.RemoteKeyResult;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;

/**
 * {@link HKPPGPKeyRetrievalStrategy}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class HKPPGPKeyRetrievalStrategy implements PGPKeyRetrievalStrategy  {

    private final HKPClientService hkpClient;
    private final String hkpClientToken;
    private final int timeout, userId, cid;
    private final TrustMode trustMode;

    /**
     * {@link TrustMode} - Controlls which key's will be retrieved from the HKP server
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v7.10.2
     */
    public enum TrustMode{
        /**
         * Retrieval keys from all known HKP sources.
         */
        ALL,

        /**
         * Only retrieval keys from trusted sources.
         */
        TRUSTED_ONLY
    }

    public HKPPGPKeyRetrievalStrategy(HKPClientService hkpClient, int userId, int cid, int timeout, TrustMode trustMode) {
        this(hkpClient, userId, cid, timeout, null, trustMode);
    }

    public HKPPGPKeyRetrievalStrategy(HKPClientService hkpClient, int userId, int cid, int timeout, String hkpClientToken, TrustMode trustMode) {
        this.hkpClientToken = hkpClientToken;
        this.timeout = timeout;
        this.trustMode = trustMode;
        this.hkpClient = Objects.requireNonNull(hkpClient, "hkpClient must not be null");
        this.userId = userId;
        this.cid = cid;
    }

    /* (non-Javadoc)
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getSecretKey(long, java.lang.String, char[])
     */
    @Override
    public PGPPrivateKey getSecretKey(long keyId, String userIdentity, char[] password) throws Exception {
        //We cannot fetch private keys using HKP
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getPublicKey(long)
     */
    @Override
    public PGPPublicKey getPublicKey(long keyId) throws Exception {
        RemoteKeyResult result = hkpClient.find(hkpClientToken, null, keyId, userId, cid, timeout);
        if(result != null) {
            if (result.getSource().isTrusted(userId, cid) || trustMode == TrustMode.ALL) {
                return result.getRing().getPublicKey(keyId);
            }
        }
        return null;
    }
}
