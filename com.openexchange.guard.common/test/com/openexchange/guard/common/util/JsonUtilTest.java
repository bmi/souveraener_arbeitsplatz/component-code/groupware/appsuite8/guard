/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import org.junit.Assert;
import org.junit.Test;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;

/**
 * {@link JsonUtilTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class JsonUtilTest {

    @Test(expected = OXException.class)
    public void testGetStringFromJsonJsonObjectStringBoolean_jsonNullAndMandatory_throwException() throws OXException {
        JsonUtil.getStringFromJson(null, "name", true);
    }

    @Test
    public void testGetStringFromJsonJsonObjectStringBoolean_jsonNullNotMandatory_returnNull() throws OXException {
        String stringFromJson = JsonUtil.getStringFromJson(null, "name", false);

        Assert.assertNull(stringFromJson);
    }

    @Test(expected = OXException.class)
    public void testGetStringFromJsonJsonObjectStringBoolean_jsonEmptyAndMandatory_throwException() throws OXException {
        JsonUtil.getStringFromJson(new JsonObject(), "name", true);
    }

    @Test
    public void testGetStringFromJsonJsonObjectStringBoolean_jsonEmptyNotMandatory_returnNull() throws OXException {
        String stringFromJson = JsonUtil.getStringFromJson(new JsonObject(), "name", false);

        Assert.assertNull(stringFromJson);
    }
}
