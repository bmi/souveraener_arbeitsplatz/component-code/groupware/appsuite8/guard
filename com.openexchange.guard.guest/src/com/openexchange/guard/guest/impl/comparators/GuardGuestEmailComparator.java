/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import java.io.Serializable;
import java.util.Comparator;
import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link GuardGuestEmailComparator} abstract class for comparing instances of {@link GuardGuestEmail}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public abstract class GuardGuestEmailComparator implements Comparator<GuardGuestEmail>, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2107208265839954688L;

    /**
     * Checks if one of the given argument is null
     *
     * @param arg0 The first argument
     * @param arg1 The second argument
     * @return True if arg0 or arg1 is null, false if both are not null
     */
    protected boolean checkForNull(Object arg0, Object arg1) {
        return arg0 == null || arg1 == null;
    }

    /**
     * Compares two objects assuming that one or both of the given arguments are null
     *
     * @param arg0 The first argument
     * @param arg1 The second argument
     * @return 0 if both arguments are null, 1 if the first argument is null, -1 if the second argument is null.
     */
    protected int compareNull(Object arg0, Object arg1) {
        if (arg0 == null && arg1 == null) {
            return 0;
        }

        if (arg0 == null) {
            return 1;
        }

        return -1;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        if (checkForNull(arg0, arg1)) {
            return compareNull(arg0, arg1);
        }
        return compareEmails(arg0, arg1);
    }

    /**
     * Compares two instances of {@link GuardGuestEmail}
     * <br>
     * <br>
     * Implementations can safely assume that none of the given arguments are <code>null</code>
     *
     * @param arg0 A E-Mail
     * @param arg1 Another E-Mail
     * @return A negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     * @see Comparator#compare(Object, Object)
     */
    public abstract int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1);
}
