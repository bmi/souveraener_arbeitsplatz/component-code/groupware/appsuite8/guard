/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

/**
 * {@link WhiteListInputValidator} performs simple white list validation based on equals
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class WhiteListInputValidator<T> extends AbstractInputValidator<T> {

    private final T[] validInputs;

    /**
     * Initializes a new {@link WhiteListInputValidator}.
     * 
     * @param validInputs The white list, i.e. a list of allowed input values which considered to be valid.
     */
    public WhiteListInputValidator(T[] validInputs) {
        this.validInputs = validInputs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.inputvalidation.WhiteList#isValid(java.lang.Object)
     */
    @Override
    public boolean isValid(T input) {
        for (T validInput : validInputs) {
            if (validInput.equals(input)) {
                return true;
            }
        }
        return false;
    }
}
