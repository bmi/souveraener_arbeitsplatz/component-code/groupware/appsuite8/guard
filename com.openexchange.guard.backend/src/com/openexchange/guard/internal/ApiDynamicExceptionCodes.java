/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;

/**
 * {@link ApiDynamicExceptionCodes}
 * Create exception from Guard MW error response
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v3.0.0
 */
public class ApiDynamicExceptionCodes implements DisplayableOXExceptionCode {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ApiDynamicExceptionCodes.class);

    private int detailNumber;

    private String message;

    private String prefix;

    /**
     * Create from returned JSON
     * Initializes a new {@link ApiDynamicExceptionCodes}.
     *
     * @param json Json returned with error message
     * @param errorResponse The http message used for fallback if json empty or incomplete
     */
    public ApiDynamicExceptionCodes(JSONObject json, String errorResponse) {
        try {
            if (json != null && json.has("code") && json.has("error_desc")) {
                this.message = json.getString("error_desc");
                String code = json.getString("code");
                // Try to split into prefix and detail number
                if (code.indexOf("-") > 0) {
                    this.prefix = code.substring(0, code.lastIndexOf("-"));
                    this.detailNumber = Integer.parseInt(code.substring(code.lastIndexOf("-") + 1));
                } else {  // Fallback
                    this.prefix = code;
                    this.detailNumber = 0;
                }
                return;
            }
            // Fallback to parsing from string
            initFromString(errorResponse);

        } catch (JSONException | NumberFormatException ex) {
            LOGGER.error("Problem parsing error message", ex);
        }
        this.prefix = "GUARDAPI";
        this.message = errorResponse;
        this.detailNumber = 0;
    }

    /**
     * Create from String error message
     * Initializes a new {@link ApiDynamicExceptionCodes}.
     *
     * @param errorMessage
     */
    private void initFromString(String errorMessage) {
        if (errorMessage == null) {
            errorMessage = "No message";
        }
        if (errorMessage.contains("reason phrase:")) {
            try {
                errorMessage = errorMessage.substring(errorMessage.indexOf("reason phrase:") + 14);
                String type = errorMessage.substring(0, errorMessage.indexOf(":"));
                this.message = errorMessage.substring(errorMessage.indexOf(":") + 1);
                this.prefix = type.substring(0, type.indexOf("-"));
                this.detailNumber = Integer.parseInt(type.substring(type.lastIndexOf("-") + 1));
                return;
            } catch (Exception ex) {
                LOGGER.error("Problem parsing error message", ex);
            }
        }
        this.message = errorMessage;
        this.detailNumber = 0;
        this.prefix = "GUARDAPI";
        return;
    }

    @Override
    public Category getCategory() {
        return Category.CATEGORY_ERROR;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getNumber() {
        return detailNumber;
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    @Override
    public String getDisplayMessage() {
        return message;
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

}
