/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Utilities extends AbstractOXCalls {

    private static final Logger LOG = LoggerFactory.getLogger(Utilities.class);

    /**
     * Checks for error in reply string
     *
     * @param response
     * @return true if contains error
     */
    public static boolean checkFail(String response) {
        if (response == null) {
            return (true);
        }
        if (response.contains("error")) {
            return (true);
        }
        return (false);
    }

    public static boolean checkFail(JsonObject resp) {
        if (resp == null) {
            return (false);
        }
        Gson gson = new Gson();
        return (checkFail(gson.toJson(resp)));
    }
}
