/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest.responses;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import javax.mail.MessagingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openexchange.guard.guest.GuardGuestEmail;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata;
import com.openexchange.guard.servlets.responses.JsonInputStreamSerializer;

/**
 * {@link GuardGuestEmailResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestEmailResponse {

    private final GuardGuestEmail email;

    /**
     * Initializes a new {@link GuardGuestEmailResponse}.
     *
     * @param email The email
     */
    public GuardGuestEmailResponse(GuardGuestEmail email) {
        this.email = Objects.requireNonNull(email, "email must not be null");
    }

    /**
     * Gets the owner of the email
     *
     * @return The owner of the email
     */
    public String getOwner() {
        return email.getOwner().getIdentity();
    }

    /**
     * Gets the unique identifier of the email
     *
     * @return The unique identifier
     */
    public String getId() {
        return email.getItemId();
    }


    /**
     * Gets an InputStream to the email's content
     *
     * @return The InputStream for the content
     * @throws IOException
     * @throws MessagingException
     */
    @JsonSerialize(using= JsonInputStreamSerializer.class)
    public InputStream getContent() throws IOException, MessagingException {
       return email.getMessgeStream();
    }

    /**
     * Gets the meta data of the email
     *
     * @return The meta data of the email
     */
    public GuardGuestEmailMetadata getMetaData() {
        return email.getMetaData();
    }
}
