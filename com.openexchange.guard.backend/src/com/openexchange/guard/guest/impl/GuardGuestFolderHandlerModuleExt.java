/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.util.Arrays;
import java.util.Collection;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.modules.Module;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.ShareTargetPath;
import com.openexchange.share.groupware.TargetProxy;
import com.openexchange.share.groupware.spi.FolderHandlerModuleExtension;

/**
 * {@link GuardGuestFolderHandlerModuleExt}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardGuestFolderHandlerModuleExt implements FolderHandlerModuleExtension {

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.ModuleExtension#getModules()
     */
    @Override
    public Collection<String> getModules() {
        return Arrays.asList(Module.MAIL.getName());
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.FolderHandlerModuleExtension#isApplicableFor(int, java.lang.String)
     */
    @Override
    public boolean isApplicableFor(int contextId, String folder) {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.FolderHandlerModuleExtension#isVisible(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public boolean isVisible(String folder, String item, int contextID, int guestID) throws OXException {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.FolderHandlerModuleExtension#exists(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public boolean exists(String folder, String item, int contextID, int guestID) throws OXException {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.FolderHandlerModuleExtension#resolveTarget(com.openexchange.share.ShareTargetPath, int, int)
     */
    @Override
    public TargetProxy resolveTarget(ShareTargetPath targetPath, int contextId, int guestId) throws OXException {
        // TODO Auto-generated method stub
        return new GuardMailTargetProxy(targetPath);
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.spi.FolderHandlerModuleExtension#resolveTarget(com.openexchange.share.ShareTarget, int)
     */
    @Override
    public TargetProxy resolveTarget(ShareTarget folderTarget, int contextId) throws OXException {
        // TODO Auto-generated method stub
        return new GuardMailTargetProxy(folderTarget);
    }

}
