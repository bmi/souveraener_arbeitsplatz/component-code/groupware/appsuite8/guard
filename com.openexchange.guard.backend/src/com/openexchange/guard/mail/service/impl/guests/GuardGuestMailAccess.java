/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import com.openexchange.exception.OXException;
import com.openexchange.mail.api.IMailProperties;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailConfig;
import com.openexchange.mail.api.MailLogicTools;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailAccess}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailAccess extends MailAccess<IGuardGuestFolderStorage, IGuardGuestMessageStorage> {

    private transient IGuardGuestFolderStorage folderStorage;
    private transient IGuardGuestMessageStorage messageStorage;
    private transient MailLogicTools mailLogicTools;
    private static IMailProperties DEFAULT_GUARD_GUEST_MAIL_PROPERTIES = new IMailProperties() {

        @Override
        public void waitForLoading() throws InterruptedException {
            //NO-OP
        }

        @Override
        public boolean isUserFlagsEnabled() {
            return false;
        }

        @Override
        public boolean isSupportSubscription() {
            return false;
        }

        @Override
        public boolean isIgnoreSubscription() {
            return true;
        }

        @Override
        public boolean isAllowNestedDefaultFolderOnAltNamespace() {
            return false;
        }

        @Override
        public int getMailFetchLimit() {
            return 1000;
        }

        @Override
        public boolean hideInlineImages() {
            //TODO: check for c.o.mail.hideInlineImages?
            return true;
        }

        @Override
        public int getConnectTimeout() {
            return -1;
        }

        @Override
        public int getReadTimeout() {
            return -1;
        }
    };

    /**
     * Initializes a new {@link GuardGuestMailAccess}.
     * @param session
     */
    protected GuardGuestMailAccess(Session session) {
        super(session);
    }

    /**
     * Initializes a new {@link GuardGuestMailAccess}.
     * @param session
     * @param accountId
     */
    public GuardGuestMailAccess(Session session, int accountId) {
        super(session, accountId);
    }

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6899772477003364633L;



    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#connectInternal()
     */
    @Override
    protected void connectInternal() throws OXException {
        //NO-OP
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#createNewMailConfig()
     */
    @Override
    protected MailConfig createNewMailConfig() {
        return new GuardGuestMailConfig();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#createNewMailProperties()
     */
    @Override
    protected IMailProperties createNewMailProperties() throws OXException {
        return DEFAULT_GUARD_GUEST_MAIL_PROPERTIES;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#checkMailServerPort()
     */
    @Override
    protected boolean checkMailServerPort() {
        return false; //No mail server port needs to be set
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#releaseResources()
     */
    @Override
    protected void releaseResources() {
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#closeInternal()
     */
    @Override
    protected void closeInternal() {
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#getFolderStorage()
     */
    @Override
    public IGuardGuestFolderStorage getFolderStorage() throws OXException {
        if(folderStorage == null) {
           folderStorage = new GuardGuestFolderStorage(this.session);
        }
        return folderStorage;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#getMessageStorage()
     */
    @Override
    public IGuardGuestMessageStorage getMessageStorage() throws OXException {
        if(messageStorage == null) {
            messageStorage = new GuardGuestMessageStorage(this.session, GuardGuestFolderStorage.TRASH_NAME);
        }
        return messageStorage;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#getLogicTools()
     */
    @Override
    public MailLogicTools getLogicTools() throws OXException {
        if(mailLogicTools == null) {
            mailLogicTools = new MailLogicTools(session, accountId);
        }
        return mailLogicTools;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#isConnected()
     */
    @Override
    public boolean isConnected() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#isConnectedUnsafe()
     */
    @Override
    public boolean isConnectedUnsafe() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#startup()
     */
    @Override
    protected void startup() throws OXException {
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailAccess#shutdown()
     */
    @Override
    protected void shutdown() throws OXException {
    }

}
