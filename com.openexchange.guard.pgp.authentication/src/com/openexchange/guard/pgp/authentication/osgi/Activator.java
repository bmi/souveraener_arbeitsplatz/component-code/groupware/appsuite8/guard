/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.pgp.authentication.osgi;

import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.pgp.authentication.PGPPasswordManagementService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link Activator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class Activator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        //@formatter:off
        return new Class[] { GuardKeyService.class,
            MailCreatorService.class,
            GuardNotificationService.class,
            PasswordChangeService.class,
            KeyTableStorage.class,
            MasterKeyService.class,
            GuardConfigurationService.class
        };
        //@formatter:on
    }

    @Override
    protected void startBundle() throws Exception {
        registerService(PasswordManagementService.class, new PGPPasswordManagementService(this));
    }
}
