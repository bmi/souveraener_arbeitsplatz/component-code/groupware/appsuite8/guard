/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.Protocol;
import com.openexchange.mail.api.AbstractProtocolProperties;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.MailProvider;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link GuardMailProvider}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class GuardMailProvider extends MailProvider{

    private final MailProvider delegateProvider;
    private final ServiceLookup services;
    private final GuardAuthenticationToken authToken;
    private final CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMailProvider}.
     *
     * @param realProvider The wrapped {@link MailProvider}
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} objects which recognizes PGP messages.
     */
    public GuardMailProvider(MailProvider delegateProvider, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        this.delegateProvider = delegateProvider;
        this.services = services;
        this.authToken = authToken;
        this.type = type;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#getProtocol()
     */
    @Override
    public Protocol getProtocol() {
        return delegateProvider.getProtocol();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#createNewMailAccess(com.openexchange.session.Session)
     */
    @Override
    public MailAccess<?, ?> createNewMailAccess(Session session) throws OXException {
        return new GuardMailAccess(
            (MailAccess<IMailFolderStorage, IMailMessageStorage>) delegateProvider.createNewMailAccess(session),
            session,
            services,
            authToken,
            type);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#createNewMailAccess(com.openexchange.session.Session, int)
     */
    @Override
    public MailAccess<? extends IMailFolderStorage, ? extends IMailMessageStorage> createNewMailAccess(Session session, int accountId) throws OXException {
        return new GuardMailAccess(
            (MailAccess<IMailFolderStorage, IMailMessageStorage>) delegateProvider.createNewMailAccess(session, accountId),
            session,
            accountId,
            services,
            authToken,
            type);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailProvider#getProtocolProperties()
     */
    @Override
    protected AbstractProtocolProperties getProtocolProperties() {
        return delegateProvider.getProtocolProps();
    }
}
