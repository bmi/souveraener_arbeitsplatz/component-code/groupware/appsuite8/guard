/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link SeenComparator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class SeenComparator extends GuardGuestEmailComparator {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -283909696821839738L;
    /**
     * The "seen" flag. See {@link MailMessage}
     */
    private static final int FLAG_SEEN = 1 << 5;

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.impl.comparators.GuardGuestEmailComparator#compareEmails(com.openexchange.guard.guest.GuardGuestEmail, com.openexchange.guard.guest.GuardGuestEmail)
     */
    @Override
    public int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        boolean seenArg0 = arg0.getMetaData() != null ? arg0.getMetaData().hasMessageFlags(FLAG_SEEN) : false;
        boolean seenArg1 = arg0.getMetaData() != null ? arg1.getMetaData().hasMessageFlags(FLAG_SEEN) : false;
        return Boolean.compare(seenArg0, seenArg1);
    }
}
