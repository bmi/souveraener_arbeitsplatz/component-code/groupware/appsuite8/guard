/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailFields;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.ISimplifiedThreadStructureEnhanced;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.mail.search.SearchTerm;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;


/**
 * {@link GuardMessageStorageThreadStructureEnhanced} - The Guard-aware message storage delegating to {@link ISimplifiedThreadStructureEnhanced}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorageThreadStructureEnhanced extends GuardMessageStorage implements ISimplifiedThreadStructureEnhanced {

    private final ISimplifiedThreadStructureEnhanced threadStructureEnhanced;
    private final CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMessageStorageThreadStructureEnhanced}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     * @param authToken
     */
    public GuardMessageStorageThreadStructureEnhanced(Session session, ISimplifiedThreadStructureEnhanced delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(session, (IMailMessageStorage) delegate, services, authToken, type);
        this.threadStructureEnhanced = delegate;
        this.type = type;
    }

    @Override
    public List<List<MailMessage>> getThreadSortedMessages(String folder, boolean includeSent, boolean cache, IndexRange indexRange, long max, MailSortField sortField, OrderDirection order, MailField[] fields, SearchTerm<?> searchTerm) throws OXException {
        List<List<MailMessage>> threadSortedMessages = threadStructureEnhanced.getThreadSortedMessages(folder, includeSent, cache, indexRange, max, sortField, order, fields, searchTerm);
        if (null == threadSortedMessages || threadSortedMessages.isEmpty()) {
            return threadSortedMessages;
        }

        // Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
            List<List<MailMessage>> processedMessages = new ArrayList<>(threadSortedMessages.size());
            for (List<MailMessage> thread : threadSortedMessages) {
                List<MailMessage> processedThread = new ArrayList<>(thread.size());
                for (MailMessage mailMessage : thread) {
                    processedThread.add(processMessage(mailMessage));
                }
                processedMessages.add(processedThread);
            }
            threadSortedMessages = processedMessages;
        }

        return threadSortedMessages;
    }

    @Override
    public List<List<MailMessage>> getThreadSortedMessages(String folder, boolean includeSent, boolean cache, IndexRange indexRange, long max, MailSortField sortField, OrderDirection order, MailField[] fields, String[] headerNames, SearchTerm<?> searchTerm) throws OXException {
        List<List<MailMessage>> threadSortedMessages = threadStructureEnhanced.getThreadSortedMessages(folder, includeSent, cache, indexRange, max, sortField, order, fields, headerNames, searchTerm);
        if (null == threadSortedMessages || threadSortedMessages.isEmpty()) {
            return threadSortedMessages;
        }

        // Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
            List<List<MailMessage>> processedMessages = new ArrayList<>(threadSortedMessages.size());
            for (List<MailMessage> thread : threadSortedMessages) {
                List<MailMessage> processedThread = new ArrayList<>(thread.size());
                for (MailMessage mailMessage : thread) {
                    processedThread.add(processMessage(mailMessage));
                }
                processedMessages.add(processedThread);
            }
            threadSortedMessages = processedMessages;
        }

        return threadSortedMessages;
    }

}
